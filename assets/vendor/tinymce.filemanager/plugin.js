/**
 * apps.js
 *
 * @author          Zach Tancinco
 * @date created    21 July 2015
 * @date modified   
 */


tinymce.PluginManager.add('filemanager', function(editor, url) {

    tinymce.activeEditor.settings.file_browser_callback = filemanager;

    function filemanager (id, value, type, win) {

        /*var width = window.innerWidth-30;
        var height = window.innerHeight-60;
        if(width > 1800) width=1800;
        if(height > 1200) height=1200;
        if(width>600){
            var width_reduce = (width - 20) % 138;
            width = width - width_reduce + 10;
        }*/

        width = 500;
        height = 500;

        console.log(id+' '+value+' '+type+' '+win);
        console.log(width+' '+height);

        var title="File Manager";

        var akey="key";
        if (typeof editor.settings.filemanager_access_key !== "undefined" && editor.settings.filemanager_access_key) {
            akey=editor.settings.filemanager_access_key;
        }
        
        tinymce.activeEditor.windowManager.open(
            {
                title: title,
                 
                file: editor.settings.external_filemanager_path+'?type='+type+'&akey='+akey,
                width: width,  
                height: height,
                resizable: true,
                maximizable: true,
                inline: 1
            }, 
            {
                setUrl: function (url) {
                    //console.log(type);
                    var fieldElm = win.document.getElementById(id);
                    fieldElm.value = editor.convertURL(url);
                    if ("fireEvent" in fieldElm) {
                        fieldElm.fireEvent("onchange")
                    } else {
                        var evt = document.createEvent("HTMLEvents");
                        evt.initEvent("change", false, true);
                        fieldElm.dispatchEvent(evt);
                    }
                    
                    //win.document.getElementById(id).value = url;                   
                    //console.log(win);
                },

                getUrlTest: function(){
                    console.log('Callback: This is a test only');
                }
            }
        );
        
        return false;
    }

    
});