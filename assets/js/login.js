$( document ).ready(function() {
    
    $(".form-signin input[type='text'], .form-signin input[type='password']").val('');

    var loginType = $(".form-signin input[type='radio']:checked").val();

    console.log(loginType);
    selectLoginType(1);
});

function selectLoginType(v){
 

	if( v == 1 ){

		var inputName = $('#inputName'); 
		inputName.attr('required', 'required');
		inputName.attr('autofocus', 'true');
		inputName.parents('.form-label-group').show();

		var inputUsername = $('#inputUsername');
		inputUsername.removeAttr('autofocus');

	}else{ 
		var inputName = $('#inputName'); 		
		inputName.removeAttr('required');
		inputName.removeAttr('autofocus');
		inputName.parents('.form-label-group').hide();

		var inputUsername = $('#inputUsername');
		inputUsername.attr('autofocus', 'true');
	}
}
