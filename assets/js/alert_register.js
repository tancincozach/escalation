$(function(){


	 $('blockquote').hide();

	 $('input[name=check_all]').click(function(){

		alert_register.misc.check_cbox($(this),'input[name=cbox_reg]');

	 });

	  $('input[name=check_all_2]').click(function(){

		alert_register.misc.check_cbox($(this),'input[name=cbox_reg_2]');

	 });

	 $('.close_alert').click(function(event){

	 	alert_register.misc.close_alert($(this).attr('alt'));

	 	event.preventDefault();
	 });	

	  $('.delete_alert').click(function(event){

	 	alert_register.misc.ajax_multi_closed_alerts($(this).attr('alt'));

	 	event.preventDefault();
	 });

	/*$('.cma_reminder_body_snip').on('click', function(){

		var me = $(this);	

		console.log(me.css('max-height'));

		//me.removeAttr('style');
		if( me.css('max-height') == '42px'){
			me.css('max-height', '100%');
		}else{ 			 
			me.css('max-height', '42px');
		}


	})*/

});


var alert_register = {

	take_control: function(tran_id ,ref_num, alert_type){

		if( confirm('Confirm working on Alert#'+ref_num) ){

			$.post('register/ajax_take_control', {tran_id, alert_type} , function(res){

				if( res.status ){
					window.location = res.url;
				}else{
					alert(res.msg);
					window.location = document.URL;
				}

			}, 'json');
			
		}
	}
}


alert_register.misc = {

	check_cbox:function( $cbox ,$children_cbox){

			$_cbox = $cbox.prop('checked');

			$($children_cbox).each(function(){

				$(this).prop( "checked",$_cbox );

			});
				

	},

	get_cbox_ids:function( $children_cbox ){

		var t_ids = [];

		$($children_cbox).each(function(){

			var tid = $(this).val();

			if($(this).prop( "checked")==true){

				t_ids.push({'tran_id':tid});

			}

		});

		return t_ids;
	},

	close_alert:function(){


		var t_ids = [];


		 t_ids = this.get_cbox_ids('input[name=cbox_reg]');

		try {

			if(t_ids.length <=0) throw 'Please select alert(s).';

			if( confirm('Confirm closing selected Alert(s)?') ){
				
				$.post('register/ajax_close_alerts', {t_ids} , function(res){


							if( res.status ){
							window.location = res.url;
						}else{
							alert(res.msg);
							window.location = document.URL;
						}
				

				}, 'json');

				
			}
		    
		}
		catch(err) {

		   alert(err);

		   return false;

		}



	},		

	ajax_multi_closed_alerts:function(){


		var t_ids = [];


		 t_ids = this.get_cbox_ids('input[name=cbox_reg_2]');

		try {

			if(t_ids.length <=0) throw 'Please select alert(s).';

			if( confirm('Confirm CLOSING selected Alert(s)?') ){
				
				$.post('register/ajax_multi_closed_alerts', {t_ids} , function(res){


							if( res.status ){
							window.location = res.url;
						}else{
							alert(res.msg);
							window.location = document.URL;
						}
				

				}, 'json');

				
			}
		    
		}
		catch(err) {

		   alert(err);

		   return false;

		}

	}
}


alert_register.useroption = {
	save: function(fields){
		var data = $(fields).serialize();
		$.post('register/ajax_save_useroption', data, function(res){

			if( res.status ){
				window.location = document.URL;
			}else{
				alert(res.msg);
				//window.location = document.URL;
			}

		}, 'json');

		return false;
	}
}