$(function(){


	 //$('blockquote').hide();
});



var callactivity = {

	confirm: function(elems){

		if( $('#set_driver_required').length > 0 ){
			//var set_driver_required = $('#set_driver_required').val();

			if( $('#set_driver_required').val() == '' ){
				alert($('#set_driver_btn_name').val()+' is Required ');
				return false;
			}

		}


		if( confirm('Confirm Submit Call') ){

			return true;
		}

		return false;

	},

	closed_alert: function(tran_id, ref_number){

		if( confirm('Please confirm to CLOSED this ALERT') ){

			$.post('callactivity/ajax_close_alert/'+(new Date()).getTime(), {tran_id, ref_number}, function(res){

				if( res.status ){
					window.location = apps_base_url+res.url;					
				}else{
					alert(res.body);
				}

			}, 'json');
		}

	},

	onclick_processlog: function(tran_id, cma_id){
		if( confirm('Start Processing on this Log') ){

			$.post('callactivity/ajax_process_log/'+(new Date()).getTime(), {tran_id, cma_id}, function(res){

				if( res.status ){
					//window.location = apps_base_url+res.url;					
				}else{
					alert(res.body);
				}

			}, 'json');
		}
	},

	take_control_off: function(tran_id){

		if( confirm('Confirm remove TAKE CONTROL') ){

			$.post('register/ajax_take_control_off', {tran_id} , function(res){

				if( res.status ){
					window.location = res.url;
				}else{
					alert(res.msg);
					window.location = document.URL;
				}

			}, 'json');
			
		}
		
	}

 
}



callactivity.email_reply = {

	open_modal: function(type, tran_id){

		$.get('callactivity/ajax_reply_modal/'+type, {tran_id}, function(res){

			if(res.status){

				var ca_reply_modal = $(res.body).on('shown.bs.modal', function (e) {
		           //alert('shown.bs.modal');
		           
		           	callactivity.email_reply.tynmce('dynamic_tinymce_text');

		        }).modal('show');

				ca_reply_modal.on('hidden.bs.modal', function (e) {
		            //$(this).remove();
		            //$('.modal-backdrop').remove();
		            window.location = document.URL;
		        });

 			}else{
 				alert(res.body);
 			}

		}, 'json');

	},

	tynmce: function( selector ){

		//var callactivity_client_id = $('#callactivity_form input[name="client_id"]').val();
		//console.log(callactivity_client_id);

		var style_formats = [
				 
				{title : 'Header 2', block : 'h2' },
				{title : 'Header 3', block : 'h3' },
				{title : 'Header 4', block : 'h4' }			 
			];
		var plugins = [
		         "advlist autolink link image lists charmap print preview hr anchor pagebreak",
		         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
		         "save table contextmenu directionality emoticons paste textcolor",
		         " template filemanager"
		    ];
		
			
			//filemanager

			//Initialize tinyMCE for Greeting
			tinymce.init({
			    selector: "#"+selector,
			    document_base_url: apps_base_url,
			    theme: "modern",
			    
			    height: 250,
			    cleanup : true,
			    plugins: plugins,
			    convert_urls: false,
			    content_css: "assets/vendor/bootstrap/css/bootstrap.min.css",
			  
			    toolbar: "undo redo | bold italic | alignleft aligncenter alignright alignjustify | fontselect fontsizeselect | styleselect | bullist numlist outdent indent | link image media | print preview | cleanup fullpage | forecolor backcolor emoticons", 
				style_formats : style_formats,

			    external_filemanager_path : 'maintenance/filemanager_files/'+($('#callactivity_form input[name="client_id"]').val())+'/',  

			    template_replace_values : {
					username : "Some User",
					staffid : "991234"
				}

			});	


			var FilemanagerDialogue = {

				click: function(URL){

					parent.tinymce.activeEditor.windowManager.getParams().setUrl(URL);
					parent.tinymce.activeEditor.windowManager.close();
				}
			}

	}

}

callactivity.set_procedure = {

	open_modal: function(alert_type, tran_id){

		$.get('callactivity/ajax_set_procedure_modal/'+alert_type, {tran_id}, function(res){

			if(res.status){

				var _modal = $(res.body).on('shown.bs.modal', function (e) {	

		        }).modal('show');

				_modal.on('hidden.bs.modal', function (e) {
		            $(this).remove();
		            $('.modal-backdrop').remove();
		            //window.location = document.URL;
		        });
			}else{
				alert(res.body);
			}
 
		}, 'json');

	}
}


callactivity.lookup = {
	modalobj: null,
	popup: function(call_lookup_id, tran_id, is_view_only){

		$.get('crlookup/ajax_callactivity_modal_tpl/?_t='+(new Date()).getTime(), {call_lookup_id, tran_id, is_view_only}, function(res){
			
			if(res.status==1){

				callactivity.lookup.modalobj = $(res.html).on('shown.bs.modal', function (e) {	

		        }).modal('show');

				callactivity.lookup.modalobj.on('hidden.bs.modal', function (e) {
		            $(this).remove();
		            $('.modal-backdrop').remove();
		            //window.location = document.URL;
		        });
			}else{
				alert(res.html);
			}
 
		}, 'json');

	},

	on_search: function(input){ 

		// Declare variables 
		//var input, filter, table, tr, td, i;
		var filter, table, tr, td, i;
		//input = document.getElementById("lookup_input_search");
		filter = input.value.toUpperCase();
		table = document.getElementById("lookup_table_data");
		tr = table.getElementsByTagName("tr"),

		total_col = (table.rows[0].cells.length-1);

		 

		// Loop through all table rows, and hide those who don't match the search query
		for (i = 0; i < tr.length; i++) {

			// td = tr[i].getElementsByTagName("td")[i];
			// if (td) {
			//   if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
			//     tr[i].style.display = "";
			//   } else {
			//     tr[i].style.display = "none";
			//   }
			// }

			tr_style_display = "none";

			for(k=0; k < total_col; k++){ 

				td = tr[i].getElementsByTagName("td")[k];
				
				if (td) {
				  if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
				    tr_style_display = "";
				    break;
				  } 
				} 

			}

			tr[i].style.display = tr_style_display;

		}
 
	},

	on_select: function(field, call_lookup_id, tran_id, is_view_only){
		
		//var lookup_data_id = field.value;
		var lookup_data_id = field;

		$.get('crlookup/ajax_callactivity_lookup_select/?_t='+(new Date()).getTime(), {lookup_data_id, call_lookup_id, tran_id, is_view_only}, function(res){
			
			if(res.status==1){ 

				//call_res_display
				$('#call_direction').val(res.call_res_display.call_direction);
				$('#caller_name').val(res.call_res_display.caller_name);
				$('#caller_phone').val(res.call_res_display.caller_phone);
				
				//screen_display
				$('#callactivity_lookup_div').html('');
				$('#callactivity_lookup_div').append(res.screen_display)
				$('#callactivity_lookup_div').append(res.escalation_display)

				$('input[name="email_output"]').val(res.email_output);
				$('input[name="to_email"]').val(res.to_email);


				callactivity.lookup.modalobj.modal('hide');

				$('#set_driver_required').val('Result Found');

			}else{
				alert(res.html);
			}
 
		}, 'json');
		
		
	},

	on_click_escalation: function(name, phone, notes){
		
		$('#caller_name').val(name);
		$('#caller_phone').val(phone);		
		//$('#call_notes').val(notes);		

	},

	on_click_noresult:function(){

		$('#set_driver_required').val('No Result Found');

		callactivity.lookup.modalobj.modal('hide');

	}

}




var alert_register = {

	take_control: function(tran_id ,ref_num, alert_type){

		if( confirm('Are you sure you are about to take control of this Alert#'+ref_num) ){

			$.post('register/ajax_take_control', {tran_id, alert_type} , function(res){

				if( res.status ){
					window.location = res.url;
				}else{
					alert(res.msg);
					window.location = document.URL;
				}

			}, 'json');
			
		}
	}
}