$(function () { 

	var report_today = new Date();
	var report_dd = report_today.getDate();
	var report_mm = report_today.getMonth()+1; //January is 0!
	var report_yyyy = report_today.getFullYear();

	//console.log(moment(report_yyyy+'-'+report_mm+'-21 00:00:00'));

    $('#datetimepicker_from').datetimepicker({
    	format: 'DD/MM/YYYY',
    	//defaultDate: moment(report_yyyy+'-'+report_mm+'-01'), //moment('2016-01-01')
		icons: {
			time: 'fa fa-clock'		
		}  
    });

    $('#datetimepicker_to').datetimepicker({
        useCurrent: true,
        format: 'DD/MM/YYYY',
        //defaultDate: moment(), //moment('2016-01-01')
		icons: {
			time: 'fa fa-clock'		
		}        
    });

    $("#datetimepicker_from").on("change.datetimepicker", function (e) {
        $('#datetimepicker_to').datetimepicker('minDate', e.date);
        $('#datetimepicker_from').datetimepicker('hide'); //hide
    });

    $("#datetimepicker_to").on("change.datetimepicker", function (e) {
        $('#datetimepicker_from').datetimepicker('maxDate', e.date);
        $('#datetimepicker_to').datetimepicker('hide'); //hide
    });

});