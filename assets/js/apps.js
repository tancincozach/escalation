 ;(function(escalation, $, undefined) {

escalation.processing = false;

escalation.common = {

        confirm: function(msg){
            
            var str = msg || 'Confirm Submit';

            if( confirm(str)){

                return true;
            }

            return false;   

        },

        alert: function(opts){

            var defaults = $.extend({                
                title: 'Alert',
                message: "Alert",
                bodystyles: '',
                bodycls: ''
            }, opts );

            var tpl = '<div class="modal fade">'
            +'    <div class="modal-dialog modal-md">'
            +'        <div class="modal-content">'
            +'            <div class="modal-header">'
            +'                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
            +'                <h4 class="modal-title">'+defaults.title+'</h4>'
            +'            </div>'
            +'            <div class="modal-body '+defaults.bodycls+'" style="'+defaults.bodystyles+'">'
            +'                <p>'+defaults.message+'</p>'
            +'            </div>'
            +'            <div class="modal-footer">'
            +'                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'
            +'            </div>'
            +'        </div><!-- /.modal-content -->'
            +'    </div><!-- /.modal-dialog -->'
            +'</div><!-- /.modal -->';

            $(tpl).modal({keyboard:false, backdrop:false})
            .on('hidden.bs.modal', function (e) {
                $(this).remove();

                if( typeof defaults.redirect != 'undefined' ){
                    
                      window.location = (defaults.redirect == '')?document.URL:defaults.redirect;
                    
                }

            }); 

        },

        search_text:function( text_elem , table_elem){                         

                    var value = $(text_elem).val().toLowerCase();

                    $(table_elem+" tbody tr td").filter(function() {
                      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);                
                    });

        },

        clear_search:function( text_elem , table_elem){

            $(text_elem).val("");

            $(table_elem+" tbody td").css({'display':'block','padding':'.75rem !important'});                 
                        


        }

    }


}(window.escalation = window.escalation || {}, jQuery));