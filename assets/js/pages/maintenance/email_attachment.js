
$( document ).ready(function(){

         $('#start_d').datetimepicker({format: 'MM/DD/YYYY'});

        $('#end_d').datetimepicker({
            useCurrent: false, //Important! See issue #1075
            format: 'MM/DD/YYYY'
        });
        $("#start_d").on("dp.change", function (e) {
            $('#end_d').data("DateTimePicker").minDate(e.date);
        });
        $("#end_d").on("dp.change", function (e) {
            $('#start_d').data("DateTimePicker").maxDate(e.date);
        });


        $('select[name=year]').change(function(){

			email_attachment.display_chart($(this).val());


        });

	
email_attachment.display_chart();

});


var email_attachment = {


		show_by_month_ajax_modal:function ( mon_val = '',yr_val=''){


				$('#summary_report_container').html('<i class="fas fa-spinner text-primary"></i>');

				$.get('maintenance/email_attachment_modal_ajax/'+((new Date()).getTime())+'/?Y='+yr_val+'&M='+mon_val,function( result ){

				$('#summary_report_container').html(result);


				},'html');
		},
		display_chart:function( yr_val=''){	

				$('#email-chart-container').html('<i class="fas fa-spinner text-primary"></i>');

					$.ajax({
						url:'maintenance/get_chart_data/'+((new Date()).getTime()),
						data:{yr:yr_val},
						method:'POST',
						success:function( chart_data ){

								$('#email-chart-container').html("");

								$('#email-chart-container').append('<div id="email-chart"></div>');

									Highcharts.chart('email-chart', {
					                    chart: {
					                        type: 'line',
					                        backgroundColor: '#F5F5F5'
					                              },
					                              title: {
					                                  text: 'Email Attachment Storage Consumption'
					                              },
					                              subtitle: {
					                                  text: 'Figures in BYTES <b>(Hover for converted values)</b>'
					                              },
					                              xAxis: {
					                                  type: 'category'
					                              },
					                              yAxis: {
						                                  title: {
						                                      text: ''
						                                  }

					                                  },
					                                  legend: {
					                                      enabled: true
					                                  },

					                                  plotOptions: {

						                                      series: {
						                                      	  cursor:'pointer',
						                                          borderWidth: 0,
						                                          dataLabels: {
						                                              enabled: true,
						                                              format: '{point.y:.1f}'
						                                          },
          							                              events: {
													                click: function (event) {
													          																	                		
													                		email_attachment.show_by_month_ajax_modal(event.point.name,$('select[name=year]').val());

													                }
													             }
						                                      }


					                                  },

					                              tooltip: {
						                                 /* headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
						                      			  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> <br/>'*/
						                      			  formatter:function(d){
	
																tip = '<span style="color:' + this.point.color + '">\u25CF</span> ' + this.series.name + ': <b> ' + filesize(this.y) + '</b><br/>';
																return tip;
						                      			  }
				                 					 },
					                                      series: [{
					                                          name: "",
/*					                                          color:'#EC9787',*/
			                                         /* colorByPoint: true,*/
			                                          data: chart_data
			                                      }]
			                              
			                    });
						},
						cache:false,
						dataType:'json'
					})

		}
}
