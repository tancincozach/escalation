
$(function(){


			$('.group_dropdown').click(function(event){

				var group = $(this).html();

				$('input[name=group_name]').val(group);				


				event.preventDefault();

			});



						//Helper function to keep table row from collapsing when being sorted
			var fixHelperModified = function(e, tr) {         
				var $originals = tr.children();         
				var $helper = tr.clone();    
				$helper.children().each(function(index) {           
					$(this).width($originals.eq(index).width());
				});         
				return $helper;     
			};



					//Make diagnosis table sortable     
			$("#callresponse-table tbody").sortable({ 
				helper: fixHelperModified,
				stop: function(event,ui) {
					renumber_table('#callresponse-table');
				}
				,placeholder: 'placeholderBackground'
			}).disableSelection();    

			/*//Delete button in table rows     
			$('table').on('click','.btn-delete',function() {
				tableID = '#' + $(this).closest('table').attr('id');
				r = confirm('Delete this item?');
				if(r) {
					$(this).closest('tr').remove();
					renumber_table(tableID);
				}
			}); */

			//Renumber table rows 
			function renumber_table(tableID) {
				
				if( !escalation.processing ){
					
					escalation.processing = true;

					var tr_ids = [];
					$(tableID + " tr").each(function() {
						/*count = $(this).parent().children().index($(this)) + 1;
						console.log(tableID+' '+count);
						$(this).find('.priority').html(count);*/
						 
						var trid = $(this).attr('id');

						if(  typeof trid != 'undefined'){
							//console.log(trid);
							trid = trid.split('-');
							tr_ids.push(trid[1]);
						} 
						
					}); 
					
					$.post('maintenance/callresponse_reorder', { 'ids':tr_ids, 'agent_name':escalation_global.agent_name}, function(res){
						escalation.processing = false;
					})
					.always(function(){
						escalation.processing = false;
					});
				}else{
					alert('Please wait... still processing the previous request');
				}
			}


});








