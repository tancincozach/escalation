
$( document ).ready(function(){

	var procedure_client_id = $('#client_id').val();

	var style_formats = [
			 
			{title : 'Header 2', block : 'h2' },
			{title : 'Header 3', block : 'h3' },
			{title : 'Header 4', block : 'h4' }			 
		];
	var plugins = [
	         "advlist autolink link image lists charmap print preview hr anchor pagebreak",
	         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
	         "save table contextmenu directionality emoticons paste textcolor",
	         " template filemanager"
	    ];
	//filemanager

	//Initialize tinyMCE for Greeting
	tinymce.init({
	    selector: "#pro_content",
	    document_base_url: apps_base_url,
	    theme: "modern",
	   /* width: 300,*/
	    height: 250,
	    cleanup : true,
	    plugins: plugins,
	    convert_urls: false,
	    content_css: "assets/vendor/bootstrap/css/bootstrap.min.css",
	    /**/
	    toolbar: "undo redo | bold italic | alignleft aligncenter alignright alignjustify | fontselect fontsizeselect | styleselect | bullist numlist outdent indent | link image media | print preview | cleanup fullpage | forecolor backcolor emoticons", 
		style_formats : style_formats,

	    external_filemanager_path : 'maintenance/filemanager_files/'+procedure_client_id+'/', 

    	/*templates: '<?php echo base_url(); ?>ajax/tinymcetemplate',*/

	    template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}

	});	


	var FilemanagerDialogue = {

		click: function(URL){

			parent.tinymce.activeEditor.windowManager.getParams().setUrl(URL);
			parent.tinymce.activeEditor.windowManager.close();
		}
	}


	Procedure.reminder.filter($('select[name="reminder_type"]').val());

	//$('#reminder_until_dt').datetimepicker( {format: "YYYY-MM-DD HH:mm"} );
	
	if( $('#reminder_time').length > 0 ){ 

		$('#reminder_time').datetimepicker({
			format: "HH:mm",			
			icons: {
				time: 'fa fa-clock'		
			}            	
        	
		});		
	}

	if( $('#reminder_until_dt').length > 0 ){ 

		$('#reminder_until_dt').datetimepicker({
			format: "YYYY-MM-DD HH:mm",
			icons: {
				time: 'fa fa-clock'		
			}            	
        	
		});

		$('#reminder_until_dt').on("change.datetimepicker", function (e) { 
	        $('#reminder_until_dt').datetimepicker('hide'); //hide
    	});	
	}


	 $(".client_search").on("keyup", function() {
			    var value = $(this).val().toLowerCase();
			    $("#client_tbl tbody tr td").filter(function() {
			      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
			       send_email.clear();
			    });
	  });



});


var Procedure = {

	check_email: function(){
		var email = $('#incoming_email').val();

		$.get('maintenance/procedure_ajax_email_check/'+((new Date()).getTime()), {email:email}, function(json){

			if(!json.status){
				//alert(json.msg); 
				
				var template = '<div class="modal" tabindex="-1" role="dialog">'
					+'  <div class="modal-dialog modal-dialog-centered" role="document">'
					+'    <div class="modal-content">'
					+'      <div class="modal-header">'
					+'        <h5 class="modal-title text-warning">Warning!</h5>'
					+'        <button type="button" class="close" data-dismiss="modal" aria-label="Close">'
					+'          <span aria-hidden="true">&times;</span>'
					+'        </button>'
					+'      </div>'
					+'      <div class="modal-body">'
					+'        <h5 class="text-center">'+json.msg+'</h5>'
					+'      </div>'
					+'      <div class="modal-footer">'
					+'        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>'
					+'      </div>'
					+'    </div>'
					+'  </div>'
					+'</div>';


				var mymodal = $(template).on('shown.bs.modal', function () {
				  
					}).modal('show');

				mymodal.on('hidden.bs.modal', function (e) {
		          
					$('.modal').remove();
					$('.modal-backdrop').remove();
		          	return false;
		        });


			}else{
				alert('EMAIL is available');
			}

		},'json');

	},

	check_client_data_src: function(){
		var client_data_src = $('#client_data_src').val();
		var client_id = $('#procedure_form #client_id').val();

		if( client_data_src == '' ){
			alert('API Data storage field is empty');
			return;
		}

		$.get('maintenance/procedure_ajax_check_tablename/'+((new Date()).getTime()), {client_data_src, client_id}, function(json){

			if(!json.status){
				//alert(json.msg); 
				
				var template = '<div class="modal" tabindex="-1" role="dialog">'
					+'  <div class="modal-dialog modal-dialog-centered" role="document">'
					+'    <div class="modal-content">'
					+'      <div class="modal-header">'
					+'        <h5 class="modal-title text-warning">Warning!</h5>'
					+'        <button type="button" class="close" data-dismiss="modal" aria-label="Close">'
					+'          <span aria-hidden="true">&times;</span>'
					+'        </button>'
					+'      </div>'
					+'      <div class="modal-body">'
					+'        <h5 class="text-center">'+json.msg+'</h5>'
					+'      </div>'
					+'      <div class="modal-footer">'
					+'        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>'
					+'      </div>'
					+'    </div>'
					+'  </div>'
					+'</div>';


				var mymodal = $(template).on('shown.bs.modal', function () {
				  
					}).modal('show');

				mymodal.on('hidden.bs.modal', function (e) {
		          
					$('.modal').remove();
					$('.modal-backdrop').remove();
		          	return false;
		        });


			}else{
				alert('API Data storage is available');
			}

		},'json');	
	}

}


Procedure.reminder = {

	filter: function(reminder_type){

		var reminder_day = $('select[name="reminder_day"]');
		var reminder_weekday = $('select[name="reminder_weekday"]');
		var reminder_month = $('select[name="reminder_month"]');
		var reminder_year = $('select[name="reminder_year"]'); 

		//console.log(reminder_type);
		// console.log(reminder_day);
		// console.log(reminder_day.parents('.form-group'));

		reminder_day.parents('.form-group').hide();
		reminder_weekday.parents('.form-group').hide();
		reminder_month.parents('.form-group').hide();
		reminder_year.parents('.form-group').hide(); 

		switch(reminder_type){
			case 'ONCE':
				reminder_day.parents('.form-group').show();
				reminder_month.parents('.form-group').show();
				reminder_year.parents('.form-group').show(); 
				break;
			case 'EVERY_DAY':

				break;
			case 'WEEKLY':
				reminder_weekday.parents('.form-group').show();
				break;
			case 'FORTHNIGHTLY':
				reminder_day.parents('.form-group').show();
				reminder_month.parents('.form-group').show();
				break;
			case 'MONTHLY':
				reminder_day.parents('.form-group').show();				
				break;
			case 'YEARLY':
				reminder_day.parents('.form-group').show();
				reminder_month.parents('.form-group').show();				
				break;
			default:

				break;
		}		

	},

	onchange_type: function(elem){

		var reminder_type = elem.value;

		this.filter(reminder_type);


	}
}