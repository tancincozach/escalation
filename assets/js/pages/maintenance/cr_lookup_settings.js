
$(function(){


});



var crlookup = {

	save: function(form){

		
		if( confirm('Confirm Save') ){

			var form_data = $(form).serialize();

			$.post('crlookup/settings_save/?_t='+(new Date()).getTime(), form_data, function(res){

				alert(res.msg);


			}, 'json');

			return false;
		}

		return false;

	}

}


crlookup.screen_display = {
	new_tr: function(pipe_lookupfields){

		$.get('crlookup/ajax_screen_display_newtr/?_t='+(new Date()).getTime(), {pipe_lookupfields}, function(res){

			//alert(res.msg);
			if( $('#table_screen_display tbody tr:last').length > 0 ){
				$('#table_screen_display tbody tr:last').after(res.html);
			}else{
				$('#table_screen_display tbody').append(res.html);
			}

		}, 'json');
	},

	on_input_change: function(field){

		var tr_parent = $(field).parents('tr');

		var _tr_id = tr_parent.attr('id').split('_');
		_tr_id = _tr_id[1];

		//alert(_tr_id);
		//console.log(_tr_id);

		$('select[name="screen_display['+_tr_id+'][display1]"]').removeClass('d-none');
		$('input[name="screen_display['+_tr_id+'][display2]"]').removeClass('d-none');

		if( field.value == '0' ){
			$('input[name="screen_display['+_tr_id+'][display2]"]').addClass('d-none');
		}else{
			$('select[name="screen_display['+_tr_id+'][display1]"]').addClass('d-none');
		}         

	},

	delete_tr: function(field){
		var tr_parent = $(field).parents('tr');
		//var table_parent = tr_parent.parents('table');

		tr_parent.fadeOut('normal', function(){
			tr_parent.remove();

			// if( $(table_parent).find('tbody > tr').length == 1 ){
			// 	$(table_parent).find('tbody > tr:first').find('.del_btn').hide();
			// }

			//console.log($(table_parent).find('tbody > tr').length);

		});

	}
}


crlookup.escalation_display = {

	new_tr: function(pipe_lookupfields, spreasheet_input){
		//alert('test');
		var spreasheet_input = (spreasheet_input)||0;
		//if( $('#table_escalation_display tbody tr.spreasheet_input').length > 0 ){
			//spreasheet_input = 1;
		//}
 
		$.get('crlookup/ajax_escalation_display_newtr/?_t='+(new Date()).getTime(), {pipe_lookupfields, spreasheet_input}, function(res){
 
			if( $('#table_escalation_display tbody tr:last').length > 0 ){
				$('#table_escalation_display tbody tr:last').after(res.html);
			}else{
				$('#table_escalation_display tbody').append(res.html);
			}

		}, 'json');
	},

	delete_tr: function(field){
		var tr_parent = $(field).parents('tr');
		//var table_parent = tr_parent.parents('table');

		tr_parent.fadeOut('normal', function(){
			tr_parent.remove();
 

		});
	}

}

crlookup.to_email = {
	new_tr: function(pipe_lookupfields){

		$.get('crlookup/ajax_toemail_newtr/?_t='+(new Date()).getTime(), {pipe_lookupfields}, function(res){

			//alert(res.msg);
			if( $('#table_toemail tbody tr:last').length > 0 ){
				$('#table_toemail tbody tr:last').after(res.html);
			}else{
				$('#table_toemail tbody').append(res.html);
			}

		}, 'json');
	},

	on_input_change: function(field){

		var tr_parent = $(field).parents('tr');

		var _tr_id = tr_parent.attr('id').split('_');
		_tr_id = _tr_id[1];

		//alert(_tr_id);
		//console.log(_tr_id);

		$('select[name="to_email['+_tr_id+'][display1]"]').removeClass('d-none');
		$('input[name="to_email['+_tr_id+'][display2]"]').removeClass('d-none');

		if( field.value == '0' ){
			$('input[name="to_email['+_tr_id+'][display2]"]').addClass('d-none');
		}else{
			$('select[name="to_email['+_tr_id+'][display1]"]').addClass('d-none');
		}         

	},

	delete_tr: function(field){
		var tr_parent = $(field).parents('tr');
		//var table_parent = tr_parent.parents('table');

		tr_parent.fadeOut('normal', function(){
			tr_parent.remove();

			// if( $(table_parent).find('tbody > tr').length == 1 ){
			// 	$(table_parent).find('tbody > tr:first').find('.del_btn').hide();
			// }

			//console.log($(table_parent).find('tbody > tr').length);

		});

	}
}