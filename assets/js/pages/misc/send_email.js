$(function(){


	 $('blockquote').hide();


	 $("#client_search").on("keyup", function() {
			    var value = $(this).val().toLowerCase();
			    $("#client_tbl tbody tr td").filter(function() {
			      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
			       send_email.clear();
			    });
	  });


	 send_email.create_editor('#client_send_email_editor');


	 send_email.check_new_client();

});




var send_email = {


		        create_editor:function( id ){
		          var style_formats = [          
		              {title : 'Header 2', block : 'h2' },
		              {title : 'Header 3', block : 'h3' },
		              {title : 'Header 4', block : 'h4' }      
		            ];
		          var plugins = [
		              'advlist autolink lists link image charmap print preview anchor',
		              'searchreplace visualblocks code fullscreen',
		              'insertdatetime media table contextmenu paste code table textcolor'
		              ];

		          var toolbar =  'undo redo | styleselect | bold italic underline| fontsizeselect '+
		                   '|alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | forecolor backcolor  | table';

		          tinymce.init({
		           setup: function (editor) {
		                  editor.on('change', function () {
		                      tinymce.triggerSave();
		                  });
		            },
		            selector: id,
		            document_base_url: '<?php echo base_url(); ?>',
		            theme: "modern",
		            height: 250,
		            cleanup : true,
		            plugins: plugins,
		            convert_urls: false,
		            menubar:false,
		            statusbar: false,
		            toolbar:toolbar,            
		            style_formats : style_formats,
		            table_adv_tab: true,
		            table_cell_adv_tab: true,
		            table_row_adv_tab: true,
		            image_advtab: true,
		            paste_as_text:true,

		            template_replace_values : {
		              username : "Some User",
		              staffid : "991234"
		            }

		          }); 
		        },

		        refresh_all:function(){

		        		$('#client_tbl > tbody > tr > td').each(function(){

							$(this).removeClass('select_client_tbl');
		        		});

		        		$('.new_client,.send_email_editor').hide();


						$('#client_hdr_name').text("");

						$(tinymce.get('client_send_email_editor').getBody()).html('');

						$('input[name=to],input[name=subject]').val("");
		        },

				select: function( client_id ,client_name,btn_object){

					this.refresh_all();
					

					$(btn_object).parent().addClass('select_client_tbl');

					$('#client_hdr_name').text(client_name);

					$('.send_email_editor').show();

					$(tinymce.get('client_send_email_editor').getBody()).html('');

					$('input[name=to],input[name=subject]').val("");

					$('input[name=client_id]').val(client_id);

				},

				clear:function(){

					this.refresh_all();


				},

				new_client:function(){

					this.clear();

					$('.new_client').show();

					return false;

				},

				refresh_client_table:function(){

					$("#client_search").val("");

					$("#client_tbl tbody tr , #client_tbl tbody td").css('display','block');					

					return false;
				},

				check_new_client:function( client_obj , btn_object ){

					 var search_val = $('input[name=created_client]').val();

					 if(search_val!='' && typeof search_val != 'undefined'){

					 	 var new_client_obj = JSON.parse(search_val);

					 	 if(new_client_obj.hasOwnProperty('client_name')){

								 var value = new_client_obj.client_name.toLowerCase();
								    $("#client_tbl tr").filter(function(i,val) {

								            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);


								            if($(val).find('label').text()==value){

								            	$btn_obj = $(val).find('button');

												 send_email.select(new_client_obj.client_id,new_client_obj.client_name,$btn_obj);	
								            }

								           	
								    });

					 	 }

					 }
				}

}