-- phpMyAdmin SQL Dump
-- version 4.0.10deb1ubuntu0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 04, 2019 at 08:54 AM
-- Server version: 5.5.62-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `escalation`
--

-- --------------------------------------------------------

--
-- Table structure for table `audit_trail`
--

CREATE TABLE IF NOT EXISTS `audit_trail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `audit_from` text COLLATE utf8_unicode_ci,
  `audit_to` text COLLATE utf8_unicode_ci,
  `tran_id` int(11) DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `created` int(11) DEFAULT NULL,
  `audit_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `more_info` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `return_message` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `tran_id_Index` (`tran_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7407727 ;

-- --------------------------------------------------------

--
-- Table structure for table `calls`
--

CREATE TABLE IF NOT EXISTS `calls` (
  `call_id` int(11) NOT NULL AUTO_INCREMENT,
  `tran_id` int(11) DEFAULT NULL,
  `call_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `caller_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caller_phone` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `call_direction` tinyint(4) DEFAULT NULL COMMENT '1 in 0 out 2 sms',
  `call_notes` text COLLATE utf8_unicode_ci,
  `agent_name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `log_status` tinyint(4) DEFAULT '1' COMMENT '1 open 0 close',
  `log_last_update` datetime DEFAULT NULL,
  `call_res_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`call_id`),
  KEY `tran_id_Index` (`tran_id`),
  KEY `call_res_id_Index` (`call_res_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=939873 ;

-- --------------------------------------------------------

--
-- Table structure for table `call_responses`
--

CREATE TABLE IF NOT EXISTS `call_responses` (
  `call_res_id` int(11) NOT NULL AUTO_INCREMENT,
  `call_res_text` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `call_res_open` smallint(6) DEFAULT '1' COMMENT '1 open 0 closed',
  `call_res_status` smallint(4) DEFAULT '1' COMMENT '1 active 0 inactive',
  `call_res_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `call_res_updated` datetime DEFAULT NULL,
  `agent_name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `call_group` varchar(30) COLLATE utf8_unicode_ci DEFAULT '1',
  `call_res_order` smallint(6) DEFAULT '0',
  `client_id` int(11) DEFAULT NULL,
  `call_form_required` smallint(6) DEFAULT '1' COMMENT '0 call form not required',
  PRIMARY KEY (`call_res_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1622 ;

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE IF NOT EXISTS `client` (
  `client_id` int(11) NOT NULL AUTO_INCREMENT,
  `cma_id` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cma_db` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cust_id` int(11) DEFAULT NULL,
  `contact_id_issue` int(11) DEFAULT NULL,
  `contact_id_callsmade` int(11) DEFAULT NULL,
  `client_status` smallint(6) DEFAULT '1',
  `client_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `client_details` text COLLATE utf8_unicode_ci,
  `client_data_src` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `added_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_update_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1212 ;

-- --------------------------------------------------------

--
-- Table structure for table `client_call_lookup`
--

CREATE TABLE IF NOT EXISTS `client_call_lookup` (
  `call_lookup_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `call_lookup_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `call_lookup_status` smallint(6) DEFAULT '1' COMMENT '1 active 0 inactive',
  `source_tablename` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'format: cs_client_id_tbname',
  `create_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_dt` datetime DEFAULT NULL,
  `button_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `call_res_display` text COLLATE utf8_unicode_ci COMMENT 'json',
  `screen_display` text COLLATE utf8_unicode_ci COMMENT 'json',
  `escalation_display` text COLLATE utf8_unicode_ci COMMENT 'json',
  `email_output` text COLLATE utf8_unicode_ci COMMENT 'json',
  `to_email` text COLLATE utf8_unicode_ci COMMENT 'json',
  `step` int(11) DEFAULT '0',
  `lookup_display` text COLLATE utf8_unicode_ci,
  `lookup_script` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`call_lookup_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=92 ;

-- --------------------------------------------------------

--
-- Table structure for table `client_custom_table`
--

CREATE TABLE IF NOT EXISTS `client_custom_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `table_name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `table_desc` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=132 ;

-- --------------------------------------------------------

--
-- Table structure for table `client_procedure`
--

CREATE TABLE IF NOT EXISTS `client_procedure` (
  `pro_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` datetime DEFAULT NULL,
  `pro_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pro_content` text COLLATE utf8_unicode_ci,
  `pro_status` smallint(6) DEFAULT '1',
  `pro_alert_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `added_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_update_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cma_settings` text COLLATE utf8_unicode_ci COMMENT 'json type',
  `call_group` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `other_options` text COLLATE utf8_unicode_ci COMMENT 'json type',
  `priority_flag` int(11) DEFAULT NULL,
  `display_screen` int(11) DEFAULT NULL,
  `incoming_email` text COLLATE utf8_unicode_ci,
  `incoming_mobileno` text COLLATE utf8_unicode_ci,
  `display_subject_line` smallint(6) DEFAULT '0' COMMENT 'use for incoming to use desc or email subj',
  `api_key` text COLLATE utf8_unicode_ci,
  `api_url` text COLLATE utf8_unicode_ci COMMENT 'api url',
  `client_data_src` text COLLATE utf8_unicode_ci COMMENT 'client data storage',
  `reminder_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'once everyday, weekly, fortnightly, montly, yearly',
  `reminder_time` time DEFAULT NULL,
  `reminder_day` int(11) DEFAULT NULL,
  `reminder_weekday` int(11) DEFAULT NULL COMMENT 'Sunday=0 AND Saturday=6 ',
  `reminder_month` int(11) DEFAULT NULL,
  `reminder_year` int(11) DEFAULT NULL,
  `reminder_until_dt` datetime DEFAULT NULL,
  `reminder_alert_details` text COLLATE utf8_unicode_ci,
  `reminder_alert_duration` int(11) DEFAULT '5' COMMENT 'used in Alert Register due',
  `call_lookup_id` int(11) DEFAULT NULL,
  `allow_multi_del` smallint(6) DEFAULT '0' COMMENT 'ALLOW MULTIPLE DELETIONS',
  `multi_del_callres` int(11) DEFAULT NULL COMMENT 'callresponse to be set on multi close',
  PRIMARY KEY (`pro_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2972 ;

-- --------------------------------------------------------

--
-- Table structure for table `data_pinpoint`
--

CREATE TABLE IF NOT EXISTS `data_pinpoint` (
  `pinpoint_id` int(11) NOT NULL AUTO_INCREMENT,
  `tran_id` int(11) DEFAULT NULL,
  `json_data` text COLLATE utf8_unicode_ci,
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `method_type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`pinpoint_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=519262 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_data`
--

CREATE TABLE IF NOT EXISTS `email_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tran_id` int(11) DEFAULT NULL,
  `msg_id` int(11) DEFAULT NULL,
  `msg_uid` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email_from` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_to` text COLLATE utf8_unicode_ci,
  `email_cc` text COLLATE utf8_unicode_ci,
  `email_bcc` text COLLATE utf8_unicode_ci,
  `email_subject` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_reply_to` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body_plain` text COLLATE utf8_unicode_ci,
  `body_html` longtext COLLATE utf8_unicode_ci,
  `msg_udate` int(11) DEFAULT NULL,
  `msg_date` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_udate` int(11) DEFAULT NULL,
  `email_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'INCOMING' COMMENT 'INCOMING FORWARDED INC-REPLY REPLYTO',
  `attachements` text COLLATE utf8_unicode_ci,
  `attachments_size` int(11) DEFAULT NULL COMMENT 'in bytes',
  PRIMARY KEY (`id`),
  KEY `tran_id_Index1` (`tran_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=881502 ;

-- --------------------------------------------------------

--
-- Table structure for table `filemanager`
--

CREATE TABLE IF NOT EXISTS `filemanager` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `file_path` text COLLATE utf8_unicode_ci,
  `file_type` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` datetime DEFAULT NULL,
  `agent_name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `file_status` smallint(6) DEFAULT '1' COMMENT '1 active 0 inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=562 ;

-- --------------------------------------------------------

--
-- Table structure for table `incoming_email_condition`
--

CREATE TABLE IF NOT EXISTS `incoming_email_condition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_from_ref` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_priority` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `incoming_email_scheduler`
--

CREATE TABLE IF NOT EXISTS `incoming_email_scheduler` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `procedure_id` int(11) DEFAULT NULL,
  `is_active` smallint(6) DEFAULT '1',
  `sched_start` time DEFAULT NULL,
  `sched_end` time DEFAULT NULL,
  `created_dt` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_updated_dt` datetime DEFAULT NULL,
  `last_update_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sched_days` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `intensifier_log`
--

CREATE TABLE IF NOT EXISTS `intensifier_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tran_id` int(11) DEFAULT NULL,
  `intensifier_tbl` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `intensifier_id` int(11) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT NULL,
  `agent_name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remarks` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `intensifier_retrieve_log`
--

CREATE TABLE IF NOT EXISTS `intensifier_retrieve_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `added` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_intensifier_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `reminder_log`
--

CREATE TABLE IF NOT EXISTS `reminder_log` (
  `reminder_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `reminder_date_run` datetime NOT NULL,
  `procedure_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`reminder_log_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=95472 ;

-- --------------------------------------------------------

--
-- Table structure for table `smsglobal_incoming_sms`
--

CREATE TABLE IF NOT EXISTS `smsglobal_incoming_sms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sms_to` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sms_from` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sms_msg` text COLLATE utf8_unicode_ci,
  `sms_userfield` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sms_date` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `method_type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=72 ;

-- --------------------------------------------------------

--
-- Table structure for table `table_audit_trail`
--

CREATE TABLE IF NOT EXISTS `table_audit_trail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_table` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ref_field` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ref_val` int(11) DEFAULT NULL,
  `data_json` text COLLATE utf8_unicode_ci,
  `created` int(11) DEFAULT NULL COMMENT 'unix time',
  `created_by` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `audit_type` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'I insert U update D delete BO backup',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19873 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE IF NOT EXISTS `transaction` (
  `tran_id` int(11) NOT NULL AUTO_INCREMENT,
  `tran_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tran_updated` datetime DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `alert_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'EMAIL SMS API REMINDER INTENSIFYER',
  `ref_number` int(11) DEFAULT NULL,
  `brief_description` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Short description of the alert',
  `tran_priority` int(11) DEFAULT '0' COMMENT '0 TBC',
  `alert_created_dt` datetime DEFAULT NULL,
  `alert_updated_dt` datetime DEFAULT NULL,
  `alert_due_dt` datetime DEFAULT NULL,
  `reminder_id` int(11) DEFAULT '0' COMMENT 'link to reminder table',
  `display_screen` int(11) DEFAULT '1' COMMENT '1 escalation 2 action team',
  `tran_status` smallint(6) DEFAULT '1',
  `json_data` longtext COLLATE utf8_unicode_ci,
  `procedure_id` int(11) DEFAULT NULL COMMENT 'ref TO client_procedure',
  `cma_id` int(11) DEFAULT NULL,
  `thread_count` int(11) DEFAULT '0',
  `agent_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Used for Take Control action',
  `is_duplicate_email` smallint(6) DEFAULT '0',
  `call_hand_start` datetime DEFAULT NULL COMMENT 'Call Handling Start',
  `call_hand_end` datetime DEFAULT NULL COMMENT 'Call Handling End',
  `lookup_data` text COLLATE utf8_unicode_ci COMMENT 'lookup json stored',
  `cust_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'cust_name from intensifier',
  `pro_allow_multi_del` smallint(6) DEFAULT '0' COMMENT 'settings under client_procedure link to allow_multi_del',
  `intensifier_id` int(11) DEFAULT NULL COMMENT 'link to intensifier.id',
  PRIMARY KEY (`tran_id`),
  KEY `clientIndex` (`client_id`),
  KEY `tran_priority_Index` (`tran_priority`),
  KEY `tran_status_Index` (`tran_status`),
  KEY `alert_created_dt_Index` (`alert_created_dt`),
  KEY `alert_updated_dt_Index` (`alert_updated_dt`),
  KEY `alert_due_dt_Index` (`alert_due_dt`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1847152 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_lvl` int(11) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated_date` datetime DEFAULT NULL,
  `last_update_by` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `added_by` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_status` tinyint(4) DEFAULT '1' COMMENT '1 active 0 inactive',
  `client_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=104 ;

-- --------------------------------------------------------

--
-- Table structure for table `users_settings`
--

CREATE TABLE IF NOT EXISTS `users_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agent_name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `options` tinytext COLLATE utf8_unicode_ci COMMENT 'user options settings',
  PRIMARY KEY (`id`),
  UNIQUE KEY `agent_name_Index` (`agent_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=395 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
