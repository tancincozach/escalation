<?php
defined('BASEPATH') || exit('No direct script access allowed');

$config['encrypto'] = 'ssl';//'tls';
$config['validate'] = false;//true;
$config['host']     = 'mail.welldone.net.au';
$config['port']     = 993;//143;
$config['username'] = 'escalationapp@welldone.com.au';
$config['password'] = 'esc@456Tion';
$config['folders'] = array(
	'inbox'  => 'INBOX',
	'sent'   => 'Sent',
	'trash'  => 'Trash',
	'spam'   => 'Spam',
	'drafts' => 'Drafts',
);
$config['expunge_on_disconnect'] = false;
$config['cache'] = array(
	'active'     => false,
	'adapter'    => 'file',
	'backup'     => 'file',
	'key_prefix' => 'imap:',
	'ttl'        => 60,
);