<?php
 
 /* ------------------------------------------------------------------------
 * Agent_status
 * ------------------------------------------------------------------------
 */

/**
 * Agent Status
 * @author 			Zach Tancinco
 * @version 		1.0
 * @date_created	2018-07-02
 * @date_updated	
 */


class Agent_status{
	
	//public $CI;
	//public $dbconn = null; //db connection handler

	function __construct() {
       //$this->CI =& get_instance();    
	}
 
 
	// public function agentStatusUpdate($key="",$userid=0,$activ=-1,$reason="",$did=0)
	// Sample URL: http://cma.welldone.com.au/index-
	// ci.php/api/agentStatusUpdate/0e15ebddd82d635e955e00377afd6012/39/0/Escalation_Issues/205
	// Sample data returned (if no errors): {"id":"562001", "tbl":"operator_status","agentid":"39",
	// "active":"0","shift_end":"1530139520","unavailable_start":"","reason":"Escalation Issues",
	// "call_tag":"","on_call_flag":0,"extension":"6296","agent_status":"addLog executed"} 

	public function agentStatusUpdate($a){

		try {

			//TO-DO block here to allow only push on LIVE


			$key = '0e15ebddd82d635e955e00377afd6012';
			//$userid = 39;
			$userid = @$a['agent_id'];
			$active = 0;
			$reason = str_replace(' ', '_', $a['reason']);
			$did 	= @$a['did'];
			$url = "http://cma.welldone.com.au/index-ci.php/api/agentStatusUpdate/".$key."/".$userid."/".$active."/".$reason."/".$did;

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			$head = curl_exec($ch);
			$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);
 
			
			$return = (array)json_decode($head);
			//print_r($return);

			if( is_array($return) AND !empty($return) ){
				if( isset($return['error']) ){
					return $head;
				}else{
					return 'Inserted';
				}
			}else{
				return 'Not Inserted';
			}
			//return ($head) ? $head : 'Not Inserted';

		}catch(Exception $e){
			return 'Caught exception: '.  $e->getMessage(). "\n";
			//return 0;
		}
		 

	}

	public function getCustomerData($did){

		try {

			//TO-DO block here to allow only push on LIVE

			/*if( $_SERVER['SERVER_NAME'] != 'escalation.welldone.net.au' ) {
				return 'Inserted - local';
				exit;
			}*/

			$key = 'c938a1690260a794a9d979c84c294617';
			//$key = '0e15ebddd82d635e955e00377afd6012';

			$url = "http://cma.welldone.com.au/index-ci.php/api/getCustomerData/$key/".$did."/CustID";
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			$head = curl_exec($ch);
			$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);
 
			
			$return = json_decode($head);
			return $return;

		}catch(Exception $e){
			return 'Caught exception: '.  $e->getMessage(). "\n";
			//return 0;
		}
		 

	}
 
}

?>
