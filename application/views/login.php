<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- <link rel="icon" href="../../../../favicon.ico"> -->

        <title><?php echo HEADER_TITLE; ?> - Login</title>

        <base href="<?php echo base_url(); ?>" />

        <!-- Bootstrap core CSS -->
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="assets/css/signin.css?v=<?php echo CSS_VER_STYLES; ?>" rel="stylesheet">
        </head>

    <body>
        <?php echo form_open('', 'class="form-signin" role="form" method="post" '); ?> 
            <div class="text-center mb-4">
               <!--  <img class="mb-4" src="https://getbootstrap.com/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72"> -->

                <h1 class="">ESCALATION</h1>                
                <h2 class="h3 mb-3 font-weight-normal">Please sign in</h2>                
            </div>

            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="1" onclick="selectLoginType(1)" checked>
              <label class="form-check-label" for="inlineRadio1">Client</label>
            </div>

            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="0" onclick="selectLoginType(0)">
              <label class="form-check-label" for="inlineRadio2">CMA</label>
            </div>

            <br />
            <br />

            <div class="form-label-group">
                <input type="text" id="inputName" name="inputName" class="form-control" placeholder="Name" required>
                <label for="inputName">Name</label>
            </div>

            <div class="form-label-group">
                <input type="text" id="inputUsername" name="inputUsername" class="form-control" placeholder="Username" required>
                <label for="inputUsername">Username</label>
            </div>

            <div class="form-label-group">
                <input type="password" id="inputPassword" name="inputPassword" class="form-control" placeholder="Password" required>
                <label for="inputPassword">Password</label>
            </div>

            <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>

            <p class="mt-5 mb-3 text-muted"><?php echo FOOTER; ?></p>

        </form>

 
        <script src="assets/vendor/jquery/jquery.min.js"></script>
        <!-- <script src="assets/vendor/popper.min.js"></script> -->
        <!-- <script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script> -->
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/vendor/holder.min.js"></script>
        <script src="assets/js/login.js"></script>

    </body>
</html>