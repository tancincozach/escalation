
<div class="col-sm-12">
	
	<div class="clearfix">
		
		<?php if( @$menu_active == 'cma_reminder' ): ?>
			<h5 class="my-3 float-left">
				CMA Reminder Register <?php echo ( @$useroption->display_screen != '' )?'('.$this->display_screen[$useroption->display_screen].')':''; ?>
			</h5>
		<?php else: ?>
			<h5 class="my-3 float-left">
				Alert Register <?php echo ( @$useroption->display_screen != '' )?'('.$this->display_screen[$useroption->display_screen].')':''; ?>
			</h5>
		<?php endif; ?>
		<div class="clearfix m-0 p-0 float-right">
		<?php if(@$useroption->display_screen!=3):?>	
			<div class="dropdown dropleft">
			  	<a class="btn  btn-sm dropdown-toggle m-1 p-1" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			    	<i class="fas fa-user-cog"></i>
			  	</a>

				<div class="dropdown-menu">
				  	<form class="px-4 py-3" style="min-width: 350px !important" onsubmit="return alert_register.useroption.save(this)">
					  	<h5>User Settings</h5>
						<small class="form-text text-muted">
							User setting is to set default option for users.
						</small>
					  	<div class="dropdown-divider"></div>
					  	<div class="form-group row">
						    <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm">Display Screen</label>
						    <div class="col-sm-7">
							    <select class="custom-select custom-select-sm m-0 p-0" name="display_screen"> 
									<option value="" <?php echo (@$useroption->display_screen=='')?'selected':''; ?> >All</option>
									<option value="1" <?php echo (@$useroption->display_screen=='1')?'selected':''; ?> >Escalation</option>
									<option value="2" <?php echo (@$useroption->display_screen=='2')?'selected':''; ?> >Action Team</option>								        
									<option value="3" <?php echo (@$useroption->display_screen=='3')?'selected':''; ?> >Back office</option>								        
							    </select>
						    </div>
					  	</div> 
					  	<div class="clearfix">
					  		<button type="submit" class="btn btn-primary btn-sm float-right">Save</button> 
					  	</div>
				  	</form> 
				</div>
			  
			</div>
		<?php endif;?>
		</div>
	</div>


	<?php if($this->session->flashdata('fmesg') != ''): ?>
	<div class="fmesg px-2 py-1 alert alert-primary mb-1" role="alert">
		<?php echo $this->session->flashdata('fmesg'); ?>
	</div>
	<?php endif; ?>

	<div class="row clearfix">

		<div class="col-sm-10">
			
			<form class="form-inline">
			  
			  	<div class="form-group mx-sm-3 mb-2">
				    <label for="search">Search  </label>
				    <input type="text" name="search" class="form-control form-control-sm" placeholder="Search" value="<?php echo @$filters['search']; ?>" >
			  	</div>

			  	<?php if( $this->client_id == 0 ): ?>
			  	<div class="form-group mx-sm-2 mb-2"> 
			  		<div class="input-group">
			  		    <div class="input-group-prepend">
							<div class="input-group-text">
								<i class="fa fa-users" aria-hidden="true"></i>
							</div>
					    </div>

					    <?php if( @$menu_active != 'cma_reminder' ): ?>
							<?php echo form_dropdown('client_id', $clients, @$filters['client_id'], ' class="custom-select custom-select-sm" style="width: 150px !important" '); ?>
						<?php else: ?>
							<?php echo form_dropdown('cust_name', $customer_names, @$filters['cust_name'], ' class="custom-select custom-select-sm" '); ?>
						<?php endif; ?>
					</div>	

				</div>
				<?php endif; ?>


				<?php if( @$menu_active != 'cma_reminder' ): ?>
			  	<div class="form-group mx-sm-2 mb-2 clearfix">
			  		<div class="input-group">
			  		    <div class="input-group-prepend">
							<div class="input-group-text">
								<i class="fas fa-bell" aria-hidden="true"></i>
							</div>
					    </div>
						<select class="custom-select custom-select-sm" name="alert_type">
							<option value="" <?php echo (@$filters['alert_type']=='')?'selected':''; ?> >---Alert Type---</option>
							<option value="API" <?php echo (@$filters['alert_type']=='API')?'selected':''; ?> >API</option>				
							<option value="EMAIL" <?php echo (@$filters['alert_type']=='EMAIL')?'selected':''; ?> >EMAIL</option>				
							<option value="REMINDER" <?php echo (@$filters['alert_type']=='REMINDER')?'selected':''; ?> >REMINDER</option>				
							<option value="CMA REMINDER" <?php echo (@$filters['alert_type']=='CMA REMINDER')?'selected':''; ?> >CMA REMINDER</option>				
						</select>
					</div>	
				</div>
				<?php endif; ?>

			  	<div class="form-group mx-sm-2 mb-2 clearfix">
			  		<div class="input-group">
			  		    <div class="input-group-prepend">
							<div class="input-group-text">
								<i class="fa fa-exclamation" aria-hidden="true"></i>
							</div>
					    </div>
						<select class="custom-select custom-select-sm" name="tran_status">
							<option value="" <?php echo (@$filters['tran_status']=='')?'selected':''; ?> >---Status---</option>
							<option value="0" <?php echo (@$filters['tran_status']=='0')?'selected':''; ?> >Closed</option>
							<option value="1" <?php echo (@$filters['tran_status']=='1')?'selected':''; ?>>Open</option>					
						</select>
					</div>	
				</div>

			  	<div class="form-group mx-sm-2 mb-2">

			  		<div class="input-group">
			  		    <div class="input-group-prepend">
							<div class="input-group-text">
								<i class="fa fa-desktop" aria-hidden="true"></i>
							</div>
					    </div>
						<select class="custom-select custom-select-sm" name="display_screen">
							<option value="" <?php echo (@$filters['display_screen']=='')?'selected':''; ?> >---Display Screen---</option>
							<?php foreach($disp_opts as $dskey=>$dsval): ?>
								<option value="<?php echo $dskey; ?>" <?php echo (@$filters->display_screen == $dskey)?'selected':''; ?> ><?php echo $dsval; ?></option> 					
							<?php endforeach; ?>				
						</select>
					</div>	 
				</div>

			  	<button type="submit" class="btn btn-primary btn-sm mb-2"> <i class="fas fa-search"></i> Filter</button>
			  	<a class="btn btn-secondary btn-sm mb-2 ml-2" href="register" role="button"> <i class="fas fa-sync "></i> Reset</a>

			</form>
		</div>

		<div class="col-sm-2 text-right">			

			<?php if( @$menu_active != 'cma_reminder' ): ?>
				<a class="btn btn-sm btn-warning" href="worker/incoming_email/cron" target="_blank">MANUAL PULL INCOMING EMAIL</a>
			<?php endif; ?>
			
		</div>

	</div>

	<table class="table table-bordered table-hover table-sm">
		<thead class="table-primary">
			<tr>
				<th scope="col">
				<?php if($this->allow_reg_cbox > 0):?>
					<div class="btn-group">
					  <button type="button" class="btn btn-info dropdown-toggle py-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					    <i class="fas fa-pen-square"></i>
					  </button>
					  <div class="dropdown-menu">
					    <h6 class="dropdown-header"><input type="checkbox" name="check_all">&nbsp;&nbsp;Check/Uncheck</h6>
					     <div class="dropdown-divider"></div>
					    <a class="dropdown-item close_alert" href="#">Closed Alert(s)</a>
					    
					  </div>
					</div>
				<?php endif;?>
				</th>		
				<th scope="col">
					<div class="btn-group">
					  <button type="button" class="btn btn-primary dropdown-toggle py-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					    <i class="fas fa-pen-square"></i>
					  </button>
					  <div class="dropdown-menu">
					    <h6 class="dropdown-header"><input type="checkbox" name="check_all_2">&nbsp;&nbsp;Check/Uncheck</h6>
					     <div class="dropdown-divider"></div>
					    <a class="dropdown-item delete_alert" href="#">Closed Alert(s)</a>
					    
					  </div>
					</div>
				</th>
				<th scope="col">Client</th>  
				<th scope="col">Alert/Log ID</th>  
				<th scope="col">CMA ID</th>  
				<th scope="col">Alert Type (EMAIL/SMS/API etc)</th>  
				<th scope="col">Description</th>  
				<th scope="col">Priority FLAG</th>  
				<th scope="col" style="width: 130px">Date/Time SET</th>  
				<th scope="col" style="width: 130px">Date/Time DUE</th>  
				<th scope="col">Repeat</th>  
				<th scope="col">Display</th>  
				<th scope="col">Status</th>  
				<th scope="col" style="width: 180px;">Options</th>  
			</tr>
		</thead>
		<tbody>

				<?php //echo $row->json_data; 
					/*if( is_string($row->json_data) && is_array(json_decode($row->json_data, true)) ){
						echo '<pre>';
						
						print_r(json_decode($row->json_data));
						echo '</pre>';
					}else{
						echo $row->json_data; 
					}*/
				?>	

			<?php foreach( $results as $row ): 
				//$due_class = 'bg-primary';//'bg-success';
				$due_class = 'bg-blue-LightSkyBlue';//'bg-success';
				
				if( $row->tran_status == '0' ){
					//$due_class = 'bg-secondary text-white';
					$due_class = 'bg-gray-Silver text-black';
				}elseif( $row->due_in_min == 11 AND  $row->tran_priority == 1){
					//$due_class = 'bg-danger';
					$due_class = 'bg-red-brighter text-white';					
				//}elseif( $row->due_in_min > -10  ){
				}elseif( $row->due_in_min == 11 ){
					//$due_class = 'bg-danger';
					$due_class = 'bg-red-IndianRed text-white';					
				//}elseif( $row->due_in_min > -10  ){
				}elseif( $row->due_in_min == 10  ){
					//$due_class = 'bg-warning';
					//$due_class = 'bg-orange-DarkOrange';
					$due_class = 'bg-orange-Orange text-black';
				}

			?>
			<tr class="<?php echo $due_class; ?>">
				<td>
					<?php if($row->tran_status==1):?>
					<input type="checkbox" name="cbox_reg" value="<?php echo $row->tran_id; ?>">
					<?php endif;?>
				</td>
				<td>
					<?php if((int)$row->pro_allow_multi_del==1 && (int)$row->tran_status==1):?>
						<input type="checkbox" name="cbox_reg_2" value="<?php echo $row->tran_id; ?>">
					<?php endif;?>
				</td>
				<td><?php echo $row->alert_type == 'CMA REMINDER'?$row->cust_name:$row->client_name; ?></td>
				<td><?php //echo $row->ref_number; 
					echo  $row->alert_type == 'CMA REMINDER'?
						'<a  href="callactivity/open_cma_reminder_view/'.$row->tran_id.'" class="text-black text-underline" target="_blank" title="View Log">'.$row->ref_number.'</a>'
						:
						'<a  href="callactivity/view/'.$row->tran_id.'" class="text-black text-underline" target="_blank" title="View Log">'.$row->ref_number.'</a>';
				?></td>
				<td><?php echo $row->cma_id; ?></td>
				<td><?php echo $row->alert_type; ?></td>
				<td style="word-break: break-all" >
					<?php if( $row->alert_type == 'CMA REMINDER' ): ?>					
						 <!-- CMA / APP reminder activated - needs processing -->

						<!-- <h6 class="border border-warning bg-warning text-dark">CMA / APP reminder activated - needs processing</h6> -->
						<div  class="cma_reminder_container">
							<input id="ch_<?php echo $row->tran_id; ?>" class="cma_reminder_ch" type="checkbox">
							<label for="ch_<?php echo $row->tran_id; ?>" class="cma_reminder_label"></label>
							<div class="cma_reminder_content">
							<?php 
								$json_data = json_decode($row->json_data);
								echo str_replace(PHP_EOL, '<br />', stripslashes($json_data->BODY));
							?>
							</div>

						</div>
					<?php else: ?> 
						
						<?php echo $row->brief_description; ?> 
						<?php if($row->thread_count > 0): ?>
							<span class="badge badge-primary float-right" title="Alert details thread count"><?php echo $row->thread_count; ?></span>
						<?php endif; ?>

					<?php endif; ?>

				</td>
				<td><?php echo isset($this->priority_flag[$row->tran_priority])?$this->priority_flag[$row->tran_priority]:$this->priority_flag['4']; ?></td>
				<td style="<?php echo ($row->tran_status==0)?'font-weight: bold':''; ?>"><?php echo (!in_array($row->alert_custom_dt, array('', '0000-00-00 00:00:00')))?date('d/m/Y H:i', strtotime($row->alert_custom_dt)):''; ?></td>
				<td style="<?php echo ($row->tran_status==1)?'font-weight: bold':''; ?>"><?php echo (!in_array($row->alert_due_dt, array('', '0000-00-00 00:00:00')))?date('d/m/Y H:i', strtotime($row->alert_due_dt)):'' //alert_due_custom_dt; ?></td>
				<td>
				
				</td>
				<td><?php echo @$this->display_screen[$row->display_screen]; ?></td>  
				<td><?php echo @$this->tran_status_desc[$row->tran_status]; ?></td>  				
				<td class="text-center">					

			 	<?php

			 		if( $row->alert_type == 'CMA REMINDER' ){
			 			if( $row->agent_name == '' ){
			 				echo '<button class="btn btn-sm btn-primary py-0" onclick="alert_register.take_control('.$row->tran_id.', \''.$row->ref_number.'\', \''.$row->alert_type.'\')">ACK</button>';
			 			}else{
			 				echo '<a  href="callactivity/open_cma_reminder/'.$row->tran_id.'" class="btn btn-sm btn-secondary text-white py-0" >'.@$row->agent_name.'</a>';
			 			}

			 			//echo '&nbsp;<a  href="callactivity/open_cma_reminder_view/'.$row->tran_id.'" class="btn btn-sm btn-secondary text-white py-0" >View</a>';
			 		}else{
			 			if( $row->agent_name == '' ){
			 				echo '<button class="btn btn-sm btn-primary py-0" onclick="alert_register.take_control('.$row->tran_id.', \''.$row->ref_number.'\', \''.$row->alert_type.'\')">Take Control</button>';
			 			}else{
			 				echo '<a  href="callactivity/open/'.$row->tran_id.'" class="btn btn-sm btn-secondary text-white py-0" >'.@$row->agent_name.'</a>';
			 			}

			 			//echo '&nbsp;<a  href="callactivity/view/'.$row->tran_id.'" class="btn btn-sm btn-secondary text-white py-0" >View</a>';
			 		}

				?>

				</td>
			</tr>
			<?php endforeach; ?>

		</tbody>
	</table>

    <div class="row">
        <div class="col-sm-6 justify-content-start">
             <?php echo $showing; ?>
        </div>
        <div class="col-sm-6 d-flex justify-content-end">            
            <?php echo $links; ?>
        </div> 
    </div>

</div>