
<div class="col-sm-12">
	
	<h5 class="my-3">Email Register</h5>

	<div class="row">
		<div class="col-sm-12">
			<strong ><?php echo stripslashes($record->email_subject); ?></strong>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6">
			From: <?php echo $this->Commonmodel->email_from_decode_format($record->email_from); ?> <br />
			To: <?php echo $this->Commonmodel->email_to_decode_format($record->email_to); ?> <br />
		</div>
		<div class="col-sm-6 text-right align-baseline">
			Date: <?php echo date('D, d M Y H:i:s'); ?>
		</div>		
	</div>
	 
	<div class="px-1 py-5 border">

		<?php echo stripslashes($record->msg_body_html); ?>

	</div>

	<br />
</div>