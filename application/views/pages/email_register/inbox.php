
<div class="col-sm-12">
	
	<h5 class="my-3">Email Register</h5>

	<div>
		<p>
			Please send an email to <strong>escalationapp@welldone.com.au</strong> to to retrieve new email(s). 
			Then click the <strong class="text-primary">Pull Email</strong> button
		</p>

	</div>

	<div class="row">
		<div class="col-sm-4">&nbsp;</div> 
		<div class="col-sm-4 text-center">
			<?php echo $pullemails; ?>
		</div> 
		<div class="col-sm-4 text-right">
			<a class="btn btn-primary btn-sm px-1 py-0" href="emailregister" role="button">Pull Email</a>
		</div> 
		
	</div>
	<table class="table table-bordered table-hover table-sm">
		<thead class="table-primary">
			<tr>
				<th scope="col" style="width: 50px;"></th>
				<th scope="col">From</th> 
				<th scope="col">To</th> 
				<th scope="col">Subject</th> 
				<th scope="col">Date</th>  
				<th scope="col">Options</th> 
			</tr>
		</thead>
		<tbody>
		
			<?php foreach( $results as $row ): ?>
			<tr>
				<td></td>
				<td><?php echo $this->Commonmodel->email_from_decode_format($row->email_from); ?></td>
				<td><?php echo $this->Commonmodel->email_to_decode_format($row->email_to); ?></td>
				<td><?php echo stripslashes($row->email_subject); ?></td> 
				<td><?php echo date('d/m/Y H:i:s', $row->msg_udate); ?></td> 
				<td class="">					
					<a class="btn btn-info btn-sm px-1 py-0" href="emailregister/open/<?php echo $row->id; ?>" role="button">View</a>
				</td>
			</tr>
			<?php endforeach; ?>

		</tbody>
	</table>

    <div class="clearfix">
        <div class="pull-left text-left">
             <?php echo $showing; ?>
        </div>
        <div class="pull-right text-right">
            <ul class="pagination pagination-sm">
                <?php echo $links; ?>
            </ul>
        </div> 
    </div>

</div>