
<div class="col-sm-2 border-right">

</div>

<div class="col-sm-10">
	<?php if($this->session->flashdata('fmesg') != ''): ?>
	<div class="fmesg px-2 py-1 alert alert-primary mb-1" role="alert">
		<?php echo $this->session->flashdata('fmesg'); ?>
	</div>
	<?php endif; ?>

	<?php if($this->session->flashdata('error') != ''): ?>
	<div class="fmesg px-2 py-1 alert alert-danger mb-1" role="alert">
		<?php echo $this->session->flashdata('error'); ?>
	</div>
	<?php endif; ?>

	<div class="row">

		<div class="col-sm-3 p-2">

			<div class="input-group mb-3">
				<?php if(isset($client_created)):?>
				 <input type="hidden" name="created_client" value="<?php echo htmlentities(json_encode(@$client_created));?>"/>
				<?php endif;?>
			  <input type="text" id="client_search" class="form-control input-sm"  placeholder="Search for client.." aria-label="Search for client.." aria-describedby="basic-addon2" value="<?php echo @$client_created->client_name;?>">
			  <div class="input-group-append">
			    <button class="btn btn-outline-danger btn-sm" type="button" onclick="javascript: send_email.refresh_client_table();"><i class="far fa-times-circle "></i></button>
			  </div>
			</div>

			<table id="client_tbl" class="table table-sm ">
			  <thead>	  
			    <tr>
			      <th scope="col" class="table-primary" >CLIENT
				      <button class="btn btn-success btn-sm py-0 float-right " onclick="javascript:send_email.new_client();">
					      <i class="fas fa-plus-square"></i>&nbsp;
					      New
				      </button>
			      </th>	      
			    </tr>
			  </thead>
			  <tbody>


				
				<?php foreach($clients as $row): ?>

			    <tr>
			      
			      <td><?php if( $this->client_id == 0 ): ?>
				  			<label><?php echo $row->client_name; ?></label>
				  			<button class="btn btn-info btn-sm py-0 float-right"   onclick="javascript:send_email.select( <?php echo @$row->client_id;?>,'<?php echo @$row->client_name;?>', this )"  >
				  				<i class="far fa-check-square"></i>&nbsp;
				  				Select
				  			</button>
				  		<?php else: ?>

				  			<label><?php echo $row->client_name; ?></label>

				  			<button class="btn btn-info btn-sm py-0 float-right" alt="<?php echo $this->client_id?>:<?php echo $row->client_name?>" >
					  			<i class="far fa-check-square "></i>&nbsp;
				  				Select
				  			</button>
				  		<?php endif; ?></td>
			      
			    </tr>
			
				<?php endforeach;?>		

			  </tbody>
			</table>

		</div>
		<div class="col-sm-8 p-2">

			<div class="send_email_editor"  style="display:none">	
				<?php echo form_open('', ' name="client_send_email_frm" class="" role="form" method="post" onsubmit=" return confirm(\'Confirm Send Email?\'); "'); ?>	
						&nbsp;
						<input type="hidden" name="action_type" value="send_email">
						<input type="hidden" name="client_id" value="">
							<h4 id="client_hdr_name" class="p-3"></h4>
							<div class="form-group  col-sm-10">
								<label class="form-label">From:</label>	
								<input type="text" name="from" class="form-control" value="<?php echo SYS_EMAIL;?>" readonly>
							</div>

							<div class="form-group  col-sm-10">
								<label class="form-label">To:</label>	
								<input type="text" name="to" class="form-control" required="required">
							</div>		

							<div class="form-group  col-sm-10">
								<label class="form-label">Subject:</label>	
								<input type="text" name="subject" class="form-control" required="required">
							</div>	
							
									
							<div class="form-group p-3">

							 	<textarea id="client_send_email_editor" name="email_body"></textarea>				
							     <br/>
								<button type="submit" class="btn btn-primary btn-sm">Send Email</button>					
								<!-- <button class="btn btn-default btn-sm cancel-email">Close</button>					 -->
								<a class="btn btn-dark btn-sm" href="send_email" role="button">Close</a>

							</div>

				<?php echo form_close();?>
			</div>


			<div class="new_client" style="display: none">

			<?php echo form_open('', ' name="client_create_frm" class="" role="form" method="post" onsubmit=" return confirm(\'Confirm Create Client?\'); "'); ?>	
				<input type="hidden" name="action_type" value="create_client">
				<h4  class="p-3">Client - Form</h4>
							<div class="form-group  col-sm-5">
								<label class="form-label">Name:</label>	
								<input type="text" name="client_name" class="form-control">
							</div>

							<h5 class="p-3">CMA Settings</h5>	
							<div class="form-group  col-sm-5">
								<label class="form-label">CMA ID:</label>	
								<input type="text" name="cma_id" class="form-control">
							</div>	
							
									
							<div class="form-group p-3">
							     
								<button type="submit" class="btn btn-primary btn-sm">Create Client</button>					
								<!-- <button class="btn btn-default btn-sm cancel-email">Close</button>					 -->
								<a class="btn btn-dark btn-sm" href="send_email" role="button">Close</a>

							</div>
			<?php echo form_close();?>

			</div>

		</div>
	</div>
	



</div>