
<div class="col-sm-12">
	
	<h5 class="my-3">Alert Register</h5>

	<table class="table table-bordered table-hover table-sm">
		<thead class="table-primary">
			<tr>
				<th scope="col">#</th>
				<th scope="col">Date</th> 
				<th scope="col">Time</th> 
				<th scope="col">Device Name</th> 
				<th scope="col">Details</th> 
				<th scope="col">Status</th> 
				<th scope="col">Options</th> 
			</tr>
		</thead>
		<tbody>
			<tr>
				<th scope="row">1234</th>
				<td>07/02/2018</td>
				<td>13:40</td>
				<td>Vehicle 123</td>
				<td>Some text here</td>
				<td>OPEN</td>
				<td class="text-right">					
					<a class="btn btn-primary btn-sm px-1 py-0" href="callactivity/open/" role="button">Select</a>
				</td>
			</tr> 
		</tbody>
	</table>

</div>