
<div class="col-sm-12">
	
	<h5 class="my-3">SMS Register</h5>

	<table class="table table-bordered table-hover table-sm">
		<thead class="table-primary">
			<tr>
				<th scope="col">#</th> 
				<th scope="col">From</th> 
				<th scope="col">To</th> 
				<th scope="col">Body</th> 
				<th scope="col">Server Date/Time</th> 
				<th scope="col">Method</th> 
				<th scope="col">Options</th> 
			</tr>
		</thead>
		<tbody>

			<?php foreach( $results as $row ): ?>
			<tr>
				<td></td>
				<td><?php echo $row->sms_from; ?></td> 
				<td><?php echo $row->sms_to; ?></td> 
				<td><?php echo stripslashes($row->sms_msg); ?></td> 
				<td><?php echo $row->created_at; ?></td> 
				<td><?php echo $row->method_type; ?></td> 
				<td class="text-right">					
					
				</td>
			</tr>
			<?php endforeach; ?>

		</tbody>
	</table>

</div>