	
<?php 
		
  
 	//$this->Commonmodel->array_print_to_html($record);
 	//$this->Commonmodel->array_print_to_html($alert_detail);
?>


<?php if($record->alert_type == 'EMAIL'): ?>

	<?php foreach($alert_detail as $emaildata): ?>

		<div class="border m-1 bg-white">
			<table class="table table-sm"> 
				 
				<tr>
					<td style="width: 130px"><strong>Date/Time</strong></td>
					<td>
						 
						<?php 									
							echo (!in_array($emaildata->email_udate, array('')))?date('d/m/Y H:i:s', $emaildata->email_udate):date('d/m/Y H:i:s', strtotime($emaildata->created_at));
						?>
						
						<span class="float-right ml-2">
							<?php echo $emaildata->email_type; ?>
						</span>
 
					</td>
				</tr>
				 

				<?php if( isset($emaildata->email_from) AND @$emaildata->email_from != '' ): ?>
				<tr>
					<td><strong>FROM</strong></td>
					<td>
						 
						<?php 
							$from_email = json_decode($emaildata->email_from);
							echo $from_email->email;
						?>

					</td>
				</tr>
				<?php endif; ?>

				<?php if( isset($emaildata->email_to) AND @$emaildata->email_to != '' ): ?>
				<tr>
					<td><strong>TO</strong></td>
					<td>							 
						<?php 
							$to_email = json_decode($emaildata->email_to);
							$tos_ar = array();
							foreach ($to_email as $to) {
								$tos_ar[] = $to->email;
							}
							echo implode('; ', $tos_ar);
						?>
					</td>
				</tr>
				<?php endif; ?>

				<?php if( isset($emaildata->email_cc) AND @$emaildata->email_cc != '' ): ?>
				<tr>
					<td><strong>CC</strong></td>
					<td>							 
						<?php 
							$cc_email = json_decode($emaildata->email_cc);
							$ccs_ar = array();
							foreach ($cc_email as $cc) {
								$ccs_ar[] = $cc->email;
							}
							echo implode('; ', $ccs_ar);
						?>
					</td>
				</tr>
				<?php endif; ?>

				<?php if( isset($emaildata->email_subject) AND @$emaildata->email_subject != '' ): ?>
				<tr>
					<td><strong>SUBJECT</strong></td>
					<td><?php echo stripslashes($emaildata->email_subject); ?></td>
				</tr>
				<?php endif; ?>


				<?php if( !empty($emaildata->attachements) ): ?>
				<tr>
					<td><strong>ATTACHMENTS</strong></td>
					<td>
						<?php  
							//print_r($emaildata->attachements);
							$attachements = json_decode($emaildata->attachements);
							foreach ($attachements as $attach) {
								$a_name = explode('_', $attach);
								unset($a_name[0]);
								//echo '<a href="'.EMAIL_ATTACH_LOC.$attach.'" target="_blank" class="mr-1">'.implode('_', $a_name).'</a>'; 
								echo '<a href="callactivity/download_attachment/?file='.$attach.'" class="mr-1">'.implode('_', $a_name).'</a>'; 
							}

						?>
					</td>
				</tr>
				<?php endif; ?>

				<?php if( isset($emaildata->body_html) AND  trim(@$emaildata->body_html) != ''): ?>
					<tr>
						<td colspan="2" class="pt-2">								 				
							<?php //echo $this->Commonmodel->parseUtf8ToIso88591(stripslashes($emaildata->body_html)); ?>
							<?php 
								//echo stripslashes($emaildata->body_html); 								 
							?>
							<div class="embed-responsive embed-responsive-16by9">
							  <iframe class="embed-responsive-item" src="callactivity/iframe_email_body/<?php echo $emaildata->id; ?>" allowfullscreen></iframe>
							</div> 
						</td>
					</tr>
				<?php else: ?>
					<tr>
						<td colspan="2" class="pt-2">								 				
							<?php  
								$body_plain = stripslashes($emaildata->body_plain);
								echo str_replace(PHP_EOL, '<br />', $body_plain); 
							?>
						</td>
					</tr>										
				<?php endif; ?>

			</table>
		</div>
	<?php endforeach; ?>

<?php else: ?>


	<table class="table table-sm"> 
		
		<?php if($record->alert_type != 'API'): ?>

		<tr>
			<td style="width: 130px"><strong>Date/Time</strong></td>
			<td>
				 
				<?php 									
					echo (!in_array($record->alert_created_dt, array('', '0000-00-00 00:00:00')))?date('d/m/Y H:i:s', strtotime($record->alert_created_dt)):date('d/m/Y H:i:s', strtotime($record->tran_created));
				?>

			</td>
		</tr>
		 <?php endif; ?>

		<?php if( isset($alert_detail->FROM) AND @$alert_detail->FROM != '' ): ?>
		<tr>
			<td><strong>FROM</strong></td>
			<td>
				 
				<?php 
					$from_email = json_decode($alert_detail->FROM);
					echo $from_email->email;
				?>

			</td>
		</tr>
		<?php endif; ?>

		<?php if( isset($alert_detail->TO) AND @$alert_detail->TO != '' ): ?>
		<tr>
			<td><strong>TO</strong></td>
			<td>							 
				<?php 
					$to_email = json_decode($alert_detail->TO);
					foreach ($to_email as $to) {
						echo $to->email;
					}
				?>
			</td>
		</tr>
		<?php endif; ?>

		<?php if( isset($alert_detail->SUBJECT) AND @$alert_detail->SUBJECT != '' ): ?>
		<tr>
			<td><strong>SUBJECT</strong></td>
			<td><?php echo stripslashes($alert_detail->SUBJECT); ?></td>
		</tr>
		<?php endif; ?>

		<?php if( isset($alert_detail->BODY) ): ?>
		<tr>
			<td colspan="2" class="pt-2">								 				
				<?php //echo $json_data->body; 

					if( is_object($alert_detail->BODY) ){
					 	
					 	echo $this->Commonmodel->array_key_val_to_table(@$alert_detail->BODY);

					}else{
						echo stripslashes($alert_detail->BODY);
					}
				?>
			</td>
		</tr>
		<?php endif; ?>

	</table>

<?php endif; ?>
