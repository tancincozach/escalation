<div class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header py-1">
                <h5 class="modal-title">Set Procedure - <?php echo $record->alert_type; ?> - (Alert#<?php echo $record->ref_number; ?>)</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            <?php echo form_open('callactivity/ajax_set_procedure_save', 'name="callactivity_set_procedure" name="callactivity_set_procedure" class="" role="form" method="post" onsubmit=" return confirm(\'Confirm Procedure?\'); "'); ?>   
                <div class="modal-body"> 
                                    
                    <input type="hidden" name="alert_type" id="ca_client_id" value="<?php echo $record->alert_type; ?>"> 
                    <input type="hidden" name="tran_id" value="<?php echo $record->tran_id; ?>">


                    <div class="form-group row">
                        <label for="reply_from" class="col-sm-4 col-form-label"><?php echo $record->alert_type; ?> Procedure</label>
                        <div class="col-sm-8">
                            
                            <select class="form-control" name="pro_id" required="">
                                <option></option>
                                <?php foreach($procedures as $procedure): ?>
                                    <option value="<?php echo $procedure->pro_id; ?>"><?php echo $procedure->client_name.' | '.$procedure->pro_name; ?></option>
                                <?php endforeach; ?>
                            </select>


                        </div>
                    </div>

                </div>

                <div class="modal-footer py-1">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm">Save</button>
                </div>

            </form>
        </div>
    </div>
</div>