<div class="col-sm-12 mb-3 callactivity">

	

	<div class="row">
		<div class="col-sm-4">
			<h5 class="my-3 float-left">
				Callactivity 
				<?php echo isset($is_view)?'<strong class="text-danger">(View Only)</strong>':''; ?>
			</h5>

			<?php 
				if( isset($is_view) AND $record->tran_status == 1){
					echo '<button class="btn btn-sm btn-primary py-0 float-left mt-3 ml-2" onclick="alert_register.take_control('.$record->tran_id.', \''.$record->ref_number.'\', \''.$record->alert_type.'\')">Take Control</button>';
				}
			?>			

			<?php if( $this->agent_name == $record->agent_name ): ?>
				<button class="ml-1 mt-2 btn btn-danger btn-sm float-left" onclick="callactivity.take_control_off('<?php echo @$record->tran_id; ?>')">
					<i class="fas fa-unlink"></i> <strong>REMOVE Take Control</strong></button>
			<?php endif; ?>			
		</div>
		<div class="col-sm-4 text-center">
			
			<?php if( !isset($_GET['take_control']) ): ?>
				<!-- <button class="btn btn-sm btn-success mt-2 flash-button" onclick="callactivity.onclick_processlog('<?php echo @$record->tran_id; ?>','<?php echo @$cma_settings->cma_id; ?>')">
					Process Log
				</button> -->
			<?php endif; ?>

		</div>
		<div class="col-sm-4 text-right">
			<h5 class="my-3"><?php echo 'Alert #: '.$record->ref_number.' ('.($record->tran_status?'OPEN':'CLOSED').')'; ?></h5>
		</div>
	</div>

	<?php if($this->session->flashdata('fmesg') != ''): ?>
	<div class="fmesg px-2 py-1 alert alert-primary mb-1" role="alert">
		<?php echo $this->session->flashdata('fmesg'); ?>
	</div>
	<?php endif; ?>
 
	<div class="row">
		<div class="col-sm-6">
			<div class="card mb-2">
				<div class="card-header">Client Details</div>
				<div class="card-body bg-light">					 
					<table class="table table-sm table-striped" width="100%">
						<tr>
							<td width="20%" >Client Name</td>
							<td><?php echo $record->client_name; ?></td>
						</tr>
						<tr>
							<td>Alert Type</td>
							<td><?php echo $record->alert_type; ?></td>
						</tr>
						<tr>
							<td>Description</td>
							<td><?php echo $record->brief_description; ?></td>
						</tr>
						<tr>
							<td>Priority Flag</td>
							<td><?php echo @$this->priority_flag[$record->tran_priority]; ?></td>
						</tr>
						<tr>
							<td>Reminders</td>
							<td>na</td>
						</tr>
						<tr>
							<td>Display Screen</td>
							<td><?php echo @$this->display_screen[$record->display_screen]; ?></td>
						</tr>
					</table>
				</div> 
			</div>

			<div class="card bg-light">
				<div class="card-header">Procedures</div>
				<div class="card-body p-0">	

					<?php if( count($procedure) > 0): ?>
						<?php //foreach ($procedures as $procedure): ?>
							<div class="border p-2 m-1 bg-white">
								<h6 class="pb-1 border-bottom"><?php echo stripslashes($procedure->pro_name); ?></h6>
								<?php echo stripslashes($procedure->pro_content); ?>
							</div>
	 
						<?php //endforeach; ?>
					<?php else: ?>
						<div class="text-center p-5">
							<strong class="text-danger">NO PROCEDURE HAS BEEN SET YET, Please contact admin</strong>							
						</div>
						<div class="text-center pb-3">
							<?php if( $record->tran_status == 1 ): ?>
								<button type="button" class="btn btn-sm btn-secondary mr-3" onclick="callactivity.closed_alert(<?php echo $record->tran_id; ?>,'<?php echo $record->ref_number; ?>')">Closed Log</button>
							<?php endif; ?>

							<button type="button" class="btn btn-sm btn-primary ml-3" onclick="callactivity.set_procedure.open_modal('<?php echo $record->alert_type; ?>',<?php echo $record->tran_id; ?>)">Set Procedure</button>
						</div>
					<?php endif; ?>
				</div>			
			</div>
		</div> <!-- end col-sm-6 -->

		<div class="col-sm-6">
			 
			<?php //$jsondata = (object)json_decode($alert_detail); ?>
			
			<div class="card">
				<div class="card-header clearfix">
					
					<h6 class="float-left pt-1 mb-0">Alert Details</h6>

					<?php if($record->alert_type == 'EMAIL'): ?>
						<a href="javascript:void(0)" onclick="callactivity.email_reply.open_modal('forward',<?php echo $record->tran_id; ?>);" class="float-right btn btn-sm">
							<i class="fas fa-arrow-right"> </i>							
							Forward
						</a>

						<a href="javascript:void(0)" onclick="callactivity.email_reply.open_modal('reply',<?php echo $record->tran_id; ?>);" class="float-right btn btn-sm">
							<i class="fas fa-reply"> </i>	
							Reply						
						</a>
					<?php endif; ?>

				</div>
				<div class="card-body bg-light" style="overflow-y: auto"> 
 

					<?php echo $alert_details_content; ?> 


				</div>
			</div>
		</div> <!-- end col-sm-6 -->

	</div> <!-- row -->


	<div class="row">
		<div class="col-sm-12">
			<div class="card mt-2 bg-secondary">
				<div class="card-header text-white">Call Form</div>
				<div class="card-body bg-light">
						
					<?php if( isset($procedure->pro_id) AND @$cma_settings->cma_id != '' AND @$cma_settings->cma_db != '' AND @$cma_settings->cust_id != ''  AND @$cma_settings->contact_id_callsmade != ''  ): ?>
					<div class="col-sm-10 callactivity-form mx-auto p-2">
						
						<?php if( @$procedure->call_lookup_id > 0 ): ?>
						<button class="btn btn-sm btn-success" onclick="callactivity.lookup.popup(<?php echo @$procedure->call_lookup_id; ?>, <?php echo $record->tran_id; ?>, <?php echo isset($is_view)?1:0; ?>)"><i class="fas fa-search"></i> <?php echo $call_lookup->button_name; ?></button>
						<?php endif; ?>

						<?php echo form_open('', 'name="callactivity_form" id="callactivity_form" class="" role="form" method="post" onsubmit=" return callactivity.confirm(this) "'); ?>	
							
							<input type="hidden" name="tran_id" value="<?php echo $record->tran_id; ?>">	
							<input type="hidden" name="client_id" value="<?php echo $record->client_id; ?>">	
							<input type="hidden" name="ref_number" value="<?php echo $record->ref_number; ?>">	
							<input type="hidden" name="cma_id" value="<?php echo @$cma_settings->cma_id; ?>">	
							<input type="hidden" name="cma_db" value="<?php echo @$cma_settings->cma_db; ?>">	
							<input type="hidden" name="cust_id" value="<?php echo @$cma_settings->cust_id; ?>">	
							<input type="hidden" name="contact_id_callsmade" value="<?php echo @$cma_settings->contact_id_callsmade; ?>">	
							<input type="hidden" name="procedure_id" value="<?php echo @$record->procedure_id; ?>">	
							
							<input type="hidden" name="email_output" value="">	
							<input type="hidden" name="to_email" value="">


							<?php if( @$procedure->call_lookup_id > 0 AND empty($record->lookup_data) AND @$procedure->call_lookup_id != ''): ?>
							<?php 
								$email_output = json_decode(@$call_lookup->email_output);
								$email_not_found = isset($email_output->no_result_found)?@$email_output->no_result_found:'csr@welldone.com.au,kellie@welldone.com.au'; 
							?>
							<input type="hidden" id="set_driver_required" name="is_lookup_not_found" value="">
							<input type="hidden" name="email_not_found" value="<?php echo $email_not_found; ?>">
							<input type="hidden" name="set_driver_btn_name" id="set_driver_btn_name" value="<?php echo @$call_lookup->button_name; ?>">
							<?php endif; ?>


							<?php if( !is_numeric($procedure->call_group) OR isset($_GET['call_form_required']) ): ?>

							<div class="row">
								<div class="col-sm-6 float-left">
									<div class="form-group row mb-1">
										<label class="col-sm-3 control-label" for="call_direction">Call Direction</label>
										<div class="col-sm-3">
											<select required="required" class="custom-select custom-select-sm mb-1" name="call_direction" id="call_direction">
												<option></option>
												<option value="1">In</option>
												<option value="0">Out</option> 
											</select>
										</div>
									</div>

									<div class="form-group row mb-1">
										<label class="col-sm-3 control-label" for="caller_name">Name</label>
										<div class="col-sm-8">
											<input type="text" required="required" id="caller_name" name="caller_name" class="form-control form-control-sm" placeholder="Person you are calling OR person calling in">
										</div>
									</div>

									<div class="form-group row mb-1">
										<label class="col-sm-3 control-label" for="caller_phone">Phone</label>
										<div class="col-sm-8">
											<input type="text" required="required" id="caller_phone" name="caller_phone" class="form-control form-control-sm">
										</div>
									</div>

									<div class="form-group row mb-1">
										<label class="col-sm-3 control-label" for="call_response">Call Response</label>
										<div class="col-sm-8"> 
											<select required="required" class="custom-select custom-select-sm mb-1" name="call_res_id" id="sel_call_res_id" >
												<option selected="selected" value=""></option>
												<?php foreach($callresponse as $response): ?>
												<option value="<?php echo $response->call_res_id; ?>"><?php echo stripslashes($response->call_res_text); ?></option>
												<?php endforeach; ?> 
											</select>				
										</div>
									</div>
									 
								</div>
								<div class="col-sm-6 float-right">
									<div class="form-group">
										<strong>Call Notes</strong><br>
										<textarea rows="3" id="call_notes" name="call_notes" class="form-control"></textarea>
									</div>									
								</div>
							</div>
							
							<div class="row clearfix text-center">
								<div class="col-sm-12">									
									<button class="btn btn-sm btn-primary" type="submit" name="submitcallbooking" >	Submit Call </button> 
								</div>
							</div>

							<?php else: ?>
								<input type="hidden" name="call_res_id" value="<?php echo $procedure->call_group; ?>">	
		 						
		 						<div class="text-right" style="margin-right: -75px">
									<a href="callactivity/open/<?php echo $record->tran_id; ?>/?call_form_required=1" class="btn btn-sm btn-primary" >Call Activity</a>
								</div>

								<div class="text-center p-5">
									<strong class="text-danger">No Calls IN or OUT required. You just need to close this escalation when the issue has been handled</strong>							
								</div>

								<div class="row clearfix text-center">
									<div class="col-sm-12">									
										<button class="btn btn-sm btn-primary" type="submit" name="submitcallbooking" >	Submit & Closed </button> 
									</div>
								</div>
							<?php endif; ?>

							<br />

						</form>

						<div id="callactivity_lookup_div">
							<?php if( !empty($record->lookup_data) ){
								$lookup_data = (object)json_decode($record->lookup_data);
								//$this->Commonmodel->array_print_to_html($lookup_data); 
					 			
					 			if( count(@$lookup_data->screen_display) > 0 ) {

						 			$screen_display_table = '<table class="table table-sm  table-bordered" style="width:50%">';
						 			foreach (@$lookup_data->screen_display as $row) {
						 				
						 				$screen_display_table .= '<tr>
											<th style="width: 40%" class="border-top-0">'.stripslashes($row->name).'</th>
											<td class="border-top-0">'.stripslashes($row->display).'</td>
						 				</tr>';
						 			}
						 			$screen_display_table .= '</table>';

					 			}
					 			

								if( count(@$lookup_data->escalation_display) > 0 ) {
						 			$escalation_display_table = '<strong>Escalation</strong>';
						 			$escalation_display_table .= '<table class="table table-sm table-bordered">';
						 			foreach (@$lookup_data->escalation_display as $row) {
						 		 
						 				$escalation_display_table .= '<tr> 
											<td>'.stripslashes($row->esc_name).'</td> 
											<td>'.stripslashes($row->name_field).'</td> 
											<td>'.stripslashes($row->phone_field).'</td> 
											<td>'.stripslashes($row->notes).'</td> 
											<td>
												<button onclick="callactivity.lookup.on_click_escalation(\''.$row->esc_name.'\', \''.$row->phone_field.'\', \''.$row->notes.'\')" class="btn btn-sm btn-success py-0"> 
													<i class="fas fa-copy"></i> 
													Select
												</button>
											</td>
						 				</tr>';

						 			}
						 			$escalation_display_table .= '</table>';
					 			}


					 			echo @$screen_display_table;
					 			echo @$escalation_display_table;

							}
							?>
							
						</div>

						 
					</div> <!-- end callactivity-form -->
					<?php else: ?>
 
						<div class="text-center p-5">
							<strong class="text-danger">Sorry, PROCEDURE is not set. Please contact admin.</strong>
						</div>
					<?php endif; ?>

				</div>
			</div>
		</div>
	</div>


	<div class="row">
		<div class="col-sm-12">
			<div class="card mt-2 bg-secondary">
				<div class="card-header text-white">Call Activity</div>
				<div class="card-body bg-light">
					 
					<table class="table table-sm table-striped table-hover " width="100%">
						<thead>
							<tr>
				                <th style="width: 50px">#</th>
				                <th>Date</th>
				                <th>Time</th>
				                <th>Call Direction</th> 
				                <th>Name</th> 
				                <th>Phone</th>
				                <th>Call Response</th>
				                <th>Call Notes</th> 
				                <th>Status</th>
				                <?php if( $this->agent_name_show ): ?>
				                <th>Agent Name</th>
				            	<?php endif; ?>
				            </tr>
			            </thead>
			            <tbody>
			            	<?php foreach ($calls as $call): ?>
			            	<tr>
			            		<td><?php echo $call->call_id; ?></td>
			            		<td><?php echo date('d/m/Y', strtotime($call->call_created)); ?></td>
			            		<td><?php echo date('H:i', strtotime($call->call_created)); ?></td>
			            		<td><?php echo @$this->call_direction_desc[$call->call_direction]; ?></td> 
			            		<td><?php echo stripslashes($call->caller_name); ?></td>
			            		<td><?php echo stripslashes($call->caller_phone); ?></td>
			            		<td><?php echo stripslashes($call->call_res_text); ?></td>
			            		<td><?php echo stripslashes($call->call_notes); ?></td>
			            		<td><?php echo @$this->tran_status_desc[$call->log_status]; ?></td> 

			            		<?php if( $this->agent_name_show ): ?>
			            			<td><?php echo $call->agent_name; ?></td>
			            		<?php endif; ?>
			            	</tr>
			            	<?php endforeach; ?>
			            </tbody>
					</table>

				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="card mt-2 bg-secondary">
				<div class="card-header text-white">Audit Trail</div>
				<div class="card-body bg-light">
				 
	                <table class="table table-hover table-bordered">
	                    <thead>
	                        <tr>
	                            <th style="width: 50px">#</th>
	                            <th>Log Type</th> 
	                            <th>Date/time</th>
	                            <th>To</th>
	                            <th>Message</th> 
	                            <th>Agent Name</th>  
	                            <th>Return</th>  
	                             
	                        </tr>
	                    </thead>
	                    <tbody>
	                            <?php 
	                                $i=1; foreach($audittrail as $row): 
	                                    /*$date = date('d/m/Y-H:i', strtotime($call->call_created));
	                                    $date = explode('-', $date);*/
	                            ?>
	                                <tr>
	                                    <td><?php echo $i++; ?></td>
	                                    <td><?php echo ucwords($row->audit_type); ?></td>
	                                    <td><?php echo date('d/m/Y H:i',$row->created); ?></td>    
	                                    <td><?php echo $row->audit_to; ?></td>
	                                    <td><?php echo stripslashes($row->message); ?></td>                                      
	                                    <td><?php 
	                                    	$more_info_json = json_decode($row->more_info);
	                                    	echo @$more_info_json->agent_name;
	                                    ?></td>
	                                    <td><?php echo $row->return_message; ?></td>
	                                    
	                                </tr> 
	                            <?php endforeach; ?>                 
	                    </tbody>
	                </table>

				</div>
			</div>
		</div>
	</div>

</div> 