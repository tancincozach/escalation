<div class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header py-1">
                <h5 class="modal-title"><?php echo ucfirst(@$email_action_type); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            <?php echo form_open('callactivity/ajax_replyfoward_send', 'name="callactivity_email_reply" name="callactivity_email_reply" class="" role="form" method="post" onsubmit=" return confirm(\'Confirm sending email?\'); "'); ?>   
                <div class="modal-body"> 
                    <input type="hidden" name="client_id" id="ca_client_id" value="<?php echo $record->client_id; ?>">
                    <input type="hidden" name="email_action_type" value="<?php echo $email_action_type; ?>">
                    <input type="hidden" name="tran_id" value="<?php echo $record->tran_id; ?>">

                    <div class="form-group row">
                        <label for="reply_from" class="col-sm-1 col-form-label">From</label>
                        <div class="col-sm-11">
                            <!-- <input type="text" class="form-control form-control-sm" name="reply_from" value="<?php echo $alert->TO[0]->email; ?>" placeholder="From"> -->
                            <input type="text" class="form-control form-control-sm" name="reply_from" value="<?php echo SYS_EMAIL; ?>" placeholder="From">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="reply_to" class="col-sm-1 col-form-label">To</label>
                        <div class="col-sm-11">
                            <input type="text" class="form-control form-control-sm" name="reply_to" value="<?php echo $alert->FROM->email; ?>" placeholder="From">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="reply_subject" class="col-sm-1 col-form-label">Subject</label>
                        <div class="col-sm-11">
                            <input type="text" class="form-control form-control-sm" name="reply_subject"  value="<?php echo stripslashes($alert->SUBJECT); ?>" placeholder="From">
                        </div>
                    </div>

                    <div class="form-group row">

                    <textarea name="reply_body" id="dynamic_tinymce_text">
                        <?php echo '<br />'?>
                        <?php echo '<br />'?>
                        <?php echo '--'?>
                        <?php echo '<br />'?>
                        <?php echo (($email_action_type=='reply')?'Reply':'Forwarded').' via Escalation System'?>
                        <?php echo '<br />'?>
                        <?php echo '<br />'?>
                        <blockquote>
                        <?php echo stripslashes($alert->BODY); ?>
                        </blockquote>
                    </textarea>

                    </div> 

                </div>

                <div class="modal-footer py-1">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm">Send</button>
                </div>

            </form>
        </div>
    </div>
</div>