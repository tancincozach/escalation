<?php 
		
  
?>


		
<div class="col-sm-12">

	<div class="row justify-content-center">

		<div class="col-sm-8 mb-3 "> 
			<div class="row">
				<div class="col-sm-6">
					<h5 class="my-3 float-left">CMA REMINDER - ACK <?php echo isset($is_view)?'<strong class="text-danger">(View Only)</strong>':''; ?></h5>
				
					<?php 
						if( isset($is_view) AND $record->tran_status == 1){
							echo '<button class="btn btn-sm btn-primary py-0 float-left mt-3 ml-2" onclick="alert_register.take_control('.$record->tran_id.', \''.$record->ref_number.'\', \''.$record->alert_type.'\')">Take Control</button>';
						}
					?>	

					<?php if( $this->agent_name == $record->agent_name ): ?>
						<button class="ml-1 mt-2 btn btn-danger btn-sm float-left" onclick="callactivity.take_control_off('<?php echo @$record->tran_id; ?>')">
							<i class="fas fa-unlink"></i> <strong>REMOVE Take Control</strong></button>
					<?php endif; ?>	
				</div>
				<!-- <div class="col-sm-4"></div> -->
				<div class="col-sm-6 text-right">
					<h5 class="my-3"><?php echo 'Alert #: '.$record->ref_number.' ('.($record->tran_status?'OPEN':'CLOSED').')'; ?></h5>
				</div>
			</div>
			 

			<?php if($this->session->flashdata('fmesg') != ''): ?>
			<div class="row">
				<div class="fmesg px-2 py-1 alert alert-primary mb-1" role="alert">
					<?php echo $this->session->flashdata('fmesg'); ?>
				</div>
			</div>
			<?php endif; ?>
		 

			<div class="row">
				
				<?php echo form_open('', 'name="cma_reminder_form" id="cma_reminder_form" class="col-sm-12" role="form" method="post" onsubmit=" return confirm(\'Confirm Acknowledge Alert?\'); "'); ?>	
					
					<input type="hidden" name="tran_id" value="<?php echo $record->tran_id; ?>">
					<input type="hidden" name="ref_number" value="<?php echo $record->ref_number; ?>">
					<input type="hidden" name="cma_id" value="<?php echo $record->cma_id; ?>">

					<?php						
						$json_obj = json_decode($record->json_data);


						//connect to cma api
				        $customer = $this->Commonmodel->curl_getCustomerData($record->cma_id);
				        $site_id = (isset($json_obj->site_id)?$json_obj->site_id:$this->Commonmodel->get_site_id($record->cma_id));
				        if( isset($customer->customer->data->CustID)){
				        	$cma_customer_link = "http://cma.welldone.com.au/index.php?CustID=".$customer->customer->data->CustID."&site=".$site_id."&pp=";
				        	$cma_customer_link = '<a href="'.$cma_customer_link.'" target="_blank">'.$record->cma_id.'</a>';
				        }

					?>

					<table class="table table-sm">
						<tr>
							<th width="150px">Date/Time SET</th>
							<td><?php echo date('d/m/Y H:i', strtotime($record->tran_created)); ?></td>
						</tr>
						<tr>
							<th>Date/Time DUE</th>
							<td><?php echo date('d/m/Y H:i', strtotime($record->alert_due_dt)); ?></td>
						</tr>
						<tr>
							<th>Customer</th>
							<td><?php echo $record->cust_name; ?></td>
						</tr>
						<tr>
							<th>CMA ID</th>
							<td><?php echo (@$cma_customer_link != '')?$cma_customer_link:$record->cma_id; ?></td>
						</tr>
						
						<?php if( !empty($json_obj->name) ): ?>
						<tr>
							<th>Name</th>
							<td><?php echo $json_obj->name; ?></td>
						</tr>
						<?php endif; ?>

						<?php if( !empty($json_obj->phone) ): ?>
						<tr>
							<th>Phone</th>
							<td><?php echo $json_obj->phone; ?></td>
						</tr>
						<?php endif; ?>

						<tr>
							<th>Message</th>
							<td><?php 							 
								echo str_replace(PHP_EOL, '<br />', stripslashes($json_obj->BODY));
							?></td>
						</tr>

						<tr>
							<th>&nbsp;</th>
							<td>
								<button class="btn btn-sm btn-primary" type="submit" name="submitcallack" > Acknowledge Alert </button> 

							</td>
						</tr>
					</table>
				</form>

			</div>


			<div class="row">
				<div class="col-sm-12">
					<div class="card mt-2  ">
						<div class="card-header  ">Audit Trail</div>
						<div class="card-body  ">
						 
			                <table class="table table-hover table-bordered">
			                    <thead>
			                        <tr>
			                            <th style="width: 50px">#</th>			                            
			                            <th>Date/time</th>			                            
			                            <th>Message</th> 
			                            <th>Agent Name</th>  
			                             
			                        </tr>
			                    </thead>
			                    <tbody>
			                            <?php 
			                                $i=1; foreach($audittrail as $row): 
			                                    /*$date = date('d/m/Y-H:i', strtotime($call->call_created));
			                                    $date = explode('-', $date);*/
			                            ?>
			                                <tr>
			                                    <td><?php echo $i++; ?></td>
			                                     
			                                    <td><?php echo date('d/m/Y H:i',$row->created); ?></td>			                                   
			                                    <td><?php echo stripslashes($row->message); ?></td>                                      
			                                    <td><?php 
				                                    	$more_info = json_decode($row->more_info); 				                                    	 
				                                    	echo $more_info->agent_name;
			                                    ?></td>
			                                    
			                                </tr> 
			                            <?php endforeach; ?>                 
			                    </tbody>
			                </table>

						</div>
					</div>
				</div>
			</div>

		</div>

	</div>
</div>