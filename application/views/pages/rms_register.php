
<div class="col-sm-12">
	
	<h5 class="my-3">Pinpoint Raw Data</h5>

	<table class="table table-bordered table-hover table-sm">
		<thead class="table-primary">
			<tr>
				<th scope="col">#</th> 
				<th scope="col">DATA</th> 
				<th scope="col">Date/Time</th> 
				<th scope="col">Method</th> 
				<th scope="col">Options</th> 
			</tr>
		</thead>
		<tbody>

			<?php foreach( $results as $row ): ?>
			<tr>
				<td></td>
				<td>
				<?php //echo $row->json_data; 
					if( is_string($row->json_data) && is_array(json_decode($row->json_data, true)) ){
						/*echo '<pre>';
						print_r(json_decode($row->json_data));
						echo '</pre>';*/
						echo $this->Commonmodel->array_key_val_to_p(json_decode($row->json_data));
					}else{
						echo $row->json_data; 
					}
				?>					
				</td>
				<td><?php echo $row->create_at; ?></td> 
				<td><?php echo $row->method_type; ?></td> 
				<td class="text-right">					
					
				</td>
			</tr>
			<?php endforeach; ?>

		</tbody>
	</table>

    <div class="row">
        <div class="col-sm-6 justify-content-start">
             <?php echo $showing; ?>
        </div>
        <div class="col-sm-6 d-flex justify-content-end">            
            <?php echo $links; ?>
        </div> 
    </div>

</div>