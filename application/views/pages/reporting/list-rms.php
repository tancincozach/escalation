<div class="col-sm-12">

	<div class="row clearfix justify-content-center">

		<div class="d-flex justify-content-center">
			
			<form class="form-inline">

				<div class="row d-flex justify-content-center">
				     
			        <div class="form-group mr-1">
			           <div class="input-group input-group-sm date my-1" id="datetimepicker_from" data-target-input="nearest">
						  	<div class="input-group-prepend">
						    	<span class="input-group-text"> Date/Time From</span>
						  	</div>				           
			                <input type="text" name="dp_from" class="form-control form-control-sm datetimepicker-input" data-target="#datetimepicker_from" required="" value="<?php echo @$filters['dp_from']; ?>" style="width: 100px !important;"/>
			                <div class="input-group-append" data-target="#datetimepicker_from" data-toggle="datetimepicker">
			                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
			                </div>
			            </div>
			        </div>
			     
			        <div class="form-group mr-1">
			           <div class="input-group input-group-sm date my-1" id="datetimepicker_to" data-target-input="nearest">
						  	<div class="input-group-prepend">
						    	<span class="input-group-text">To</span>
						  	</div>				           
			                <input type="text" name="dp_to" class="form-control form-control-sm datetimepicker-input" data-target="#datetimepicker_to" required="" value="<?php echo @$filters['dp_to']; ?>" style="width: 100px !important;"/>
			                <div class="input-group-append" data-target="#datetimepicker_to" data-toggle="datetimepicker">
			                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
			                </div>
			            </div>
			        </div>

				  	<?php if( $this->client_id == 0 ): ?>
				  	<div class="form-group mr-1"> 
				  		<div class="input-group input-group-sm">
				  		    <div class="input-group-prepend">
								<div class="input-group-text">
									<i class="fa fa-users" aria-hidden="true"></i>
								</div>
						    </div>
 
							<?php echo form_dropdown('client_id', $clients, @$filters['client_id'], ' class="custom-select custom-select-sm" style="width: 150px !important" '); ?>
							 
						</div>	

					</div>
					<?php endif; ?>

				  	<div class="form-group mr-1">
				  		<div class="input-group input-group-sm">
				  		    <div class="input-group-prepend">
								<div class="input-group-text">
									<i class="fas fa-bell" aria-hidden="true"></i>
								</div>
						    </div>
							<select class="custom-select custom-select-sm" name="alert_type" style="width: 70px !important">
								<option value="" <?php echo (@$filters['alert_type'])==''?'selected':''; ?> >-All-</option>
								<option value="API" <?php echo (@$filters['alert_type'])=='API'?'selected':''; ?> >API</option>				
								<option value="EMAIL" <?php echo (@$filters['alert_type'])=='EMAIL'?'selected':''; ?> >EMAIL</option>				
								<option value="REMINDER" <?php echo (@$filters['alert_type'])=='REMINDER'?'selected':''; ?> >REMINDER</option>				
								<option value="CMA REMINDER" <?php echo (@$filters['alert_type'])=='CMA REMINDER'?'selected':''; ?> >CMA REMINDER</option>				
							</select>
						</div>	
					</div>

				    <div class="form-group">
						<button type="submit" class="btn btn-primary btn-sm"> <i class="fas fa-search"></i> Filter</button>
						<a class="btn btn-secondary btn-sm ml-1" href="reporting" role="button"> <i class="fas fa-sync "></i> Reset</a>
						<button type="submit" name="btn_export" class="btn btn-warning btn-sm ml-1 "> <i class="fas fa-download "></i> Export</button>
				    </div>
				</div>
			</form> <!-- end filter -->


		</div>
	</div> <!-- end ow clearfix justify-content-center -->

	<div class="clearfix">
		
		<?php 
		 
			$this->db->where('client_id', $client_id);
			$this->db->where(" table_desc IN('api data', 'api storage') ");
			$api_storage = $this->db->get('client_custom_table');
			$api_storage = @$api_storage->row()->table_name;


		?>
		<table class="table table-bordered table-hover table-sm">
			<thead class="table-primary">
				<tr>
	   
					<th scope="col">Client</th>
					<th scope="col">Alert #</th>
					<th scope="col">Alert Type</th>   
					<th scope="col">Date</th>   
					<th scope="col">Time START & 1st call</th>   
					<?php 

						$custom_table_fields = $this->Clientcustomtable->get_table_fields($api_storage, 'array');
						unset($custom_table_fields['id']);
						unset($custom_table_fields['tran_id']);
						unset($custom_table_fields['created_dt']);

						foreach($custom_table_fields as $val):
					?>
					<th scope="col"><?php echo $val; ?></th>  
					<?php endforeach; ?>
				</tr>
			</thead>
			<tbody>

	 

				<?php foreach( $results as $row ): 
	 

				?>
				<tr>
					<td><?php echo $row->client_name; ?></td>
					<td><?php echo $row->ref_number; ?></td>
					<td><?php echo $row->alert_type; ?></td>
					<td><?php echo $row->alert_created_dt; ?></td>
					<td>
					<?php  

						$call = $this->db->query("SELECT call_created FROM calls WHERE tran_id = $row->tran_id ORDER BY call_created ASC LIMIT 1 ")->row();

						echo isset($call->call_created)?$this->Commonmodel->timestampdiff($row->alert_created_dt, $call->call_created):''; 

					?>
					</td>
						 
					<?php  

						$cs_2_alert_param['where']['tran_id'] = $row->tran_id;
						$cs_2_alert_param['sorting'] = ' id ASC '; 
						$cs_2_alert_row = $this->Clientcustomtable->get_table_row($api_storage, $cs_2_alert_param, 'row_array');
						unset($cs_2_alert_row['id']);
						unset($cs_2_alert_row['tran_id']);
						unset($cs_2_alert_row['created_dt']);
						
						// echo '<pre>';
						// print_r($cs_2_alert_row);				
						// echo '</pre>';
						if( count($cs_2_alert_row) > 0 ):
						foreach($custom_table_fields as $field):
					?>
					<td>
						<?php echo @$cs_2_alert_row[$field]; ?>
					</td>
				 	<?php endforeach; endif; ?>			
					 
				</tr>
				<?php endforeach; ?>

			</tbody>
		</table>

	    <div class="row">
	        <div class="col-sm-6 justify-content-start">
	             <?php echo $showing; ?>
	        </div>
	        <div class="col-sm-6 d-flex justify-content-end">            
	            <?php echo $links; ?>
	        </div> 
	    </div>

	</div> <!-- end .row -->


</div>