<div class="col-sm-12">

	<div class="row clearfix justify-content-center">

		<div class="d-flex justify-content-center">
			
			<form class="form-inline">

				<div class="row d-flex justify-content-center">
				     
			        <div class="form-group mr-1">
			           <div class="input-group input-group-sm date my-1" id="datetimepicker_from" data-target-input="nearest">
						  	<div class="input-group-prepend">
						    	<span class="input-group-text"> Date/Time From</span>
						  	</div>				           
			                <input type="text" name="dp_from" class="form-control form-control-sm datetimepicker-input" data-target="#datetimepicker_from" required="" value="<?php echo @$filters['dp_from']; ?>" style="width: 100px !important;"/>
			                <div class="input-group-append" data-target="#datetimepicker_from" data-toggle="datetimepicker">
			                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
			                </div>
			            </div>
			        </div>
			     
			        <div class="form-group mr-1">
			           <div class="input-group input-group-sm date my-1" id="datetimepicker_to" data-target-input="nearest">
						  	<div class="input-group-prepend">
						    	<span class="input-group-text">To</span>
						  	</div>				           
			                <input type="text" name="dp_to" class="form-control form-control-sm datetimepicker-input" data-target="#datetimepicker_to" required="" value="<?php echo @$filters['dp_to']; ?>" style="width: 100px !important;"/>
			                <div class="input-group-append" data-target="#datetimepicker_to" data-toggle="datetimepicker">
			                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
			                </div>
			            </div>
			        </div>

				  	<?php if( $this->client_id == 0 ): ?>
				  	<div class="form-group mr-1"> 
				  		<div class="input-group input-group-sm">
				  		    <div class="input-group-prepend">
								<div class="input-group-text">
									<i class="fa fa-users" aria-hidden="true"></i>
								</div>
						    </div>
 
							<?php echo form_dropdown('client_id', $clients, @$filters['client_id'], ' class="custom-select custom-select-sm" style="width: 150px !important" '); ?>
							 
						</div>	

					</div>
					<?php endif; ?>

				  	<div class="form-group mr-1">
				  		<div class="input-group input-group-sm">
				  		    <div class="input-group-prepend">
								<div class="input-group-text">
									<i class="fas fa-bell" aria-hidden="true"></i>
								</div>
						    </div>
							<select class="custom-select custom-select-sm" name="alert_type" style="width: 70px !important">
								<option value="" <?php echo (@$filters['alert_type'])==''?'selected':''; ?> >-All-</option>
								<option value="API" <?php echo (@$filters['alert_type'])=='API'?'selected':''; ?> >API</option>				
								<option value="EMAIL" <?php echo (@$filters['alert_type'])=='EMAIL'?'selected':''; ?> >EMAIL</option>				
								<option value="REMINDER" <?php echo (@$filters['alert_type'])=='REMINDER'?'selected':''; ?> >REMINDER</option>				
								<option value="CMA REMINDER" <?php echo (@$filters['alert_type'])=='CMA REMINDER'?'selected':''; ?> >CMA REMINDER</option>				
							</select>
						</div>	
					</div>

				    <div class="form-group">
						<button type="submit" class="btn btn-primary btn-sm"> <i class="fas fa-search"></i> Filter</button>
						<a class="btn btn-secondary btn-sm ml-1" href="reporting" role="button"> <i class="fas fa-sync "></i> Reset</a>
						<button type="submit" name="btn_export" class="btn btn-warning btn-sm ml-1 "> <i class="fas fa-download "></i> Export</button>
				    </div>
				</div>
			</form> <!-- end filter -->


		</div>
	</div> <!-- end ow clearfix justify-content-center -->

	<div class="clearfix">
		
		<table class="table table-bordered table-hover table-sm">
			<thead class="table-primary">
				<tr>
	 
					<th scope="col">Client</th>  
					<th scope="col">Alert #</th>  
					<th scope="col">CMA ID</th>  
					<th scope="col">Alert Type (EMAIL/SMS/API etc)</th>  
					<th scope="col">Description</th>  
					<th scope="col">Priority FLAG</th>    
					<th scope="col">Display</th>  
					<th scope="col" style="width: 130px">Date SET</th>
					<th scope="col" style="width: 130px">Time SET</th>

					<th scope="col" style="width: 130px">Date DUE</th>  
					<th scope="col" style="width: 130px">Time DUE</th> 

					<th scope="col" style="width: 130px">Date Take Control</th>
					<th scope="col" style="width: 130px">Time Take Control</th>

					<th scope="col" style="width: 130px">Date CLOSE</th>
					<th scope="col" style="width: 130px">Time CLOSE</th>
					<th scope="col" >Time between DUE & TAKE CONTROL (in minute)</th>   
					<th scope="col" >Time between TAKE CONTROL & CLOSE (in minute)</th>   
					<th scope="col">Status</th>   
				</tr>
			</thead>
			<tbody>

	 

				<?php foreach( $results as $row ): 
	 

				?>
				<tr>
					 
					<td><?php echo $row->alert_type == 'CMA REMINDER'?$row->cust_name:$row->client_name; ?></td>
					<td><?php echo $row->ref_number; 
					?></td>
					<td><?php echo $row->cma_id; ?></td>
					<td><?php echo $row->alert_type; ?></td>
					<td>
						<?php if( $row->alert_type == 'CMA REMINDER' ): ?>					
							 CMA / APP reminder activated - needs processing 
						<?php else: ?>  
							<?php echo $row->brief_description; ?>  
						<?php endif; ?>

					</td>
					<td><?php echo @$this->priority_flag[$row->tran_priority]; ?></td>
					<td><?php echo @$this->display_screen[$row->display_screen]; ?></td>  

					<td><?php echo (!in_array($row->alert_custom_dt, array('', '0000-00-00 00:00:00')))?date('d/m/Y', strtotime($row->alert_custom_dt)):''; ?></td>
					<td><?php echo (!in_array($row->alert_custom_dt, array('', '0000-00-00 00:00:00')))?date('H:i:s', strtotime($row->alert_custom_dt)):''; ?></td>

					<td class="bg-light"><?php echo (!in_array($row->alert_due_custom_dt, array('', '0000-00-00 00:00:00')))?date('d/m/Y', strtotime($row->alert_due_custom_dt)):''; ?></td>
					<td class="bg-light"><?php echo (!in_array($row->alert_due_custom_dt, array('', '0000-00-00 00:00:00')))?date('H:i', strtotime($row->alert_due_custom_dt)):''; ?></td>
	 
					<td><?php echo (!in_array($row->call_hand_start, array('', '0000-00-00 00:00:00')))?date('d/m/Y', strtotime($row->call_hand_start)):''; ?></td>
					<td><?php echo (!in_array($row->call_hand_start, array('', '0000-00-00 00:00:00')))?date('H:i:s', strtotime($row->call_hand_start)):''; ?></td>
	 
					<td class="bg-light"><?php echo (!in_array($row->call_hand_end, array('', '0000-00-00 00:00:00')))?date('d/m/Y', strtotime($row->call_hand_end)):''; ?></td>
					<td class="bg-light"><?php echo (!in_array($row->call_hand_end, array('', '0000-00-00 00:00:00')))?date('H:i:s', strtotime($row->call_hand_end)):''; ?></td>
	 
					<td><?php echo @$row->calculate1; ?></td>  				
					<td><?php echo @$row->calculate2; ?></td>  				
					<td><?php echo @$this->tran_status_desc[$row->tran_status]; ?></td>  				
					 
				</tr>
				<?php endforeach; ?>

			</tbody>
		</table>

	    <div class="row">
	        <div class="col-sm-6 justify-content-start">
	             <?php echo $showing; ?>
	        </div>
	        <div class="col-sm-6 d-flex justify-content-end">            
	            <?php echo $links; ?>
	        </div> 
	    </div>

	</div> <!-- end .row -->


</div>