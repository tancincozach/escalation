<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- <link rel="icon" href="../../../../favicon.ico"> -->

        <?php if( in_array($this->uri->segment(1), array('register', 'cma_reminder'))): ?>
        <meta http-equiv="refresh" content="30">
        <?php endif; ?>

        <title><?php echo HEADER_TITLE; ?></title>

        <base href="<?php echo base_url(); ?>" />

        <!-- Bootstrap core CSS -->
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">
        <!-- <link href="assets/vendor/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" rel="stylesheet"> -->
        <link href="assets/vendor/bootstrap-datetimepicker/tempusdominus/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome-free-5.1.1/css/all.min.css" rel="stylesheet">
        <link href="assets/css/styles.css?v=<?php echo CSS_VER_STYLES; ?>" rel="stylesheet">

        <?php echo @$css_file; ?>

        <script type="text/javascript">
            var apps_base_url = '<?php echo base_url(); ?>';

            var escalation_global = {
                n:'<?php echo $this->security->get_csrf_token_name(); ?>',
                h:'<?php echo $this->security->get_csrf_hash(); ?>',
                agent_name:"<?php echo $this->agent_name;?>",
                user_id:"<?php echo $this->user_id;?>",
                cookie_name:"<?php echo $this->config->item('sess_cookie_name'); ?>",
                base_url: '<?php echo base_url(); ?>'
            } 


        </script>

    </head>

    <body>

    	<div class="container-fluid">
    		<header class="apps-header py-3">
    			<div class="row flex-nowrap align-items-center">
    				<div class="col-4 pt-1">
    					<!-- LEFT HEADER CONTENT -->
    				</div>
    				<div class="col-4 text-center">
    					ESCALATION SYSTEM
    				</div>
    				<div class="col-4 d-flex justify-content-end align-items-center">
    					<!-- RIGHT HEADER CONTENT -->
    				</div>
    			</div>
    		</header>


            <div class="row justify-content-center apps-top-menu">
                
                <div class="nav-scroller bg-white box-shadow">
                  <nav class="nav nav-underline">
                   <!--  <a class="nav-link" href="#">
                      Friends
                      <span class="badge badge-pill bg-light align-text-bottom">27</span>
                    </a> -->
                    
                    <!-- <a class="nav-link <?php echo (@$menu_active=='dashboard')?'active':''; ?> " href="dashboard">Dashboard</a> -->

                    <a class="nav-link <?php echo (@$menu_active=='register')?'active':''; ?>" href="register">Register</a> 
                    <a class="nav-link <?php echo (@$menu_active=='cma_reminder')?'active':''; ?>" href="register/cma_reminder">CMA Reminder</a> 
                    <a class="nav-link <?php echo (@$menu_active=='send_email')?'active':''; ?>" href="send_email">Send Email</a> 
                    
                    <!-- <a class="nav-link <?php echo (@$menu_active=='rmsregister')?'active':''; ?>" href="rms">Pinpoint Raw Data</a> -->
                    
                    <!-- <a class="nav-link <?php echo (@$menu_active=='emailregister')?'active':''; ?> " href="emailregister">Email Register</a>
                    <a class="nav-link <?php echo (@$menu_active=='smsregister')?'active':''; ?>" href="dashboard/smsregister">SMS Register</a>  -->

                   <!--  <a class="nav-link" href="#">
                      Sample
                      <span class="badge badge-pill bg-warning align-text-bottom">27</span>
                    </a> -->

                    <?php if($this->user_lvl > 0): ?>
                        
                        <a class="nav-link <?php echo (@$menu_active=='reporting')?'active':''; ?>" href="reporting">Reporting</a> 
                   
                        <a class="nav-link <?php echo (@$menu_active=='maintenance')?'active':''; ?>" href="maintenance">Maintenance</a> 

                    <?php endif; ?>

                  </nav>
                </div>

            </div>

        </div>

        <main role="main" class="container-fluid">
  
            <div class="row border">
                
                <?php $this->load->view($view_file, @$data); ?>

            </div><!-- /.row -->

        </main><!-- /.container -->

        <footer class="apps-footer">
            <p><?php echo FOOTER; ?></p>
            <p><a href="logout" onclick="return confirm('Confirm Logout');">Logout</a></p>
            <br />
            <p><a href="#">Back to top</a></p>
        </footer>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
 
        <script src="assets/vendor/jquery/jquery.min.js"></script>
        <!-- <script src="assets/vendor/popper.min.js"></script> -->
        <!-- <script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script> -->
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/vendor/holder.min.js"></script>
        
        <!-- <script src="assets/vendor/moment/2.10.6/moment.min.js"></script> -->
        <script src="assets/vendor/moment/2.21.0/moment.min.js"></script>

        <!-- <script src="assets/vendor/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script> -->
        <script src="assets/vendor/bootstrap-datetimepicker/tempusdominus/js/tempusdominus-bootstrap-4.min.js"></script>        

        <script src="assets/js/apps.js?v=<?php echo JS_VER_APPS; ?>"></script>
        <?php echo @$js_file; ?>
    </body>
</html>