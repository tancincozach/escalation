
<h5>Call Response</h5>


<?php if($this->session->flashdata('msg') != ''): ?>
<div class="fmesg px-2 py-1 alert alert-primary mb-1" role="alert">
	<?php echo $this->session->flashdata('msg'); ?>
</div>
<?php endif; ?>
<?php if($this->session->flashdata('error') != ''): ?>
<div class="fmesg px-2 py-1 alert alert-primary mb-1" role="alert">
	<?php echo $this->session->flashdata('error'); ?>
</div>
<?php endif; ?>

<div class="row">
	
	<div class="col-sm-2">


		<div class="input-group mb-3">
				<?php if(isset($client_created)):?>
				 <input type="hidden" name="created_client" value="<?php echo htmlentities(json_encode(@$client_created));?>"/>
				<?php endif;?>
			  <input type="text" id="client_search" onkeyup="escalation.common.search_text(this,'.client_tbl');" class="form-control input-sm"  placeholder="Search for client.." aria-label="Search for client.." aria-describedby="basic-addon2" value="<?php echo @$client_created->client_name;?>">
			  <div class="input-group-append">
			    <button class="btn btn-outline-danger btn-sm" type="button" onclick="escalation.common.clear_search(this,'.client_tbl');"><i class="far fa-times-circle "></i></button>
			  </div>
			</div>

			<table class="client_tbl table table-sm ">
			  <thead>	  
			    <tr>
			      <th scope="col" class="table-primary" >CLIENT
			      </th>	      
			    </tr>
			  </thead>
			  <tbody>

			    <tr>
			    	<td>
			    	DEFAULT
			    		<a href="maintenance/callresponse/?client=all"  class="btn btn-info btn-sm py-0 float-right"    >
				  				<i class="far fa-check-square"></i>&nbsp;
				  				
				  			</a>
			    	</td>
			    </tr>
				
				<?php foreach($clients as $row): ?>

			    <tr>
			      
			      <td><?php if( $this->client_id == 0 ): ?>
				  			<label><?php echo $row->client_name; ?></label>
				  			<a href="maintenance/callresponse/?client_id=<?php echo $row->client_id; ?>"  class="btn btn-info btn-sm py-0 float-right"  >
				  				<i class="far fa-check-square"></i>&nbsp;
				  				
				  			</a>
				  		<?php else: ?>

				  			<label><?php echo $row->client_name; ?></label>

				  			<a href="maintenance/callresponse/?client_id=<?php echo $row->client_id; ?>"  class="btn btn-info btn-sm py-0 float-right"  alt="<?php echo $this->client_id?>:<?php echo $row->client_name?>" >
					  			<i class="far fa-check-square "></i>&nbsp;
				  				
				  			</a>
				  		<?php endif; ?></td>
			      
			    </tr>
			
				<?php endforeach;?>		

			  </tbody>
			</table>
	</div>
	<div class="col-sm-10">	

		<div class="row">
			<div class="col-sm-6">
				
					<a class="btn btn-sm btn-success py-0" href="maintenance/call_response_form/add/<?php echo (isset($client) && $client->client_id > 0)?'?client_id='.$client->client_id:''; ?>"><i class="fas fa-plus-circle"></i> New</a>
				
			</div>
			<div class="col-sm-6 text-right">
				<h5><?php echo @$client->client_name; ?></h5>
			</div> 
		</div>

		<style>
			
			#callresponse-table > tbody >  tr {

				cursor:pointer;
			}
		</style>

		<?php if( isset($call_response) AND count($call_response) > 0 ): ?>
			<table id="callresponse-table" class="table table-bordered table-hover table-sm" >
		<thead class="table-primary">
			<tr>
				<th scope="col">Response</th> 
				<th scope="col">Group</th> 
				<th scope="col">Client</th> 
				<th scope="col">Log Status</th> 
				<th scope="col">Status</th> 
				<th scope="col">Date/Time</th> 				
				<th scope="col">Options</th> 
			</tr>
		</thead>
		<tbody>
		<?php 

		 if(isset($call_response) && count($call_response) > 0):	
			foreach($call_response as $row):
			?>
				 <tr id="callresitem-<?php echo @$row->call_res_id;?>">
				 	<td><?php echo $row->call_res_text ?></td>
				 	<td><?php echo $row->call_group ?></td>
				 	<td><?php echo ($row->client_name!='' ? $row->client_name:'DEFAULT');?></td>
				 	<td><?php 
				 			switch ((int)$row->call_res_open) {
				 				case 0:
				 						echo 'Closed';
				 					break;
				 				case 1:
					 					echo 'Open';
				 					break;
				 				case 2:
				 				    	echo 'Call Update';
				 					break;
				 				
				 				default:
										echo '';
				 					break;
				 			}
				 	?></td>
				 	<td><?php echo (@$row->call_res_status==1) ? 'Active':'Inactive';?></td>
				 	<td><?php echo date('Y-m-d H:i:s',strtotime($row->call_res_created));?></td>
				 	<td>
			 		<?php if($this->client_id == 0 || ($this->client_id > 0 && $this->client_id==@$row->client_id)):?>				 		
					 	<a href="maintenance/call_response_form/edit/?cr_id=<?php echo $row->call_res_id;?>"/><i class="fas fa-edit"></i>Update</a>&nbsp;&nbsp;
<!-- 					 	<a onclick="return escalation.common.confirm('Are you sure you want to delete ?');" href="maintenance/callresponse/delete/?client_id=<?php //echo $row->client_id; ?>&cr_id=<?php //echo $row->call_res_id;?>"/><i class="fas fa-trash-alt"></i>Delete</a>					 	 -->
				 	<?php elseif($this->client_id > 0 && $client_all==true):?>

					 <?php endif;?>
					</td>
				 </tr>

			 <?php 

			 endforeach;
			else:
			?>
			<tr>
				<td colspan="6">No Call Response Found.</td>
			</tr>
			<?php
			endif; 
		?>
		</tbody>
	</table>


		<?php endif; ?>
	</div>
</div>