<div class="col-sm-12">
	

<div class="row border mt-1 mb-1">
	<div class="col-sm-4"><h5> Call Response - FORM </h5></div>
	<div class="col-sm-4"><h5><?php echo strtoupper($form_type); ?></h5></div>
	<div class="col-sm-4"><h5><?php echo @$client_row->client_name; ?></h5></div>
</div>
<?php


?>
<?php echo form_open('','method="post" class="form-horizontal"'); ?>

	<input type="hidden" name="form_type" value="<?php echo @$form_type; ?>">	 
	<input type="hidden" name="cr_id" value="<?php echo @$call_response_row->call_res_id; ?>">	 
		<input type="hidden" name="client_id" value="<?php echo @$client_row->client_id; ?>">	
	  <div class="form-group row mb-1">
	    <label for="formGroupExampleInput" class="col-sm-2 col-form-label">Name :</label>
        <div class="col-sm-4">
	    	<input type="text" class="form-control" id="call_response_name" name="call_response_name" placeholder="Call Response Name" value="<?php echo @$call_response_row->call_res_text?>" required>
	    </div>
	  </div>

		


  	   <div class="form-group row mb-1">
	    <label for="formGroupExampleInput2" class="col-sm-2 col-form-label">Log Status :</label>
	    <div class="col-sm-4">
		    <select name="log_status" class="custom-select">	
		    <?php foreach($tran_status_desc as $key=> $log_val):?>						    	
		    	<option value="<?php echo $key;?>" <?php echo (isset($call_response_row) &&  $call_response_row->call_res_open==$key) ? 'SELECTED':''?>><?php echo $log_val;?></option>
		    <?php endforeach;?>						    		
		    </select>
	    </div>
  	  </div>


  	   <div class="form-group row mb-1">
	    <label for="formGroupExampleInput2" class="col-sm-2 col-form-label">Group :</label>
	    <div class="col-sm-4">

		   <div class="input-group">
			      <input type="text" class="form-control" name="group_name" value="<?php echo  isset($call_response_row->call_group) ? $call_response_row->call_group:'GROUP 1';?>" id="group" aria-label="Text input with dropdown button">
			      <div class="input-group-btn">
			        <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
			        <div class="dropdown-menu dropdown-menu-right">
			        <?php if(count($group_name) > 0 ):
			        		foreach($group_name as $row):
			        	?>
			          <a class="dropdown-item group_dropdown" href="#"><?php echo $row->call_group?></a>
			          <div role="separator" class="dropdown-divider"></div>
		           <?php 	endforeach;
		           		  endif;
			          ?>
			          								          
			        </div>
			      </div>
		    </div>
		    Must be a string
	    </div>
  	  </div>

		<div class="form-group row mb-1">
			<label for="formGroupExampleInput2" class="col-sm-2 col-form-label">Call Form required</label>
			<div class="col-sm-4">
				<select name="call_form_required" class="custom-select">							    	
					<option value="1" <?php echo (isset($call_response_row) && $call_response_row->call_form_required==1) ? 'SELECTED':''?>>YES</option>
					<option value="0" <?php echo (isset($call_response_row) && $call_response_row->call_form_required==0) ? 'SELECTED':''?>>NO</option>
				</select>

				"YES" - Call Form show, Logs CALL IN/OUT.<br />
				"NO" - Use submit and close form. Group will be cleared<br />

			</div>
		</div>

		<div class="form-group row mb-1">
			<label for="formGroupExampleInput2" class="col-sm-2 col-form-label">Status :</label>
			<div class="col-sm-4">
				<select name="call_response_status" class="custom-select">							    	
					<option value="1" <?php echo (isset($call_response_row) && $call_response_row->call_res_status==1) ? 'SELECTED':''?>>Active</option>
					<option value="0" <?php echo (isset($call_response_row) && $call_response_row->call_res_status==0) ? 'SELECTED':''?>>Inactive</option>
				</select>
			</div>
		</div>

  	  	<div class="form-group">
  	  		<button class="btn btn-primary btn-sm">Submit</button>
  	  		<a href="javascript:history.back();" class="btn btn-info btn-sm">Back</a>
  	  </div>
<?php echo form_close(); ?>


</div>