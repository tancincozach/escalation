
<div class="col-sm-2 border-right">

    <?php echo $this->load->view('maintenance/menu', array('menu_active'=>$maintain['menu_active']), true); ?>

</div>

<div class="col-sm-10 maintain">
    <?php echo $this->load->view($maintain['view_file'], $maintain['data'], true); ?>
</div>