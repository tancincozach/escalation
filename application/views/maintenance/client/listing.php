
<h5>Client</h5>


<?php if($this->session->flashdata('fmesg') != ''): ?>
<div class="fmesg px-2 py-1 alert alert-primary mb-1" role="alert">
	<?php echo $this->session->flashdata('fmesg'); ?>
</div>
<?php endif; ?>

<div class="row">
	
	 
	<div class="col-sm-2 pb-1">

		<?php if( $this->client_id == 0 ): ?>
			<a class="btn btn-sm btn-success" href="maintenance/client_form/add/">New Client</a>
		<?php endif; ?>		

	</div>

	<div class="col-sm-12">	 
		<?php if( isset($results) AND count($results) > 0 ): ?>
			<table class="table table-sm table-striped table-bordered">
				<thead> 
					<tr>
						<th>Name</th>
		 
						<th style="width: 150px;">Created</th>
						<th style="width: 150px;">Last Update</th>
						<th style="width: 100px;">Status</th>
						<th style="width: 350px"></th>
						<th style="width: 75px;"></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach( $results as $client): ?>
						<tr>
							<td><?php echo stripslashes($client->client_name); ?></td>
				 
							<td><?php echo date('d/m/Y H:i:s', strtotime($client->created_at)); ?></td>
							<td><?php echo (!in_array($client->updated_at, array('', '0000-00-00 00:00:00')))?date('d/m/Y H:i:s', strtotime($client->updated_at)):''; ?></td>
							<td><?php echo ($client->client_status)?'<span class="text-success">Active</span>':'<span class="text-muted">Inactive</span>'; ?></td>
							<td>
								<a href="maintenance/procedures/<?php echo ($this->client_id == 0)?'?client_id='.$client->client_id:''; ?>" class=" px-1" title="Manage Procedure">									 
									<i class="fas fa-tasks"></i>
									Procedure
								</a>
								<a href="maintenance/callresponse/<?php echo ($this->client_id == 0)?'?client_id='.$client->client_id:''; ?>" class=" px-1" title="Manage Call Response">									 
									<i class="fas fa-phone-square"></i>
									Call Response
								</a>
								<a href="maintenance/file_manager/<?php echo ($this->client_id == 0)?'?client_id='.$client->client_id:''; ?>" class=" px-1" title="Manage Files">									 
									<i class="fas fa-folder-open"></i>
									File Manager
								</a>
							</td>
							<td>
								 <a href="maintenance/client_form/update/?<?php echo ($this->client_id == 0)?'client_id='.$client->client_id:''; ?>" title="Edit">
								 	<i class="fas fa-edit"></i>
								 	<!-- Edit -->
								 </a>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>

		    <div class="row">
		        <div class="col-sm-6 justify-content-start">
		             <?php echo $showing; ?>
		        </div>
		        <div class="col-sm-6 d-flex justify-content-end">            
		            <?php echo $links; ?>
		        </div> 
		    </div>
		<?php endif; ?>
	</div>

</div>