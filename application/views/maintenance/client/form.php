
 
<div class="row border mt-1 mb-1">
	<div class="col-sm-4"><h5> Client - Form </h5></div>
	<div class="col-sm-4"><h5><?php echo strtoupper($form_type); ?></h5></div>
	<div class="col-sm-4"><h5><?php echo @$clientobj->client_name; ?></h5></div>
</div>
 

<?php if($this->session->flashdata('fmesg') != ''): ?>
<div class="fmesg px-2 py-1 alert alert-primary mb-1" role="alert">
	<?php echo $this->session->flashdata('fmesg'); ?>
</div>
<?php endif; ?>

<div class="col-lg-10 col-sm-12">
	<?php echo form_open('', 'class="" role="form" method="post" onsubmit=" return confirm(\'Confirm Saving Client?\'); "'); ?>	

	<input type="hidden" name="client_id" value="<?php echo @$clientobj->client_id; ?>">	 
	<input type="hidden" name="form_type" value="<?php echo @$form_type; ?>">	 


	<div class="form-group row mb-1">
		<label class="col-lg-2 col-sm-4 control-label" for="client_name">Name</label>
		<div class="col-lg-4 col-sm-8">
			<input type="text" required="required" id="client_name" name="client_name" maxlength="100" class="form-control form-control-sm" value="<?php echo stripslashes(@$clientobj->client_name); ?>">
		</div>
	</div>
 

	<div class="form-group row mb-1">
		<label class="col-lg-2 col-sm-4 control-label" for="call_direction">Status</label>
		<div class="col-sm-2">
			<select required="required" class="custom-select custom-select-sm mb-1" name="client_status">				
				<option value="1" <?php echo (@$clientobj->client_status=='1')?'selected':''; ?> >Active</option> 
				<option value="0" <?php echo (@$clientobj->client_status=='0')?'selected':''; ?>>Inactive</option>
			</select>
		</div>
	</div>

	<br />

	<div class="form-group row mb-1">
		<label class="col-lg-2 col-sm-2 control-label" for=""></label>
		<div class="col-lg-8 col-sm-10">
			<button class="btn btn-sm btn-primary" type="submit" name="submit" >Save</button>

			<a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="btn btn-sm btn-secondary ml-3">Back</a> 
		</div>
	</div>



</div>