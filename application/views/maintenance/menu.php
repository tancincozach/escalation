
<ul class="nav flex-column maintain-menu">
    <li class="nav-item">
        <a class="nav-link <?php echo (@$menu_active=='general_setting')?'active':''; ?>" href="maintenance">General Settings</a>
    </li>

    <?php if( $this->client_id == '0' ): ?>
    <li class="nav-item">
        <a class="nav-link <?php echo (@$menu_active=='client')?'active':''; ?>" href="maintenance/client">Client</a>
    </li>
    <?php endif; ?>
    
    <li class="nav-item">
        <a class="nav-link <?php echo (@$menu_active=='procedures')?'active':''; ?>" href="maintenance/procedures">Procedures</a>
    </li>
    
    <li class="nav-item">
        <a class="nav-link <?php echo (@$menu_active=='call_response')?'active':''; ?>" href="maintenance/callresponse/?client=all">Call Response</a>
    </li>

    <li class="nav-item">
        <a class="nav-link <?php echo (@$menu_active=='crlookup')?'active':''; ?>" href="crlookup/?client=all" title="Call Response Lookup">CR Lookup Setting</a>
    </li>

    <li class="nav-item">
        <a class="nav-link <?php echo (@$menu_active=='incoming_email_priority')?'active':''; ?>" href="maintenance/incoming_email_priority" title="Incoming Email Priority Condition">Incoming Email Priority</a>
    </li>

    <li class="nav-item">
        <a class="nav-link <?php echo (@$menu_active=='file_manager')?'active':''; ?>" href="maintenance/file_manager?client=all">File Manager</a>
    </li>


    <?php if( in_array($this->user_lvl, array('99', '3')) ): ?>
    <li class="nav-item">
        <a class="nav-link <?php echo (@$menu_active=='email_attachment')?'active':''; ?>" href="maintenance/email_attachment">Email Attachments Monitoring</a>
    </li>   
    <?php endif; ?>

    <?php if( $this->client_id == '0' ): ?>
    <li class="nav-item">
        <a class="nav-link <?php echo (@$menu_active=='users')?'active':''; ?>" href="maintenance/users/?client=all">Users</a>
    </li>   
    <?php endif; ?>
 
   <!--  <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
    </li>  -->
</ul>
 