<h5>Lookup Settings</h5>

<div class="row">

	<div class="col-sm-12">
		 
		<?php if($this->session->flashdata('fmesg') != ''): ?>
		<div class="fmesg px-2 py-1 alert alert-primary mb-1" role="alert">
			<?php echo $this->session->flashdata('fmesg'); ?>
		</div>
		<?php endif; ?>

		 
		<div class="row">

			<div class="col-sm-6">					
				<div class="form-group row mb-0">
					<label class="col-lg-3 col-sm-3 control-label text-right" for="call_lookup_name">Lookup Name</label>
					<div class="col-lg-6 col-sm-6">
						<p class="form-control-static  mb-1"><strong><?php echo stripslashes($record->call_lookup_name); ?></strong></p>
					</div>
				</div>

				<div class="form-group row mb-1">
					<label class="col-lg-3 col-sm-3 control-label text-right" for="source_tablename">Source table name</label>
					<div class="col-lg-6 col-sm-6">
						<p class="form-control-static">

							<a href="crlookup/lookup_data/<?php echo $record->call_lookup_id; ?>" target="_blank">
							<strong>
							<?php 
								$source_tablename = stripslashes($record->source_tablename); 
								$source_tablename = explode('_', $source_tablename);
								unset($source_tablename[0]);
								unset($source_tablename[1]);

								echo implode('_', $source_tablename);
							?>
							</strong>

							</a>
						</p>
					</div>
				</div> 
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6 border m-1">
				<h5>File Upload (.csv)</h5>
				<?php echo form_open_multipart('crlookup/settings_save', 'class="form-inline" method="post" role="form" onsubmit="return confirm(\'Confirm Upload\');" '); ?>
                   
					<input type="hidden" name="call_lookup_id" value="<?php echo $record->call_lookup_id; ?>">
					<input type="hidden" name="client_id" value="<?php echo $record->client_id; ?>" >
					<input type="hidden" name="source_tablename" value="<?php echo $record->source_tablename; ?>" >
					<input type="hidden" name="form_type" value="upload" >

                    <div class="form-group">

						<div class="custom-file">
						  <input type="file" class="custom-file-input " id="customFile" name="source_file_data">
						  <label class="custom-file-label" for="customFile">Choose file</label>
						</div>  
                    </div>
 
					                 
                    <div class="form-group ml-2">                        
                        <input type="submit" class="btn btn-sm btn-primary" name="upload" value="Upload" />
                    </div>
                </form>
                <p class="help-block">
                	Using this upload tool will replace all the records and fields. <br />
                	Please make sure column fields are in first row and no special characters and no duplicates.
                </p>				
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6 border m-1 bg-light">
				<h5>Lookup Fields</h5>
				<?php 
					if( count($field_list) > 0 ){
					  	echo implode(', ', $field_list);
					}
				?>
			</div>
		</div>



		 
		<div class="row">

			<div class="col-sm-6 border m-1">	
				<h5>Lookup Modal Display</h5> 
				 
				<p class="help-block"></p>

				<?php echo form_open('crlookup/settings_save', 'class="" role="form" method="post" onsubmit="return crlookup.save(this);" '); ?>		 

					<input type="hidden" name="call_lookup_id" value="<?php echo $record->call_lookup_id; ?>">
					<input type="hidden" name="client_id" value="<?php echo $record->client_id; ?>" >
					<input type="hidden" name="form_type" value="lookup_modal_display" >	
					
 

					<div class="form-group row mb-1">
						<label class="col-lg-3 col-sm-3 control-label text-right" for="lookup_display">Dropdown display</label>
						<div class="col-sm-8">
							<?php 

								$_field_list_option = $field_list_option;
								//$_field_list_option[''] = '---select---';
								//echo form_dropdown('lookup_display', $_field_list_option, @$record->lookup_display, ' '); 
								 
								$record_lookup_display = explode(',',@$record->lookup_display);
								
								unset($_field_list_option['']);
								foreach ($_field_list_option as $chkbox):

									$checkit = (in_array($chkbox, $record_lookup_display))?'checked':'';
							?>

								<div class="custom-control custom-checkbox">
								  <input type="checkbox" class="custom-control-input" name="lookup_display[]" id="lookup_display_<?php echo $chkbox; ?>" value="<?php echo $chkbox; ?>" <?php echo $checkit; ?> >
								  <label class="custom-control-label" for="lookup_display_<?php echo $chkbox; ?>"><?php echo $chkbox; ?></label>
								</div>
							<?php endforeach; ?>

						</div>
					</div>

					<div class="form-group row mb-1">
						<label class="col-lg-3 col-sm-3 control-label text-right" for="lookup_script">Script / Instruction</label>
						<div class="col-sm-8">							
							 <textarea class="form-control" name="lookup_script" ></textarea>
						</div>
					</div>
 
					<div class="text-right">
						<button type="submit" class="btn btn-primary btn-sm mb-2">Save</button> 		
					</div>
					   

				</form>
			 
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6 border m-1">
				<h5>Button Name</h5> 
				 
				<p class="help-block">This is the name of the button the agent will click if needing to search a spreadsheet.</p>

				<?php echo form_open('crlookup/settings_save', 'class="form-inline" role="form" method="post" onsubmit="return crlookup.save(this);" '); ?>		 

					<input type="hidden" name="call_lookup_id" value="<?php echo $record->call_lookup_id; ?>">
					<input type="hidden" name="client_id" value="<?php echo $record->client_id; ?>" >
					<input type="hidden" name="form_type" value="button_name" >					

					<div class="input-group mb-2 mr-sm-2">
						<div class="input-group-prepend">
							<div class="input-group-text">Set Button name</div>
						</div>
						<input type="text" class="form-control" name="button_name" value="<?php echo $record->button_name; ?>" placeholder="" required="" minlength="5" maxlength="45">
					</div>
				  	<button type="submit" class="btn btn-primary btn-sm mb-2">Save</button> 

				</form>
 
			</div>
		</div><!--end Button Name-->

		<div class="row">
			<div class="col-sm-6 border m-1">
				<h5>Call Response Display</h5>  
				
				<p class="help-block">These is the details that go into the call activity after agent selects from the lookup</p>


				<?php echo form_open('crlookup/settings_save', 'class="" role="form" method="post" onsubmit="return crlookup.save(this);" '); ?>	
					<input type="hidden" name="call_lookup_id" value="<?php echo $record->call_lookup_id; ?>">
					<input type="hidden" name="client_id" value="<?php echo $record->client_id; ?>" >
					<input type="hidden" name="form_type" value="call_res_display" >	
					
					<div class="form-group row mb-1">
						<label class="col-lg-3 col-sm-3 control-label text-right" for="call_direction">Call Direction</label>
						<div class="col-sm-3"> 
							<?php echo form_dropdown('call_res_display[call_direction]',array(''=>'', '1'=>'In', '0'=>'Out'), @$call_res_display->call_direction, ' class="custom-select custom-select-sm mb-1" '); ?>
						</div>
					</div> 

					<div class="form-group row mb-1">
						<label class="col-lg-3 col-sm-3 control-label text-right" for="caller_name">Name</label>
						<div class="col-sm-8">
							<?php echo form_dropdown('call_res_display[caller_name]', $field_list_option, @$call_res_display->caller_name); ?>						
						</div>
					</div>

					<div class="form-group row mb-1">
						<label class="col-lg-3 col-sm-3 control-label text-right" for="caller_phone">Phone</label>
						<div class="col-sm-8">
							<?php echo form_dropdown('call_res_display[caller_phone]', $field_list_option, @$call_res_display->caller_phone); ?>						
						</div>
					</div>
 
					<div class="text-right">
						<button type="submit" class="btn btn-primary btn-sm mb-2">Save</button> 		
					</div>
					   
				</form>
			</div>
		</div><!--end Call Response Display-->


		<div class="row">
			<div class="col-sm-6 border m-1">
				<h5>Screen Display 
					<button class="btn btn-success btn-sm px-1 py-0 m-0" onclick="crlookup.screen_display.new_tr('<?php echo implode('|', $field_list); ?>')"><i class="fa fa-plus"></i> New</button>
				</h5>  
				
				<p class="help-block">This will display between the call activity and the escalation.  If not needed leave blank</p>


				<?php echo form_open('crlookup/settings_save', 'class="" role="form" method="post" onsubmit="return crlookup.save(this);" '); ?>	
					<input type="hidden" name="call_lookup_id" value="<?php echo $record->call_lookup_id; ?>">
					<input type="hidden" name="client_id" value="<?php echo $record->client_id; ?>" >
					<input type="hidden" name="form_type" value="screen_display" >	
					
 					<table class="table table-sm" id="table_screen_display">
 						<thead>
 							<tr>
 								<th>Field Name</th>
 								<th>Field Display</th>
 								<th></th>
 							</tr>
 						</thead>
 						<tbody> 
 							<?php 
 								if( count($screen_display) > 0 ){

		 							foreach($screen_display as $i=>$tr){

		 							  echo  $this->CRLookupmodel->screen_display_new_tr(time().$i, $field_list, $tr); 
		 							}
	 							}else{
	 								//echo  $this->CRLookupmodel->screen_display_tr(time().'1', $field_list, ''); 
		 						}
 							?> 

 						</tbody>
 					</table>
 					<div class="text-right">
 						<button type="submit" class="btn btn-primary btn-sm mb-2">Save</button>  						
 					</div>
					
				</form>
			</div>
		</div><!--end Call Response Display-->


		<div class="row">
			<div class="col-sm-9 border m-1">
				<h5>Escalation Display 
					<button class="btn btn-success btn-sm px-1 py-0 m-0" onclick="crlookup.escalation_display.new_tr('<?php echo implode('|', $field_list); ?>', 0)"><i class="fa fa-plus"></i> New Spreadsheet</button>
					<button class="btn btn-success btn-sm px-1 py-0 m-0" onclick="crlookup.escalation_display.new_tr('<?php echo implode('|', $field_list); ?>', 1)"><i class="fa fa-plus"></i> New Free Type</button>
				</h5>
				
				<p class="help-block">This will display between the call activity and the escalation.  If not needed leave blank</p>


				<?php echo form_open('crlookup/settings_save', 'class="" role="form" method="post" onsubmit="return crlookup.save(this);" '); ?>	
					<input type="hidden" name="call_lookup_id" value="<?php echo $record->call_lookup_id; ?>">
					<input type="hidden" name="client_id" value="<?php echo $record->client_id; ?>" >
					<input type="hidden" name="form_type" value="escalation_display" >	
					
 					<table class="table table-sm" id="table_escalation_display">
 						<thead>
 							<tr>
 								<th>Input</th>
 								<th>Escalation Name</th>
 								<th>Name Field</th>
 								<th>Phone Field</th>
 								<th>Notes</th>
 								<th></th>
 							</tr>
 						</thead>
 						<tbody> 
 							<?php 
 								if( count(@$escalation_display) > 0 ){

		 							foreach($escalation_display as $i=>$tr){

		 							  echo  $this->CRLookupmodel->escalation_display_new_tr(time().$i, $field_list, $tr); 
		 							}
	 							}else{
	 								//echo  $this->CRLookupmodel->screen_display_tr(time().'1', $field_list, ''); 
		 						}
 							?> 

 						</tbody>
 					</table>
 					<div class="text-right">
 						<button type="submit" class="btn btn-primary btn-sm mb-2">Save</button>  						
 					</div>
					
				</form>
			</div>
		</div><!--end Escalation Display-->


		<div class="row">
			<div class="col-sm-6 border m-1">
				<h5>Email Output</h5>
				
				<p class="help-block"></p>


				<?php echo form_open('crlookup/settings_save', 'class="" role="form" method="post" onsubmit="return crlookup.save(this);" '); ?>	
					<input type="hidden" name="call_lookup_id" value="<?php echo $record->call_lookup_id; ?>">
					<input type="hidden" name="client_id" value="<?php echo $record->client_id; ?>" >
					<input type="hidden" name="form_type" value="email_output" >	 
					
					<div class="form-group row mb-1">
						<label class="col-lg-4 col-sm-4 control-label text-right" for="">Show Alert Details</label>
						<div class="col-sm-3"> 
							<?php echo form_dropdown('email_output[show_ad]',array('0'=>'No', '1'=>'Yes'), @$email_output->show_ad, ' class="custom-select custom-select-sm mb-1" '); ?>
						</div>
					</div> 

					<div class="form-group row mb-1">
						<label class="col-lg-4 col-sm-4 control-label text-right" for="">Show Call Activity</label>
						<div class="col-sm-3">
							<?php echo form_dropdown('email_output[show_ca]', array('0'=>'No', '1'=>'Yes'), @$email_output->show_ca, ' class="custom-select custom-select-sm mb-1" '); ?>						
						</div>
					</div>

					<div class="form-group row mb-1">
						<label class="col-lg-4 col-sm-4 control-label text-right" for="">Email every call activity</label>
						<div class="col-sm-3">
							<?php echo form_dropdown('email_output[email_every_ca]', array('0'=>'No', '1'=>'Yes'), @$email_output->email_every_ca, ' class="custom-select custom-select-sm mb-1" '); ?>						
						</div>
					</div>

					<div class="form-group row mb-1">
						<label class="col-lg-4 col-sm-4 control-label text-right" for="">Email if "No Result Found"</label>
						<div class="col-sm-8">													
							<input type="text" class="form-control" name="email_output[no_result_found]" value="<?php echo !isset($email_output->no_result_found)?'csr@welldone.com.au,kellie@welldone.com.au':@$email_output->no_result_found; ?>"  >
						</div>
					</div>
 
					<div class="text-right">
						<button type="submit" class="btn btn-primary btn-sm mb-2">Save</button> 		
					</div>
					
				</form>
			</div>
		</div><!--end Email Output-->


		<div class="row">
			<div class="col-sm-6 border m-1">
				<h5>TO EMAIL
					<button class="btn btn-success btn-sm px-1 py-0 m-0" onclick="crlookup.to_email.new_tr('<?php echo implode('|', $field_list); ?>')"><i class="fa fa-plus"></i> New</button>
				</h5>
				
				<p class="help-block"></p>

				<?php echo form_open('crlookup/settings_save', 'class="" role="form" method="post" onsubmit="return crlookup.save(this);" '); ?>	
					<input type="hidden" name="call_lookup_id" value="<?php echo $record->call_lookup_id; ?>">
					<input type="hidden" name="client_id" value="<?php echo $record->client_id; ?>" >
					<input type="hidden" name="form_type" value="to_email" >	
					
 					<table class="table table-sm" id="table_toemail">
 						<thead>
 							<tr>
 								<th>Input</th>
 								<th>Email</th>
 								<th></th>
 							</tr>
 						</thead>
 						<tbody> 
 							<?php 
 								if( count($to_email) > 0 ){

		 							foreach($to_email as $i=>$tr){

		 							  echo  $this->CRLookupmodel->to_email_new_tr(time().$i, $field_list, $tr); 
		 							}
	 							}else{
	 								//echo  $this->CRLookupmodel->screen_display_tr(time().'1', $field_list, ''); 
		 						}
 							?> 

 						</tbody>
 					</table>
 					<div class="text-right">
 						<button type="submit" class="btn btn-primary btn-sm mb-2">Save</button>  						
 					</div>
					
				</form>
			</div>
		</div><!--end Email Output-->



	</div>

</div>