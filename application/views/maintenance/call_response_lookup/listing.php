<h5>Lookup List</h5>

<?php if($this->session->flashdata('fmesg') != ''): ?>
<div class="fmesg px-2 py-1 alert alert-primary mb-1" role="alert">
	<?php echo $this->session->flashdata('fmesg'); ?>
</div>
<?php endif; ?>

<div class="row">

	<div class="col-sm-2">

		<div class="input-group mb-3">
				<?php if(isset($client_created)):?>
				 <input type="hidden" name="created_client" value="<?php echo htmlentities(json_encode(@$client_created));?>"/>
				<?php endif;?>
			  <input type="text" id="client_search" onkeyup="escalation.common.search_text(this,'.client_tbl');" class="form-control input-sm"  placeholder="Search for client.." aria-label="Search for client.." aria-describedby="basic-addon2" value="<?php echo @$client_created->client_name;?>">
			  <div class="input-group-append">
			    <button class="btn btn-outline-danger btn-sm" type="button" onclick="escalation.common.clear_search(this,'.client_tbl');"><i class="far fa-times-circle "></i></button>
			  </div>
			</div>

			<table class="client_tbl table table-sm ">
			  <thead>	  
			    <tr>
			      <th scope="col" class="table-primary" >CLIENT
			      </th>	      
			    </tr>
			  </thead>
			  <tbody>
				
				<?php foreach($clients as $row): ?>

			    <tr>
			      
			      <td><?php if( $this->client_id == 0 ): ?>
				  			<label><?php echo $row->client_name; ?></label>
				  			<a href="crlookup/?client_id=<?php echo $row->client_id; ?>"  class="btn btn-info btn-sm py-0 float-right"  >
				  				<i class="far fa-check-square"></i>&nbsp;
				  				
				  			</a>
				  		<?php else: ?>

				  			<label><?php echo $row->client_name; ?></label>

				  			<a href="crlookup/?client_id=<?php echo $row->client_id; ?>"  class="btn btn-info btn-sm py-0 float-right"  alt="<?php echo $this->client_id?>:<?php echo $row->client_name?>" >
					  			<i class="far fa-check-square "></i>&nbsp;
				  				
				  			</a>
				  		<?php endif; ?></td>
			      
			    </tr>
			
				<?php endforeach;?>		

			  </tbody>
			</table>

	</div>

	<div class="col-sm-10">	


		<div class="row">
			<div class="col-sm-6">
				<?php if( $this->client_id > 0 OR isset($records) ): ?>
					<strong>NEW: </strong>
					<a class="btn btn-sm btn-primary py-0" href="crlookup/setup/<?php echo ($this->client_id == 0)?'?client_id='.$client->client_id:''; ?>">
						<i class="fab fa-hubspot"></i> 
						New
					</a>		 
				<?php endif; ?>				
			</div>
			<div class="col-sm-6 text-right">
				<h5><?php echo @$client->client_name; ?></h5>
			</div> 
		</div>

		<?php if( isset($records) ): ?>
			<table class="table table-sm">
				<thead>
					<th>Name</th>
					<th>Storage name</th>
					<th style="width: 150px;">Created</th>
					<th style="width: 150px;">Updated</th>
					<th style="width: 50px;">Status</th>
					<th style="width: 100px;">&nbsp;</th>
				</thead>
				<tbody>
					
					<?php foreach($records as $row): ?>
					<tr>
						<td><?php echo stripslashes($row->call_lookup_name); ?></td>
						<td><?php echo stripslashes($row->source_tablename); ?></td>
						<td><?php echo date('d/m/Y H:i:s',strtotime($row->create_dt)); ?></td>
						<td><?php echo ( !in_array($row->updated_dt, array('', '0000:00:00 00:00:00')) )?date('d/m/Y H:i:s',strtotime($row->updated_dt)):''; ?></td>						
						<td></td>
						<td>
							<td class="text-center">

								<?php if( $row->step == 2 ): ?>
								<a href="<?php echo 'crlookup/setup/2/'.$row->call_lookup_id.'/?client_id='.$row->client_id; ?>" title="Edit" >
									<i class="fas fa-edit"></i>								
								</a>
								<?php endif; ?> 

								<?php if( $row->step == 3 ): ?>
								<a href="<?php echo 'crlookup/settings/'.$row->call_lookup_id.'/?client_id='.$row->client_id; ?>"" title="Setup" >
									<i class="fas fa-edit"></i>								
								</a>
								<?php endif; ?> 	 
							</td>

						</td>
					</tr>
					<?php endforeach; ?>
					
				</tbody>
			</table>
		<?php endif; ?>
	</div>

</div>