 
<div class="row">

	<div class="col-sm-12 px-5">
		<div class="table-responsive">

			<?php if( count($record) > 0 ): ?>

			<div class="text-center">
				<h4><?php echo $look_up_name; ?></h4>
			</div>

			<table class="table table-sm">
				<thead>
					<?php foreach($table_fields as $fields): ?>
					<th><?php echo $fields; ?></th>
					<?php endforeach; ?>
				</thead>
				<tbody>
					
					<?php foreach($table_data as $row): ?>
					<tr>
						<?php foreach($table_fields as $fields): ?>
							<td><?php echo stripslashes($row->{$fields}); ?></td>					 
						<?php endforeach; ?>
					</tr>
					<?php endforeach; ?>
					
				</tbody>
			</table>
		 	<?php endif; ?>

		</div>
	</div>
</div>