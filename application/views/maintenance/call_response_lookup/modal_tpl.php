<div class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header py-1">
                <h5 class="modal-title">Lookup</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            <?php echo form_open('', 'name="callactivity_email_reply" name="callactivity_lookup" class="" role="form" method="post" onsubmit=" return confirm(\'Confirm sending email?\'); "'); ?>   
                <div class="modal-body text-center">  
  					
  					<p><?php echo stripslashes($lookup_script); ?></p>
                   
                    <div style="height: 400px; overflow-y: scroll;">

                        <div class="row">
                            
                            <div class="col-sm-3"> 
                            </div>
                            <div class="col-sm-6"> 
                                <input type="text" class="form-control" id="lookup_input_search" onkeyup="callactivity.lookup.on_search(this)" placeholder="Search ..." title="">                                
                            </div>
                            <div class="col-sm-3 float-right text-right">                                 
                                <button type="button" class="btn btn-secondary btn-sm mr-1" onclick="callactivity.lookup.on_click_noresult()">No Result Found</button>
                            </div>
                        </div>
                        <?php  if( is_array($lookup_fields) ): ?>
                        <table class="table table-sm" id="lookup_table_data">
                            <tr>
                                <?php foreach($lookup_fields as $head): ?>
                                <th><?php echo $head; ?></th>
                                <?php endforeach; ?>
                                <th>&nbsp;</th>
                            </tr>
                            <?php  
                                $table_tr = '';
                                foreach($table_data  as $key=>$row){

                                    $field_values = '';
                                    $_table_tr = '';
                                    foreach ($lookup_fields as $val) {
                                        $field_values .= trim(@$row->$val);
                                        $_table_tr .= '<td>'.trim(@$row->$val).'</td>';
                                    }
                                    
                                    if( $field_values == '' ) continue;

                                    $table_tr .= $_table_tr.'<td><button type="button" class="btn btn-sm btn-success pt-0 pb-0" onclick="callactivity.lookup.on_select('.$row->id.','.$call_lookup_id.', '.$tran_id.', '.$is_view_only.')" ><i class="fas fa-copy"></i> Select </button></td>';

 
                                    $table_tr .= '</tr>';

                                }

                                echo $table_tr;

                            ?>
                        </table>
                        <?php else: ?>
                            No lookup field has been set
                        <?php endif; ?>

                    </div>



                </div>

                <div class="modal-footer py-1">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                    <!-- <button type="submit" class="btn btn-primary btn-sm">Send</button> -->
                </div>

            </form>
        </div>
    </div>
</div>