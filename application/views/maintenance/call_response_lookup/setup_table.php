<h5>Lookup Settings Setup</h5>

<br />

<div class="row">

	<div class="col-sm-12">
		
		<?php if($this->session->flashdata('fmesg') != ''): ?>
		<div class="fmesg px-2 py-1 alert alert-primary mb-1" role="alert">
			<?php echo $this->session->flashdata('fmesg'); ?>
		</div>
		<?php endif; ?>


		<?php if($step == 1): ?>
		<div class="row">
			<div class="col-sm-6">					
				<h5 class="text-primary">Step 1</h5>
				<?php echo form_open('crlookup/setup_save', 'class="" role="form" method="post" onsubmit="return confirm(\'Confirm creating Lookup Name\')"  "'); ?>	
					<input type="hidden" name="step" value="1">
					<input type="hidden" name="client_id" value="<?php echo $client_id; ?>" >
					<div class="form-group row mb-1">
						<label class="col-lg-3 col-sm-3 control-label text-right" for="call_lookup_name">Lookup Name</label>
						<div class="col-lg-6 col-sm-6">
							<input type="text" required="required" name="call_lookup_name" maxlength="100" class="form-control form-control-sm" value="">
						</div>
					</div>

					<br />
					<div class="form-group row mb-1">
						<label class="col-lg-3 col-sm-3 control-label text-right" for="">&nbsp;</label>
						<div class="col-lg-6 col-sm-6 text-right">
							<button type="submit" class="btn btn-sm btn-primary">Next</button>							
						</div>					 
					</div>
				</form>
			</div>
		</div>
		<?php endif; ?>
		

		<?php if($step == 2): ?>
		<div class="row">
			<div class="col-sm-6">					
				<h5 class="text-primary">Step 2</h5>
				<?php echo form_open('crlookup/setup_save', 'class="" role="form" method="post" onsubmit="return confirm(\'Confirm creating source table name\')" '); ?>	
					<input type="hidden" name="step" value="2">
					<input type="hidden" name="call_lookup_id" value="<?php echo $record->call_lookup_id; ?>">
					<input type="hidden" name="client_id" value="<?php echo $record->client_id; ?>" >
					
					<div class="form-group row mb-1">
						<label class="col-lg-3 col-sm-3 control-label text-right" for="call_lookup_name">Lookup Name</label>
						<div class="col-lg-6 col-sm-6">
							<p class="form-control-static"><?php echo stripslashes($record->call_lookup_name); ?></p>
						</div>
					</div>

					<div class="form-group row mb-1">
						<label class="col-lg-3 col-sm-3 control-label text-right" for="source_tablename">Source table name</label>
						<div class="col-lg-6 col-sm-6">
							<input type="text" required="required" name="source_tablename" minlength="5" maxlength="15" class="form-control form-control-sm" value="" pattern="[a-z]{5,15}">
							<small class="form-text text-muted">
							  Source table name must be 5-15 characters long, contain small letters only and no spaces, special characters.
							</small>							
						</div>
					</div>

					<br />
					<div class="form-group row mb-1">
						<label class="col-lg-3 col-sm-3 control-label text-right" for="">&nbsp;</label>
						<div class="col-lg-6 col-sm-6 text-right">
							<button type="submit" class="btn btn-sm btn-primary">Next</button>							
						</div>					 
					</div>
				</form>
			</div>
		</div>
		<?php endif; ?>


	</div>

</div>