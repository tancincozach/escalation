
<h5>Incoming Email Priority Condition</h5>


<?php if($this->session->flashdata('fmesg') != ''): ?>
<div class="fmesg px-2 py-1 alert alert-primary mb-1" role="alert">
	<?php echo $this->session->flashdata('fmesg'); ?>
</div>
<?php endif; ?>

<div class="row">
	
	 
	<div class="col-sm-12 pb-1">
		Some description here
	</div>

	<div class="col-sm-2 pb-1">

		<?php if( $this->client_id == 0 ): ?>
			<a class="btn btn-sm btn-success" href="maintenance/incoming_email_priority_form/add/">New Condition</a>
		<?php endif; ?>		

	</div>

	<div class="col-sm-12">	 
		<?php if( isset($results) AND count($results) > 0 ): ?>
			<table class="table table-sm table-striped table-bordered">
				<thead> 
					<tr> 
						<th style="width: 150px;">FROM referrence</th>
						<th style="width: 100px;">Priority email</th> 
						<th style="width: 150px;">Created</th>
						<th style="width: 75px;"></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach( $results as $row): ?>
						<tr>
							<td><?php echo $row->email_from_ref; ?></td>
							<td><?php echo $row->email_priority; ?></td>
				 
							<td><?php echo date('d/m/Y H:i:s', strtotime($row->date_created)); ?></td>
							<td>
								<a href="maintenance/incoming_email_priority_form/update/?id=<?php echo $row->id; ?>" title="Edit">
								 	<i class="fas fa-edit"></i>
								 	<!-- Edit -->
								 </a>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>

		    <div class="row">
		        <div class="col-sm-6 justify-content-start">
		             <?php echo @$showing; ?>
		        </div>
		        <div class="col-sm-6 d-flex justify-content-end">            
		            <?php echo @$links; ?>
		        </div> 
		    </div>
		<?php endif; ?>
	</div>

</div>