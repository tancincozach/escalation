
 
<div class="row border mt-1 mb-1">
	<div class="col-sm-4"><h5> Incoming Email Priority - Form </h5></div> 
</div>
 

<?php if($this->session->flashdata('fmesg') != ''): ?>
<div class="fmesg px-2 py-1 alert alert-primary mb-1" role="alert">
	<?php echo $this->session->flashdata('fmesg'); ?>
</div>
<?php endif; ?>

<div class="col-lg-10 col-sm-12">
	<?php echo form_open('', 'class="" role="form" method="post" onsubmit=" return confirm(\'Confirm Saving Client?\'); "'); ?>	
  
	<input type="hidden" name="form_type" value="<?php echo @$form_type; ?>">	 
	<input type="hidden" name="id" value="<?php echo @$clientobj->id; ?>">	 


	<div class="form-group row mb-1">
		<label class="col-lg-2 col-sm-4 control-label" for="email_from_ref">FROM REF email address</label>
		<div class="col-lg-4 col-sm-8">
			<input type="text" required="required" id="email_from_ref" name="email_from_ref" maxlength="100" class="form-control form-control-sm" value="<?php echo stripslashes(@$clientobj->email_from_ref); ?>">
		</div>
	</div>
	<div class="form-group row mb-1">
		<label class="col-lg-2 col-sm-4 control-label" for="email_priority">PRIORITY email address</label>
		<div class="col-lg-4 col-sm-8">
			<input type="text" required="required" id="email_priority" name="email_priority" maxlength="100" class="form-control form-control-sm" value="<?php echo stripslashes(@$clientobj->email_priority); ?>">
		</div>
	</div>
 

	<br />

	<div class="form-group row mb-1">
		<label class="col-lg-2 col-sm-2 control-label" for=""></label>
		<div class="col-lg-8 col-sm-10">
			<button class="btn btn-sm btn-primary" type="submit" name="submit" >Save</button>

			<a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="btn btn-sm btn-secondary ml-3">Back</a> 
		</div>
	</div>



</div>