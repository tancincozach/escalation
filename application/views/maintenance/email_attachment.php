
<div class="col-sm-12">
	
	<h5 class="my-3">Email Attachments Monitoring</h5>

	<?php if($this->session->flashdata('fmesg') != ''): ?>
	<div class="fmesg px-2 py-1 alert alert-primary mb-1" role="alert">
		<?php echo $this->session->flashdata('fmesg'); ?>
	</div>
	<?php endif; ?>


	<style>

		

	</style>

	<?php echo form_open('', ' id="frm-chart" class="form-inline" role="form" method="post" onsubmit=" return confirm(\'Submit Search?\'); "'); ?>	

			<div class="form-group row mb-1">
				<label class="col-md-3 control-label text-right" for="reminder_month">Year:</label>
				<div class='col-md-4'>
				<select class="form-control" name="year" >
					<?php
					$Startyear=date('Y');
					$endYear=2018;
					$yearArray = range($Startyear,$endYear);
					?>
					
					    <?php
					    foreach ($yearArray as $year) {
					        // this allows you to select a particular year
					        $selected = ($year == $Startyear) ? 'selected' : '';
					        echo '<option '.$selected.' value="'.$year.'">'.$year.'</option>';
					    }
					    ?>						
				 </select>
							     
			    </div>
			</div>
				
	</form>
	<div class="clearfix"></div><br/>

	<input type="hidden" name="selected_point" value=""/>

	<div id="email-chart-container" class="clearfix">
		<!-- <i class="fas fa-spinner text-primary"></i> -->
	</div>




	<div id="summary_report_container">


	</div>
</div>