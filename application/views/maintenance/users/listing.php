
<h5>Users</h5>


<?php if($this->session->flashdata('msg') != ''): ?>
<div class="fmesg px-2 py-1 alert alert-primary mb-1" role="alert">
	<?php echo $this->session->flashdata('msg'); ?>
</div>
<?php endif; ?>
<?php if($this->session->flashdata('error') != ''): ?>
<div class="fmesg px-2 py-1 alert alert-primary mb-1" role="alert">
	<?php echo $this->session->flashdata('error'); ?>
</div>
<?php endif; ?>

<div class="row">
<!-- 	
	<div class="col-sm-2">

		<ul class="list-group">
			<?php
		if(isset($clients )):
			?>
	  	<li class="list-group-item px-2 py-1">
	  	  			<a href="maintenance/users/?client=all" style="display: block">DEFAULT</a>
	  	</li>
		<?php
			 foreach($clients as $row): ?>
		  	<li class="list-group-item px-2 py-1">
				<a href="maintenance/users/?client_id=<?php echo $row->client_id; ?>" style="display: block"><?php echo $row->client_name; ?></a>
		  	</li> 
		 	<?php 

	 		endforeach;

	 		else:

		 	 ?>
				<li class="list-group-item px-2 py-1">No Client(s) Found.</li>
		 	<?php
		 		endif;
		 	?>	
		</ul>

	</div> -->
	<div class="col-sm-12">	

		<div class="row">
			<div class="col-sm-6">
				
					<!-- <a class="btn btn-sm btn-success" href="maintenance/user_form/add/<?php echo (isset($client) && $client->client_id > 0)?'?client_id='.$client->client_id:''; ?>"><i class="fas fa-plus-circle"></i> New</a> -->
					<a class="btn btn-sm btn-success" href="maintenance/user_form/add"><i class="fas fa-plus-circle"></i> New</a>
				
			</div>
			<div class="col-sm-6 text-right">
				<h5><?php echo @$client->client_name; ?></h5>
			</div> 
		</div>

		<?php //if( isset($users) AND count($users) > 0 ): ?>
		<table class="table table-bordered table-hover table-sm">
		<thead class="table-primary">
			<tr>
				<th scope="col">Username</th> 
				<th scope="col">Fullname</th> 
				<th scope="col">User Level</th> 
				<th scope="col">Display Screen</th> 
				<th scope="col">Client</th> 
				<th scope="col">Date Updated</th> 
				<th scope="col">Date Created</th> 
				<th scope="col">Status</th> 
				<th scope="col">Options</th> 
			</tr>
		</thead>
		<tbody>
		<?php 
	 if(isset($users)):	
		foreach($users as $row):
		?>
			 <tr>
			 	<td><?php echo $row->username ?></td>
			 	<td><?php echo $row->fullname;?></td>
			 	<td><?php 

			 				switch ((int)$row->user_lvl) {
			 					case 0:
			 						echo 'Operator';	
			 						break;
			 					case 1:
			 						echo 'Supervisor';
			 						break;
			 					case 2:
			 						echo 'Support/Teamleader';	
			 						break;
			 					case 3:
									echo 'CCAdmin';			 					
			 						break;
			 					case 5:

			 						echo 'Client';
			 					
			 						break;
			 					
			 					default:
			 					 	echo '';
			 						break;
			 				}


			 	;?></td>			 	
			 	<td><?php 

			 	$opt = json_decode(@$row->options) ;
			 	switch (@$opt->display_screen) {
			 		case 1:
			 			 echo "Escalation";
			 			break;
			 		
			 		case 2:
			 			 echo "Action Team";
			 			break;
			 		
			 		case 3:
			 			 echo "Back Office";
			 			break;
			 		
			 		default:
			 			echo "All";
			 			break;
			 	};
			 	?></td>		 		
			 	<td><?php echo @$row->client_name;?></td>		 		
			 	<td><?php echo ($row->last_updated_date!='' ? date('Y-m-d H:i:s',strtotime($row->last_updated_date)):'');?></td>
			 	<td><?php echo date('Y-m-d H:i:s',strtotime(@$row->date_created));?></td>
			 	<td><?php echo (@$row->user_status==1) ? 'Active':'Inactive';?></td>
			 			 	<td>
			 		<?php if($this->client_id == 0 || ($this->client_id > 0 && $this->client_id==@$row->client_id)):?>				 		
					 	<a href="maintenance/user_form/edit/?uid=<?php echo $row->id;?>"/><i class="fas fa-edit"></i>Update</a>&nbsp;&nbsp;
<!-- 					 	<a onclick="return escalation.common.confirm('Are you sure you want to delete ?');" href="maintenance/callresponse/delete/?client_id=<?php //echo $row->client_id; ?>&cr_id=<?php //echo $row->call_res_id;?>"/><i class="fas fa-trash-alt"></i>Delete</a>					 	 -->
				 	<?php elseif($this->client_id > 0 && $client_all==true):?>

					 <?php endif;?>
					</td>
			 </tr>

		 <?php 
		 endforeach;
		else:?>
			<tr>
				<td colspan="8"> No User(s) Found.</td>
			</tr>
		<?php
		endif;
		?>
		</tbody>
	</table>


		<?php //endif; ?>
	</div>
</div>