<div class="col-sm-12">
<div class="row border mt-1 mb-1">
	<div class="col-sm-4"><h5> User - FORM </h5></div>
	<div class="col-sm-4"><h5><?php echo strtoupper($form_type); ?></h5></div>
	<div class="col-sm-4"><h5><?php echo @$client_row->client_name; ?></h5></div>
</div>
<?php echo form_open('','method="post" class="form-horizontal"'); ?>
					<input type="hidden" name="form_type" value="<?php echo @$form_type; ?>">	 
					<input type="hidden" name="uid" value="<?php echo @$user_row->id; ?>">	 
					<input type="hidden" name="client_id" value="<?php echo @$client_row->client_id; ?>">

			  	   	 <div class="form-group row mb-1">
					    <label for="lvl" class="col-sm-2 col-form-label">Client :</label>
				        	<div class="col-sm-4">
							    <select name="client" id="client" class="custom-select"> 
							    		<option value="">Select Client</option>
										<?php foreach($clients as $row):?>
											<option value="<?php echo $row->client_id;?>" <?php echo (@$user_row->client_id==$row->client_id) ?'SELECTED':'';?>><?php echo $row->client_name;?></option>
										<?php endforeach;?>
							    </select>
						   </div>
				  	  </div> 
	
					  <div class="form-group row mb-1">
					    <label for="full_name" class="col-sm-2 col-form-label">Full Name :</label>
					    <div class="col-sm-4">
					    	<input type="text" class="form-control" id="full_name" name="full_name" placeholder="Full Name" value="<?php echo @$user_row->fullname?>" required>
					    </div>
					  </div>
					 	<div class="form-group row mb-1">
						    <label for="user_name" class="col-sm-2 col-form-label">User Name :</label>
						     <div class="col-sm-4">
						    	<input type="text" class="form-control" id="user_name" name="user_name" placeholder="User Name" value="<?php echo @$user_row->username?>" required>
						    </div>
					  </div>
				  	 	  	   <div class="form-group row mb-1">
						    <label for="password" class="col-sm-2 col-form-label">Password :</label>
							    <div class="col-sm-4">
							   		<input class="form-control" type="password" name="password" id="password" value="" <?php if(!isset($user_row)):?>required<?php endif;?>>
							   	</div>
							   <?php if(isset($user_row)):?>
							   		<em class="text-success" style="font-weight:bold">Leave Password blank if not updating</em>
							   <?php endif;?>
				  	  </div>
			  	   	 <div class="form-group row mb-1">
					    <label for="lvl" class="col-sm-2 col-form-label">User Level :</label>
				        	<div class="col-sm-4">
							    <select name="lvl" id="lvl" class="custom-select" onchange="user.access_lvl_selection(this);" required> 
							    	<option value="0" <?php echo (@$user_row->user_lvl==0) ?'SELECTED':'';?>>Operator</option>
							    	<option value="1" <?php echo (@$user_row->user_lvl==1) ?'SELECTED':'';?>>Supervisor</option>
							    	<option value="2" <?php echo (@$user_row->user_lvl==2) ?'SELECTED':'';?>>Support/Teamleader</option>
							    	<option value="3" <?php echo (@$user_row->user_lvl==3) ?'SELECTED':'';?>>CCAdmin</option>
							    	<option value="5" <?php echo (@$user_row->user_lvl==5) ?'SELECTED':'';?>>Client</option>
							    </select>
						   </div>
				  	  </div> 

				  	  <div class="form-group row mb-1">
					    <label for="lvl" class="col-sm-2 col-form-label">Display Screen :</label>
				        	<div class="col-sm-4">
			  					  <select class="custom-select" name="display_screen"> 
									<option value="" <?php echo (@$user_row->display_screen=='')?'selected':''; ?> >All</option>
									<option value="1" <?php echo (@$user_row->display_screen=='1')?'selected':''; ?> >Escalation</option>
									<option value="2" <?php echo (@$user_row->display_screen=='2')?'selected':''; ?> >Action Team</option>								        
									<option value="3" <?php echo (@$user_row->display_screen=='3')?'selected':''; ?> >Back office</option>								        
							    </select>
						   </div>
				  	  </div>   
				  	  <div class="form-group row mb-1">
					    <label for="formGroupExampleInput2" class="col-sm-2 col-form-label">Status :</label>
					    <div class="col-sm-4">
						    <select name="status" class="custom-select">						    	
						    	<option value="1" <?php echo (@$user_row->user_status==1) ? 'SELECTED':''?>>Active</option>
						    	<option value="0" <?php echo (@$user_row->user_status==0) ? 'SELECTED':''?>>Inactive</option>
						    </select>
						 </div>
				  	  </div>
				  	  <div class="form-group">
				  	  		<button class="btn btn-primary btn-sm">Submit</button>
				  	  		<a href="javascript:history.back();" class="btn btn-info btn-sm">Back</a>

				  	  </div>
		<?php echo form_close(); ?>


</div>