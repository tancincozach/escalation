
 
<div class="row border mt-1 mb-1">
	<div class="col-sm-4"><h5> File Manager - FORM </h5></div>
	<div class="col-sm-4"><h5><?php echo strtoupper($form_type); ?></h5></div>
	<div class="col-sm-4"><h5><?php echo @$client_row->client_name; ?></h5></div>
</div>
 

<?php if($this->session->flashdata('msg') != ''): ?>
<div class="fmesg px-2 py-1 alert alert-primary mb-1" role="alert">
	<?php echo $this->session->flashdata('msg'); ?>
</div>
<?php endif; ?>

<div class="col-lg-10 col-sm-12">
	<?php echo form_open_multipart('', 'class="" role="form" method="post" onsubmit=" return confirm(\'Confirm Uploading file?\'); "'); ?>	

		<input type="hidden" name="form_type" value="<?php echo @$form_type; ?>">	 
		<input type="hidden" name="fid" value="<?php echo @$file_row->id; ?>">	 
		<input type="hidden" name="client_id" value="<?php echo @$client_row->client_id; ?>">


		<div class="form-group row mb-1">
			<label class="col-lg-2 col-sm-2 control-label">Name</label>
			<div class="col-lg-5 col-sm-10">
				 <input type="text" class="form-control"  value="<?php echo @$file_row->file_name;?>" name="filename" <?php if($form_type=='edit'):?>readonly<?php else:?> required <?php endif;?>>
			</div>
		</div>
	<?php if($form_type=='edit'):?>
		<input type="hidden" name="source_file_to_be_replaced" value="<?php echo @$file_row->file_path; ?>">
		<div class="form-group row mb-1">
			<label class="col-lg-2 col-sm-2 control-label">Existing File</label>
			<div class="col-lg-5 col-sm-10">
			<a href="<?php echo @$file_row->file_path;?>"  target="_blank" /><?php echo @$file_row->file_name;?></a>	
			</div>
		</div>
		
	<?php endif;?>


	<div class="form-group row mb-1">
		<label class="col-lg-2 col-sm-2 control-label" for="filename">File</label>
		<div class="col-md-5">
			<input type="file" name="file_upload"/>
		</div>
	</div>
	
	<div class="form-group row mb-1">
		<label class="col-sm-2 control-label" for="file_status">Status</label>
		<div class="col-md-5">
			<select required="required" class="custom-select custom-select-sm mb-1" name="file_status">				
				<option value="1" <?php echo (@$file_row->file_status=='1')?'selected':''; ?> >Active</option> 
				<option value="0" <?php echo (@$file_row->file_status=='0')?'selected':''; ?>>Inactive</option>
			</select>
		</div>
	</div>

	<br />

	<div class="form-group row mb-1">
		<div class="col-md-5">
			<button class="btn btn-sm btn-primary" type="submit" name="submit" >Save</button> 
	  		<a href="javascript:history.back();" class="btn btn-info btn-sm">Back</a>
		</div>
	</div>



</div>