
<h5>File Manager</h5>


<?php if($this->session->flashdata('msg') != ''): ?>
<div class="fmesg px-2 py-1 alert alert-primary mb-1" role="alert">
	<?php echo $this->session->flashdata('msg'); ?>
</div>
<?php endif; ?>

<?php if($this->session->flashdata('error') != ''): ?>
<div class="fmesg px-2 py-1 alert alert-primary mb-1" role="alert">
	<?php echo $this->session->flashdata('error'); ?>
</div>
<?php endif; ?>
<div class="row">
	
	<div class="col-sm-2">

		<div class="input-group mb-3">
				<?php if(isset($client_created)):?>
				 <input type="hidden" name="created_client" value="<?php echo htmlentities(json_encode(@$client_created));?>"/>
				<?php endif;?>
			  <input type="text" id="client_search" onkeyup="escalation.common.search_text(this,'.client_tbl');" class="form-control input-sm"  placeholder="Search for client.." aria-label="Search for client.." aria-describedby="basic-addon2" value="<?php echo @$client_created->client_name;?>">
			  <div class="input-group-append">
			    <button class="btn btn-outline-danger btn-sm" type="button" onclick="escalation.common.clear_search(this,'.client_tbl');"><i class="far fa-times-circle "></i></button>
			  </div>
			</div>

			<table class="client_tbl table table-sm ">
			  <thead>	  
			    <tr>
			      <th scope="col" class="table-primary" >CLIENT
			      </th>	      
			    </tr>
			  </thead>
			  <tbody>
			  <?php 

			  	if($this->allow_reg_cbox > 0):

			  ?>
			    <tr>
			    	<td>
			    	ALL
			    		<a href="maintenance/file_manager/?client=all"  class="btn btn-info btn-sm py-0 float-right"    >
				  				<i class="far fa-check-square"></i>&nbsp;
				  				
				  			</a>
			    	</td>
			    </tr>			   
			<?php 
				endif;
			 ?>
				<?php foreach($clients as $row): ?>

			    <tr>
			      
			      <td><?php if( $this->client_id == 0 ): ?>
				  			<label><?php echo $row->client_name; ?></label>
				  			<a href="maintenance/file_manager/?client_id=<?php echo $row->client_id; ?>"  class="btn btn-info btn-sm py-0 float-right"  >
				  				<i class="far fa-check-square"></i>&nbsp;
				  				
				  			</a>
				  		<?php else: ?>

				  			<label><?php echo @$row->client_name; ?></label>

				  			<a href="maintenance/file_manager/?client_id=<?php echo $row->client_id; ?>"  class="btn btn-info btn-sm py-0 float-right"  alt="<?php echo $this->client_id?>:<?php echo $row->client_name?>" >
					  			<i class="far fa-check-square "></i>&nbsp;
				  				
				  			</a>
				  		<?php endif; ?></td>
			      
			    </tr>
			
				<?php endforeach;?>		

			  </tbody>
			</table>

	</div>
	<div class="col-sm-10">	

		<div class="row">
			<div class="col-sm-6">
				<?php if(!isset($show_all) AND isset($files)): ?>
					<a class="btn btn-sm btn-success py-0" href="maintenance/file_manager_form/add/<?php echo (isset($client) && $client->client_id > 0)?'?client_id='.$client->client_id:''; ?>"><i class="fas fa-plus-circle"></i> New</a>
				<?php endif; ?>				
			</div>
			<div class="col-sm-6 text-right">
				<h5><?php echo @$client->client_name; ?></h5>
			</div> 	
		</div>

			<table class="table table-bordered table-hover table-sm">
					<thead class="table-primary">
					<th scope="col">Name</th>
					<?php if(isset($show_all)):?>
					<th scope="col">Client</th>
					<?php endif;?>
					<th scope="col" >Created</th>
					<th scope="col" >Updated</th>
					<th scope="col" >Status</th>
					<th scope="col" ></th>
				</thead>
				<tbody>
					<?php

						if(isset($files) && count($files) > 0 ):
								 foreach($files as $row): ?>
							<tr>
								<td><?php echo stripslashes($row->file_name); ?></td>
								<?php if(isset($show_all)):?>
								<td><?php echo stripslashes(@$row->client_name); ?></td>
								<?php endif;?>
								<td><?php echo date('d/m/Y H:i:s',strtotime($row->created_at)); ?></td>
								<td><?php echo ( !in_array($row->update_at, array('', '0000:00:00 00:00:00')) )?date('d/m/Y H:i:s',strtotime($row->update_at)):''; ?></td>
								<td><?php echo ($row->file_status)?'<span class="text-success">Active</span>':'<span class="text-muted">Inactive</span>'; ?></td>
								<td >
									<a href="maintenance/file_manager_form/edit/?fid=<?php echo $row->id; ?>"><i class="fas fa-edit"></i>Update</a>&nbsp;&nbsp;
									<a href="<?php echo @$row->file_path;?>"><i class="fas fa-download"></i>Download</a>&nbsp;&nbsp;
							 		<a onclick="return escalation.common.confirm('Are you sure you want to delete ?');" href="maintenance/file_manager/delete/?client_id=<?php echo $row->client_id; ?>&fid=<?php echo $row->id; ?>"/><i class="fas fa-trash-alt"></i>Delete</a>					 	
								</td>
							</tr>
							<?php 
							endforeach; 
						else:?>
							<tr>
								<td colspan="5">No file(s) found on this client.</td>
							</tr>
						<?php
						endif;
					?>
				</tbody>
			</table>


	</div>
</div>