<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>File Manager</title>

 		<base href="<?php echo base_url(); ?>"/>

 		<link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
 		<link href="assets/vendor/tinymce.filemanager/styles.css" rel="stylesheet">
 		<link href="assets/vendor/fontawesome-free-5.0.6/css/fontawesome-all.min.css" rel="stylesheet">

 		<!--<script src="assets/js/jquery-1.11.3.min.js"></script>-->
		 
		<script type="text/javascript" charset="utf-8">
			// Documentation for client options:
			
			var FilemanagerDialogue = {

				click: function(URL){

					parent.tinymce.activeEditor.windowManager.getParams().setUrl(URL);
					parent.tinymce.activeEditor.windowManager.close();
				}
			}
		
			$(document).ready(function() {
 
			});

		</script>
	</head>
	<body> 

	<?php

		$file_icon = array(
			'pdf' => '<i class="fas fa-file-pdf"></i>',
			'jpg' => '<i class="fas fa-image"></i>',
			'JPG' => '<i class="fas fa-image"></i>',
			'jpeg' => '<i class="fas fa-image"></i>',
			'JPEG' => '<i class="fas fa-image"></i>',
			'png' => '<i class="fas fa-image"></i>',
			'PNG' => '<i class="fas fa-image"></i>',
			'ods' => '<i class="fas fa-file-word"></i>',
			'doc' => '<i class="fas fa-file-word"></i>',
			'xls' => '<i class="fas fa-file-excel"></i>',
			'xlsx' => '<i class="fas fa-file-excel"></i>',
		);

	?>

		<div class="container"> 
			<ul class="pl-1" style="list-style-type: none;">
				<?php foreach($files as $file): ?>
				<li class="pl-2 py-1 border-0">
					<span class="float-left" style="width: 20px"><?php echo $file_icon[file_ext($file->file_path)]; ?></span>
					<a href="javascript:void(0)" onclick="FilemanagerDialogue.click('<?php echo base_url().$file->file_path; ?>');"><?php echo $file->file_name; ?></a>
					<?php //echo get_mime_by_extension(base_url().$file->file_path); ?>
				</li>
				<?php endforeach; ?>
			</ul>



		</div>	
	</body>
</html>