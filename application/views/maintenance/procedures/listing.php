
<h5>Procedures</h5>


<?php if($this->session->flashdata('fmesg') != ''): ?>
<div class="fmesg px-2 py-1 alert alert-primary mb-1" role="alert">
	<?php echo $this->session->flashdata('fmesg'); ?>
</div>
<?php endif; ?>

<div class="row">
	
	<div class=" col-md-2 col-sm-2">


		<div class="input-group mb-3">
				<?php if(isset($client_created)):?>
				 <input type="hidden" name="created_client" value="<?php echo htmlentities(json_encode(@$client_created));?>"/>
				<?php endif;?>
			  <input type="text" id="client_search" onkeyup="escalation.common.search_text(this,'.client_tbl');" class="form-control input-sm"  placeholder="Search for client.." aria-label="Search for client.." aria-describedby="basic-addon2" value="<?php echo @$client_created->client_name;?>">
			  <div class="input-group-append">
			    <button class="btn btn-outline-danger btn-sm" type="button" onclick="escalation.common.clear_search(this,'.client_tbl');"><i class="far fa-times-circle "></i></button>
			  </div>
			</div>

			<table class="client_tbl table table-sm ">
			  <thead>	  
			    <tr>
			      <th scope="col" class="table-primary" >CLIENT
			      </th>	      
			    </tr>
			  </thead>
			  <tbody>


				
				<?php foreach($clients as $row): ?>

			    <tr>
			      
			      <td><?php if( $this->client_id == 0 ): ?>
				  			<label><?php echo $row->client_name; ?></label>
				  			<a href="maintenance/procedures/?client_id=<?php echo $row->client_id; ?>"  class="btn btn-info btn-sm py-0 float-right"  >
				  				<i class="far fa-check-square"></i>&nbsp;
				  				
				  			</a>
				  		<?php else: ?>

				  			<label><?php echo $row->client_name; ?></label>

				  			<a href="maintenance/procedures/?client_id=<?php echo $row->client_id; ?>"  class="btn btn-info btn-sm py-0 float-right"  alt="<?php echo $this->client_id?>:<?php echo $row->client_name?>" >
					  			<i class="far fa-check-square "></i>&nbsp;
				  				
				  			</a>
				  		<?php endif; ?></td>
			      
			    </tr>
			
				<?php endforeach;?>		

			  </tbody>
			</table>
		</div>
	<div class="col-md-10 col-sm-10">	

		<div class="row">
			<div class="col-sm-6">
				<?php if( $this->client_id > 0 OR isset($procedures) ): ?>
					<strong>NEW: </strong>
					<a class="btn btn-sm btn-primary py-0" href="maintenance/procedure_form/API/add/<?php echo ($this->client_id == 0)?'?client_id='.$client->client_id:''; ?>">
						<i class="fas fa-code-branch"></i> 
						API
					</a>
					<a class="btn btn-sm btn-success py-0" href="maintenance/procedure_form/EMAIL/add/<?php echo ($this->client_id == 0)?'?client_id='.$client->client_id:''; ?>">
						<i class="fa fa-envelope-open"></i>
						EMAIL
					</a>
					<a class="btn btn-sm btn-danger py-0" href="maintenance/procedure_form/REMINDER/add/<?php echo ($this->client_id == 0)?'?client_id='.$client->client_id:''; ?>">
						<i class="fa fa-bell"></i>
						REMINDER
					</a>
				<?php endif; ?>				
			</div>
			<div class="col-sm-6 text-right">
				<h5><?php echo @$client->client_name; ?></h5>
			</div> 
		</div>

		<?php if( isset($procedures) AND count($procedures) > 0 ): ?>
			<table class="table table-sm">
				<thead>
					<th>Name</th>
					<th>Type</th>
					<th style="width: 150px;">Created</th>
					<th style="width: 150px;">Updated</th>
					<th style="width: 50px;">Status</th>
					<th style="width: 100px;"></th>
				</thead>
				<tbody>
					<?php foreach($procedures as $row): ?>
					<tr>
						<td><?php echo stripslashes($row->pro_name); ?></td>
						<td><?php 
							echo $row->pro_alert_type.' '; 

							switch ($row->pro_alert_type) {
									case 'API':
										//echo '<span class="text-primary">('.$row->api_key.')</span>';
										echo 'URL: ( <span class="text-primary"> '.base_url().$row->api_url.$row->api_key.'</span> )';
										break;
									case 'EMAIL':
										echo '<span class="text-success">('.$row->incoming_email.')</span>';
										break;
									case 'REMINDER':
										echo '<span class="text-info">('.$row->reminder_type.')</span>';
										
										switch ($row->reminder_type) {
											case 'EVERY_DAY':
												echo ' Every day at '.substr($row->reminder_time, 0, 5);

												break;
											case 'ONCE':
												echo ' '.date('d/m/Y H:i',strtotime($row->reminder_year.'-'.$row->reminder_month.'-'.$row->reminder_day.' '.$row->reminder_time));
												break;
											case 'WEEKLY':
												$reminder_weekday_opt = $this->Commonmodel->reminder_options('reminder_weekday');
												echo ' Every '.$reminder_weekday_opt[$row->reminder_weekday].' at '.substr($row->reminder_time, 0, 5);
												break;
											case 'FORTHNIGHTLY':
												echo ' Every 2 weeks starting '.date('l jS \of F Y',strtotime(date('Y').'-'.$row->reminder_month.'-'.$row->reminder_day)).' at '.substr($row->reminder_time, 0, 5);
												break;
											case 'MONTHLY':
												echo ' Every '.$row->reminder_day.date('S',strtotime(date('Y-m').'-'.$row->reminder_day)).' of the month  at '.substr($row->reminder_time, 0, 5);
												break;
											case 'YEARLY':
												echo ' Every '.$row->reminder_day.date('S',strtotime(date('Y').'-'.$row->reminder_month.'-'.$row->reminder_day)).' of '.date('F',strtotime(date('Y-').$row->reminder_day.'-'.$row->reminder_day)).' at '.substr($row->reminder_time, 0, 5);
												break;
											
											default:
												# code...
												break;
										}
 										
 										echo ((in_array($row->reminder_until_dt, array('','0000-00-00 00:00:00')))?'':' &nbsp;<strong class="text-danger"> Until '.date('d/m/Y H:i', strtotime($row->reminder_until_dt))).'</strong>'; 										

										break;
									
									default:
										# code...
										break;
								}	

						?></td>
						<td><?php echo date('d/m/Y H:i:s',strtotime($row->created_at)); ?></td>
						<td><?php echo ( !in_array($row->update_at, array('', '0000:00:00 00:00:00')) )?date('d/m/Y H:i:s',strtotime($row->update_at)):''; ?></td>
						<td><?php echo ($row->pro_status)?'<span class="text-success">Active</span>':'<span class="text-muted">Inactive</span>'; ?></td>
						<td class="text-center">
							<a href="maintenance/procedure_form/<?php echo $row->pro_alert_type; ?>/edit/?pro_id=<?php echo $row->pro_id; ?>" title="Edit" >
								<i class="fas fa-edit"></i>								
							</a>
						 	&nbsp;&nbsp;
							<a href="maintenance/procedure_audit_trail/<?php echo $row->pro_id; ?>" title="Audit Trail" >
								<i class="fas fa-history "></i>								
							</a>
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>


		<?php endif; ?>
	</div>

</div>