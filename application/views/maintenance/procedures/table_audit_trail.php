 
<div class="row border pt-2 pb-1 mb-1 bg-secondary text-white clearfix">
	<div class="col-sm-6 clearfix">		
		<h5 class="float-left px-2">
			<a class="text-white" href="maintenance/procedures/<?php echo ($this->client_id > 0)?'':'?client_id='.$procedure->client_id; ?>">
				<i class="fa fa-arrow-alt-circle-left "></i>
			</a>
		</h5>
		<h5 class="float-left"> Procedure - Audit Trail</h5> 
	</div>
	<div class="col-sm-6 clearfix">
		<h5 class="float-right"> [<?php echo stripslashes(@$procedure->pro_name); ?>]</h5> 		
	</div>
	
</div>  

<div class="row">
 
	<div class="col-sm-12">	
 
		<?php if( isset($records) AND count($records) > 0 ): ?>
			<table class="table table-sm">
				<thead>
					<th style="width: 250px;">Audit Details</th> 
					<th>Details</th> 
					<th>CMA Settings</th>
					<th></th>
				</thead>
				<tbody>
					<?php foreach($records as $row):  

						$json_data = json_decode($row->data_json);
						$cma_setting = json_decode(@$json_data->cma_settings); 
						$other_option = json_decode(@$json_data->other_options); 
						$audit_type = array('I'=>'Add', 'U'=>'Update'); 
					?>
					<tr>
						<td>
						<?php 
							echo '<strong>Date: </strong> '.date('d/m/Y', $row->created).'<br />'; 						 
							echo '<strong>Time: </strong> '.date('H:i:s', $row->created).'<br />'; 						 
							echo '<strong>Type : </strong> '.@$audit_type[$row->audit_type].'<br />'; 
							echo '<strong>Audit By :</strong> '.@$row->created_by; 
						?>
						</td>
						<td>
						<?php 
							echo '<strong>Alert Type:</strong> '.@$procedure->pro_alert_type.'<br />'; 
							echo '<strong>Description:</strong> '.@stripslashes($json_data->pro_name).'<br />'; 
							echo '<strong>Priority Flag:</strong> '.@$this->priority_flag[$json_data->priority_flag].'<br />'; 
							echo '<strong>Display Screen:</strong> '.@$this->display_screen[$json_data->display_screen].'<br />'; 
							echo '<strong>Call Response Group:</strong> '.@$json_data->pro_name.'<br />'; 
							echo '<br />';
					 
							echo '<strong>Email each call response:</strong> '.(($other_option->email->each_call->is_send==1)?'Yes':'No');
							echo ' '.@$other_option->email->each_call->email_addrs.'<br />'; 
							
							echo '<strong>Email on CLOSE call response:</strong> '.(($other_option->email->on_close->is_send==1)?'Yes':'No');
							echo ' '.@$other_option->email->on_close->email_addrs.'<br />'; 

						?>							
						</td>  
						<td>
						<?php 
							echo '<strong>CMA ID: </strong> '.@$cma_setting->cma_id.'<br />';
							echo '<strong>CMA DB: </strong> '.@$cma_setting->cma_db.'<br />';
							echo '<strong>CUST ID: </strong> '.@$cma_setting->cust_id.'<br />';
							echo '<strong>Contact ID - issue: </strong> '.@$cma_setting->contact_id_issue.'<br />';
							echo '<strong>Contact ID - calls made: </strong> '.@$cma_setting->contact_id_callsmade.'<br />';

						?>							
						</td>
						<td><?php 
							
							/*echo '<pre>';
							print_r($json_data);
							echo '</pre>';*/ 
							switch ($procedure->pro_alert_type) {
									case 'API':
										echo '<span class="text-primary">'.@$json_data->api_key.'</span>';
										break;
									case 'EMAIL':
										echo '<span class="text-success">'.@$json_data->incoming_email.'</span>';
										break;
									case 'REMINDER':
										echo '<span class="text-info">'.@$json_data->reminder_type.'</span>';
										
										switch (@$json_data->reminder_type) {
											case 'EVERY_DAY':
												echo ' Every day at '.substr($json_data->reminder_time, 0, 5);

												break;
											case 'ONCE':
												echo ' '.date('d/m/Y H:i',strtotime($json_data->reminder_year.'-'.$json_data->reminder_month.'-'.$json_data->reminder_day.' '.$json_data->reminder_time));
												break;
											case 'WEEKLY':
												$reminder_weekday_opt = $this->Commonmodel->reminder_options('reminder_weekday');
												echo ' Every '.$reminder_weekday_opt[$json_data->reminder_weekday].' at '.substr($json_data->reminder_time, 0, 5);
												break;
											case 'FORTHNIGHTLY':
												echo ' Every 2 weeks starting '.date('l jS \of F Y',strtotime(date('Y').'-'.$json_data->reminder_month.'-'.$json_data->reminder_day)).' at '.substr($json_data->reminder_time, 0, 5);
												break;
											case 'MONTHLY':
												echo ' Every '.$json_data->reminder_day.date('S',strtotime(date('Y-m').'-'.$json_data->reminder_day)).' of the month  at '.substr($json_data->reminder_time, 0, 5);
												break;
											case 'YEARLY':
												echo ' Every '.$json_data->reminder_day.date('S',strtotime(date('Y').'-'.$json_data->reminder_month.'-'.$json_data->reminder_day)).' of '.date('F',strtotime(date('Y-').$json_data->reminder_day.'-'.$json_data->reminder_day)).' at '.substr($json_data->reminder_time, 0, 5);
												break;
											
											default:
												# code...
												break;
										}
 										
 										echo ((in_array(@$json_data->reminder_until_dt, array('','0000-00-00 00:00:00')))?'':' &nbsp;<strong class="text-danger"> Until '.date('d/m/Y H:i', strtotime(@$json_data->reminder_until_dt))).'</strong>'; 										

										break;
									
									default:
										# code...
										break;
								}	

						?></td> 
					</tr>
					<tr>
						<td></td> 
						<td colspan="3">
							<?php echo ($procedure->pro_alert_type == 'REMINDER')?'<strong>Alert Details</strong><br />'.stripslashes($procedure->reminder_alert_details).'<br /><br />':''; ?>
							<strong>Procedure</strong>
							<?php echo stripslashes($json_data->pro_content); ?>
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>


		<?php endif; ?>
	</div>

</div>