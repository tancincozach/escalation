
 
<div class="row border pt-2 pb-1 mb-1 bg-secondary text-white">
	<div class="col-sm-4  pl-0 clearfix">
		<h5 class="float-left px-2">
		<a class="text-white" href="maintenance/procedures/<?php echo ($this->client_id > 0)?'':'?client_id='.$procedure->client_id; ?>">
			<i class="fa fa-arrow-alt-circle-left "></i>
		</a>
		</h5>
		<h5 class="float-left"> Procedure - FORM </h5>
	</div>
	<div class="col-sm-4"><h5><?php echo strtoupper($form_type); ?></h5></div>
	<div class="col-sm-4 clearfix">
		
		<?php if( isset($procedure->pro_id) ): ?>
		<a class="float-right text-white" href="maintenance/procedure_audit_trail/<?php echo @$procedure->pro_id; ?>" title="Audit Trail" >
			<i class="fas fa-history "></i>								
		</a>
		<?php endif; ?>

		<h5 class="ml-2 float-right"><?php echo @$client->client_name; ?></h5>		
	</div>
</div>
 

<?php if($this->session->flashdata('fmesg') != ''): ?>
<div class="fmesg px-2 py-1 alert alert-primary mb-1" role="alert">
	<?php echo $this->session->flashdata('fmesg'); ?>
</div>
<?php endif; ?>

<div class="row clearfix">
	
	<div class="col-lg-10 col-sm-10">
		<?php echo form_open('', ' id="procedure_form" class="" role="form" method="post" onsubmit=" return confirm(\'Confirm Saving Procedure?\'); "'); ?>	

		<input type="hidden" name="pro_id" value="<?php echo @$procedure->pro_id; ?>">	
		<input type="hidden" name="client_id" id="client_id" value="<?php echo @$procedure->client_id; ?>">	 
		<input type="hidden" name="form_type" value="<?php echo @$form_type; ?>">	 
		<input type="hidden" name="pro_alert_type" value="<?php echo @$procedure->pro_alert_type; ?>">	 
	 
		<div class="form-group row mb-1">
			<label class="col-lg-2 col-sm-2 control-label" for="pro_name">Alert Type</label>
			<div class="col-lg-8 col-sm-10">
				<?php echo @$procedure->pro_alert_type; ?>
			</div>
		</div>

		<?php if( @$procedure->pro_alert_type == 'API' ): ?>

			<div class="form-group row mb-1">
				<label class="col-lg-2 col-sm-2 control-label text-right" for="api_url">API URL</label>
								
					
				<?php if( empty($procedure->api_url) ): ?>
					<div class="col-lg-2 col-sm-4">
						<select name="api_url" class="custom-select custom-select-sm mb-1" >
							<option value="api/v1/">API - v1</option>							
							<option value="webhook_pinpoint/alert/">Pinpoint</option>							
						</select>
					</div>
				<?php else: ?>
					<div class="col-lg-8 col-sm-10">
						<span class="text-primary"><?php echo base_url().$procedure->api_url.$procedure->api_key; ?></span>
					</div>
				<?php endif; ?> 

			</div> 

			<!-- <div class="form-group row mb-1">
				<label class="col-lg-2 col-sm-2 control-label text-right" for="custom_data_storage">Has own data storage</label>
				<div class="col-lg-8 col-sm-10">				
					
					<div class="row">
						<select>
							<option value="1">Yes</option>
							<option value="0">No</option>
						</select>
					</div>				

				</div>
			</div>  -->			

			<div class="form-group row mb-1">
				<label class="col-lg-2 col-sm-2 control-label text-right" for="custom_data_storage">API Data storage</label>
				<div class="col-lg-8 col-sm-10">

					<?php if( empty($procedure->client_data_src) ): ?>
						<div class="row">	
							<div class="col-sm-4 px-3">
								<input type="text" id="client_data_src" name="client_data_src" pattern="[a-z]{5,15}" maxlength="15" class="form-control form-control-sm" value="">
							</div>
							<div class="col-sm-2">
								<button class="btn btn-sm btn-info my-0" type="button" onclick="Procedure.check_client_data_src()" >Check If Available</button> 
							</div>
							<small class="form-text text-muted">
							  IF API Data storage is not required please leave empty otherwise must be 5-15 characters long, contain small letters only and no spaces, special characters.
							</small>	
						</div>	
					<?php else: ?>
						<?php echo @$procedure->client_data_src; ?>
					<?php endif; ?> 
				</div>
			</div> 

		<?php elseif( @$procedure->pro_alert_type == 'EMAIL' ): ?>

			<div class="form-group row mb-1">
				<label class="col-lg-2 col-sm-2 control-label" for="incoming_email">Incoming Email</label>
				<div class="col-lg-8 col-sm-10">				
					
					<div class="row">
						<div class="col-sm-8 px-3">
							<input type="text" required="required" id="incoming_email" name="incoming_email" maxlength="100" class="form-control form-control-sm" value="<?php echo stripslashes(@$procedure->incoming_email); ?>">
						</div>
						<div class="col-sm-2">
							<button class="btn btn-sm btn-info" type="button" onclick="Procedure.check_email()" >Check EMAIL</button> 
						</div>
					</div>				

				</div>
			</div> 

			<div class="form-group row mb-1">
				<label class="col-lg-2 col-sm-2 control-label" for="display_subject_line">Display Subject Line</label>
				<div class="col-sm-2">
					<select required="required" class="custom-select custom-select-sm mb-1" name="display_subject_line">				
						<option value="0" <?php echo (@$procedure->display_subject_line=='0')?'selected':''; ?>>NO</option>
						<option value="1" <?php echo (@$procedure->display_subject_line=='1')?'selected':''; ?> >YES</option> 
					</select>
				</div>
			</div>

		<?php elseif( @$procedure->pro_alert_type == 'SMS' ): ?>


			<div class="form-group row mb-1">
				<label class="col-lg-2 col-sm-2 control-label" for="incoming_mobileno">Incoming Mobile No</label>
				<div class="col-lg-8 col-sm-10">
					<input type="text" required="required" id="incoming_mobileno" name="incoming_mobileno" maxlength="100" class="form-control form-control-sm" value="<?php echo stripslashes(@$procedure->incoming_mobileno); ?>">
				</div>
			</div>
 

		<?php elseif( @$procedure->pro_alert_type == 'REMINDER' ): ?>
 
			<div id="reminder_div">
				<div class="form-group row mb-1">
					<label class="col-sm-2 control-label text-right" for="reminder_type">Type</label>
					<div class="col-sm-2"> 
						<?php echo form_dropdown('reminder_type', $this->Commonmodel->reminder_options('reminder_type'), @$procedure->reminder_type, ' required="required" class="custom-select custom-select-sm mb-1" onchange="Procedure.reminder.onchange_type(this)" '); ?>
					</div>
				</div>

				<div class="form-group row mb-1">
					<label class="col-sm-2 control-label text-right" for="reminder_weekday">Week Day</label>
					<div class="col-lg-2 col-md-2">					 
						<?php echo form_dropdown('reminder_weekday', $this->Commonmodel->reminder_options('reminder_weekday'), @$procedure->reminder_weekday, ' required="required" class="custom-select custom-select-sm mb-1" '); ?>
					</div>
				</div>

				<div class="form-group row mb-1">
					<label class="col-sm-2 control-label text-right" for="reminder_day">Day</label>
					<div class="col-lg-2 col-md-2">					 
						<?php echo form_dropdown('reminder_day', $this->Commonmodel->reminder_options('reminder_day'), @$procedure->reminder_day, ' required="required" class="custom-select custom-select-sm mb-1" '); ?>
					</div>
				</div>

				<div class="form-group row mb-1">
					<label class="col-sm-2 control-label text-right" for="reminder_month">Month</label>
					<div class="col-lg-2 col-md-2">					 
						<?php echo form_dropdown('reminder_month', $this->Commonmodel->reminder_options('reminder_month'), @$procedure->reminder_month, ' required="required" class="custom-select custom-select-sm mb-1" '); ?>
					</div>
				</div>

				<div class="form-group row mb-1">
					<label class="col-sm-2 control-label text-right" for="reminder_year">Year</label>
					<div class="col-lg-2 col-md-2">					 
						<?php echo form_dropdown('reminder_year', $this->Commonmodel->reminder_options('reminder_year'), @$procedure->reminder_year, ' required="required" class="custom-select custom-select-sm mb-1" '); ?>
					</div>
				</div>

				<!-- <div class="form-group row mb-1">
					<label class="col-lg-2 col-sm-2 control-label text-right" for="reminder_time">Time</label>
					<div class="col-lg-2 col-md-2">		
						<input type="text" required="required" id="reminder_time" name="reminder_time" maxlength="100" class="form-control form-control-sm" value="<?php echo substr(@$procedure->reminder_time, 0, 5); ?>">
					</div>
				</div>	 -->

				<div class="form-group row mb-1">
					<label class="col-lg-2 col-sm-2 control-label text-right" for="reminder_time">Time</label>
					<div class="col-lg-2 col-md-2">
		                <div class="input-group date" id="reminder_time" data-target-input="nearest">
		                    <input type="text" class="form-control form-control-sm datetimepicker-input" data-target="#reminder_time" name="reminder_time" value="<?php echo substr(@$procedure->reminder_time, 0, 5); ?>"/>
		                    <div class="input-group-append" data-target="#reminder_time" data-toggle="datetimepicker">
		                        <div class="input-group-text px-1 text-primary"><i class="fa fa-clock"></i></div>
		                    </div>
		                </div>
					</div>
				</div>	

				<div class="form-group row mb-1">
					<label class="col-lg-2 col-sm-2 control-label text-right" for="reminder_until_dt">Repeat Until</label>
					<div class="col-lg-2 col-md-2">		
						<!-- <input type="text" name="reminder_until_dt" id="reminder_until_dt" class="form-control form-control-sm" value="<?php echo (!in_array(@$procedure->reminder_until_dt, array('', '0000-00-00 00:00:00')))?date('Y-m-d H:i:s', strtotime($procedure->reminder_until_dt)):''; ?>" > -->
					
		                <div class="input-group date" id="reminder_until_dt" data-target-input="nearest">
		                    <input type="text" class="form-control form-control-sm datetimepicker-input" data-target="#reminder_until_dt" name="reminder_until_dt" value="<?php echo (!in_array(@$procedure->reminder_until_dt, array('', '0000-00-00 00:00:00')))?date('Y-m-d H:i:s', strtotime($procedure->reminder_until_dt)):''; ?>"/>
		                    <div class="input-group-append" data-target="#reminder_until_dt" data-toggle="datetimepicker">
		                        <div class="input-group-text px-1 text-primary"><i class="fa fa-calendar"></i></div>
		                    </div>
		                </div>
					</div>
				</div>	



				<div class="form-group row mb-1">
					<label class="col-lg-2 col-sm-2 control-label text-right" for="reminder_alert_details">Alert Details</label>
					<div class="col-lg-8 col-sm-10">
						<textarea class="form-control" name="reminder_alert_details" rows="3"><?php echo stripslashes(@$procedure->reminder_alert_details); ?></textarea>
					</div>
				</div>

			</div>

		<?php endif; ?>

		<div class="form-group row mb-1">
			<label class="col-lg-2 col-sm-2 control-label" for="reminder_alert_duration">Duration</label>
			<div class="col-lg-1 col-md-1 pr-0">		
				<input type="text" name="reminder_alert_duration" class="form-control form-control-sm" value="<?php echo (@$procedure->reminder_alert_duration=='')?5:$procedure->reminder_alert_duration; ?>" required>						
			</div>
			minute(s). <span class="">This is used on Alert Register Date/Time DUE.</span>
		</div>

		<div class="form-group row mb-1">
			<label class="col-lg-2 col-sm-2 control-label" for="pro_name">Description</label>
			<div class="col-lg-8 col-sm-10">
				<input type="text" required="required" id="pro_name" name="pro_name" maxlength="100" class="form-control form-control-sm" value="<?php echo stripslashes(@$procedure->pro_name); ?>">
			</div>
		</div>

		<div class="form-group row mb-1">
			<label class="col-sm-2 control-label" for="call_direction">Priority Flag</label>
			<div class="col-sm-2">
				<select required="required" class="custom-select custom-select-sm mb-1" name="priority_flag">
					<option></option> 
					<?php foreach($this->priority_flag as $fkey=>$fval): ?>
						<option value="<?php echo $fkey; ?>" <?php echo (@$procedure->priority_flag == $fkey)?'selected':''; ?> ><?php echo $fval; ?></option> 					
					<?php endforeach; ?>

				</select>
			</div>
		</div>

		<div class="form-group row mb-1">
			<label class="col-sm-2 control-label" for="call_direction">Display Screen</label>
			<div class="col-sm-2">
				<select required="required" class="custom-select custom-select-sm mb-1" name="display_screen">
					<option></option> 
					<?php foreach($this->display_screen as $dskey=>$dsval): ?>
						<option value="<?php echo $dskey; ?>" <?php echo (@$procedure->display_screen == $dskey)?'selected':''; ?> ><?php echo $dsval; ?></option> 					
					<?php endforeach; ?>

				</select>
			</div>
		</div>

		<div class="form-group row mb-1">
			<label class="col-lg-2 col-sm-2 control-label" for="pro_name">Procedure</label>
			<div class="col-lg-8 col-sm-10">
				<textarea class="form-control" name="pro_content" id="pro_content"  rows="3"><?php echo stripslashes(@$procedure->pro_content); ?></textarea>
			</div>
		</div>
		
		<div class="form-group row mb-1">
			<label class="col-lg-2 col-sm-2 control-label" for="cma_db">Call Response Group</label>
			<div class="col-sm-2">
				<select required="required" class="custom-select custom-select-sm mb-1" name="call_group">
					<option value=""></option>
					<?php foreach($call_group as $group): 
						if( trim($group->call_group ) == '' ) continue;
					 ?>
						<option value="<?php echo $group->call_group; ?>" <?php echo (@$procedure->call_group == $group->call_group)?'selected':''; ?> ><?php echo $group->call_group; ?></option> 					
					<?php endforeach; ?>
					<?php  if(count($call_form_not_required) > 0): ?>
						<option value="" disabled="">CALL FORM not required</option>
					<?php endif; ?>
					<?php 
 
					foreach($call_form_not_required as $group):?>
						<option value="<?php echo $group->call_res_id; ?>" <?php echo (@$procedure->call_group == $group->call_res_id)?'selected':''; ?> ><?php echo $group->call_res_text; ?></option> 					
					<?php endforeach; ?>
				</select>
				
			</div>
		</div>
		
		<div class="form-group row mb-1">
			<label class="col-lg-2 col-sm-2 control-label" for="call_lookup_id">Call Response Lookup</label>
			<div class="col-sm-2">
				 <?php echo form_dropdown("call_lookup_id", @$call_lookup_options, @$procedure->call_lookup_id, ' class="custom-select custom-select-sm mb-1"'); ?>
			</div>
		</div>

		<div class=" my-2 py-1">
			<h6><strong>CMA Settings</strong></h6>

			<div class="form-group row mb-1">
				<label class="col-lg-2 col-sm-2 control-label text-right" for="cma_id">CMA ID</label>
				<div class="col-sm-2">
					<input type="text" required="required" id="cma_id" name="cma_settings[cma_id]" maxlength="100" class="form-control form-control-sm" value="<?php echo @$cma_settings->cma_id; ?>">
				</div>
			</div>

		
			<div class="form-group row mb-1">
				<label class="col-lg-2 col-sm-2 control-label text-right" for="cma_db">CMA DB</label>
				<div class="col-sm-2">
					<select required="required" class="custom-select custom-select-sm mb-1" name="cma_settings[cma_db]">
						<option value=""></option>
						<option value="cma_international" <?php echo (@$cma_settings->cma_db=='cma_international')?'selected':''; ?> >International</option> 
						<option value="cma_melbourne" <?php echo (@$cma_settings->cma_db=='cma_melbourne')?'selected':''; ?> >Melbourne</option> 
						<option value="cma_nowra" <?php echo (@$cma_settings->cma_db=='cma_nowra')?'selected':''; ?> >Nowra</option> 
						<option value="cma_site4" <?php echo (@$cma_settings->cma_db=='cma_site4')?'selected':''; ?> >Site4</option> 
					</select>
					
				</div>
			</div> 

			<div class="form-group row mb-1">
				<label class="col-lg-2 col-sm-2 control-label text-right" for="cust_id">CUST ID</label>
				<div class="col-sm-2">
					<input type="text" required="required" id="cust_id" name="cma_settings[cust_id]" maxlength="100" class="form-control form-control-sm" value="<?php echo stripslashes(@$cma_settings->cust_id); ?>">
				</div>
			</div>  

			<div class="form-group row mb-1">
				<label class="col-lg-2 col-sm-2 control-label text-right" for="contact_id_issue">Contact ID - issue</label>
				<div class="col-sm-2">
					<input type="text" required="required" id="contact_id_issue" name="cma_settings[contact_id_issue]" maxlength="100" class="form-control form-control-sm" value="<?php echo stripslashes(@$cma_settings->contact_id_issue); ?>">
				</div>
			</div>  

			<div class="form-group row mb-1">
				<label class="col-lg-2 col-sm-2 control-label text-right" for="contact_id_callsmade">Contact ID - calls made</label>
				<div class="col-sm-2">
					<input type="text" required="required" id="contact_id_callsmade" name="cma_settings[contact_id_callsmade]" maxlength="100" class="form-control form-control-sm" value="<?php echo stripslashes(@$cma_settings->contact_id_callsmade); ?>">
				</div>
			</div> 


			<h6 class="ml-4"><strong>Auto Log Initial Alert(For PINPOINT API and Incoming Email only)</strong></h6>
			<div class="form-group row mb-1">
				<label class="col-lg-2 col-sm-2 control-label text-right" for="contact_id_callsmade">Auto Initial Log</label>
				<div class="col-sm-2">					

					<select required="required" class="custom-select custom-select-sm mb-1" id="auto_log_initial" name="cma_settings[auto_log_initial]">						
						<option value="0" <?php echo (@$cma_settings->auto_log_initial=='0' OR @$cma_settings->auto_log_initial == '')?'selected':''; ?> >No</option> 
						<option value="1" <?php echo (@$cma_settings->auto_log_initial=='1')?'selected':''; ?> >Yes</option>  
					</select>

				</div>
			</div> 

			<div class="form-group row mb-1">
				<label class="col-lg-2 col-sm-2 control-label text-right" for="aut_log_contactid">Auto Log - Contact ID</label>
				<div class="col-sm-2">
					<input type="text"  id="aut_log_contactid" name="cma_settings[aut_log_contactid]" maxlength="100" class="form-control form-control-sm" value="<?php echo stripslashes(@$cma_settings->aut_log_contactid); ?>">
				</div>
			</div> 


		</div> 

		<div class="form-group row mb-1">
			<label class="col-sm-2 control-label" for="cr_options[email_each_call[is_send]">Email each call response</label>
			<div class="col-sm-8">
				<div class="row">
					<div class="col-sm-3 px-3">
						<select required="required" class="custom-select custom-select-sm mb-1" name="cr_options[email][each_call][is_send]">				
							<option></option> 
							<option value="1" <?php echo (@$cr_options->email->each_call->is_send=='1')?'selected':''; ?> >Yes</option> 
							<option value="0" <?php echo (@$cr_options->email->each_call->is_send=='0')?'selected':''; ?>>No</option>
						</select>
					</div>

					<div class="col-sm-8">
						<input type="text" name="cr_options[email][each_call][email_addrs]" id="cr_options_email_each_call_email_addrs" class="form-control form-control-sm" value="<?php echo stripslashes(@$cr_options->email->each_call->email_addrs); ?>">
					</div>
				</div>
			</div>
		</div>

		<div class="form-group row mb-1">
			<label class="col-sm-2 control-label" for="cr_options[email][on_close][is_send]">Email on CLOSE call response</label>
			<div class="col-sm-8">
				<div class="row">
					<div class="col-sm-3 px-3">
						<select required="required" class="custom-select custom-select-sm mb-1" name="cr_options[email][on_close][is_send]">				
							<option></option> 
							<option value="1" <?php echo (@$cr_options->email->on_close->is_send=='1')?'selected':''; ?> >Yes</option> 
							<option value="0" <?php echo (@$cr_options->email->on_close->is_send=='0')?'selected':''; ?>>No</option>
						</select>
					</div>

					<div class="col-sm-8">
						<input type="text" name="cr_options[email][on_close][email_addrs]" id="cr_options_email_on_close_email_addrs" class="form-control form-control-sm" value="<?php echo stripslashes(@$cr_options->email->on_close->email_addrs); ?>">
					</div>
				</div>
			</div>
		</div>

		<div class="form-group row mb-1">
			<label class="col-sm-2 control-label" for="cr_options[limit_text][type]">Limiting TEXT to display in CMA (<i>Multiple CLOSE</i>)</label>
			<div class="col-sm-8">
				<div class="row">
					<div class="col-sm-3 px-3">
						<select class="custom-select custom-select-sm mb-1" name="cr_options[limit_text][type]">				
							<option value="">--none--</option> 
							<option value="1" <?php echo (@$cr_options->limit_text->type=='1')?'selected':''; ?>>Break by word(s)</option> 
							<option value="2" <?php echo (@$cr_options->limit_text->type=='2')?'selected':''; ?>>By character limit</option>
						</select>
					</div>

					<div class="col-sm-8">
						<input type="text" name="cr_options[limit_text][type_val]" id="cr_options_limit_text" class="form-control form-control-sm" value="<?php echo stripslashes(@$cr_options->limit_text->type_val); ?>">
					</div>
				</div>
				<div class="row">
					<ul>
						<li><strong>Break by word(s)</strong> - Send content before the word(s) specify</li>
						<li><strong>By character limit</strong> - Send content base on character limit being set.</li>
					</ul>
				</div>
			</div>
		</div>

		<div class=" my-2 py-1">
			
			<h6><strong>Email Output</strong></h6>

			<div class="form-group row mb-1">
				<label class="col-sm-2 control-label text-right" for="cr_options[email_output][show_ad]">Show Alert Details</label>
				<div class="col-sm-8">
					<div class="row">
						<div class="col-sm-3 px-3">
							<select required="required" class="custom-select custom-select-sm mb-1" name="cr_options[email_output][show_ad]">				
								<option value=""></option> 
								<option value="1" <?php echo (@$cr_options->email_output->show_ad=='1')?'selected':''; ?> >Yes</option> 
								<option value="0" <?php echo (@$cr_options->email_output->show_ad=='0')?'selected':''; ?>>No</option>
							</select>
						</div> 
					</div>
				</div>
			</div>

			<div class="form-group row mb-1">
				<label class="col-sm-2 control-label text-right" for="cr_options[email_output][show_ca]">Show Call Activity</label>
				<div class="col-sm-8">
					<div class="row">
						<div class="col-sm-3 px-3">
							<select required="required" class="custom-select custom-select-sm mb-1" name="cr_options[email_output][show_ca]">				
								<option value=""></option> 
								<option value="1" <?php echo (@$cr_options->email_output->show_ca=='1')?'selected':''; ?> >Yes</option> 
								<option value="0" <?php echo (@$cr_options->email_output->show_ca=='0')?'selected':''; ?>>No</option>
							</select>
						</div> 
					</div>
				</div>
			</div>
		</div>

		<div class=" my-2 py-1">
			
			<h6><strong>Allow Multiple CLOSE</strong></h6>

			<div class="form-group row mb-1">
				<label class="col-sm-2 control-label text-right" for="allow_multi_del"><span style="font-size: 85%">ALLOW MULTIPLE DELETIONS</span></label>
				<div class="col-sm-2">
					<select required="required" class="custom-select custom-select-sm mb-1" name="allow_multi_del">				
						<option value="0" <?php echo (@$procedure->allow_multi_del=='0')?'selected':''; ?>>NO</option>
						<option value="1" <?php echo (@$procedure->allow_multi_del=='1')?'selected':''; ?> >Yes</option> 
					</select>
				</div>
			</div>
 

			<div class="form-group row mb-1">
				<label class="col-lg-2 col-sm-2 control-label text-right" for="multi_del_callres">Multi Close Call Response Set</label>
				<div class="col-sm-2">
					 <?php echo form_dropdown("multi_del_callres", @$multi_closed_cr, @$procedure->multi_del_callres, ' class="custom-select custom-select-sm mb-1"'); ?>
				</div>
			</div>
		</div> <!-- Allow Multiple CLOSE -->

		<div class="form-group row mb-1">
			<label class="col-sm-2 control-label" for="pro_status">Status</label>
			<div class="col-sm-2">
				<select required="required" class="custom-select custom-select-sm mb-1" name="pro_status">				
					<option value="1" <?php echo (@$procedure->pro_status=='1')?'selected':''; ?> >Active</option> 
					<option value="0" <?php echo (@$procedure->pro_status=='0')?'selected':''; ?>>Inactive</option>
				</select>
			</div>
		</div>
		


		<br />
		<hr />
		<div class="form-group row mb-1">
			<label class="col-lg-2 col-sm-2 control-label" for=""></label>
			<div class="col-lg-8 col-sm-10">
				<button class="btn btn-sm btn-primary mr-2" type="submit" name="submit" >Save</button> 
				<a class="btn btn-sm btn-secondary" href="maintenance/procedures/<?php echo ($this->client_id > 0)?'':'?client_id='.$procedure->client_id; ?>">Back</a>
			</div>
		</div>
 
	</div>	


	<div class="col-lg-2 col-sm-2">
		
		<a href="maintenance/callresponse/<?php echo ($this->client_id == 0)?'?client_id='.$client->client_id:''; ?>" class="btn btn-sm btn-primary mb-1" target="_blank">
			<i class="fas fa-phone-square"></i>
			Call Response
		</a>
		<br />
		<a href="maintenance/file_manager/<?php echo ($this->client_id == 0)?'?client_id='.$client->client_id:''; ?>" class="btn btn-sm btn-primary mb-1" target="_blank">
			<i class="fas fa-folder-open"></i>
			File Manager
		</a>
		
	</div>

</div>


