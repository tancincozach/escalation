<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Callresponsemodel extends CI_Model { 


	function insert($set){

        $agent_name = ''; 

        if( isset($set['agent_name']) ) {
            $agent_name = @$set['agent_name']; 
            unset($set['agent_name']);
        }


		try {
			
			if( empty($set) ) throw new Exception("Empty insert data", 1);		
			
			if( $this->db->insert('call_responses', $set) ){

	  			$cr_id =  $this->db->insert_id();

                 $this->insert_audit_trail(
                    array(
                        'ref_table'=>'call_responses',
                        'ref_field'=>'call_res_id',
                        'audit_type'=>'I',
                        'ref_val'=>$cr_id,
                        'created_by'=>$agent_name,
                        'created'=>strtotime("now"),
                        'data_json'=> json_encode($set)
                    ));


                 return $cr_id;
	  		}else{
	  			return false;
	  		}
			

		} catch (Exception $e) {
			return false;
		}

	}
  

	function update($call_res_id, $set){
		$call_res_id = trim($call_res_id);
		

        $agent_name = ''; 

        if( isset($set['agent_name']) ) {
            $agent_name = @$set['agent_name']; 
            unset($set['agent_name']);
        }

		try {
			
			if( empty($call_res_id) ) throw new Exception("call_res_id is required", 1);
			if( empty($set) ) throw new Exception("set param is required", 1);
						
			$this->db->where('call_res_id', $call_res_id);
			$this->db->update('call_responses', $set);

             	$this->insert_audit_trail(
	                array(
	                    'ref_table'=>'call_responses',
	                    'ref_field'=>'call_res_id',
	                    'audit_type'=>'U',
	                    'ref_val'=>$call_res_id,
	                    'created_by'=>$agent_name,
	                    'created'=>strtotime("now"),
	                    'data_json'=> json_encode($set)
	                ));

             	return $call_res_id;

		} catch (Exception $e) {
			return false;
		}

	}	

	function delete($call_res_id){
		
		try {
			
			if( empty($call_res_id) ) throw new Exception("call_res_id is required", 1);
					
			return $this->db->delete('call_responses', array('call_res_id' => $call_res_id));  
			
		} catch (Exception $e) {
			return false;
		}

	}

 	function row($params){

 		try {
 			
 			if( empty($params) ) throw new Exception("Params is required", 1);
 			

			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

			$query = $this->db->get('call_responses');

			return $query->row();

 		} catch (Exception $e) {
 			return false;
 		}


 	}	

 	function listing($params=array(), $paging=TRUE){

 		try { 			

 			//if( empty($params) ) throw new Exception("Can't return full list", 1);
 			
 			if( $paging ){

	 			//TOTAL ROWS
	 			
				//where clause
				if(isset($params['where'])){
					$this->db->where($params['where']);
				}

				if(isset($params['where_str']) && $params['where_str']!='' ){
					$this->db->where($params['where_str'], null, false);
				}

	 			$this->db->select('count(*) as total');

				$this->db->join('client', 'client ON client.client_id = call_responses.client_id', 'LEFT OUTER');
	 			$query = $this->db->get('call_responses');
				
				$total_rows = $query->row()->total;
				$query->free_result(); //free results
			}
			
			//RESULTS
			
			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

			//limits
			if(isset($params['limits'])){
				$this->db->limit($params['limits']['limit'], $params['limits']['start']); 
			}

			//sorting
			if( isset($params['sorting']) ){
				if( is_array($params['sorting']) ){
					$this->db->order_by($params['sorting']['sort'], $params['sorting']['order']);
				}else{
					$this->db->order_by($params['sorting']);
				}

			}else{
				$this->db->order_by('`call_responses`.`call_res_created`', 'desc');
			}

			$this->db->join('client', 'client ON client.client_id = call_responses.client_id', 'LEFT OUTER');
 			$query = $this->db->get('call_responses');

			$result = $query->result();


			$query->free_result(); //free results

			 
			return ($paging) ? array('results'=>$result, 'total_rows'=>$total_rows) : $result;
			 

 		} catch (Exception $e) {
 			return false;
 		}

 	}


 	function group_names( $client_id=''){
 		try {			

			$query = $this->db->where('client_id',$client_id)
							  ->select('call_group')
							  ->group_by('call_group','ASC')
							  ->order_by('call_group')
							  ->get('call_responses');

			return $query->result();


 			
 		} catch (Exception $e) {
 				return false;
 			
 		}

 		
 	}



 	function dropown( $client_id=''){

 		try {	

 			if( empty($client_id)) throw new Exception("Error Processing Request", 1);
 			

 			$this->db->select('call_res_id, call_res_text');
 			$this->db->where('client_id',$client_id);
 			$this->db->where('call_res_status', 1);
 			$this->db->order_by('call_res_text', 'ASC');
			$query = $this->db->get('call_responses');

			$options = array();
			$options[''] = '';
			foreach ($query->result() as $row) {
				$options[$row->call_res_id] = $row->call_res_text;
			}

			return $options;

 			
 		} catch (Exception $e) {
			return false;
 			
 		}

 		
 	}


 	function call_form_not_required( $client_id=''){
 		try {			

			$query = $this->db->where('client_id',$client_id)
							  ->where('call_form_required', 0)
							  ->select('call_res_id, call_res_text')							  
							  ->order_by('call_res_text', 'ASC')
							  ->get('call_responses');

			return $query->result();


 			
 		} catch (Exception $e) {
 				return false;
 			
 		}

 		
 	}


    function insert_audit_trail( $params ) {   

        try
            {
            if(count($params)==0) throw new Exception("Error : Empty Parameter", 1);

            if(!isset($params['ref_table']))
                throw new Exception("Error : Table name must not be empty.");

            if(!isset($params['ref_field']) && !isset($params['ref_val']))
                throw new Exception("Error : Table index name and value  must not be empty.");      
            if(!isset($params['created_by']))
                throw new Exception("Error : Agent Name  must not be empty.");
            if(!isset($params['data_json']))
                throw new Exception("Error : Message Activity  must not be empty.");            

            return ($this->db->insert('table_audit_trail', $params))?$this->db->insert_id():0;

        }catch(Exception $error) { 
            return  $error->getMessage();
        }
    }


  	function get_audit_trail( $pro_id = ''){
    
    	try{

			if($pro_id=='') throw new Exception("Error : pro_id is required", 1);

			$query = $this->db
			        ->where('ref_table', 'client_procedure')
			      	->where('ref_field','pro_id')
			      	->where('ref_val',$pro_id)
			      	->get('table_audit_trail'); 

			 
			return $query->result(); 
			

		}catch(Exception $error){ 
			return  0;
		}
  	}    


}