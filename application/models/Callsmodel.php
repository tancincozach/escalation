<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Callsmodel extends CI_Model { 


	function insert($set){

		try {
			
			if( empty($set) ) throw new Exception("Empty insert data", 1);
			if( empty($set['tran_id']) ) throw new Exception("Client is required", 1);
			
			if( $this->db->insert('calls', $set) ){
	  			return $this->db->insert_id();

	  		}else{
	  			return false;
	  		}
			

		} catch (Exception $e) {
			return false;
		}

	}
  

	function update($call_id, $set){

		$call_id = trim($call_id);

		try {
			
			if( empty($call_id) ) throw new Exception("call_id is required", 1);
			if( empty($set) ) throw new Exception("set param is required", 1);
						
			$this->db->where('call_id', $call_id);
			return $this->db->update('calls', $set);

		} catch (Exception $e) {
			return false;
		}

	}

 	function row($params){

 		try {
 			
 			if( empty($params) ) throw new Exception("Params is required", 1);
 			

			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

			$query = $this->db->get('calls');

			return $query->row();

 		} catch (Exception $e) {
 			return false;
 		}


 	}	

 	function listing($params=array(), $paging=TRUE){

 		try { 			

 			if( empty($params) ) throw new Exception("Can't return full list", 1);
 			

 			if( $paging ){

	 			//TOTAL ROWS
	 			
				//where clause
				if(isset($params['where'])){
					$this->db->where($params['where']);
				}

				if(isset($params['where_str']) && $params['where_str']!='' ){
					$this->db->where($params['where_str'], null, false);
				}

	 			$this->db->select('count(*) as total');

				$this->db->join('call_responses', 'call_responses ON call_responses.call_res_id = calls.call_res_id', 'LEFT OUTER');
	 			$query = $this->db->get('calls');
				
				$total_rows = $query->row()->total;
				$query->free_result(); //free results
			}
			
			//RESULTS
			
			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

			//limits
			if(isset($params['limits'])){
				$this->db->limit($params['limits']['limit'], $params['limits']['start']); 
			}

			//sorting
			if( isset($params['sorting']) ){
				if( is_array($params['sorting']) ){
					$this->db->order_by($params['sorting']['sort'], $params['sorting']['order']);
				}else{
					$this->db->order_by($params['sorting']);
				}

			}else{
				$this->db->order_by('`calls`.`call_created`', 'asc');
			}

			$this->db->select('calls.*, call_responses.call_res_text');

			$this->db->join('call_responses', 'call_responses ON call_responses.call_res_id = calls.call_res_id', 'LEFT OUTER');
 			$query = $this->db->get('calls');

			$result = $query->result();
			$query->free_result(); //free results

			 
			return ($paging) ? array('results'=>$result, 'total_rows'=>$total_rows) : $result ;
			 

 		} catch (Exception $e) {
 			return false;
 		}

 	}

    function insert_audit_trail( $params ) {   

        try
            {
            if(count($params)==0) throw new Exception("Error : Empty Parameter", 1);

            if(!isset($params['ref_table']))
                throw new Exception("Error : Table name must not be empty.");

            if(!isset($params['ref_field']) && !isset($params['ref_val']))
                throw new Exception("Error : Table index name and value  must not be empty.");      
            if(!isset($params['created_by']))
                throw new Exception("Error : Agent Name  must not be empty.");
            if(!isset($params['data_json']))
                throw new Exception("Error : Message Activity  must not be empty.");            

            return ($this->db->insert('table_audit_trail', $params))?$this->db->insert_id():0;

        }catch(Exception $error) { 
            return  $error->getMessage();
        }
    }

  	function get_audit_trail( $call_id = ''){
    
    	try{

			if($call_id=='') throw new Exception("Error : call_id is required", 1);

			$query = $this->db
			        ->where('ref_table', 'calls')
			      	->where('ref_field','call_id')
			      	->where('ref_val',$call_id)
			      	->get('table_audit_trail'); 

			 
			return $query->result(); 
			

		}catch(Exception $error){ 
			return  0;
		}
  	} 	
}