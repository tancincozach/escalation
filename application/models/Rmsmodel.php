<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rmsmodel extends CI_Model { 

	function insert($set){

		return $this->db->insert('data_pinpoint', $set);

	}

 	function get_row($params){

 		try {
 			
			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

			$query = $this->db->get('data_pinpoint');

			return $query->row();

 		} catch (Exception $e) {
 			return false;
 		}



 	}	

 	function get_result_pagination($params){

 		try { 			

 			//TOTAL ROWS
 			
			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

 			$this->db->select('count(*) as total');
 			$query = $this->db->get('data_pinpoint');
			$total_rows = $query->row()->total;
			$query->free_result(); //free results

			
			//RESULTS
			
			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

			//limits
			if(isset($params['limits'])){
				$this->db->limit($params['limits']['limit'], $params['limits']['start']); 
			}

			//sorting
			if( isset($params['sorting']) ){
				if( is_array($params['sorting']) ){
					$this->db->order_by($params['sorting']['sort'], $params['sorting']['order']);
				}else{
					$this->db->order_by($params['sorting']);
				}

			}else{
				$this->db->order_by('`data_pinpoint`.`create_at`', 'desc');
			}

 			$query = $this->db->get('data_pinpoint');

			$result = $query->result();
			$query->free_result(); //free results

			return array('results'=>$result, 'total_rows'=>$total_rows);

 		} catch (Exception $e) {
 			return false;
 		}

 	}
}