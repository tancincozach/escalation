<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Commonmodel extends CI_Model{ 

	function pagination_config( $set_config = array() ) { 

		$config = array(
				'per_page'=>15,
				'uri_segment'=>3
		); 

		$config = $set_config;

        //config for bootstrap pagination class integration

        $config["enable_query_strings"] = TRUE; 
        $config["page_query_string"]    = TRUE; 

        /*$config['full_tag_open']    = '<ul class="pagination">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['first_tag_open']   = '<li class="page-item">';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="page-item prev">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="page-item active"><a class="page-link">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li class="page-item">';
        $config['num_tag_close']    = '</li>';*/

        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span></li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';

		return $config;
	}



    function parseUtf8ToIso88591($string){
        if(!is_null($string)){
            $iso88591_1 = utf8_decode($string);
            $iso88591_2 = iconv('UTF-8', 'ISO-8859-1', $string);
            $string = mb_convert_encoding($string, 'ISO-8859-1', 'UTF-8');       
        }

        return $string;
    }


    function reminder_options($option=''){


        $reminder_option = array();

        switch ($option) {
            case 'reminder_type':

                $reminder_option = array(
                    'ONCE'=>'ONCE', 
                    'EVERY_DAY'=>'EVERY_DAY', 
                    'WEEKLY'=>'WEEKLY', 
                    'FORTHNIGHTLY'=>'FORTHNIGHTLY', 
                    'MONTHLY'=>'MONTHLY', 
                    'YEARLY'=>'YEARLY'
                );

                break;
            case 'reminder_day':

                for($i=1; $i<=31; $i++){ 
                    $reminder_option[$i] = $i;
                }
                
                break;
            case 'reminder_weekday':

                $reminder_option = array(
                    '0'=>'Sunday',              
                    '1'=>'Monday',
                    '2'=>'Tuesday',
                    '3'=>'Wednesday',
                    '4'=>'Thursday',
                    '5'=>'Friday',
                    '6'=>'Saturday'
                );
                
                break;
            case 'reminder_month':

                $reminder_option = array(
                    '1'=>'January',
                    '2'=>'February',
                    '3'=>'March',
                    '4'=>'April',
                    '5'=>'May',
                    '6'=>'June',
                    '7'=>'July ',
                    '7'=>'August',
                    '8'=>'September',
                    '9'=>'October',
                    '10'=>'November',
                    '11'=>'December',
                );
                
                break;
            case 'reminder_year':
                for($i=date('Y'); $i<=date('Y')+1; $i++){                
                    $reminder_option[$i] = $i;
                }
                
                break;        
            default:
                
                break;
        }  

        return $reminder_option;

    }

    function email_from_decode_format($from_obj){

        $from_obj = json_decode($from_obj);
         
        $from = '';

        if( isset($from_obj->name) AND trim(@$from_obj->name) != '' ){
            $from = $from_obj->name.' <'.$from_obj->email.'>';
        }else{
            $from = $from_obj->email;
        }

        return htmlentities($from);
    }

    function email_to_decode_format($to_obj){

        $to_obj = json_decode($to_obj);
         
        $tos = array();

        foreach ($to_obj as $to) {
            if( isset($from_obj->name) AND trim(@$from_obj->name) != '' ){
                $tos[] = $to->name.' <'.$to->email.'>';
            }else{
                $tos[] = $to->email;
            }
        }


        return htmlentities(implode(', ', $tos));
    }

    function array_print_to_html($ar){
        echo '<pre>';
        print_r($ar);
        echo '</pre>';
    }

    function format_json_body($json_data){

       
        if( is_array(json_decode($json_data)) ){
            $json_data = (array)json_decode($json_data);
        } 

        return $json_data;


    }

    function array_key_val_to_table( $data_array ){

        $table = '';

        foreach ($data_array as $row_array) {

            unset($row_array['id']);
            unset($row_array['tran_id']);


            $created_dt = date('d/m/Y H:i', strtotime($row_array['created_dt']));
            unset($row_array['created_dt']);

            $table .= '<div class="border m-1 bg-white">';
            $table .= '<table class="table table-sm">';
            $table .= '<tbody>';
            
            $table .= '<tr class="bg-secondary text-white">';
            
            $table .= '<td width="30%">Date/Time</td>';
            $table .= '<td>'.$created_dt.'</td>';

            $table .= '</tr>';

            foreach ($row_array as $key => $value) {
                $table .= '<tr>';
                
                $table .= '<td width="30%">'.stripslashes($key).'</td>';
                $table .= '<td>'.stripslashes($value).'</td>';

                $table .= '</tr>';
            }

            $table .= '</tbody>';
            $table .= '</table>';
            $table .= '</div>';
            $table .= '<br />';
        }
 
        return $table;
    }

    function array_key_val_to_p( $data ){
        
        $p = '';  
        foreach ($data as $key => $value) { 
            $p .= '<p class="p-0 m-0"><strong>'.stripslashes($key).'</strong> : '.stripslashes($value).'</p>';  
        } 
        return $p;
    }

    function isHTML($string){
        return $string != strip_tags($string) ? true:false;
    }

    function remove_multi_br($html){

        return preg_replace('#(<br */?>\s*)+#i', '<br />', $html);
    }

    function clean_htmlcontent($input_lines){
        $_input_lines = strip_tags($input_lines, '<br>');
        $_input_lines = preg_replace("/[\n\r]/", "<br />", $_input_lines); 
        $input_line = preg_replace("/(<br *\/?>\s*)+/i", "<br />", $_input_lines);
        return $input_line;

    }

    function insert_cma_call($params){

        $this->load->library("Cma_external");
 
        //Auto log calls to CMA                  
        $set_cma['cma_db']      = @$params['cma_db'];
        $set_cma['cust_id']     = @$params['cust_id'];
        $set_cma['contact_id']  = @$params['contact_id'];

        if(isset($params['ob1_phone']))
            $set_cma['ob1_phone']   = @$params['ob1_phone'];               
        if(isset($params['ob1_name']))
            $set_cma['ob1_name']    = @$params['ob1_name'];

        $set_cma['message']     = @$params['message'];
        $set_cma['agent_id']    = @$params['agent_id'];
                 
        $insert_calls = $this->cma_external->insert_calls_log($set_cma);

        //audit log
        $audit['tran_id']       = @$params['tran_id'];
        
        if(isset($params['audit_from']))
            $audit['audit_from']      = @$params['audit_from'];
        
        if(isset($params['audit_to']))
            $audit['audit_to']      = @$params['audit_to'];
        
        $audit['audit_type']    = @$params['audit_type'];
        $audit['message']       = stripslashes(nl2br(@$params['message']));
        $audit['more_info']     = '';
        $audit['return_message']= $insert_calls>0?'Call inserted to CMA':$insert_calls;
        $this->insert_audit_trail($audit);

        return $audit['return_message'];

    }

    function get_site_id($cma_id){

        $site_id = intval($cma_id/1000); 
        
        if($site_id < 1){ 
            $site_id = 1; 
        }

        return $site_id;
    }


    function curl_agentstatus($type='agentStatusUpdate', $params){

        $this->load->library("Agent_status");

        try {
            
            if( $this->user_lvl == 5 ){
                throw new Exception("No Agent status for Client", 1);
                
            }


            $return_message = '';
            $message = '';

            switch ($type) {
                case 'agentStatusUpdate':
                    
                    $_param = array();
                    $_param['agent_id']  = @$params['agent_id'];
                    $_param['reason']   = @$params['reason'];
                    $_param['did']      = @$params['cma_id']; 


                    $message = $_param['reason'];

                    $return_message = $this->agent_status->agentStatusUpdate($_param);

                    break; 

                default:
                    # code...
                    break;
            }

            $more_info = array();
            $more_info = $params;
            $more_info['agent_name'] = $this->agent_name;


            $audit['tran_id']       = @$params['tran_id']; 
            $audit['audit_type']    = $type;
            $audit['message']       = stripslashes(nl2br($message));
            $audit['more_info']     = json_encode($more_info);
            $audit['return_message']= $return_message;
            $this->insert_audit_trail($audit);


        } catch (Exception $e) {
            return '';
        } 

    }


    function curl_getCustomerData($cma_id=''){

        $this->load->library("Agent_status");

        try {
            
            if( $cma_id == ''){
                throw new Exception("Customer CMA ID is required", 1);                
            }
 
            $customer = $this->agent_status->getCustomerData($cma_id);

            //$customer = (object)@$customer->data;

            return $customer;

        } catch (Exception $e) {
            return 'Caught exception: '.  $e->getMessage(). "\n";
        } 

    }


    function insert_audit_trail($set){
        $set['created'] = strtotime('now');
        if( is_array($set) AND !empty($set) ){
            $this->db->insert('audit_trail', $set);
        }

    } 

    /**
     *  params
     *      from, to, subject, cc, bcc
     *      tran_id
     */
    function send_email($params=array()){

        $this->load->library('email');

        $audit = array(
            'audit_type' => (isset($params['audit_type']) AND @$params['audit_type'] != '')?$params['audit_type']:'email',
            'tran_id' => @$params['tran_id'],
            'message' => @$params['message'],
            'audit_to' => (is_array($params['to']) AND !empty($params['to']) ) ? implode(',', $params['to']) : $params['to']
        );

        try {
            
            $config['protocol'] = 'smtp';
            $config['smtp_host'] = 'mail.welldone.com.au';
            $config['smtp_user'] = 'collection@welldone.com.au';
            $config['smtp_pass'] = 'C0113Ct10n';
            $config['smtp_port'] = '25';
            $config['mailtype'] = 'html';
            $config['wordwrap'] = TRUE;
            $config['charset']  = 'iso-8859-1';
            $config['crlf']     = '\r\n';
            $config['newline']  = '\r\n';

            $this->email->initialize($config);

            $this->email->clear(TRUE);

            $to         = trim(@$params['to']);
            $subject    = trim(@$params['subject']);
            $message    = @$params['message']; 


            if(!is_array($to)) { 
                $to = str_replace(" ","",$to);
                $to = str_replace(";",",",$to);
                $to = explode(",",$to); 
            }

            $to = array_filter($to, 'strlen');

            if( empty($to) ) throw new Exception("Mailer Error: Empty TO", 1);
            if( empty($subject) ) throw new Exception("Mailer Error: Empty Subject", 1);
            

            if( isset($params['reply_to']) AND trim(@$params['reply_to']) != '' ){
                $this->email->reply_to($params['reply_to']);
            }


            if( isset($params['from']) AND trim(@$params['from']) != '' ){
                $this->email->from($params['from']);
            }else{
                //$this->email->from('escalationapp@welldone.com.au', 'Escalation System');
                $this->email->from(SYS_EMAIL, SYS_EMAIL_NAME);
            }
            
            if( isset($params['cc']) AND trim(@$params['cc']) != '' ){
                $this->email->cc($params['cc']);
            }
            
            if( isset($params['bcc']) AND trim(@$params['bcc']) != '' ){
                $this->email->bcc($params['bcc']);
            }
            

            $this->email->to($to);
            
            //$this->email->cc('another@another-example.com');
            //$this->email->bcc('them@their-example.com'); 
            

            $this->email->subject($subject);
            $this->email->message($message);


            if( $_SERVER['SERVER_NAME'] != 'localhost' ){

                if($this->email->send(FALSE)){
                    $result = 'message_sent';
                }else{

                    $result = "Mailer Error ".$this->email->print_debugger(array('headers'));
                }
            
            }else{
                $result = 'message_sent - localhost';
            }

            
           /* if($this->email->send(FALSE)){
                $result = 'message_sent';
            }else{

                $result = "Mailer Error ".$this->email->print_debugger(array('headers'));
            }*/

   
            $audit['audit_from']    = (isset($params['from']) AND trim(@$params['from']) != '')?@$params['from']:SYS_EMAIL;
            $audit['audit_to']      = implode(',', $to);
            $audit['return_message']= $result;        
            //$audit['message']       = ;
            $audit['message']       = ($this->isHTML($message))?stripslashes($message):stripslashes(nl2br($message));
            
            $more_info = '';
            $more_info['subject'] = $subject;
            $more_info['agent_name'] = isset($params['agent_name'])?$params['agent_name']:@$this->agent_name;
            
            $audit['more_info']     = json_encode($more_info);
                               
            $this->insert_audit_trail($audit);

            return $result;
            



        } catch (Exception $e) {

            $audit['audit_from']    = (isset($params['from']) AND trim(@$params['from']) != '')?@$params['from']:SYS_EMAIL;
            $audit['audit_to']      = implode(',', @$to);
            $audit['return_message']= $e->getMessage();        
            $audit['message']       = stripslashes(nl2br($message));
            //$audit['more_info']     = '';
            $this->insert_audit_trail($audit);

            return $e->getMessage();
        }
 
    }

    function send_sms($params){

        $provider = (!isset($params['provider']))?'messagenet':$params['provider'];

        $audit = array(
            'audit_type' => 'sms',
            'tran_id' => @$params['tran_id'],
            'message' => @$params['message'],
            //'audit_to' => (is_array($params['to']) AND !empty($params['to']) ) ? implode(',', $params['to']) : $params['to'],
            'more_info' => json_encode(array('provider'=>$provider))
        );

        if( $provider == 'messagenet' ){

            $this->load->library('email');

            $config['protocol'] = 'smtp';
            $config['smtp_host'] = 'mail.welldone.com.au';
            $config['smtp_user'] = 'collection@welldone.com.au';
            $config['smtp_pass'] = 'C0113Ct10n';
            $config['smtp_port'] = '25';
            $config['mailtype'] = 'text';
            $config['wordwrap'] = TRUE;
            $config['charset'] = 'iso-8859-1';

            $this->email->initialize($config);


            $send_to = $params['to'];
            $send_to = str_replace(" ","",$send_to);
            $send_to = str_replace(";",",",$send_to);

            $send_to = explode(',', $send_to);
            
            foreach($send_to as $to){ 
                
                $to = str_replace(' ', '', $to);

                if(!preg_match('/^(\+6|6).*/',$to)) {
                    if(preg_match('/^0(.*)/',$to, $match)) $to = preg_replace('/^0(.*)/','61$1',$to);
                    else $to = '61'.$to;
                }

                if( trim($to) != '' ){
                    $to = $to.'@messagenet.com.au';
                     
                    $this->email->from(SYS_EMAIL);
                    $this->email->to(trim($to));
                    $this->email->subject(' ');
                    $this->email->message(stripslashes($params['message']));
        

                    if( $_SERVER['SERVER_NAME'] != 'localhost' ){

                        if($this->email->send(FALSE)){
                            $result = 'message_sent';
                        }else{

                            $result = $this->email->print_debugger(array('headers'));
                        }
                    
                    }else{
                        $result = 'message_sent - localhost';
                    }

                    $audit['audit_from']    = (isset($params['from']) AND trim(@$params['from']) != '')?@$params['from']:SYS_EMAIL;
                    $audit['audit_to']      = $to;
                    $audit['return_message']= 'message_sent';        
                    $audit['message']       = stripslashes(nl2br($message));                    
                                       
                    $this->insert_audit_trail($audit);
                }
            }

            return 'message_sent';            

        }

        if( $provider == 'smsglobal' ){

            //https://www.smsglobal.com/http-api.php?action=sendsms&user=testuser&password=secret&&from=Test&to=61447100250&text=Hello%20world
                    
            //$url = "https://www.smsglobal.com/http-api.php?";     
            $url = "https://api.smsglobal.com/http-api.php?";
     

            $build_query['action']   = 'sendsms';
            $build_query['user']     = 'ylrjh0kx';
            $build_query['password'] = 'wQVvWX6m';
            $build_query['from']     = '13CURE';
 

            $send_to = $params['to'];
            $send_to = str_replace(" ","",$send_to);
            $send_to = str_replace(";",",",$send_to);

            $send_to = explode(',', $send_to);

            foreach ($send_to as $phoneNumber) {
                
                if( trim($phoneNumber) != '' ){

                    if( $_SERVER['SERVER_NAME'] != 'localhost' ){

                        if(!preg_match('/^(\+6|6).*/',$phoneNumber)) {
                            if(preg_match('/^0(.*)/',$phoneNumber, $match)) $phoneNumber = preg_replace('/^0(.*)/','61$1',$phoneNumber);
                            else $phoneNumber = '61'.$phoneNumber;
                        }
                        
                        $build_query['to']       = $phoneNumber;
                        $build_query['text']     = $messsage;

                        $url .= http_build_query($build_query);      
                          
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_HEADER, TRUE);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                        curl_setopt($ch, CURLOPT_USERPWD, $build_query['user'].":".$build_query['password']);
                        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                        $head = curl_exec($ch);
                        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                        curl_close($ch);

                        if( !strpos(' '.$head, 'ERROR')){
                            $result = 'message_sent';                
                        }else{
                            $result = $head; 
                        }

                    }else{
                        $result = 'message_sent - localhost';
                    }

                    $audit['audit_from']    = (isset($params['from']) AND trim(@$params['from']) != '')?@$params['from']:SYS_EMAIL;
                    $audit['audit_to']      = $to;
                    $audit['return_message']= 'message_sent';        
                    $audit['message']       = stripslashes(nl2br($message));                    
                                       
                    $this->insert_audit_trail($audit);
                }
            }

            return 'message_sent';

        }

    }

    public function format_size_units($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return   $bytes;
    }


    function timestampdiff($qw,$saw, $format='%H:%I:%S')
    {
        $datetime1 = new DateTime("$qw");
        $datetime2 = new DateTime("$saw");
        $interval = $datetime1->diff($datetime2);
        return $interval->format($format);
    }

}