<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Proceduremodel extends CI_Model { 


	function insert($set){

		try {
			
			if( empty($set) ) throw new Exception("Empty insert data", 1);
			if( empty($set['client_id']) ) throw new Exception("Client is required", 1);
			if( empty($set['pro_alert_type']) ) throw new Exception("Alert Type is required", 1);
			
			if( $this->db->insert('client_procedure', $set) ){
	  			$pro_id =  $this->db->insert_id();

                 $this->insert_audit_trail(
                    array(
                        'ref_table'=>'client_procedure',
                        'ref_field'=>'pro_id',
                        'audit_type'=>'I',
                        'ref_val'=>$pro_id,
                        'created_by'=>@$set['added_by'],
                        'created'=>strtotime("now"),
                        'data_json'=> json_encode($set)
                    ));

                return $pro_id;

	  		}else{
	  			return false;
	  		}
			

		} catch (Exception $e) {
			return false;
		}

	}
  

	function update($pro_id, $set){
		$pro_id = trim($pro_id);
		
		try {
			
			if( empty($pro_id) ) throw new Exception("pro_id is required", 1);
			if( empty($set) ) throw new Exception("set param is required", 1);
						
			$this->db->where('pro_id', $pro_id);
			if( $this->db->update('client_procedure', $set) ){

             	$this->insert_audit_trail(
	                array(
	                    'ref_table'=>'client_procedure',
	                    'ref_field'=>'pro_id',
	                    'audit_type'=>'U',
	                    'ref_val'=>$pro_id,
	                    'created_by'=>@$set['last_update_by'],
	                    'created'=>strtotime("now"),
	                    'data_json'=> json_encode($set)
	                ));

             	return $pro_id;
            }else{
            	return 0;
            }

		} catch (Exception $e) {
			return false;
		}

	}

 	function row($params){

 		try {
 			
 			if( empty($params) ) throw new Exception("Params is required", 1);
 			

			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

			$this->db->select('client_procedure.*, client.client_name');
			$this->db->join('client', 'client ON client.client_id = client_procedure.client_id', 'LEFT OUTER');
			$query = $this->db->get('client_procedure');

			return $query->row();

 		} catch (Exception $e) {
 			return false;
 		}
 
 	}

 	function join_client_row($params){

 		try {
 			
 			if( empty($params) ) throw new Exception("Params is required", 1);
 			

			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

			//$this->db->select('client_procedure.*, client.client_name, ');
			$this->db->join('client', 'client ON client.client_id = client_procedure.client_id', 'LEFT OUTER');
			$query = $this->db->get('client_procedure');

			return $query->row();

 		} catch (Exception $e) {
 			return false;
 		}
 
 	}	

 	function listing($params=array(), $paging=TRUE){

 		try { 			

 			if( empty($params) ) throw new Exception("Can't return full list", 1);
 			
 			if( $paging ){

	 			//TOTAL ROWS
	 			
				//where clause
				if(isset($params['where'])){
					$this->db->where($params['where']);
				}

				if(isset($params['where_str']) && $params['where_str']!='' ){
					$this->db->where($params['where_str'], null, false);
				}

	 			$this->db->select('count(*) as total');

				$this->db->join('client', 'client ON client.client_id = client_procedure.client_id', 'LEFT OUTER');
	 			$query = $this->db->get('client_procedure');
				
				$total_rows = $query->row()->total;
				$query->free_result(); //free results
			}
			
			//RESULTS
			
			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

			//limits
			if(isset($params['limits'])){
				$this->db->limit($params['limits']['limit'], $params['limits']['start']); 
			}

			//sorting
			if( isset($params['sorting']) ){
				if( is_array($params['sorting']) ){
					$this->db->order_by($params['sorting']['sort'], $params['sorting']['order']);
				}else{
					$this->db->order_by($params['sorting']);
				}

			}else{
				$this->db->order_by('`client_procedure`.`created_at`', 'desc');
			}

			$this->db->select('client_procedure.*, client.client_name');
			$this->db->join('client', 'client ON client.client_id = client_procedure.client_id', 'LEFT OUTER');
 			$query = $this->db->get('client_procedure');

			$result = $query->result();
			$query->free_result(); //free results

			 
			return ($paging) ? array('results'=>$result, 'total_rows'=>$total_rows) : $result;
			 

 		} catch (Exception $e) {
 			return false;
 		}

 	}


    function insert_audit_trail( $params ) {   

        try
            {
            if(count($params)==0) throw new Exception("Error : Empty Parameter", 1);

            if(!isset($params['ref_table']))
                throw new Exception("Error : Table name must not be empty.");

            if(!isset($params['ref_field']) && !isset($params['ref_val']))
                throw new Exception("Error : Table index name and value  must not be empty.");      
            if(!isset($params['created_by']))
                throw new Exception("Error : Agent Name  must not be empty.");
            if(!isset($params['data_json']))
                throw new Exception("Error : Message Activity  must not be empty.");            

            return ($this->db->insert('table_audit_trail', $params))?$this->db->insert_id():0;

        }catch(Exception $error) { 
            return  $error->getMessage();
        }
    }


  	function get_audit_trail( $pro_id = ''){
    
    	try{

			if($pro_id=='') throw new Exception("Error : pro_id is required", 1);

			$query = $this->db
			        ->where('ref_table', 'client_procedure')
			      	->where('ref_field','pro_id')
			      	->where('ref_val',$pro_id)
			      	->get('table_audit_trail'); 

			 
			return $query->result(); 
			

		}catch(Exception $error){ 
			return  0;
		}
  	}    

}