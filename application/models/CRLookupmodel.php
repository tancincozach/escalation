<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CRLookupmodel extends CI_Model { 


	function insert($set){

        $agent_name = ''; 

        if( isset($set['agent_name']) ) {
            $agent_name = @$set['agent_name']; 
            unset($set['agent_name']);
        }


		try {
			
			if( empty($set) ) throw new Exception("Empty insert data", 1);		
			
			if( $this->db->insert('client_call_lookup', $set) ){

	  			$cr_id =  $this->db->insert_id();

                 $this->insert_audit_trail(
                    array(
                        'ref_table'=>'client_call_lookup',
                        'ref_field'=>'call_lookup_id',
                        'audit_type'=>'I',
                        'ref_val'=>$cr_id,
                        'created_by'=>$agent_name,
                        'created'=>strtotime("now"),
                        'data_json'=> json_encode($set)
                    ));


                 return $cr_id;
	  		}else{
	  			return false;
	  		}
			

		} catch (Exception $e) {
			return false;
		}

	}
  

	function update($call_lookup_id, $set){
		$call_lookup_id = trim($call_lookup_id);
		

        $agent_name = ''; 

        if( isset($set['agent_name']) ) {
            $agent_name = @$set['agent_name']; 
            unset($set['agent_name']);
        }

		try {
			
			if( empty($call_lookup_id) ) throw new Exception("call_lookup_id is required", 1);
			if( empty($set) ) throw new Exception("set param is required", 1);
						
			$this->db->where('call_lookup_id', $call_lookup_id);
			$this->db->update('client_call_lookup', $set);

             	$this->insert_audit_trail(
	                array(
	                    'ref_table'=>'client_call_lookup',
	                    'ref_field'=>'call_lookup_id',
	                    'audit_type'=>'U',
	                    'ref_val'=>$call_lookup_id,
	                    'created_by'=>$agent_name,
	                    'created'=>strtotime("now"),
	                    'data_json'=> json_encode($set)
	                ));

             	return $call_lookup_id;

		} catch (Exception $e) {
			return false;
		}

	}	

	function delete($call_lookup_id){
		
		try {
			
			if( empty($call_lookup_id) ) throw new Exception("call_lookup_id is required", 1);
					
			return $this->db->delete('client_call_lookup', array('call_lookup_id' => $call_lookup_id));  
			
		} catch (Exception $e) {
			return false;
		}

	}

 	function row($params){

 		try {
 			
 			if( empty($params) ) throw new Exception("Params is required", 1);
 			

			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

			$query = $this->db->get('client_call_lookup');

			return $query->row();

 		} catch (Exception $e) {
 			return false;
 		}


 	}	

 	function listing($params=array(), $paging=TRUE){

 		try { 			

 			//if( empty($params) ) throw new Exception("Can't return full list", 1);
 			
 			if( $paging ){

	 			//TOTAL ROWS
	 			
				//where clause
				if(isset($params['where'])){
					$this->db->where($params['where']);
				}

				if(isset($params['where_str']) && $params['where_str']!='' ){
					$this->db->where($params['where_str'], null, false);
				}

	 			$this->db->select('count(*) as total');

				$this->db->join('client', 'client ON client.client_id = client_call_lookup.client_id', 'LEFT OUTER');
	 			$query = $this->db->get('client_call_lookup');
				
				$total_rows = $query->row()->total;
				$query->free_result(); //free results
			}
			
			//RESULTS
			
			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

			//limits
			if(isset($params['limits'])){
				$this->db->limit($params['limits']['limit'], $params['limits']['start']); 
			}

			//sorting
			if( isset($params['sorting']) ){
				if( is_array($params['sorting']) ){
					$this->db->order_by($params['sorting']['sort'], $params['sorting']['order']);
				}else{
					$this->db->order_by($params['sorting']);
				}

			}else{
				$this->db->order_by('`client_call_lookup`.`create_dt`', 'desc');
			}

			$this->db->join('client', 'client ON client.client_id = client_call_lookup.client_id', 'LEFT OUTER');
 			$query = $this->db->get('client_call_lookup');

			$result = $query->result();


			$query->free_result(); //free results

			 
			return ($paging) ? array('results'=>$result, 'total_rows'=>$total_rows) : $result;
			 

 		} catch (Exception $e) {
 			return false;
 		}

 	}	

 	function get_array($params=array(), $with_empty=false){

 		try { 			
			
			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

			//limits
			if(isset($params['limits'])){
				$this->db->limit($params['limits']['limit'], $params['limits']['start']); 
			}

			//sorting
			if( isset($params['sorting']) ){
				if( is_array($params['sorting']) ){
					$this->db->order_by($params['sorting']['sort'], $params['sorting']['order']);
				}else{
					$this->db->order_by($params['sorting']);
				}

			}else{
				$this->db->order_by('`client_call_lookup`.`call_lookup_name`', 'asc');
			}

			$this->db->select('call_lookup_id, call_lookup_name');

			$this->db->join('client', 'client ON client.client_id = client_call_lookup.client_id', 'LEFT OUTER');
 			$query = $this->db->get('client_call_lookup');

			$result = $query->result(); 
			$query->free_result(); //free results

			$result_array = array();

			if($with_empty){
				$result_array[] = '--no call response lookup--';
			}

			foreach ($result as $row) {
			 	$result_array[$row->call_lookup_id] = $row->call_lookup_name;
			 } 


			return $result_array;
			 

 		} catch (Exception $e) {
 			return false;
 		}

 	}


 	public function screen_display_new_tr($key, $field_list_option, $screen_display){


 		//$key = time().rand(0);
		

		$name = '<input type="text" required="required" name="screen_display['.$key.'][name]" minlength="2" maxlength="30" class="form-control form-control-sm" value="'.@$screen_display->name.'" >';
		$input = form_dropdown("screen_display[".$key."][input]", array("0"=>"Spreadsheet Field", "1"=>"freetype"), @$screen_display->input, ' onchange="crlookup.screen_display.on_input_change(this)"  class="custom-select custom-select-sm mb-1"');
		$display1 = form_dropdown("screen_display[".$key."][display1]", $field_list_option, @$screen_display->display, ' class="'.(@$screen_display->input!='1'?'':'d-none').' custom-select custom-select-sm mb-1" ');
		$display2 = '<input type="text"  name="screen_display['.$key.'][display2]" minlength="2" maxlength="30" class="form-control form-control-sm '.(@$screen_display->input!='1'?'d-none':'').'" value="'.@$screen_display->display.'" >';
		
		$delete = '<a href="javascript:void(0)" title="Edit" onclick="crlookup.screen_display.delete_tr(this)" class="text-danger del_btn" >
				 	<i class="fa fa-times"></i> 	
				 </a>';
		return '<tr id="screendisplaytr_'.$key.'">
			<td class="border-top-0">
				'.$name.'
			</td>
			<td class="border-top-0">
				'.$input.'
			</td>
			<td class="border-top-0">
				'.$display1.'
				'.$display2.'
			</td>
			<td class="border-top-0">
				'.$delete.' 
			</td>
		</tr>';
 	}


 	public function escalation_display_new_tr($key, $field_list_option, $escalation_display, $spreasheet_input=0){

 		//echo '$escalation_display->input'.@$escalation_display->input;

 		 $spreasheet_input = isset($escalation_display->input)?$escalation_display->input:$spreasheet_input;

 		if( $spreasheet_input == 0){ 

			//$input = form_dropdown("screen_display[".$key."][input]", array("0"=>"Spreadsheet Field", "1"=>"freetype"), (@$escalation_display->input == ''?"0":@$escalation_display->input), ' style="width: 150px" ');
			$input = '<input type="hidden"  name="escalation_display['.$key.'][input]"  value="0" > Spreadsheet';
			$esc_name = form_dropdown("escalation_display[".$key."][esc_name]", $field_list_option, @$escalation_display->esc_name, ' class="custom-select custom-select-sm mb-1" ');
			$name_field = form_dropdown("escalation_display[".$key."][name_field]", $field_list_option, @$escalation_display->name_field, ' class="custom-select custom-select-sm mb-1" ');
			$phone_field = form_dropdown("escalation_display[".$key."][phone_field]", $field_list_option, @$escalation_display->phone_field, ' class="custom-select custom-select-sm mb-1" ');	
			$notes = '<input type="text"  name="escalation_display['.$key.'][notes]"  maxlength="50" class="form-control form-control-sm"  value="'.@$escalation_display->notes.'" >';
		
		}else{
			
			$input = '<input type="hidden"  name="escalation_display['.$key.'][input]"  value="1" > Free Type';		 
			$esc_name = '<input type="text"  name="escalation_display['.$key.'][esc_name]"  maxlength="50" required="required" class="form-control form-control-sm"  value="'.@$escalation_display->esc_name.'" >';
			$name_field = '<input type="text"  name="escalation_display['.$key.'][name_field]"  maxlength="50" class="form-control form-control-sm"  value="'.@$escalation_display->name_field.'" >';
			$phone_field = '<input type="text"  name="escalation_display['.$key.'][phone_field]"  maxlength="50" class="form-control form-control-sm"  value="'.@$escalation_display->phone_field.'" >';
			$notes = '<input type="text"  name="escalation_display['.$key.'][notes]"  maxlength="50" class="form-control form-control-sm"  value="'.@$escalation_display->notes.'" >';
		}


		$delete = '<a href="javascript:void(0)" title="Edit" onclick="crlookup.escalation_display.delete_tr(this)" class="text-danger del_btn" >
				 	<i class="fa fa-times"></i> 	
				 </a>';

		return '<tr id="escalationtr_'.$key.'" class="'.(!$spreasheet_input?'spreasheet_input':'').'">
			<td class="border-top-0">
				'.$input.'
			</td>
			<td class="border-top-0">
				'.$esc_name.'
			</td>
			<td class="border-top-0">
				'.$name_field.'
			</td>
			<td class="border-top-0">
				'.$phone_field.'
			</td>
			<td class="border-top-0">
				'.$notes.'
			</td>			 
			<td class="border-top-0">
				'.$delete.' 
			</td>
		</tr>'; 

 	}

 

 	public function to_email_new_tr($key, $field_list_option, $to_email){


 		//$key = time().rand(0);
			
		$input = form_dropdown("to_email[".$key."][input]", array("0"=>"Spreadsheet Field", "1"=>"freetype"), @$to_email->input, ' onchange="crlookup.to_email.on_input_change(this)"  class="custom-select custom-select-sm mb-1" ');
		$display1 = form_dropdown("to_email[".$key."][display1]", $field_list_option, @$to_email->email, ' class="'.(@$to_email->input!='1'?'':'d-none').' custom-select custom-select-sm mb-1" ');
		$display2 = '<input type="text"  name="to_email['.$key.'][display2]" minlength="2" maxlength="30" class="form-control form-control-sm '.(@$to_email->input!='1'?'d-none':'').'" value="'.@$to_email->email.'" >';
		
		$delete = '<a href="javascript:void(0)" title="Edit" onclick="crlookup.to_email.delete_tr(this)" class="text-danger del_btn" >
				 	<i class="fa fa-times"></i> 	
				 </a>';
		return '<tr id="toemailtr_'.$key.'">
			<td class="border-top-0">
				'.$input.'
			</td>
			<td class="border-top-0">
				'.$display1.'
				'.$display2.'
			</td>
			<td class="border-top-0">
				'.$delete.' 
			</td>
		</tr>';
 	}

    function insert_audit_trail( $params ) {   

        try
            {
            if(count($params)==0) throw new Exception("Error : Empty Parameter", 1);

            if(!isset($params['ref_table']))
                throw new Exception("Error : Table name must not be empty.");

            if(!isset($params['ref_field']) && !isset($params['ref_val']))
                throw new Exception("Error : Table index name and value  must not be empty.");      
            if(!isset($params['created_by']))
                throw new Exception("Error : Agent Name  must not be empty.");
            if(!isset($params['data_json']))
                throw new Exception("Error : Message Activity  must not be empty.");            

            return ($this->db->insert('table_audit_trail', $params))?$this->db->insert_id():0;

        }catch(Exception $error) { 
            return  $error->getMessage();
        }
    }


  	function get_audit_trail( $call_lookup_id = ''){
    
    	try{

			if($call_lookup_id=='') throw new Exception("Error : call_lookup_id is required", 1);

			$query = $this->db
			        ->where('ref_table', 'client_call_lookup')
			      	->where('ref_field','call_lookup_id')
			      	->where('ref_val',$call_lookup_id)
			      	->get('table_audit_trail'); 

			 
			return $query->result(); 
			

		}catch(Exception $error){ 
			return  0;
		}
  	}    


}