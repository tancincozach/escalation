<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Audittrailmodel extends CI_Model{ 

    function get_results( $params=array() ){

        if(!empty($params['where'])){           
            $this->db->where($params['where']);
        }

        if(isset($params['where_str']) && $params['where_str']!='' ){
            $this->db->where($params['where_str'], null, false);
        }

        $query = $this->db->get('audit_trail');

        return $query->result();
         
    }


    function get_row($id) {

        $this->db->where('id', $id);

        $query = $this->db->get('audit_trail');

        return $query->row();

    }


}