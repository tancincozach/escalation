<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usersettingsmodel extends CI_Model { 


	function set_option($agent_name, $update_options){

		try {
			
			if( empty($agent_name) ) throw new Exception("Agent Name is unknown", 1);

			$this->db->where( 'agent_name', trim($agent_name) );
			$query = $this->db->get('users_settings');
			$row = $query->row();
			
			$id = 1;

			$current = (array)json_decode(@$row->options);

			$set = array();
			//$set['display_screen'] = $display_screen;
			
			$set['options'] = json_encode(array_merge($current, $update_options));

			if ($query->num_rows() > 0) {
				//update
				$this->db->where('agent_name', $agent_name);
				if( !$this->db->update('users_settings',$set) ){
					return 0;
				}
                
                $this->insert_audit_trail(
                    array(
                        'ref_table'=>'users_settings',
                        'ref_field'=>'agent_name',
                        'audit_type'=>'U',
                        'ref_val'=>$agent_name,
                        'created_by'=>@$set['agent_name'],
                        'created'=>strtotime("now"),
                        'data_json'=> json_encode($set)
                    ));

			}else{
				//insert
				$set['agent_name'] = trim($agent_name);
				$this->db->insert('users_settings', $set);

                 $this->insert_audit_trail(
                    array(
                        'ref_table'=>'users_settings',
                        'ref_field'=>'agent_name',
                        'audit_type'=>'I',
                        'ref_val'=>$agent_name,
                        'created_by'=>@$set['agent_name'],
                        'created'=>strtotime("now"),
                        'data_json'=> json_encode($set)
                    ));

			}


			return 1;

		} catch (Exception $e) {
			return 0;
		}


	}

	 

 	function options($agent_name){

 		try {
 			
 			if( empty($agent_name) ) throw new Exception("Params is required", 1);
 		 

			$this->db->where('agent_name', trim($agent_name));
			$query = $this->db->get('users_settings');
			$row = $query->row();
			$obj = json_decode(@$row->options);
			return $obj;

 		} catch (Exception $e) {
 			return false;
 		}


 	}
 	  
    function insert_audit_trail( $params ) {   

        try
            {
            if(count($params)==0) throw new Exception("Error : Empty Parameter", 1);

            if(!isset($params['ref_table']))
                throw new Exception("Error : Table name must not be empty.");

            if(!isset($params['ref_field']) && !isset($params['ref_val']))
                throw new Exception("Error : Table index name and value  must not be empty.");      
            if(!isset($params['created_by']))
                throw new Exception("Error : Agent Name  must not be empty.");
            if(!isset($params['data_json']))
                throw new Exception("Error : Message Activity  must not be empty.");            

            return ($this->db->insert('table_audit_trail', $params))?$this->db->insert_id():0;

        }catch(Exception $error) { 
            return  $error->getMessage();
        }
    }


  	function get_audit_trail( $pro_id = ''){
    
    	try{

			if($pro_id=='') throw new Exception("Error : pro_id is required", 1);

			$query = $this->db
			        ->where('ref_table', 'client_procedure')
			      	->where('ref_field','pro_id')
			      	->where('ref_val',$pro_id)
			      	->get('table_audit_trail'); 

			 
			return $query->result(); 
			

		}catch(Exception $error){ 
			return  0;
		}
  	}  


}