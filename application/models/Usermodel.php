<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usermodel extends CI_Model { 


	function insert($set){

		try {
			
			if( empty($set) ) throw new Exception("Empty insert data", 1);			
			
			if( $this->db->insert('users', $set) ){

				$uid =  $this->db->insert_id();

                 $this->insert_audit_trail(
                    array(
                        'ref_table'=>'users',
                        'ref_field'=>'id',
                        'audit_type'=>'I',
                        'ref_val'=>$uid,
                        'created_by'=>@$set['added_by'],
                        'created'=>strtotime("now"),
                        'data_json'=> json_encode($set)
                    ));
                 return $uid ;

	  		}else{
	  			return false;
	  		}
			

		} catch (Exception $e) {
			return false;
		}

	}
  

	function update($id, $set){

		$id = trim($id);

		try {
			
			if( empty($id) ) throw new Exception("id is required");
			if( empty($set) ) throw new Exception("set param is required");
						
			$this->db->where('id', $id);
			 $this->db->update('users', $set);

  		 	$this->insert_audit_trail(
	                array(
	                    'ref_table'=>'users',
	                    'ref_field'=>'id',
	                    'audit_type'=>'U',
	                    'ref_val'=>$id,
	                    'created_by'=>@$set['last_update_by'],
	                    'created'=>strtotime("now"),
	                    'data_json'=> json_encode($set)
	                ));

             	return $id;

		} catch (Exception $e) {
			return false;
		}

	}

 	function row($params){

 		try {
 			
 			if( empty($params) ) throw new Exception("Params is required", 1);
 			

			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

			$query = $this->db->get('users');

			return $query->row();

 		} catch (Exception $e) {
 			return false;
 		}


 	}	

 	function user_setting_row($params){

 		try {
 			
 			if( empty($params) ) throw new Exception("Params is required", 1);
 			

			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

			$this->db->join('users_settings','users_settings.agent_name=users.username','left');

			$query = $this->db->get('users');

			return $query->row();

 		} catch (Exception $e) {
 			return false;
 		}


 	}	

 	function listing($params=array(), $paging=TRUE){

 		try { 			

 			//if( empty($params) ) throw new Exception("Can't return full list", 1);
 			
 			if( $paging ){

	 			//TOTAL ROWS
	 			
				//where clause
				if(isset($params['where'])){
					$this->db->where($params['where']);
				}

				if(isset($params['where_str']) && $params['where_str']!='' ){
					$this->db->where($params['where_str'], null, false);
				}

	 			$this->db->select('count(*) as total');

	 			$query = $this->db->get('users');
				
				$total_rows = $query->row()->total;
				$query->free_result(); //free results
			}
			
			//RESULTS
			
			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

			//limits
			if(isset($params['limits'])){
				$this->db->limit($params['limits']['limit'], $params['limits']['start']); 
			}

			//sorting
			if( isset($params['sorting']) ){
				if( is_array($params['sorting']) ){
					$this->db->order_by($params['sorting']['sort'], $params['sorting']['order']);
				}else{
					$this->db->order_by($params['sorting']);
				}

			}else{
				$this->db->order_by('`users`.`fullname`', 'desc');
			}

			$this->db->join('client', 'client ON client.client_id = users.client_id', 'LEFT OUTER');
 			$query = $this->db->get('users');

			$result = $query->result();


			$query->free_result(); //free results

			 
			return ($paging) ? array('results'=>$result, 'total_rows'=>$total_rows) : $result;
			 

 		} catch (Exception $e) {
 			return false;
 		}

 	}


 	function listing_with_settings($params=array(), $paging=TRUE){

 		try { 			

 			//if( empty($params) ) throw new Exception("Can't return full list", 1);
 			
 			if( $paging ){

	 			//TOTAL ROWS
	 			
				//where clause
				if(isset($params['where'])){
					$this->db->where($params['where']);
				}

				if(isset($params['where_str']) && $params['where_str']!='' ){
					$this->db->where($params['where_str'], null, false);
				}

	 			$this->db->select('count(*) as total');

	 			$query = $this->db->get('users');
				
				$total_rows = $query->row()->total;
				$query->free_result(); //free results
			}
			
			//RESULTS
			
			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

			//limits
			if(isset($params['limits'])){
				$this->db->limit($params['limits']['limit'], $params['limits']['start']); 
			}

			//sorting
			if( isset($params['sorting']) ){
				if( is_array($params['sorting']) ){
					$this->db->order_by($params['sorting']['sort'], $params['sorting']['order']);
				}else{
					$this->db->order_by($params['sorting']);
				}

			}else{
				$this->db->order_by('`users`.`fullname`', 'desc');
			}

			$this->db->join('client', 'client ON client.client_id = users.client_id', 'LEFT OUTER');
			$this->db->join('users_settings', 'users_settings ON users_settings.agent_name = users.username', 'LEFT OUTER');
			
 			$query = $this->db->get('users');

			$result = $query->result();


			$query->free_result(); //free results

			 
			return ($paging) ? array('results'=>$result, 'total_rows'=>$total_rows) : $result;
			 

 		} catch (Exception $e) {
 			return false;
 		}

 	}



    function insert_audit_trail( $params ) {   

        try
            {
            if(count($params)==0) throw new Exception("Error : Empty Parameter", 1);

            if(!isset($params['ref_table']))
                throw new Exception("Error : Table name must not be empty.");

            if(!isset($params['ref_field']) && !isset($params['ref_val']))
                throw new Exception("Error : Table index name and value  must not be empty.");      
            if(!isset($params['created_by']))
                throw new Exception("Error : Agent Name  must not be empty.");
            if(!isset($params['data_json']))
                throw new Exception("Error : Message Activity  must not be empty.");            

            return ($this->db->insert('table_audit_trail', $params))?$this->db->insert_id():0;

        }catch(Exception $error) { 
            return  $error->getMessage();
        }
    }


  	function get_audit_trail( $pro_id = ''){
    
    	try{

			if($pro_id=='') throw new Exception("Error : pro_id is required", 1);

			$query = $this->db
			        ->where('ref_table', 'client_procedure')
			      	->where('ref_field','pro_id')
			      	->where('ref_val',$pro_id)
			      	->get('table_audit_trail'); 

			 
			return $query->result(); 
			

		}catch(Exception $error){ 
			return  0;
		}
  	}  


}