<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transactionmodel extends CI_Model { 


	function insert($set){

		$this->db->db_debug = FALSE;

		try {
			
			if( empty($set) ) throw new Exception("Empty insert data", 1);
			
			$tran_id = $this->_add($set);

			if( $tran_id > 0 ){

				$this->db->db_debug = TRUE;

				return $tran_id;
			}else{
				if( strpos('_'.$this->db->_error_message(), 'ref_number_Index') !== false ){	 			
		 			$this->_add($set);
		 		}
			}
			

		} catch (Exception $e) {
			return false;
		}

	}


	function _add($set){
		
		//$prefix = @trim($this->session->userdata('ref_no_prefix'));
		
		// $query_res = $this->db->query('SELECT count(*) as num_rows FROM transaction')->row();
		// $ref_number = (100001)+$query_res->num_rows;
		
		$query_res = $this->db->query('SELECT ref_number  FROM transaction ORDER BY tran_id desc LIMIT 1')->row();
		$ref_number = $query_res->ref_number+1;
		
  		$set['ref_number'] 	= $ref_number;	

  		if( $this->db->insert('transaction', $set) ){
  			return $this->db->insert_id();

  		}else{
  			return false;
  		}
	}
 


	function update($tran_id, $set){

		$tran_id = trim($tran_id);

		try {
			
			if( empty($tran_id) ) throw new Exception("tran_id is required", 1);
			if( empty($set) ) throw new Exception("set param is required", 1);
						
			$this->db->where('tran_id', $tran_id);
			return $this->db->update('transaction', $set);

		} catch (Exception $e) {
			return false;
		}

	}

 	function row($params){

 		try {
 			
 			if( empty($params) ) throw new Exception("Params is required", 1);
 			

			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

			$this->db->join('client', 'client ON client.client_id = transaction.client_id', 'LEFT OUTER');
			$query = $this->db->get('transaction');

			return $query->row();

 		} catch (Exception $e) {
 			return false;
 		}


 	}

 	function row_plain($params){

 		try {
 			
 			if( empty($params) ) throw new Exception("Params is required", 1);
 			

			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}
			
			$query = $this->db->get('transaction');

			return $query->row();

 		} catch (Exception $e) {
 			return false;
 		}


 	}	

 	function listing($params=array(), $paging=TRUE){

 		try { 			

 			if( empty($params) ) throw new Exception("Can't return full list", 1);
 			

 			if( $paging ){

	 			//TOTAL ROWS
	 			
				//where clause
				if(isset($params['where'])){
					$this->db->where($params['where']);
				}

				if(isset($params['where_str']) && $params['where_str']!='' ){
					$this->db->where($params['where_str'], null, false);
				}

	 			$this->db->select('count(*) as total');

				$this->db->join('client', 'client ON client.client_id = transaction.client_id', 'LEFT OUTER');
	 			$query = $this->db->get('transaction');
				
				$total_rows = $query->row()->total;
				$query->free_result(); //free results
			}
			
			//RESULTS
			
			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

			//limits
			if(isset($params['limits'])){
				$this->db->limit($params['limits']['limit'], $params['limits']['start']); 
			}

			//sorting
			if( isset($params['sorting']) ){
				if( is_array($params['sorting']) ){
					$this->db->order_by($params['sorting']['sort'], $params['sorting']['order']);
				}else{
					$this->db->order_by($params['sorting']);
				}

			}else{
				 	
				$order_by = '`tran_status` DESC,
								IF(`tran_status`=1, due_in_min, "") DESC, 								 
								IF(`tran_status`=1, IF(`tran_priority` = 0, 4, `tran_priority`), "") ASC,  
								IF(`tran_status`=1, `alert_due_dt`, "") ASC,
								IF(`tran_status`=0, `alert_custom_dt`, "") DESC  
							';

				
				$this->db->order_by($order_by);
			}

			$select = 'transaction.*, client.client_name';
			$select .= ', IF( alert_updated_dt IS NULL , alert_created_dt, alert_updated_dt) AS alert_custom_dt';
			
			$select .= ', IF(alert_due_dt IS NULL,  IF(alert_updated_dt IS NULL, alert_created_dt, alert_updated_dt), alert_due_dt ) AS alert_due_custom_dt';
			
			$select .= ', CASE 
						WHEN tran_status = 0 THEN 0
						WHEN CURRENT_TIMESTAMP > alert_due_dt THEN 11	
						WHEN TIMESTAMPDIFF(MINUTE, alert_due_dt, CURRENT_TIMESTAMP ) > -10 THEN 10
						ELSE 0
					END AS due_in_min';

			$select .= ', TIMESTAMPDIFF(MINUTE, alert_due_dt, call_hand_start) as calculate1';
			$select .= ', TIMESTAMPDIFF(MINUTE, call_hand_start,call_hand_end) as calculate2';

			$this->db->select($select);
			$this->db->join('client', 'client ON client.client_id = transaction.client_id', 'LEFT OUTER');
 			$query = $this->db->get('transaction');

			$result = $query->result();
			$query->free_result(); //free results
			 
			// echo '<pre>';
			// echo $this->db->last_query();
			// echo '</pre>';

			return ($paging) ? array('results'=>$result, 'total_rows'=>$total_rows) : $result;
			 

 		} catch (Exception $e) {
 			return false;
 		}

 	}	

 	function listing2($params=array(), $paging=TRUE){

 		try { 			

 			if( empty($params) ) throw new Exception("Can't return full list", 1);
 			

 			if( $paging ){

	 			//TOTAL ROWS
	 			
				//where clause
				if(isset($params['where'])){
					$this->db->where($params['where']);
				}

				if(isset($params['where_str']) && $params['where_str']!='' ){
					$this->db->where($params['where_str'], null, false);
				}

	 			$this->db->select('count(*) as total');

				$this->db->join('client', 'client ON client.client_id = transaction.client_id', 'LEFT OUTER');
	 			$query = $this->db->get('transaction');
				
				$total_rows = $query->row()->total;
				$query->free_result(); //free results
			}
			
			//RESULTS
			
			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

			//limits
			if(isset($params['limits'])){
				$this->db->limit($params['limits']['limit'], $params['limits']['start']); 
			}

			//sorting
			if( isset($params['sorting']) ){
				if( is_array($params['sorting']) ){
					$this->db->order_by($params['sorting']['sort'], $params['sorting']['order']);
				}else{
					$this->db->order_by($params['sorting']);
				}

			}else{
				 
				$order_by = '`tran_status` DESC,
								IF(`tran_status`=1, due_in_min, "") DESC, 								 
								IF(`tran_status`=1, IF(`tran_priority` = 0, 4, `tran_priority`), "") ASC,  
								IF(`tran_status`=1, `alert_due_dt`, "") ASC,
								IF(`tran_status`=0, `alert_custom_dt`, "") DESC  
							';


				//$this->db->order_by(' `tran_status` DESC, tran_priority ASC, IF(`tran_status`=1, `due_in_min`,  IF(alert_due_dt IS NULL, IF( alert_updated_dt IS NULL , alert_created_dt, alert_updated_dt), alert_due_dt)) DESC ');
				$this->db->order_by($order_by);
			}

			$select = 'transaction.*, client.client_name';
			$select .= ', IF( alert_updated_dt IS NULL , alert_created_dt, alert_updated_dt) AS alert_custom_dt';
			
			//$select .= ', IF(alert_due_dt IS NULL,  IF(alert_updated_dt IS NULL, alert_created_dt, alert_updated_dt) + INTERVAL 10 MINUTE, alert_due_dt ) AS alert_due_custom_dt';
			$select .= ', IF(alert_due_dt IS NULL,  IF(alert_updated_dt IS NULL, alert_created_dt, alert_updated_dt), alert_due_dt ) AS alert_due_custom_dt';
			
 
			
			$select .= ', CASE 
						WHEN tran_status = 0 THEN 0
						WHEN CURRENT_TIMESTAMP > alert_due_dt THEN 11	
						WHEN TIMESTAMPDIFF(MINUTE, alert_due_dt, CURRENT_TIMESTAMP ) > -10 THEN 10
						ELSE 0
					END AS due_in_min';

			$select .= ', TIMESTAMPDIFF(MINUTE, alert_due_dt, call_hand_start) as calculate1';
			$select .= ', TIMESTAMPDIFF(MINUTE, call_hand_start,call_hand_end) as calculate2';

			$this->db->select($select);
			$this->db->join('client', 'client ON client.client_id = transaction.client_id', 'LEFT OUTER');
 			$query = $this->db->get('transaction');

			$result = $query->result();
			$query->free_result(); //free results
			 
			//echo $this->db->last_query();

			return ($paging) ? array('results'=>$result, 'total_rows'=>$total_rows) : $result;
			 

 		} catch (Exception $e) {
 			return false;
 		}

 	}


 	/**
 	 * This function is to get all the Customer Name which Cient in Escalation System
 	 * but the data are from the Intensifier push to escalation
 	 * @return [type] [description]
 	 */
 	function get_customer_name(){

 		$this->db->group_by('cust_name');
 		$this->db->where('alert_type', 'CMA REMINDER');
 		$query = $this->db->get('transaction');
 		$result = $query->result();

 		$ar = array();
 		$ar[''] = ' --- All Client --- ';
 		foreach ($result as $row) {
 			$ar[$row->cust_name] = $row->cust_name;
 		}

 		return $ar;

 	}


 	function get_thread_count($tran_id=''){

 		try {
 			
 			if( empty($tran_id) ) throw new Exception("Params is required", 1);
 			
			//where clause 
			$this->db->where('tran_id', $tran_id); 

			$this->db->select('thread_count');
			$query = $this->db->get('transaction');

			$row = $query->row();
			return $row->thread_count;

 		} catch (Exception $e) {
 			return false;
 		}

 	}
}