<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clientcustomtable extends CI_Model { 


 	public function create_base_table($client_id, $table_name='', $table_desc='Callresponse lookup', $more_fields=''){

 		$this->load->dbforge();
 
 		$jsonobj = new stdClass();
 		$jsonobj->status = 1;
 		$jsonobj->msg = '';

 		try {

 			$db_debug = $this->db->db_debug; //save setting
			$this->db->db_debug = FALSE; //disable debugging for queries


 			//$new_table_name = DYNAMIC_TABLE_PREFIX.'_'.$client_id.'_'.trim($table_name);

 			if( empty($table_name) ) throw new Exception("Source Table name is empty", 1);
 	 
			
			$this->dbforge->add_field('id');
			$this->dbforge->add_field("`created_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP");
		 	
		 	if( is_array($more_fields) AND !empty($more_fields) ){
		 		foreach ($more_fields as $mfield) {
		 		 	$this->dbforge->add_field($mfield);
		 		}
		 	}


			$attributes = array('ENGINE' => 'InnoDB');

			if( !$this->dbforge->create_table($table_name, FALSE, $attributes) ){
				$jsonobj->status = 0;

				$error_msg = $this->db->error();
				$jsonobj->msg = $error_msg['message'];
 
			}else{

				$cct['client_id'] = $client_id;
				$cct['table_name'] = $table_name;
				$cct['table_desc'] = $table_desc;
				$this->db->insert('client_custom_table', $cct);				
			}
			
			$this->db->db_debug = $db_debug; //restore setting

			return $jsonobj;

 		} catch (Exception $e) {

 			$jsonobj->status = 0;
 			$jsonobj->msg = $e->getMessage();

 			return $jsonobj;
 		}

 	}


 	public function add_column($table_name, $fields=array()) {

 		$this->load->dbforge();
 
 		$jsonobj = new stdClass();
 		$jsonobj->status = 1;
 		$jsonobj->msg = '';

 		try {
 			
 			$db_debug = $this->db->db_debug; //save setting
			$this->db->db_debug = FALSE; //disable debugging for queries

 			$table_fields = array();

 			foreach ($fields as $field) {

 				if( empty($field) ) continue;

 				$table_fields[$field] = array('type'=>'TEXT');
 			}

			if( count($table_fields) == 0 ) throw new Exception("Empty fields", 1);
			
	        	
	        if( !$this->dbforge->add_column($table_name, $table_fields) ){
				$jsonobj->status = 0;

				$error_msg = $this->db->error();
				$jsonobj->msg = $error_msg['message'];
 
	        }else{

	        	//audit trail here 

                $this->insert_audit_trail(
                    array(
                        'ref_table'=>$table_name,
                        'ref_field'=>'id',
                        'audit_type'=>'UF',
                        'ref_val'=>0,
                        'created_by'=>@$this->agent_name,
                        'created'=>strtotime("now"),
                        'data_json'=> json_encode($table_fields)
                    ));

			}

			$this->db->db_debug = $db_debug; //restore setting

			return $jsonobj;

 		} catch (Exception $e) {
 			$jsonobj->status = 0;
 			$jsonobj->msg = $e->getMessage();

 			return $jsonobj;
 		}

 	}

 	public function remove_column($table_name){
 		$this->load->dbforge();
 
 		$jsonobj = new stdClass();
 		$jsonobj->status = 1;
 		$jsonobj->msg = '';


 		try {
 
 			$db_debug = $this->db->db_debug; //save setting
			$this->db->db_debug = FALSE; //disable debugging for queries

 			 
	        //$list_fields = $this->db->list_fields($table_name);
	        $list_fields = $this->get_table_fields($table_name);
 
	        foreach ($list_fields as $field) { 
	        	
	        	if( !in_array($field, array('id', 'created_dt')) ){
	        		 
	        		if( !$this->dbforge->drop_column($table_name, $field) ){
	        			$jsonobj->status = 0;
						$error_msg = $this->db->error(); 
	        			$jsonobj->msg .= $error_msg['message'];
	        		}
	        	}	        	 
	        } 

			$this->db->db_debug = $db_debug; //restore setting

			return $jsonobj;
 			
 		} catch (Exception $e) {
 			$jsonobj->status = 0;
 			$jsonobj->msg = $e->getMessage();

 			return $jsonobj;
 		}
 	}

 	public function get_table_fields($table_name, $type=''){
 		$fields = $this->db->list_fields($table_name);
 		
 		unset($fields[0]);
 		unset($fields[1]);

 		if( $type == 'array' ){
 			$_f = array();
 			foreach ($fields as $f) {
 				$_f[$f] = $f;
 			}

 			$fields = $_f;
 		}else{
 			$fields = array_values($fields);
 		}


 		return $fields;

 	}


 	public function get_table_data($table_name, $params='', $result_type='', $sort_text='') {


		//where clause
		if(isset($params['where'])){
			$this->db->where($params['where']);
		}		

		if(isset($params['where_str']) && $params['where_str']!='' ){
			$this->db->where($params['where_str'], null, false);
		}


		if( trim($sort_text) != ''  ){
			$this->db->order_by($sort_text);

		}

		$query = $this->db->get($table_name);
 
		//$result = $query->result();

		if( $result_type=='result_array' ){
			$result = $query->result_array();						
		}else{
			$result = $query->result();			
		}

		$query->free_result(); //free results 

 		return $result;
 	}


 	public function get_table_row($table_name, $params='', $result_type="row") {

 		try {
 			
 			if( empty($params) ) throw new Exception("Params is required", 1);
 			

			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}


			//sorting
			if( isset($params['sorting']) ){
				if( is_array($params['sorting']) ){
					$this->db->order_by($params['sorting']['sort'], $params['sorting']['order']);
				}else{
					$this->db->order_by($params['sorting']);
				}
			}

			//limits
			if(isset($params['limits'])){
				$this->db->limit($params['limits']['limit'], $params['limits']['start']); 
			}


			$query = $this->db->get($table_name);


			if( $result_type=='row_array' ){
				return $query->row_array();
			}else{
				return $query->row();
			}

 

 		} catch (Exception $e) {
 			return false;
 		}
 	}


 	public function truncate_table($table_name){
		return $this->db->empty_table($table_name);
 	}


	function add_batch($table_name, $set){

		$this->truncate_table($table_name);

		return $this->db->insert_batch($table_name, $set);
	}

 	public function check_table_name_exist($table_name){
 
 		$this->db->where('table_name', $table_name);
 		$table = $this->db->get('client_custom_table');

 		return $table->num_rows();
 	}



 	function clean_fields($fields){		

		$new_fields = array();
		foreach ($fields as $field) {
			$field = $this->_clean_field($field);
			if( !empty($field) )
				$new_fields[] = $field;
		}

		/*// counts the number of words
		$word_count = array_count_values($new_fields);
		// words we've seen so far
		$words_so_far = array();
		// for each word, check if we've encountered it so far
		//    - if not, add it to our list
		//    - if yes, replace it
		foreach($new_fields as $k => $word){
		    if($word_count[$word] > 1 && in_array($word, $words_so_far)){
		        $new_fields[$k] = $your_replacement_here;
		    }
		    elseif($word_count[$word] > 1){
		        $words_so_far[] = $word;
		    }
		}*/

		return $new_fields;

 	}

 	function _clean_field($field){

        $res = preg_replace("/[^a-zA-Z0-9\s]/", " ", $field);
        //echo '|'.$res.'|';
        $res = trim($res);
        $res = preg_replace('/\s+/', '_',$res);

		return $res;
 	}
  

    function insert_audit_trail( $params ) {   

        try
            {
            if(count($params)==0) throw new Exception("Error : Empty Parameter", 1);

            if(!isset($params['ref_table']))
                throw new Exception("Error : Table name must not be empty.");

            if(!isset($params['ref_field']) && !isset($params['ref_val']))
                throw new Exception("Error : Table index name and value  must not be empty.");      
            if(!isset($params['created_by']))
                throw new Exception("Error : Agent Name  must not be empty.");
            if(!isset($params['data_json']))
                throw new Exception("Error : Message Activity  must not be empty.");            

            return ($this->db->insert('table_audit_trail', $params))?$this->db->insert_id():0;

        }catch(Exception $error) { 
            return  $error->getMessage();
        }
    }

  	function get_audit_trail( $client_id = ''){
    
    	try{

			if($client_id=='') throw new Exception("Error : client_id is required", 1);

			$query = $this->db
			        ->where('ref_table', 'client')
			      	->where('ref_field','client_id')
			      	->where('ref_val',$client_id)
			      	->get('table_audit_trail'); 

			 
			return $query->result(); 
			

		}catch(Exception $error){ 
			return  0;
		}
  	} 
}