<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clientmodel extends CI_Model { 


	function insert($set){
 

		try {
			
			if( empty($set) ) throw new Exception("Empty insert data", 1);  
			
			if( $this->db->insert('client', $set) ){

				$client_id = $this->db->insert_id();

                     $this->insert_audit_trail(
                        array(
                            'ref_table'=>'client',
                            'ref_field'=>'client_id',
                            'audit_type'=>'I',
                            'ref_val'=>$client_id,
                            'created_by'=>@$set['added_by'],
                            'created'=>strtotime("now"),
                            'data_json'=> json_encode($set)
                        ));


	  			return $client_id;

	  		}else{
	  			return false;
	  		}
			

		} catch (Exception $e) {
			return false;
		}

	}
  

	function update($client_id, $set){

		$client_id = trim($client_id);

		try {
			
			if( empty($client_id) ) throw new Exception("client_id is required", 1);
			if( empty($set) ) throw new Exception("set param is required", 1);
						
			$this->db->where('client_id', $client_id);
			
			if( $this->db->update('client', $set) ){

             $this->insert_audit_trail(
                array(
                    'ref_table'=>'client',
                    'ref_field'=>'client_id',
                    'audit_type'=>'U',
                    'ref_val'=>$client_id,
                    'created_by'=>@$set['last_update_by'],
                    'created'=>strtotime("now"),
                    'data_json'=> json_encode($set)
                ));

             	return $client_id;
            }else{
            	return false;
            }

		} catch (Exception $e) {
			return false;
		}

	}

 	function row($params,$row=true){

 		try {
 			
 			if( empty($params) ) throw new Exception("Params is required", 1);

			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

			$query = $this->db->get('client');


			if($row==true){

				return $query->row();	

			}else{

				return $query->row_array();	
			}
			

 		} catch (Exception $e) {
 			return false;
 		}


 	}	

 	function listing($params=array(), $paging=TRUE){

 		try { 			

 			//if( empty($params) ) throw new Exception("Can't return full list", 1);
 			

 			if( $paging ){

	 			//TOTAL ROWS
	 			
				//where clause
				if(isset($params['where'])){
					$this->db->where($params['where']);
				}

				if(isset($params['where_str']) && $params['where_str']!='' ){
					$this->db->where($params['where_str'], null, false);
				}

	 			$this->db->select('count(*) as total'); 
	 			$query = $this->db->get('client');
				
				$total_rows = $query->row()->total;
				$query->free_result(); //free results
			}
			
			//RESULTS
			
			//select
			if(isset($params['select'])){
				$this->db->select($params['select']);
			}
			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

			//limits
			if(isset($params['limits'])){
				$this->db->limit($params['limits']['limit'], $params['limits']['start']); 
			}

			//sorting
			if( isset($params['sorting']) ){
				if( is_array($params['sorting']) ){
					$this->db->order_by($params['sorting']['sort'], $params['sorting']['order']);
				}else{
					$this->db->order_by($params['sorting']);
				}

			}else{
				$this->db->order_by('`created_at`', 'desc');
			}

 			$query = $this->db->get('client');

			$result = $query->result();
			$query->free_result(); //free results

			 
			return ($paging) ? array('results'=>$result, 'total_rows'=>$total_rows) : $result;
			 

 		} catch (Exception $e) {
 			return false;
 		}

 	}


 	function combine_clients_array(){

 		try {
 			
 			//client table
 			$this->db->select('client_id, client_name');
 			$this->db->order_by('client_name', 'asc');
 			$query = $this->db->get('client');
			$result = $query->result();

			$arr = array();
			$arr[''] = ' - All Client / CUST NAME - ';
			foreach ($result as $row) {
				$arr[$row->client_id] = $row->client_name;
			}


			//transaction.cust_name
	 		$this->db->group_by('cust_name');
	 		$this->db->order_by('cust_name', 'asc');
	 		$this->db->where('alert_type', 'CMA REMINDER');
	 		$query = $this->db->get('transaction');
	 		$result = $query->result();

	 		$arr['---'] = ' --- CMA REMINDERS CUST NAME --- ';
	 		foreach ($result as $row) {
	 			$arr[$row->cust_name] = $row->cust_name;
	 		}

	 		return $arr;

		} catch (Exception $e) {
			
		}	

 	}


    function insert_audit_trail( $params ) {   

        try
            {
            if(count($params)==0) throw new Exception("Error : Empty Parameter", 1);

            if(!isset($params['ref_table']))
                throw new Exception("Error : Table name must not be empty.");

            if(!isset($params['ref_field']) && !isset($params['ref_val']))
                throw new Exception("Error : Table index name and value  must not be empty.");      
            if(!isset($params['created_by']))
                throw new Exception("Error : Agent Name  must not be empty.");
            if(!isset($params['data_json']))
                throw new Exception("Error : Message Activity  must not be empty.");            

            return ($this->db->insert('table_audit_trail', $params))?$this->db->insert_id():0;

        }catch(Exception $error) { 
            return  $error->getMessage();
        }
    }

  	function get_audit_trail( $client_id = ''){
    
    	try{

			if($client_id=='') throw new Exception("Error : client_id is required", 1);

			$query = $this->db
			        ->where('ref_table', 'client')
			      	->where('ref_field','client_id')
			      	->where('ref_val',$client_id)
			      	->get('table_audit_trail'); 

			 
			return $query->result(); 
			

		}catch(Exception $error){ 
			return  0;
		}
  	} 
}