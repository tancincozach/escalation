<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emaildatamodel extends CI_Model { 

	var $table = 'email_data';

	function insert($set){

		$db_debug = $this->db->db_debug;
		
		try {
		
			$this->db->db_debug = false;

			if( !$this->db->insert($this->table, $set) ){
				throw new Exception(@$this->db->error(), 1);
			}

			$this->db->db_debug = $db_debug;

			return 1;

		} catch (Exception $e) {

			$this->db->db_debug = $db_debug;

			return $e->getMessage();
		}


	}

	function update($id, $set){


		try {
			
			if( empty($id) ) throw new Exception("id is required", 1);
			if( empty($set) ) throw new Exception("set param is required", 1);
						
			$this->db->where('id', $id);
			
			if( $this->db->update($this->table, $set) ){

             	return $id;
            }else{
            	return false;
            }

		} catch (Exception $e) {
			return false;
		}

	}

 	function row($params){

 		try {
 			
			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}		

			//sorting
			if( isset($params['sorting']) ){
				if( is_array($params['sorting']) ){
					$this->db->order_by($params['sorting']['sort'], $params['sorting']['order']);
				}else{
					$this->db->order_by($params['sorting']);
				}

			}else{
				$this->db->order_by('`email_data`.`created_at`', 'asc');
			}

			$query = $this->db->get($this->table);

			return $query->row();

 		} catch (Exception $e) {
 			return false;
 		}



 	}	

 	function get_result($params){

 		try { 

			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}


			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

			//limits
			if(isset($params['limits'])){
				$this->db->limit($params['limits']['limit'], $params['limits']['start']); 
			}

			//sorting
			if( isset($params['sorting']) ){
				if( is_array($params['sorting']) ){
					$this->db->order_by($params['sorting']['sort'], $params['sorting']['order']);
				}else{
					$this->db->order_by($params['sorting']);
				}

			}else{
				$this->db->order_by('`email_data`.`created_at`', 'desc');
			}

			if(isset($params['select'])){

				$this->db->select($params['select']);
			}	

 			$query = $this->db->get($this->table);

			$result = $query->result();

			$query->free_result(); //free results

			return $result;

 		} catch (Exception $e) {
 			return false;
 		}

 	} 	

 	function get_result_chart($params){

 		try { 

			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

			if(isset($params['select']) && $params['select']!='' ){
				$this->db->select($params['select']);
			}
			
 			$query = $this->db->get($this->table);


 			//echo $this->db->last_query().'<br/>';

			$result = $query->row();

			$query->free_result(); //free results

			return $result;

 		} catch (Exception $e) {
 			return false;
 		}

 	}

 	function get_result_pagination($params){

 		try { 			

 			//TOTAL ROWS
 			
			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

 			$this->db->select('count(*) as total');
 			$query = $this->db->get($this->table);
			$total_rows = $query->row()->total;
			$query->free_result(); //free results

			
			//RESULTS
			
			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

			//limits
			if(isset($params['limits'])){
				$this->db->limit($params['limits']['limit'], $params['limits']['start']); 
			}

			//sorting
			if( isset($params['sorting']) ){
				if( is_array($params['sorting']) ){
					$this->db->order_by($params['sorting']['sort'], $params['sorting']['order']);
				}else{
					$this->db->order_by($params['sorting']);
				}

			}else{
				$this->db->order_by('`email_data`.`created_at`', 'desc');
			}

 			$query = $this->db->get($this->table);

			$result = $query->result();
			$query->free_result(); //free results

			return array('results'=>$result, 'total_rows'=>$total_rows);

 		} catch (Exception $e) {
 			return false;
 		}

 	}

 	/**
 	 * join to transaction
 	 * @param  [type] $params [description]
 	 * @return array
 	 */
 	function get_result_pagination_link($params ){

 		try { 			

 			//TOTAL ROWS
 			
			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

 			$this->db->select('count(*) as total');
 			$this->db->join('transaction', 'transaction ON transaction.tran_id = email_data.tran_id', 'LEFT OUTER');
 			$query = $this->db->get($this->table);
			$total_rows = $query->row()->total;
			$query->free_result(); //free results

			
			//RESULTS
			
			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

			//limits
			if(isset($params['limits'])){
				$this->db->limit($params['limits']['limit'], $params['limits']['start']); 
			}

			//sorting
			if( isset($params['sorting']) ){
				if( is_array($params['sorting']) ){
					$this->db->order_by($params['sorting']['sort'], $params['sorting']['order']);
				}else{
					$this->db->order_by($params['sorting']);
				}

			}else{
				$this->db->order_by('`email_data`.`created_at`', 'desc');
			}


			if(isset($params['select'])){
				$this->db->select($params['select']); 
			}


			$this->db->join('transaction', 'transaction ON transaction.tran_id = email_data.tran_id', 'LEFT OUTER');


 			$query = $this->db->get($this->table);

 			//echo $this->db->last_query();

			$result = $query->result();
			$query->free_result(); //free results

			return array('results'=>$result, 'total_rows'=>$total_rows);

 		} catch (Exception $e) {
 			return false;
 		}

 	}
}