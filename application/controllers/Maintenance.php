<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maintenance extends MY_Controller {

	public function __construct(){
		
		parent::__construct();

		if( $this->user_lvl == 0 ){
			$this->session->set_flashdata('flash_error_msg', "You don't have permission to access the previous page");
			redirect('dashboard');
		}

		$this->view_data['view_file'] = 'maintenance/index';
		$this->view_data['menu_active'] = 'maintenance';
		$this->view_data['data'] = '';
	}

	public function index(){
		

		$data = '';
		
		$this->view_data['maintain']['data'] = $data;
		$this->view_data['maintain']['menu_active'] = 'general_setting';
		$this->view_data['maintain']['view_file'] = 'maintenance/general_setting';
		
		//$this->view_data['js_file'] = '<script src="assets/js/page_js/dashboard.js"></script>'; 		
		$this->load->view('template', $this->view_data); 

	}

	public function users(){
		$this->load->model('Clientmodel','client');
		$this->load->model('Usermodel','user');

		try {
			
			$data = '';

			$get = $this->input->get();

/*			if( $this->client_id > 0 ){
				$clients_params['where']['client_id'] = $this->client_id;
			}
*/
/*			$clients_params['select']				 = 'client_id, client_name';
			$clients_params['where']['client_status'] = 1;
			$clients_params['sorting']['sort'] 		 = 'client_name';
			$clients_params['sorting']['order']		 = 'ASC';
			$data['clients'] = $this->client->listing($clients_params, false);*/


			/*$cr_params = array();
			$client_id = '';
			
			if( $this->client_id > 0 ){

 					$client_id = $this->client_id; 

		
 				
			}else{
				if( isset($get['client_id']) ){
					$client_id = $get['client_id'];	 
				}
			} 


			 
			if( $client_id > 0 ){

				$cr_params['where']['users.client_id'] = $client_id;	


				$data['users'] = $this->user->listing($cr_params, false);				 
			 
				$data['client'] = $this->client->row( array('where'=>array('client_id'=>$client_id)) );
			
			} 

			if(isset($get['client'])){

				$cr_params['where']['users.client_id'] = 0;	

				$data['users'] = $this->user->listing($cr_params, false);	

				$data['client_all'] = true;

				$client_row = new stdClass(); 

				$client_row->client_name = 'DEFAULT';

				$client_row->client_id = 0;

				$data['client'] = $client_row;



			}*/
			$params['sorting']['sort'] 		 = 'fullname';
			$params['sorting']['order']		 = 'ASC';
			$data['users'] = $this->user->listing_with_settings($params, false);	
			$this->view_data['maintain']['data'] = $data;
			$this->view_data['maintain']['menu_active'] = 'users';
			$this->view_data['maintain']['view_file'] = 'maintenance/users/listing';
			
			//$this->view_data['js_file'] = '<script src="assets/js/page_js/dashboard.js"></script>'; 		
			$this->load->view('template', $this->view_data); 

		} catch (Exception $e) {
			echo $e->getMessage();
		}

	}

	public function user_form($form_type=''){

		$this->load->model('Usermodel','user');
		$this->load->model('Clientmodel','client');
		$this->load->model('Usersettingsmodel','user_settings');

		$data = '';

		$param = $this->input->get();

		if($_POST){

			$post = $this->input->post();


			$set['user_lvl']  = $post['lvl'];
			$set_opt['display_screen'] = '';

			$set['fullname']  = $post['full_name'];
			$set['username']  = $post['user_name'];
			$set['password']  = md5($post['password']);	
			$set['added_by']  = $this->agent_name;			
			$set['user_status']  = $post['status'];
			$set['user_lvl']  = $post['lvl'];
			$set['client_id']  = $post['client'];
			$set_opt['display_screen'] = @$post['display_screen'];



			try {

				 if(empty($set['fullname']) 
			 	 || empty($set['username'])) throw new Exception("ERROR: Please don't leave the fields empty.");
				 
 				if(empty($form_type)) throw new Exception("ERROR: Cannot find action.");

				 switch ($form_type) {

				 	case 'add':				 			

				 				$set['client_id'] 	     = isset($post['client_id']) ? $post['client_id']:0;


								$uid = $this->user->insert($set);

								if(!$uid){
									 throw new Exception("ERROR: Failure to add user..");
								}
								
								$this->user_settings->set_option($set['username'],$set_opt);

								if($uid > 0){

									$this->session->set_flashdata('msg', ' User successfully added');			

									$redirect = 'maintenance/users/?client_id='.$post['client_id'];	

									if($set['client_id']==0){
										$redirect = 'maintenance/users/?client=all';	
									}
									

								}else{
									$this->session->set_flashdata('msg', 'Failed to add user.');	
									$redirect = 'maintenance/users/'.(isset($post['client_id'])?'?client_id='.$post['client_id']:'');
								}


				 		break;				 	
				 	case 'edit':

				 		 	if(empty($post['uid']) || !isset($post['uid'])) throw new Exception("ERROR: Cannot update user.");
								$set['last_updated_date'] = date('Y-m-d H:i:s')	;
								$set['last_update_by'] = $this->agent_name	;

								$row = $this->user->row(array('where'=>array('id'=>$post['uid'])));	


				 				if(!$this->user->update($post['uid'],$set)){
								 	throw new Exception("ERROR: Failure to update user..");	
								}

								$this->user_settings->set_option($row->username,$set_opt);

								$this->session->set_flashdata('msg', 'Updating of user is sucessfull ');

								if($post['client_id']==0){
									$redirect = 'maintenance/users/?client=all';	
								}else{
									$redirect = 'maintenance/users/'.(isset($post['client_id'])?'?client_id='.$post['client_id']:'');	
								}
								
				
				 		break;
				 	
				 	default:

				 			$redirect = 'maintenance/users/'.(isset($post['client_id'])?'?client_id='.$post['client_id']:'');

				 		break;
				 }

					
			} catch (Exception $e) {

				$this->session->set_flashdata('error', $e->getMessage());
				$redirect = 'maintenance/users/'.(($this->client_id == 0)?'?client_id='.$post['client_id']:'');

			}	


				redirect($redirect)	;
		}	


		$param = $this->input->get();
		 
		$client_row = new stdClass(); 

		$client_row->client_name = 'DEFAULT';

		$client_row->client_id = 0;


		try {

			switch ($form_type) {
				case 'add':

						$clients_params['where']['client_id'] = @$param['client_id'];

						$data['client_row'] = $this->client->row($clients_params);

						if(!isset($param['client_id'])){

							$data['client_row'] = $client_row;
						}
						
					break;
				case 'edit':

						if(!isset($param['uid']))throw new Exception("Cannot find selected user");
					
				
						$data['user_row'] = $this->user->row(array('where'=>array('id'=>$param['uid'])));	
						


						$user_opt = $this->user_settings->options(@$data['user_row']->username);


						$data['user_row']->display_screen = @$user_opt->display_screen;


						$clients_params['where']['client_id'] = $data['user_row']->client_id;


						$data['client_row'] = $this->client->row($clients_params);


						if($data['user_row']->client_id==0){

							$data['client_row'] = $client_row;

						}

					break;
				
				default:					

					break;
			}

			
		} catch (Exception $e) {
				$this->session->set_flashdata('error', $e->getMessage());
		}



		$data['form_type']  = $form_type;

		$clients_params = array();

		$clients_params['select']				 = 'client_id, client_name';
		$clients_params['where']['client_status'] = 1;
		$clients_params['sorting']['sort'] 		 = 'client_name';
		$clients_params['sorting']['order']		 = 'ASC';
		$data['clients'] = $this->client->listing($clients_params, false);


		$data['tran_status_desc'] = $this->tran_status_desc;

		$this->view_data['maintain']['data'] = $data;
		$this->view_data['maintain']['menu_active'] = 'users';
		$this->view_data['maintain']['view_file'] = 'maintenance/users/form';
		
		
		$this->load->view('template', $this->view_data);

	}




	public function callresponse( $action =''){
		
		
		$this->load->model('Clientmodel','client');
		$this->load->model('Callresponsemodel','call_response');

		try {
			
			$data = '';

			$get = $this->input->get();

			if( $this->client_id > 0 ){
				$clients_params['where']['client_id'] = $this->client_id;
			}

			$clients_params['select']				 = 'client_id, client_name';
			$clients_params['where']['client_status'] = 1;
			$clients_params['sorting']['sort'] 		 = 'client_name';
			$clients_params['sorting']['order']		 = 'ASC';
			$data['clients'] = $this->client->listing($clients_params, false);

			$cr_params = array();
			$client_id = '';
			
			if( $this->client_id > 0 ){

 					$client_id = $this->client_id; 

/* 					if($get['client_id']!=$client_id) throw new Exception("You can only view your ", 1);*/
 						
 				
			}else{
				if( isset($get['client_id']) ){
					$client_id = $get['client_id'];	 
				}
			} 


			 
			if( $client_id > 0 ){

				$cr_params['where']['call_responses.client_id'] = $client_id;	
				$cr_params['sorting'] = array('sort'=>'call_res_order', 'order'=>'asc');
				
				$data['call_response'] = $this->call_response->listing($cr_params, false);				 
			 
				$data['client'] = $this->client->row( array('where'=>array('client_id'=>$client_id)) );
				//echo $this->db->last_query();
				//print_r($data['client']);
			} 

			if(isset($get['client'])){

				$cr_params['where']['call_responses.client_id'] = 0;	
								
				$cr_params['sorting'] = array('sort'=>'call_res_order', 'order'=>'asc');

				$data['call_response'] = $this->call_response->listing($cr_params, false);	

				$data['client_all'] = true;

				$client_row = new stdClass(); 

				$client_row->client_name = 'DEFAULT';

				$client_row->client_id = 0;

				$data['client'] = $client_row;



			}


			if(isset($get['cr_id']) && isset($get['client_id'])){

						$this->call_response->delete($get['cr_id']);
						$this->session->set_flashdata('msg', 'Deleting of call response is sucessfull ');

					if($get['client_id']==0){
						redirect('maintenance/callresponse/?client=all');	
					}else{
						redirect('maintenance/callresponse/?client_id='.$get['client_id']);
					}

			}



			$this->view_data['maintain']['data'] = $data;
			$this->view_data['maintain']['menu_active'] = 'call_response';
			$this->view_data['maintain']['view_file'] = 'maintenance/call_response/listing';
			$this->view_data['js_file'] = '<script type="text/javascript" src="assets/js/jquery-ui.min.js"></script>'.PHP_EOL;
			$this->view_data['js_file'] .= '<script src="assets/js/pages/maintenance/call_response.js"></script>'; 
					
			$this->load->view('template', $this->view_data); 

		} catch (Exception $e) {
			echo $e->getMessage();
		}

	}


	public function call_response_form($form_type=''){

		$this->load->model('Callresponsemodel','call_response');
		$this->load->model('Clientmodel','client');

		$data = '';

		if($_POST){

			$post = $this->input->post();


			$set = array();


			$set['call_form_required']    = @$post['call_form_required'];

			$set['call_res_text']    = $post['call_response_name'];
			$set['call_res_open']    = $post['log_status'];
			$set['call_res_status']	 = $post['call_response_status'];			
			$set['call_group']	     = (@$post['call_form_required']==0)?'':$post['group_name'];
			$set['agent_name']	     = $this->agent_name;
			$set['user_id']	 	     = $this->user_id;

			$form_type = $post['form_type'];



			try {

				if(empty($set['call_res_text'])) throw new Exception("ERROR: Please don't leave the fields empty.");
				 
 				if(empty($form_type)) throw new Exception("ERROR: Cannot find action.");

				 switch ($form_type) {

				 	case 'add':				 			

				 				$set['client_id'] 	     = isset($post['client_id']) ? $post['client_id']:0;

								$cr_id = $this->call_response->insert($set);

								if(!$cr_id){
									 throw new Exception("ERROR: Failure to add call response..");
								}
								

								if($cr_id > 0){

									$this->session->set_flashdata('fmesg', ' Call response successfully added');			

									$redirect = 'maintenance/callresponse/?client_id='.$post['client_id'];	

									if($set['client_id']==0){
										$redirect = 'maintenance/callresponse/?client=all';	
									}
									

								}else{
									$this->session->set_flashdata('fmesg', 'Failed to add call response.');	
									$redirect = 'maintenance/callresponse/'.(isset($post['client_id'])?'?client_id='.$post['client_id']:'');
								}


				 		break;				 	
				 	case 'edit':

				 		 	if(empty($post['cr_id']) || !isset($post['cr_id'])) throw new Exception("ERROR: Cannot update call response.");

	 		 					$set['call_res_updated'] = date('Y-m-d H:i:s')	;
								//$set['last_update_by'] = $this->agent_name	;

				 				if(!$this->call_response->update($post['cr_id'],$set)){
								 	throw new Exception("ERROR: Failure to update call response..");	
								}
									$this->session->set_flashdata('msg', 'Updating of call response is sucessfull ');

									if($post['client_id']==0){
										$redirect = 'maintenance/callresponse/?client=all';	
									}else{
										$redirect = 'maintenance/callresponse/'.(isset($post['client_id'])?'?client_id='.$post['client_id']:'');
									}								
				
				 		break;
				 	
				 	default:

				 			$redirect = 'maintenance/call_response/'.(isset($post['client_id'])?'?client_id='.$post['client_id']:'');

				 		break;
				 }

					
			} catch (Exception $e) {

				$this->session->set_flashdata('error', $e->getMessage());
				$redirect = 'maintenance/call_response/'.(($this->client_id == 0)?'?client_id='.$post['client_id']:'');

			}	


				redirect($redirect)	;
		}	


		$param = $this->input->get();
		 
		$client_row = new stdClass(); 

		$client_row->client_name = 'DEFAULT';

		$client_row->client_id = 0;


		try {

			switch ($form_type) {
				case 'add':

						$clients_params['where']['client_id'] = @$param['client_id'];

						$data['client_row'] = $this->client->row($clients_params);

						if(!isset($param['client_id'])){

							$data['client_row'] = $client_row;
						}
						
					break;
				case 'edit':

						if(!isset($param['cr_id']))throw new Exception("Cannot find selected call response");
					
				
						$data['call_response_row'] = $this->call_response->row(array('where'=>array('call_res_id'=>$param['cr_id'])));	

						$clients_params['where']['client_id'] = $data['call_response_row']->client_id;


						$data['client_row'] = $this->client->row($clients_params);


						if($data['call_response_row']->client_id==0){

							$data['client_row'] = $client_row;

						}

					break;
				
				default:					

					break;
			}

			
		} catch (Exception $e) {
				$this->session->set_flashdata('error', $e->getMessage());
		}



		$data['group_name']  = $this->call_response->group_names($data['client_row']->client_id);

		$data['form_type']  = $form_type;



		$data['clients'] = $this->client->listing(array(),false);	

		$data['tran_status_desc'] = $this->tran_status_desc;

		$this->view_data['maintain']['data'] = $data;
		$this->view_data['maintain']['menu_active'] = 'call_response';
		$this->view_data['maintain']['view_file'] = 'maintenance/call_response/form';
		$this->view_data['js_file'] = '<script src="assets/js/pages/maintenance/call_response.js"></script>'; 
		
		
		$this->load->view('template', $this->view_data);

	}

	public function callresponse_reorder(){

		$this->load->model('Callresponsemodel','callresponse');

		$callresids = $_POST['ids'];
 
		foreach($callresids as $i=>$call_res_id){
			
			$set['call_res_order'] 	= $i;
			$set['agent_name'] 		= $_POST['agent_name'];
			$this->callresponse->update($call_res_id, $set);
		}

	}



	public function file_manager($form_type=""){

		
		$this->load->model('Clientmodel','client');
		$this->load->model('Filemanagermodel','filemanager');

	
		try {
			
			$data = '';

			$get = $this->input->get();

			if( $this->client_id > 0 ){
				$clients_params['where']['client_id'] = $this->client_id;
			}

			$clients_params['select']				 = 'client_id, client_name';
			$clients_params['where']['client_status'] = 1;
			$clients_params['sorting']['sort'] 		 = 'client_name';
			$clients_params['sorting']['order']		 = 'ASC';
			$data['clients'] = $this->client->listing($clients_params, false);

			$fm_params = array();
			$client_id = '';


			if( $this->client_id > 0 ){

						$client_id = $this->client_id; 
							
					
			}else{
				if( isset($get['client_id']) ){
					$client_id = $get['client_id'];	 
				}
			} 


			if(!isset($get['client'])){
			 	
				$fm_params['where']['filemanager.client_id'] = $client_id;	

			}else{

				$data['show_all'] = 1;
			}

			if( $client_id > 0 ){

				$fm_params['where']['filemanager.client_id'] = $client_id;				
							
			 
				$data['client'] = $this->client->row( array('where'=>array('client_id'=>$client_id)) );
			
			} 


				$fm_params['select'] = 'filemanager.*';

				$data['files'] = $this->filemanager->listing($fm_params, false);				 

				if(count($data['files']) > 0 ){

					 foreach($data['files'] as $row){
				 

					 	if($row->client_id > 0){
							$c_row = $this->client->row(array('where'=>array('client_id'=>$row->client_id)));
					 		$row->client_name = $c_row->client_name;	
					 	}
					 	
					 }
				}
				$data['client'] = $this->client->row( array('where'=>array('client_id'=>$client_id)) );


			if($form_type =='delete' && isset($get['fid'])){
					
					$file_row  = $this->filemanager->row(array('where'=>array('id'=>$get['fid'])));

					$file_path = './'.$file_row->file_path;


					try {

						if(empty($file_row)) 
							throw new Exception("Cannot find file data", 1);
						
						if(!file_exists(@$file_path))
							throw new Exception("Cannot find file in directory");
						
							if($this->filemanager->delete($get['fid'])){
								unlink($file_path);  
							}

							$this->session->set_flashdata('msg', 'Deleting of file is sucessfull ');

							if($get['client_id']==0){
								redirect('maintenance/file_manager/?client=all');	
							}else{
								redirect('maintenance/file_manager/?client_id='.$get['client_id']);
							}
						
					} catch (Exception $e) {

							$this->session->set_flashdata('error', $e->getMessage());

							if($get['client_id']==0){
								redirect('maintenance/file_manager/?client=all');	
							}else{
								redirect('maintenance/file_manager/?client_id='.$get['client_id']);
							}
						
					}

			}


			$this->view_data['maintain']['data'] = $data;
			$this->view_data['maintain']['menu_active'] = 'file_manager';
			$this->view_data['maintain']['view_file'] = 'maintenance/file_manager/listing';
			
			$this->view_data['js_file'] = '<script src="assets/js/page_js/maintenance/file_manager.js"></script>'; 		
			$this->load->view('template', $this->view_data); 

		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	public function file_manager_form( $form_type=''){

		

		$this->load->model('Clientmodel','client');
		$this->load->model('Filemanagermodel','filemanager');

		$data = '';

		if($_POST){

			$post = $this->input->post();


			$set = array();




			$set['file_name']    = $post['filename'];
			$set['file_status']    = $post['file_status'];			
			$set['agent_name']	     = $this->agent_name;
			$set['user_id']	 	     = $this->user_id;

			$form_type = $post['form_type'];

					try {

						if(empty($set['file_name']) || empty($_FILES)) throw new Exception("ERROR: Please don't leave the fields empty.");

							$filename = preg_replace("/[^a-z0-9.]+/i", "_", trim($set['file_name']));

							if((int)$post['client_id']==0){

								$folder = 'files/client/0/';	

							}else{								

								$client_row = $this->client->row(array('where'=>array('client_id'=>(int)$post['client_id'])));

								if(empty($client_row)) throw new Exception("Client does not exist.");

								if($this->client_id > 0 && $this->client_id==$post['client_id']) throw new Exception("ERROR: You can only upload file on what client you are assigned to");

								$folder = 'files/client/'.$client_row->client_id.'/';

							}	

							if($form_type=='edit'){

								 if($_FILES['file_upload']['tmp_name']!='' && $post['source_file_to_be_replaced']){

										$file_to_be_replaced = $post['source_file_to_be_replaced'];

										if (file_exists('./'.$file_to_be_replaced)) {
												unlink('./'.$file_to_be_replaced);    
										}	 	
								 }
														
							}

													
							$config['upload_path'] = './'.$folder;
							//$config['allowed_types'] = 'gif|jpg|png';
							$config['allowed_types'] = '*';
							$config['overwrite'] = FALSE;

							

							if($form_type=='edit' && $_FILES['file_upload']['tmp_name']!='' && $post['source_file_to_be_replaced']){

								$file_to_be_replaced = $post['source_file_to_be_replaced'];

								if (file_exists('./'.$file_to_be_replaced)) {
										unlink('./'.$file_to_be_replaced);    
								}								

							}
							
							if(!is_dir($config['upload_path'])) {			    
							  	mkdir($config['upload_path'],0777,TRUE);
				  			    chmod($config['upload_path'], 0777);
							} 
							$config['file_name'] = $filename;


							$this->load->library('upload', $config);


							$this->upload->display_errors('<p>', '</p>');

							if (!$this->upload->do_upload('file_upload'))
								throw new Exception($this->upload->display_errors());

							$upload_data = $this->upload->data();

							chmod($upload_data['upload_path'], 0777);

							 //chmod($config['upload_path'], 0777);

							$set['file_name'] 	= $post['filename'];
							$set['file_path'] 	= $folder.$upload_data['file_name'];

							$file_type 			= explode('.', $upload_data['file_name']);
							$set['file_type'] 	= $file_type[count($file_type)-1];

							$set['agent_name'] 	= $this->agent_name;
							$set['user_id'] 	= $this->user_id;						


							 switch ($form_type) {

							 	case 'add':				 			

							 				$set['client_id'] 	     = isset($post['client_id']) ? $post['client_id']:0;


											$fid = $this->filemanager->insert($set);

											if(!$fid){
												 throw new Exception("ERROR: Failure to add file..");
											}
											

											if($fid > 0){

												$this->session->set_flashdata('msg', ' File successfully added');			

												$redirect = 'maintenance/file_manager/?client_id='.$post['client_id'];	

												if($set['client_id']==0){
													$redirect = 'maintenance/file_manager/?client=all';	
												}
												

											}else{
												$this->session->set_flashdata('msg', 'Failed to add file.');	
												$redirect = 'maintenance/file_manager/'.(isset($post['client_id'])?'?client_id='.$post['client_id']:'');
											}


							 		break;				 	
							 	case 'edit':

							 		 	if(empty($post['fid']) || !isset($post['fid'])) throw new Exception("ERROR: Cannot update file.");

				 		 					$set['update_at'] = date('Y-m-d H:i:s')	;
											//$set['last_update_by'] = $this->agent_name	;

							 				if(!$this->filemanager->update($post['fid'],$set)){
											 	throw new Exception("ERROR: Failure to update file..");	
											}
												$this->session->set_flashdata('msg', 'Updating of file is sucessfull ');

											if($post['client_id']==0){
												$redirect = 'maintenance/file_manager/?client=all';	
											}else{
												$redirect = 'maintenance/file_manager/'.(isset($post['client_id'])?'?client_id='.$post['client_id']:'');	
											}
											
							
							 		break;
							 	
							 	default:

							 			$redirect = 'maintenance/file_manager/'.(isset($post['client_id'])?'?client_id='.$post['client_id']:'');

							 		break;
							 }


								
						
					} catch (Exception $e) {

						$this->session->set_flashdata('error', $e->getMessage());


						$redirect = 'maintenance/file_manager/'.(isset($post['client_id'])?'?client_id='.$post['client_id']:'');
					}
			


				redirect($redirect)	;
		}	


		$param = $this->input->get();
		 
		$client_row = new stdClass(); 

		$client_row->client_name = 'DEFAULT';

		$client_row->client_id = 0;


		try {

			switch ($form_type) {
				case 'add':

						$clients_params['where']['client_id'] = @$param['client_id'];

						$data['client_row'] = $this->client->row($clients_params);				

						if(!isset($param['client_id'])){

							$data['client_row'] = $client_row;
						}
						
					break;
				case 'edit':

						if(!isset($param['fid']))throw new Exception("Cannot find selected file");
					
				
						$data['file_row'] = $this->filemanager->row(array('where'=>array('id'=>$param['fid'])));	

						$clients_params['where']['client_id'] = $data['file_row']->client_id;


						$data['client_row'] = $this->client->row($clients_params);


						if($data['file_row']->client_id==0){

							$data['client_row'] = $client_row;

						}

					break;
				
				default:					

					break;
			}

			
		} catch (Exception $e) {
				$this->session->set_flashdata('error', $e->getMessage());
		}



		$data['form_type']  = $form_type;



		$data['clients'] = $this->client->listing(array(),false);	

		$data['tran_status_desc'] = $this->tran_status_desc;

		$this->view_data['maintain']['data'] = $data;
		$this->view_data['maintain']['menu_active'] = 'file_manager';
		$this->view_data['maintain']['view_file'] = 'maintenance/file_manager/form';
		$this->view_data['js_file'] = '<script src="assets/js/page_js/maintenance/file_manager.js"></script>'; 		
		
		$this->load->view('template', $this->view_data);
	}


	public function filemanager_files($client_id=""){
		
		$this->load->helper('file');
		$this->load->helper('file_ext_helper');
		$this->load->model('Filemanagermodel', 'filemanager');

		$files = $this->filemanager->listing(array('where'=>array('filemanager.client_id'=>$client_id, 'file_status'=>'1')), false);

		$data['files'] = $files;
 
		$this->load->view('maintenance/file_manager/filemanager-list', $data);		
	}

	public function email_attachment(){

		$this->load->model('Emaildatamodel');
		$this->load->library("pagination");

		if( in_array($this->user_lvl, array('99', '3')) ){


			$data = '';

			$params = $this->input->get();


			$this->view_data['maintain']['data'] = $data;
			$this->view_data['maintain']['menu_active'] = 'email_attachment';
			$this->view_data['maintain']['view_file'] = 'maintenance/email_attachment';
			$this->view_data['js_file']  = '<script src="assets/vendor/Highcharts/code/highcharts.js"></script>'.PHP_EOL; 
			$this->view_data['js_file'] .= '<script src="assets/vendor/Highcharts/code/custom-events.js"></script>'.PHP_EOL; 
			$this->view_data['js_file'] .= '<script src="assets/js/filesize.min.js"></script>'.PHP_EOL; 						
			$this->view_data['js_file'] .= '<script src="assets/js/pages/maintenance/email_attachment.js?=v1"></script>'; 
 
			
			$this->load->view('template', $this->view_data);
 
		}else{
			redirect('dashboard');
		}

	}



	public function get_chart_data(){

			$this->load->model('Emaildatamodel');

			$lastday = date('t',strtotime('today'));

			$post = $this->input->post();


			$yr  = (isset($post['yr']) && $post['yr']!='' ? $post['yr']:date('Y'));

			$data = array();

			$color = array(
								'#6B5B95', //Jan
								'#ECDB54', //Feb
								'#E94B3C', //Mar
								'#944743', //Apr
								'#DBB1CD', //May
								'#EC9787', //Jun
								'#00A591', //Jul
								'#6B5B95', //Aug
								'#6C4F3D', //Sep
								'#BFD641', //Oct
								'#2E4A62', //Nov
								'#D2691E'  //Dec
						);

			$i=0;

			

			for($month_ctr=1; $month_ctr <=12;$month_ctr++){

					$start_date = date('Y-m-d',mktime(0, 0, 0, $month_ctr, 1, $yr));

					$end_day = date('t',strtotime($yr.'-'.$month_ctr.'-01'));

					$end_date = date('Y-m-d', strtotime($yr.'-'.$month_ctr.'-'.$end_day));
				
					$result = $this->Emaildatamodel->get_result_chart(array('select'=>'sum(attachments_size) as total_size_per_month','where_str'=>" created_at BETWEEN '".$start_date."' and '".$end_date."'"));


						if($result->total_size_per_month!=''){

							$data[] = array(	
											"y"=>(float)$result->total_size_per_month,
	   										"name"=>date("F",strtotime($start_date)),
	   										"c_size"=>$this->Commonmodel->format_size_units((float)$result->total_size_per_month)
	   										/*,
	   										"color"=>$color[$i]*/
										);	
						}else{
							
						 $data[] = array(	
											"y"=>0,
	   										"name"=>date("F",strtotime($start_date)),
	   										"c_size"=>$this->Commonmodel->format_size_units((float)$result->total_size_per_month)
	   										/*,
	   										"color"=>$color[$i]*/
										);
						}

				$i++;


			}
 			

			echo json_encode($data);

	}


	public function email_attachment_modal_ajax(){

		  $this->load->model('Emaildatamodel');

		  $month = $this->input->get('M');

		  $month_parse =  date_parse($month);
		
		  $year = ($this->input->get('Y') ? $this->input->get('Y'):date('Y'));

		  $start_date = date('Y-m-d',mktime(0, 0, 0,  $month_parse['month'], 1, $year));

		  $end_day = date('t',strtotime($year.'-'.$month_parse['month'].'-01'));

		  $end_date = date('Y-m-d', strtotime($year.'-'.$month_parse['month'].'-'.$end_day));

		  $results = $this->Emaildatamodel->get_result_pagination_link(
		  	array(
		  		'sorting'=>array('sort'=>'transaction.tran_id','order'=>'DESC'),
		  		'select'=>'transaction.tran_id, attachements, attachments_size,created_at, ref_number',
		  		'where_str'=>" attachements!='' AND attachments_size IS NOT NULL AND created_at BETWEEN '".$start_date."' and '".$end_date."'")
		  	);


		   $results 	= $results['results'];


			?>

		<h5 class="float-left ml-3">Data Storage Consumption Summary</h5>
		<div class="container-fluid">;
			<table class="table table-bordered table-hover table-sm">
					<thead class="table-primary ">
						<tr>
							<th scope="col" width="150px">Alert ID</th>
							<th scope="col">Files</th>
							<th scope="col">File Size</th>
							<th scope="col" width="150px">Date</th>
						</tr>
					</thead>
					<tbody>

			 			<?php 
			 			$total_size = 0;

		 				if(isset($results) && count($results) > 0):	

			 				foreach ($results as $row) :

			 				
			 				?>
								<tr>
									<td> 
									<a href="callactivity/open/<?php echo @$row->tran_id; ?>" class="mr-1" target="_blank"><?php echo @$row->ref_number; ?></a>
							
									</td>
									<td>
										<?php  
											//print_r($emaildata->attachements);
											$attachements = json_decode(@$row->attachements);
											foreach ($attachements as $attach) {
												$a_name = explode('_', $attach);
												unset($a_name[0]);										
												echo '<a href="callactivity/download_attachment/?file='.$attach.'" class="mr-1">'.implode('_', $a_name).'</a><br/>'; 

											}

										?>
									</td> 
									<td><?php echo @$row->attachments_size; ?></td>
									<td><?php echo (!in_array($row->created_at, array('', '0000-00-00 00:00:00')))?date('d/m/Y H:i', strtotime($row->created_at)):''; ?></td>
					 				
								</tr>
					   <?php
							$total_size+=$row->attachments_size;
						 endforeach; 
					   else:
					   ?>
							<tr>
								<td colspan="4" align="center">No files found for this month.</td>
							</tr>

						<?php
							endif;
						 ?>	
						 <tr>
						 	<td class="bg-primary text-white" colspan="2" style="font-weight:bold;text-align:right">
						 		TOTAL SIZE IN BYTES
						 	</td>
						 	<td class="bg-success text-white" colspan="2"><strong><?php echo $total_size?></strong></td>						  	
						 </tr>
						 <tr>
						 	<td class="bg-primary text-white" colspan="2" style="font-weight:bold;text-align:right">
						 		CONVERTED TOTAL SIZE
						 	</td>
						 	<td class="bg-info text-white" colspan="2"><strong><?php echo $this->Commonmodel->format_size_units((float)$total_size);?></strong></td>						  	
						 </tr>


					</tbody>
				</table>
			</div>
			<?php


	}


	public function client(){
		
		$this->load->model('Clientmodel'); 
		$this->load->library("pagination");

		try {
			
			$data = '';

			$params = $this->input->get();
			$page = (isset($params['per_page'])) ? $params['per_page'] : 0;
			unset($params['per_page']);

			//$query_params = $this->build_where($params);

			$params_query = @http_build_query($params);

			$per_page = 50;
			$query_params['limits']['start'] = $page;
			$query_params['limits']['limit'] = $per_page; 
			$query_params['sorting']['sort'] = 'client_name';
			$query_params['sorting']['order']= 'ASC';

			if( $this->client_id > 0 ){
				$query_params['where']['client.client_id'] = $this->client_id;
			}


			$appt_results 				= $this->Clientmodel->listing($query_params);
	        $p_config["base_url"] 		= base_url() . "maintenance/client/?".$params_query;
	        $p_config["total_rows"] 	= $appt_results['total_rows'];
	        $p_config["per_page"] 		= $per_page;
	        $p_config["uri_segment"] 	= 3; 
	        $config = $this->Commonmodel->pagination_config($p_config);	 
	        $this->pagination->initialize($config);

	        $data['results'] 	= $appt_results['results'];
			$data["links"] 		= $this->pagination->create_links();
			$data['showing']	= 'Showing '.(($page==0)?1:$page+1).' to '.($page+count($data['results'])).' of '.$appt_results['total_rows'].' entries';


			$this->view_data['maintain']['data'] = $data;
			$this->view_data['maintain']['menu_active'] = 'client';
			$this->view_data['maintain']['view_file'] = 'maintenance/client/listing';
			
			//$this->view_data['js_file'] = '<script src="assets/js/page_js/dashboard.js"></script>'; 		
			$this->load->view('template', $this->view_data); 

		} catch (Exception $e) {
			echo $e->getMessage();
		} 

	}

	public function client_form($form_type=''){
		
		$this->load->model('Clientmodel');		

		try {
			
			$data = '';

			if( $_POST ) {

				$post = $this->input->post();

				// print_r($post);
				// exit;

				$set = array();

				$set['client_name'] 		= $post['client_name'];
				$set['cma_id'] 				= $post['cma_id'];
				$set['cma_db'] 				= $post['cma_db'];
				$set['cust_id'] 			= $post['cust_id'];
				$set['contact_id_issue'] 	= $post['contact_id_issue'];
				$set['contact_id_callsmade'] = $post['contact_id_callsmade'];
				$set['client_status'] 		= $post['client_status'];

				if( $post['form_type'] == 'add' ){
					$set['created_at'] 	= date('Y-m-d H:i:s');
					$set['added_by'] 	= $this->agent_name;
					
					$client_id = $this->Clientmodel->insert($set);

					//filter client to unable to add
					if( $client_id > 0 AND $this->client_id == 0 ){
						$this->session->set_flashdata('fmesg', '<strong>'.$post['client_name'].'</strong> successfully updated');
						redirect('maintenance/client');
					}else{
						$this->session->set_flashdata('fmesg', 'Failed add new client');
						redirect('maintenance/client');
					}

				}elseif ($post['form_type'] == 'update' ) {
					
					$set['updated_at'] 		= date('Y-m-d H:i:s');
					$set['last_update_by'] 	= $this->agent_name;

					if( $this->client_id > 0 ) {
						if( $this->client_id != $post['client_id'] ) {
							$this->session->set_flashdata('fmesg', 'Warning: Your accessing an invalid Client.');
							redirect('maintenance/client');
						}
					}

					if( $this->Clientmodel->update($post['client_id'], $set) ){
						$this->session->set_flashdata('fmesg', '<strong>'.$post['client_name'].'</strong> successfully updated');
						redirect('maintenance/client');
					}else{
						$this->session->set_flashdata('fmesg', 'Failed updated');
						redirect('maintenance/client');
					}

				}

			}

 
			$get = $this->input->get();

			$clientobj = new stdClass(); 

			$client_id = '';

			if($this->client_id > 0) { 
				$client_id = $this->client_id;
			}else{
				$client_id = @$get['client_id'];
			}

			if( $client_id == '' AND $form_type=='update' ) {
				$this->session->set_flashdata('fmesg', 'Warning: Your accessing an invalid Client.');						
				$redirect = 'maintenance/client';
				redirect($redirect);
			}

			if( $form_type == '' ) {
				$this->session->set_flashdata('fmesg', 'Warning: Unrecognize action.');						
				$redirect = 'maintenance/client';
				redirect($redirect);
			}

			$params['where']['client_id'] = $client_id;
			$clientobj = $this->Clientmodel->row($params);
			
		  
 			$data['form_type'] = $form_type;
 			$data['clientobj'] = $clientobj;

			$this->view_data['maintain']['data'] = $data;
			$this->view_data['maintain']['menu_active'] = 'client';
			$this->view_data['maintain']['view_file'] = 'maintenance/client/form';
			$this->load->view('template', $this->view_data); 

		} catch (Exception $e) {
			echo $e->getMessage();
		}

	}


	public function procedures(){

		$this->load->model('Clientmodel');
		$this->load->model('Proceduremodel');

		try {
			
			$data = '';

			$get = $this->input->get();

			if( $this->client_id > 0 ){
				$clients_params['where']['client_id'] = $this->client_id;
			}

			$clients_params['select']				 = 'client_id, client_name';
			$clients_params['where']['client_status'] = 1;
			$clients_params['sorting']['sort'] 		 = 'client_name';
			$clients_params['sorting']['order']		 = 'ASC';
			$data['clients'] = $this->Clientmodel->listing($clients_params, false);

			$pro_params = array();
			$client_id = '';
			
			if( $this->client_id > 0 ){
 				$client_id = $this->client_id; 
			}else{
				if( isset($get['client_id']) ){
					$client_id = $get['client_id'];	 
				}
			} 

			 
			if( $client_id > 0 ){
				
				$pro_params['where']['client_procedure.client_id'] = $client_id;
				$pro_params['sorting'] = 'pro_alert_type ASC, pro_name ASC';
				$data['procedures'] = $this->Proceduremodel->listing($pro_params, false);				 
			 
				$data['client'] = $this->Clientmodel->row( array('where'=>array('client_id'=>$client_id)) );
				//echo $this->db->last_query();
				//print_r($data['client']);
			}

			$data['client_id'] = $client_id;
			$this->view_data['js_file'] = '<script type="text/javascript" src="assets/vendor/tinymce/js/tinymce/tinymce.min.js"></script>'.PHP_EOL;
			$this->view_data['js_file'] .= '<script type="text/javascript" src="assets/vendor/tinymce.filemanager/plugin.min.js"></script>'.PHP_EOL;
			$this->view_data['js_file'] .= '<script src="assets/js/pages/maintenance/procedure.js"></script>'; 	
			$this->view_data['maintain']['data'] = $data;
			$this->view_data['maintain']['menu_active'] = 'procedures';
			$this->view_data['maintain']['view_file'] = 'maintenance/procedures/listing';
			
			//$this->view_data['js_file'] = '<script src="assets/js/page_js/dashboard.js"></script>'; 		
			$this->load->view('template', $this->view_data); 

		} catch (Exception $e) {
			echo $e->getMessage();
		}

	}


	public function procedure_form($pro_alert_type='', $form_type=''){

		$this->load->model('Clientmodel');
		$this->load->model('Proceduremodel');
		$this->load->model('Callresponsemodel');
		$this->load->model('CRLookupmodel');
		$this->load->model('Clientcustomtable');

		try {
			
			$data = '';


			if( $_POST ) {

				$post = $this->input->post();
				
				$pro_alert_type = $post['pro_alert_type'];

				$redirect = '';

				if( $post['form_type'] == 'add' ){

					if( $pro_alert_type  == 'API' ) {

						$set['api_key'] = md5(APP_API_KEY.$post['client_id'].date('U'));
						$set['api_url'] = addslashes($post['api_url']);

						if( trim($post['client_data_src']) != '' ){

							$_client_data_src = 'cs_'.$post['client_id'].'_'.$post['client_data_src'];

							$set['client_data_src'] = $_client_data_src;

							$more_fields[] = array();
							$more_fields[] = '`tran_id` INT(11) DEFAULT NULL';
							$create_base_table = $this->Clientcustomtable->create_base_table($post['client_id'], $_client_data_src, 'api storage', $more_fields);
							
							if( !$create_base_table->status ){
								$this->session->set_flashdata('fmesg', 'Sorry, Failed to create the API Data Storage');
								$redirect = 'maintenance/procedure_form/API/add/'.(($this->client_id == 0)?'?client_id='.$post['client_id']:'');
								//print_r($create_base_table);
								//exit;
								redirect($redirect);
							}				
						}
					}
 
					if( $pro_alert_type  == 'EMAIL' ) {
						$set['incoming_email'] 		= trim($post['incoming_email']);
						$set['display_subject_line'] = trim($post['display_subject_line']);

						$check_incoming_email = $this->Proceduremodel->row(array('where'=>array('incoming_email'=>$set['incoming_email'])));

						if( isset($check_incoming_email->incoming_email) OR $set['incoming_email'] == '' ){
							$this->session->set_flashdata('fmesg', $set['incoming_email'].' is already been used by other EMAIL procedure');							
							$redirect = 'maintenance/procedure_form/EMAIL/add/'.(($this->client_id == 0)?'?client_id='.$post['client_id']:'');
							redirect($redirect);
							exit;
						}

					}

 
					if( $pro_alert_type == 'SMS' ) {
						$set['incoming_mobileno'] 	= trim($post['incoming_mobileno']);
					}

					if( $pro_alert_type == 'REMINDER' ) {

						$set['reminder_alert_details'] 	= addslashes($post['reminder_alert_details']);
						$set['reminder_type'] 			= $post['reminder_type'];
						$set['reminder_time'] 			= $post['reminder_time'];
						$set['reminder_until_dt'] 		= $post['reminder_until_dt'];
 						
 						
						switch ($post['reminder_type']) {
							case 'ONCE':
								$set['reminder_day'] 		= $post['reminder_day'];
								$set['reminder_weekday'] 	= $post['reminder_weekday'];
								$set['reminder_month'] 		= $post['reminder_month'];
								$set['reminder_year'] 		= $post['reminder_year'];
								break;
							case 'EVERY_DAY':

								break;
							case 'WEEKLY':
								$set['reminder_weekday'] 	= $post['reminder_weekday']; 
								break;
							case 'FORTHNIGHTLY':
								$set['reminder_day'] 		= $post['reminder_day'];
								$set['reminder_month'] 		= $post['reminder_month'];
								break;
							case 'MONTHLY':
								$set['reminder_day'] 		= $post['reminder_day'];								
								break;
							case 'YEARLY':
								$set['reminder_day'] 		= $post['reminder_day'];
								$set['reminder_month'] 		= $post['reminder_month'];
								break;
							default:

								break;
						}
					}

					$set['reminder_alert_duration'] = (trim($post['reminder_alert_duration'])=='')?5:$post['reminder_alert_duration'];

					$set['client_id'] 		= $post['client_id'];
					$set['pro_name'] 		= addslashes($post['pro_name']);
					$set['pro_content'] 	= addslashes($post['pro_content']);
					$set['pro_alert_type'] 	= $post['pro_alert_type'];
					$set['pro_status'] 		= $post['pro_status'];

					$set['cma_settings'] 	= json_encode($post['cma_settings']);

					$set['call_group'] 		= $post['call_group']; 
					$set['call_lookup_id'] 	= $post['call_lookup_id'];
					$set['priority_flag'] 	= $post['priority_flag'];
					$set['display_screen'] 	= $post['display_screen'];

					$set['other_options'] = json_encode($post['cr_options']); 
					$set['allow_multi_del'] = @$post['allow_multi_del'];
					$set['multi_del_callres'] = @$post['multi_del_callres'];


					$set['added_by'] 		= $this->agent_name;

					$pro_id = $this->Proceduremodel->insert($set);

					if($pro_id > 0){

						$this->session->set_flashdata('fmesg', 'PROCEDURE '.$post['pro_alert_type'].' procedure successfully added');							
						$redirect = 'maintenance/procedures/'.(($this->client_id == 0)?'?client_id='.$post['client_id']:'');

					}else{
						$this->session->set_flashdata('fmesg', 'Failed to add procedure');	
						$redirect = 'maintenance/procedure_form/add/'.(($this->client_id == 0)?'?client_id='.$post['client_id']:'');
					}

					//echo $redirect;
					redirect($redirect);

				}elseif ($post['form_type'] == 'edit') {
					
					$pro_id = $post['pro_id'];

					if( $pro_alert_type == 'EMAIL' ) {
						$set['incoming_email'] 	= trim($post['incoming_email']);
						$set['display_subject_line'] = trim($post['display_subject_line']);
						
						$check_incoming_email = $this->Proceduremodel->row(array('where_str'=>'incoming_email="'.$set['incoming_email'].'" AND pro_id != '.$pro_id));

						if( isset($check_incoming_email->incoming_email) OR $set['incoming_email'] == '' ){
							$this->session->set_flashdata('fmesg', $set['incoming_email'].' is already been used by other EMAIL procedure');							
							$redirect = 'maintenance/procedure_form/EMAIL/edit/?pro_id='.$pro_id;
							redirect($redirect);
							exit;
						}

					}

 
					if( $pro_alert_type == 'SMS' ) {
						$set['incoming_mobileno'] 	= trim($post['incoming_mobileno']);
					}					 



					if( $pro_alert_type == 'REMINDER' ) {

						$set['reminder_alert_details'] 	= addslashes($post['reminder_alert_details']);
						$set['reminder_type'] 			= $post['reminder_type'];
						$set['reminder_time'] 			= $post['reminder_time'];
						$set['reminder_until_dt'] 		= $post['reminder_until_dt'];
						//$set['reminder_alert_duration'] = (trim($post['reminder_alert_duration'])=='')?5:$post['reminder_alert_duration'];
 
						switch ($post['reminder_type']) {
							case 'ONCE':
								$set['reminder_day'] 		= $post['reminder_day'];
								$set['reminder_month'] 		= $post['reminder_month'];
								$set['reminder_year'] 		= $post['reminder_year'];
								break;
							case 'EVERY_DAY':

								break;
							case 'WEEKLY':
								$set['reminder_weekday'] 	= $post['reminder_weekday']; 
								break;
							case 'FORTHNIGHTLY':
								$set['reminder_day'] 		= $post['reminder_day'];
								$set['reminder_month'] 		= $post['reminder_month'];
								break;
							case 'MONTHLY':
								$set['reminder_day'] 		= $post['reminder_day'];								
								break;
							case 'YEARLY':
								$set['reminder_day'] 		= $post['reminder_day'];
								$set['reminder_month'] 		= $post['reminder_month'];
								break;
							default:

								break;
						}
					}

					$set['reminder_alert_duration'] = (trim($post['reminder_alert_duration'])=='')?5:$post['reminder_alert_duration'];
					
					$set['pro_name'] 		= addslashes($post['pro_name']);
					$set['pro_content'] 	= addslashes($post['pro_content']);
					//$set['pro_alert_type'] 	= $post['pro_alert_type'];
					$set['update_at'] 		= date('Y-m-d H:i:s');
					$set['pro_status'] 		= $post['pro_status'];
					$set['last_update_by'] 	= $this->agent_name;
				 
					$set['cma_settings'] = json_encode($post['cma_settings']);

 
					$set['call_group'] 		= $post['call_group'];
					$set['call_lookup_id'] 	= $post['call_lookup_id'];
					$set['priority_flag'] 	= $post['priority_flag'];
					$set['display_screen'] 	= $post['display_screen']; 

					$set['other_options'] = json_encode($post['cr_options']);  
					$set['allow_multi_del'] = @$post['allow_multi_del'];
					$set['multi_del_callres'] = @$post['multi_del_callres'];
					
					//echo '-'.$post['client_id'].'-'.$this->client_id.'-'; exit;

					//check if current client is correct
					if( $this->client_id > 0 ){

						if( $post['client_id'] != $this->client_id ) {

							//$this->session->set_flashdata('fmesg', 'Warning: Your accessing an invalid Procedure.');						
							$this->session->set_flashdata('fmesg', 'Warning: Your accessing an invalid Procedure.');						
							$redirect = 'maintenance/procedures/'.(($this->client_id == 0)?'?client_id='.$post['client_id']:'');
							
							redirect($redirect);
							exit;
						}

					}


					if( $this->Proceduremodel->update($pro_id, $set) ){
						
						$this->session->set_flashdata('fmesg', 'PROCEDURE '.$post['pro_alert_type'].' successfully updated');							
						$redirect = 'maintenance/procedures/'.(($this->client_id == 0)?'?client_id='.$post['client_id']:'');
						 

					}else{

						$this->session->set_flashdata('fmesg', 'Failed to update procedure');	
						$redirect = 'maintenance/procedure_form/edit/?pro_id='.$pro_id;
					 
					}
				 
					redirect($redirect);

				}

			}
 
			$get = $this->input->get();

			$procedure = new stdClass(); 

			if( @$get['pro_id'] > 0 ){
				$pro_params['where']['pro_id'] = $get['pro_id'];
				$procedure = $this->Proceduremodel->row($pro_params);
				
			}else{

				if( $this->client_id > 0 ){
					$procedure->client_id = $this->client_id;	
				}else{
					
					if( !isset($get['client_id']) OR @$get['client_id'] == '' ) throw new Exception("Client not recognized", 1);

					$procedure->client_id = $get['client_id'];	
				}
				$procedure->pro_alert_type = $pro_alert_type;
			}
			 
 			if ($form_type == 'edit') {

				if( $this->client_id > 0 ){
					if( $procedure->client_id != $this->client_id ) {

						$this->session->set_flashdata('fmesg', 'Warning: Your accessing an invalid Procedure.');						
						$redirect = 'maintenance/procedures/'.(($this->client_id == 0)?'?client_id='.$post['client_id']:'');
						redirect($redirect);
					}
				}
 				
 			}

 			$client_params['where']['client_id'] = $procedure->client_id;
 			$data['client'] = $this->Clientmodel->row($client_params);

 			$data['form_type'] = $form_type; 			
 			$data['procedure'] = $procedure;
 			$data['call_group'] = $this->Callresponsemodel->group_names($procedure->client_id);
 			$data['call_form_not_required'] = $this->Callresponsemodel->call_form_not_required($procedure->client_id);
 			$data['cma_settings'] = json_decode(@$procedure->cma_settings);
 			$data['cr_options'] = json_decode(@$procedure->other_options);
 			$data['call_lookup_options'] = $this->CRLookupmodel->get_array(array('where'=>array('client_call_lookup.client_id'=>@$procedure->client_id)), true);
 			$data['multi_closed_cr'] = $this->Callresponsemodel->dropown($procedure->client_id);


			$this->view_data['maintain']['data'] = $data;
			$this->view_data['maintain']['menu_active'] = 'procedures';
			$this->view_data['maintain']['view_file'] = 'maintenance/procedures/form';
			
			//$this->view_data['js_file'] = '<script src="assets/js/page_js/dashboard.js"></script>'; 		
			
			$this->view_data['js_file'] = '<script type="text/javascript" src="assets/vendor/tinymce/js/tinymce/tinymce.min.js"></script>'.PHP_EOL;
			$this->view_data['js_file'] .= '<script type="text/javascript" src="assets/vendor/tinymce.filemanager/plugin.min.js"></script>'.PHP_EOL;
			$this->view_data['js_file'] .= '<script src="assets/js/pages/maintenance/procedure.js"></script>'; 		

			$this->load->view('template', $this->view_data); 

		} catch (Exception $e) {
			echo $e->getMessage();
		}

	}


	public function procedure_audit_trail($pro_id=''){

		$this->load->model('Proceduremodel');
		 

		$data = '';

		$pro_params = array();
		$pro_params['where']['pro_id'] = $pro_id;
		$procedure = $this->Proceduremodel->row($pro_params);


		$audit = array();
		$audit['ref_table'] = 'client_procedure';
		$audit['ref_field'] = 'pro_id';
		$audit['ref_val'] = $pro_id;

		$this->db->where($audit);
		$this->db->order_by('created', 'desc');
		$list =  $this->db->get('table_audit_trail')->result();
		//echo $this->db->last_query();

		$data['records'] = $list;
		$data['procedure'] = $procedure;

		$this->view_data['maintain']['data'] = $data;
		$this->view_data['maintain']['menu_active'] = 'procedures';
		$this->view_data['maintain']['view_file'] = 'maintenance/procedures/table_audit_trail';
		
		$this->load->view('template', $this->view_data); 
	}

	function procedure_ajax_email_check(){

		$this->load->model('Proceduremodel');

		$json = array('status'=>1);

		try {
			
			$get = $this->input->get();

			$incoming_email 	= trim($get['email']);

			$procedure = $this->Proceduremodel->row(array('where'=>array('incoming_email'=>$incoming_email)));

			if( isset($procedure->incoming_email) OR $incoming_email == '' ){
				$json['status'] = 0;
				//$json['msg'] = $incoming_email.' <br /> is already been used by <br />procedure:  '.$procedure->pro_name.' ('.$procedure->client_name.')' ;
			
				$msg =  'Incoming Email ('.$incoming_email.') is currently set to <br />';
				$msg .=  'Procedure: '.$procedure->pro_name.' <br />';
				$msg .=  'Client: '.$procedure->client_name.' <br />';
				$json['msg'] = $msg;

			}

			echo json_encode($json);

		} catch (Exception $e) {
		
			$json['msg'] = $e->getMessage();

			echo json_encode($json);

		}

	}

	function procedure_ajax_check_tablename(){

		$this->load->model('Clientcustomtable');

		$json = array('status'=>1);

		try {
			
			$get = $this->input->get();

			$_client_data_src = trim($get['client_data_src']);
			$client_id 	= trim($get['client_id']);

			$client_data_src = 'cs_'.$client_id.'_'.$_client_data_src;

			if( $this->Clientcustomtable->check_table_name_exist($client_data_src) > 0 ){

				$json['status'] = 0;
				
				$msg =  'API Data storage <strong>'.$_client_data_src.'</strong> is already been used, please choose another name <br />'; 
				$json['msg'] = $msg;

			}

			echo json_encode($json);

		} catch (Exception $e) {
		
			$json['msg'] = $e->getMessage();

			echo json_encode($json);

		}

	}


	public function incoming_email_priority(){		  

		try {
			
			$data = '';

			$params = $this->input->get();
			$page = (isset($params['per_page'])) ? $params['per_page'] : 0;
			unset($params['per_page']);
 

			$per_page = 50;
	 
			$this->db->order_by('email_from_ref');
			$this->db->order_by('email_priority'); 
			$query = $this->db->get('incoming_email_condition');


			/*$appt_results 				= $this->Clientmodel->listing($query_params);
	        $p_config["base_url"] 		= base_url() . "maintenance/client/?".$params_query;
	        $p_config["total_rows"] 	= $appt_results['total_rows'];
	        $p_config["per_page"] 		= $per_page;
	        $p_config["uri_segment"] 	= 3; 
	        $config = $this->Commonmodel->pagination_config($p_config);	 
	        $this->pagination->initialize($config);

	        $data['results'] 	= $appt_results['results'];
			$data["links"] 		= $this->pagination->create_links();
			$data['showing']	= 'Showing '.(($page==0)?1:$page+1).' to '.($page+count($data['results'])).' of '.$appt_results['total_rows'].' entries';
			*/

			$data['results'] 	= $query->result();

			$this->view_data['maintain']['data'] = $data;
			$this->view_data['maintain']['menu_active'] = 'incoming_email_priority';
			$this->view_data['maintain']['view_file'] = 'maintenance/incoming_email_condition/listing';
			
			//$this->view_data['js_file'] = '<script src="assets/js/page_js/dashboard.js"></script>'; 		
			$this->load->view('template', $this->view_data); 

		} catch (Exception $e) {
			echo $e->getMessage();
		} 

	}

	public function incoming_email_priority_form($form_type='add'){
	 

		try {
			
			$data = '';
			$get = $this->input->get();
			$id = @$get['id'];


			if( $_POST ) {

				$post = $this->input->post();

				// print_r($post);
				// exit;

				$set = array();

				$set['email_from_ref'] 		= $post['email_from_ref']; 
				$set['email_priority'] 		= $post['email_priority']; 

				if( $post['form_type'] == 'add' ){
					$set['date_created'] 	= date('Y-m-d H:i:s'); 
					  
					if( $this->db->insert('incoming_email_condition', $set) ){
						$this->session->set_flashdata('fmesg', 'New Email priority condition successfully added');
						redirect('maintenance/incoming_email_priority');
					}else{
						$this->session->set_flashdata('fmesg', 'Failed add new Email priority condition');
						redirect('maintenance/incoming_email_priority');
					}
 

				}elseif ($post['form_type'] == 'update' ) {
					
					$set['date_updated'] 		= date('Y-m-d H:i:s'); 

					if( $post['id'] > 0 ) { 

						$this->db->where('id', $post['id']);
						if( $this->db->update('incoming_email_condition', $set) ){
							$this->session->set_flashdata('fmesg', ' Email priority condition successfully updated');
							redirect('maintenance/incoming_email_priority');
						}else{
							$this->session->set_flashdata('fmesg', 'Failed to updated Email priority condition');
							redirect('maintenance/incoming_email_priority');
						}

					}

				}

			}
 

			$clientobj = new stdClass();  
  
			if( $id > 0  ){

				$this->db->where('id', $id);
				$clientobj = $this->db->get('incoming_email_condition')->row();

				$form_type = 'update';
			} 
			
		  
 			$data['form_type'] = $form_type;
 			$data['clientobj'] = $clientobj;

			$this->view_data['maintain']['data'] = $data;
			$this->view_data['maintain']['menu_active'] = 'incoming_email_priority';
			$this->view_data['maintain']['view_file'] = 'maintenance/incoming_email_condition/form';
			$this->load->view('template', $this->view_data); 

		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}
}
