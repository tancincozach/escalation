<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * Call Reponse Lookup
 */
class Crlookup extends MY_Controller {

	public function __construct(){
		
		parent::__construct();

		$this->view_data['view_file'] = 'maintenance/index';
		$this->view_data['menu_active'] = 'maintenance';
		$this->view_data['data'] = '';
	}

	public function index(){

		$this->load->model('Clientmodel');
		$this->load->model('CRLookupmodel');


		try { 

			$data = '';
			

			$get = $this->input->get();

			if( $this->client_id > 0 ){
				$clients_params['where']['client_id'] = $this->client_id;
			}

			$clients_params['select']				 = 'client_id, client_name';
			$clients_params['where']['client_status'] = 1;
			$clients_params['sorting']['sort'] 		 = 'client_name';
			$clients_params['sorting']['order']		 = 'ASC';
			$data['clients'] = $this->Clientmodel->listing($clients_params, false);


			$client_id = '';
			
			if( $this->client_id > 0 ){
					$client_id = $this->client_id; 
			}else{
				if( isset($get['client_id']) ){
					$client_id = $get['client_id'];	 
				}
			} 



			if( $client_id > 0 ){
				
				$params['where']['client_call_lookup.client_id'] = $client_id;
				$params['sorting'] = 'call_lookup_name ASC';
				$data['records'] = $this->CRLookupmodel->listing($params, false);				 
			 
				$data['client'] = $this->Clientmodel->row( array('where'=>array('client_id'=>$client_id)) );
				//echo $this->db->last_query();
				//print_r($data['client']);
			}


			$data['client_id'] = $client_id;

			$this->view_data['maintain']['data'] = $data;
			$this->view_data['maintain']['menu_active'] = 'crlookup';
			$this->view_data['maintain']['view_file'] = 'maintenance/call_response_lookup/listing';
			
			//$this->view_data['js_file'] = '<script src="assets/js/page_js/dashboard.js"></script>'; 		
			$this->load->view('template', $this->view_data); 
		} catch (Exception $e) {
			
		}
	}

	public function setup($step=1, $call_lookup_id=''){

		$this->load->model('CRLookupmodel');
		$data = '';
		$get = $this->input->get();

		if( !isset($get['client_id']) OR trim(@$get['client_id']) == '') {
			$this->session->set_flashdata('fmesg', 'Sorry client is missing');	
			redirect('crlookup');
		}

		$data['step'] = $step;
		$data['client_id'] = $get['client_id'];		
 		
 		if( $call_lookup_id > 0 ){

 			$params = array();
 			$params['where'] = array('call_lookup_id'=>$call_lookup_id);
 			$record = $this->CRLookupmodel->row($params);
 			$data['record'] = $record;

 			$data['step'] = $record->step;	

 			if( $record->step == 3 ){
 				redirect('crlookup/settings/'.$call_lookup_id.'/?client_id='.$client_id);	
 			}


 			if( count($data['record']) == 0) {

 			}

 		}
 

 		//print_r($data['record']);
		
		$this->view_data['maintain']['data'] = $data;
		$this->view_data['maintain']['menu_active'] = 'crlookup';
		$this->view_data['maintain']['view_file'] = 'maintenance/call_response_lookup/setup_table';
		
		//$this->view_data['js_file'] = '<script src="assets/js/page_js/dashboard.js"></script>'; 		
		$this->load->view('template', $this->view_data); 

	}

	public function setup_save(){

		$this->load->model('CRLookupmodel');
		$this->load->model('Clientcustomtable');

		try {
			
			if( !$_POST ) throw new Exception("Error Processing Request", 1);
			
			$post = $this->input->post();

			if( !isset($post['client_id']) OR trim(@$post['client_id']) == '' ) throw new Exception("Client not set", 1);
			
			if( $post['step'] == '1' ){

				$set = array();
				$set['step'] 				= 2;
				$set['call_lookup_name'] 	= addslashes($post['call_lookup_name']);
				$set['client_id'] 			= $post['client_id'];
				$set['agent_name'] 			= $this->agent_name;

				$call_lookup_id = $this->CRLookupmodel->insert($set);

				if( !$call_lookup_id ){
					$this->session->set_flashdata('fmesg', 'Sorry unable to save lookup name, please contact admin');	
					redirect('crlookup/setup/?client_id='.$post['client_id']);
				}else{
					$this->session->set_flashdata('fmesg', 'Lookup Name successfully save');	
					redirect('crlookup/setup/2/'.$call_lookup_id.'/?client_id='.$post['client_id']);					
				}

			}

			
			if( $post['step'] == '2' ){

	 			$params = array();
	 			$params['where'] = array('call_lookup_id'=>$post['call_lookup_id']);
	 			$record = $this->CRLookupmodel->row($params);

				$call_lookup_id = $post['call_lookup_id'];
				$table_name = trim($post['source_tablename']);
				$client_id = $record->client_id;

				$table_name = preg_replace("/[^a-z]+/", "", $table_name);
				$table_name = DYNAMIC_TABLE_PREFIX.'_'.$client_id.'_'.trim($table_name);

				if( $this->Clientcustomtable->check_table_name_exist($table_name) > 0 ){

					$this->session->set_flashdata('fmesg', 'Source table name already exist');	
					redirect('crlookup/setup/2/'.$call_lookup_id.'/?client_id='.$post['client_id']);		

				}else{

					$table_name_res = $this->Clientcustomtable->create_base_table($client_id, $table_name);
					
					if( !$table_name_res->status ){

						$this->session->set_flashdata('fmesg', $table_name_res->msg);	
						redirect('crlookup/setup/2/'.$call_lookup_id.'/?client_id='.$client_id);		

					}
				}

				$set = array();
				$set['step'] = 3;
				$set['source_tablename'] = $table_name;
				$set['updated_dt'] = date('Y-m-d H:i:s');
				$set['agent_name'] = $this->agent_name;

				if( !$this->CRLookupmodel->update($call_lookup_id, $set) ){
					$this->session->set_flashdata('fmesg', 'Sorry unable to source table name');	
				 	redirect('crlookup/setup/2/'.$call_lookup_id.'/?client_id='.$client_id);	
				}else{
					$this->session->set_flashdata('fmesg', 'Source table name successfully created');	
					redirect('crlookup/settings/'.$call_lookup_id.'/?client_id='.$client_id);	

				}				 

			}



		} catch (Exception $e) {
			
				echo $e->getMessage();

		}


	}


	public function settings($call_lookup_id){

		$this->load->model('Clientmodel');
		$this->load->model('CRLookupmodel');
		$this->load->model('Clientcustomtable');


		try { 

			$data = '';

			$get = $this->input->get();
 
 			$params = array();
 			$params['where'] = array('call_lookup_id'=>$call_lookup_id);
 			$record = $this->CRLookupmodel->row($params);
 			$data['record'] = $record; 
 			
 			if( $record->source_tablename != '' ){
 				$field_list = $this->Clientcustomtable->get_table_fields($record->source_tablename, 'array'); 
 				$data['field_list'] = $field_list; 				 
				$data['field_list_option'] = array_merge(array(''=>'--freetype--'), $field_list);
				
				$data['call_res_display'] = json_decode(@$record->call_res_display);
				$data['screen_display'] = json_decode(@$record->screen_display);
				$data['escalation_display'] = json_decode(@$record->escalation_display);
				$data['email_output'] = json_decode(@$record->email_output);
				$data['to_email'] = json_decode(@$record->to_email);
 			}


			$this->view_data['maintain']['data'] = $data;
			$this->view_data['maintain']['menu_active'] = 'crlookup';
			$this->view_data['maintain']['view_file'] = 'maintenance/call_response_lookup/setup_settings';
			
			$this->view_data['js_file'] = '<script src="assets/js/pages/maintenance/cr_lookup_settings.js?ver='.JS_VER_CRLOOKUP.'"></script>'; 		
			$this->load->view('template', $this->view_data); 
		} catch (Exception $e) {
			echo $e->getMessage();	
		}

	}

	public function lookup_data($call_lookup_id){

		$this->load->model('CRLookupmodel');
		$this->load->model('Clientcustomtable');

		$params = array();
		$params['where'] = array('call_lookup_id'=>$call_lookup_id);
		$record = $this->CRLookupmodel->row($params);		
		
		$data['record'] = $record;
		$data['look_up_name'] = $record->call_lookup_name;

		if( $record->source_tablename != '' ){


			$table_name = $record->source_tablename;
			$data['table_data'] = $this->Clientcustomtable->get_table_data($table_name);
			$data['table_fields'] = $this->Clientcustomtable->get_table_fields($table_name);
		}
	 

		$this->view_data['view_file'] = 'maintenance/call_response_lookup/lookup_data';
		$this->view_data['menu_active'] = 'maintenance';
		$this->view_data['data'] = $data;
 
		$this->load->view('template', $this->view_data); 

	}

	public function settings_save(){

		$this->load->model('Clientmodel');
		$this->load->model('CRLookupmodel');
		$this->load->model('Clientcustomtable');

		$jsonobj = new stdClass();
		$jsonobj->status = 1;
		$jsonobj->msg = '';

		try { 

			if( !$_POST ) throw new Exception("Error Processing Request", 1);
			
			$post = $this->input->post();

			if( !isset($post['call_lookup_id']) OR trim(@$post['call_lookup_id']) == '' ) throw new Exception("Lookup ID is not set", 1);
			if( !isset($post['client_id']) OR trim(@$post['client_id']) == '' ) throw new Exception("Client is missing", 1);
			if( !isset($post['form_type']) OR trim(@$post['form_type']) == '' ) throw new Exception("Action not recognise", 1);
			
			$form_type 		= $post['form_type'];
			$call_lookup_id = $post['call_lookup_id'];
			$client_id = $post['client_id'];
 
			switch ($form_type) {

				case 'button_name': 
					$set = array();
					  
					$set['updated_dt'] = date('Y-m-d H:i:s');
					$set['agent_name'] = $this->agent_name;
					$set['button_name'] = addslashes($post['button_name']);

					if( !$this->CRLookupmodel->update($call_lookup_id, $set) ){ 
						$jsonobj->status = 0;
						$jsonobj->msg = 'Failed to save Button name, something went wrong';
					}else{
						$jsonobj->msg = 'Button Name successfully save';
					}						   

					break;
				

				case 'upload': 
						
					$table_name = trim($post['source_tablename']);


					if( $_FILES['source_file_data']['size'] > 0){
                     
	                    $file = $_FILES["source_file_data"]["tmp_name"];
	                    $handle = fopen($file,"r");
	                 
	                    $batch = array();

	                    if( $handle ) {

	                    	$data = fgetcsv($handle, 1000, ",");

	                    	//clean the fields
	                    	$cleanfields = array_values($this->Clientcustomtable->clean_fields($data));

	                    	//removing all the columns
	                    	$removecolumn = $this->Clientcustomtable->remove_column($table_name);

 
	                    	$addcolumn = $this->Clientcustomtable->add_column($table_name, $cleanfields);

	                    	if( !$addcolumn->status ){
								$this->session->set_flashdata('fmesg', 'Sorry unable to create column title, please check the csv file');	
								redirect('crlookup/settings/'.$call_lookup_id.'/?client_id='.$client_id);
								exit;
	                    	}

	                        //$ctr=0;
	                        $batch_data = array();
	                        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
	     
	                            //if($ctr!=0){ 

	                                $_batch_data = array();
	                                foreach ($cleanfields as $key=>$field) {
	                                	
	                                	$_batch_data[$field] = $data[$key];

	                                }

	                                $batch_data[] = $_batch_data; 
	                            //}
	                            
	                            //$ctr++;
	                        }


		                    if( $this->Clientcustomtable->add_batch($table_name, $batch_data) ){
								$this->session->set_flashdata('fmesg', 'Uploading data successfully');	
								redirect('crlookup/settings/'.$call_lookup_id.'/?client_id='.$client_id);
		                    }  

	                    }
	                    
	                    fclose($handle);

             
        			}					  
					break;
				
				case 'call_res_display': 

					if( isset($post['call_res_display']) ){						

						$set = array();
						  
						$set['updated_dt'] = date('Y-m-d H:i:s');
						$set['agent_name'] = $this->agent_name;
						$set['call_res_display'] = json_encode($post['call_res_display']);

						if( !$this->CRLookupmodel->update($call_lookup_id, $set) ){ 
							$jsonobj->status = 0;
							$jsonobj->msg = 'Failed to save Call Response Display, something went wrong';
						}else{
							$jsonobj->msg = 'Call Response Display successfully save';
						}						   
					}

					break;
				
				case 'screen_display': 
 
						$set = array();
						  
						$set['updated_dt'] = date('Y-m-d H:i:s');
						$set['agent_name'] = $this->agent_name;


						$_sc = @$post['screen_display'];
						$screen_display = array();

						if( count($_sc) > 0 ){

							foreach ($_sc as $key => $value) {

								if( $value['input'] == 0 ){
									$value['display'] = $value['display1'];
								}else{
									$value['display'] = $value['display2'];
								}

								unset($value['display1']);
								unset($value['display2']);

								$screen_display[] =  $value;
							}
						}

						$set['screen_display'] = json_encode($screen_display);

						if( !$this->CRLookupmodel->update($call_lookup_id, $set) ){ 
							$jsonobj->status = 0;
							$jsonobj->msg = 'Failed to save Screen Display, something went wrong';
						}else{
							$jsonobj->msg = 'Screen Display successfully save';
						}						   

				 
					break; 
				
				case 'escalation_display': 
 
						$set = array();
						  
						$set['updated_dt'] = date('Y-m-d H:i:s');
						$set['agent_name'] = $this->agent_name;


						$_sc = @$post['escalation_display'];
						$escalation_display = array();

						if( count($_sc) > 0 ){

							foreach ($_sc as $key => $value) {

								$escalation_display[] =  $value;
							}
						}

						$set['escalation_display'] = json_encode($escalation_display);

						if( !$this->CRLookupmodel->update($call_lookup_id, $set) ){ 
							$jsonobj->status = 0;
							$jsonobj->msg = 'Failed to save Escalation Display, something went wrong';
						}else{
							$jsonobj->msg = 'Escalation Display successfully save';
						}						   

						//echo $this->Commonmodel->array_print_to_html($post);
				 
					break; 
				
				case 'email_output': 
 
						$set = array();
						  
						$set['updated_dt'] = date('Y-m-d H:i:s');
						$set['agent_name'] = $this->agent_name; 
						$set['email_output'] = json_encode(@$post['email_output']);

						if( !$this->CRLookupmodel->update($call_lookup_id, $set) ){ 
							$jsonobj->status = 0;
							$jsonobj->msg = 'Failed to save Email Ouput, something went wrong';
						}else{
							$jsonobj->msg = 'Email Ouput successfully save';
						}						   

						//echo $this->Commonmodel->array_print_to_html($post);
				 
					break; 
				
				case 'to_email': 
 
						$set = array();
						  
						$set['updated_dt'] = date('Y-m-d H:i:s');
						$set['agent_name'] = $this->agent_name;


						$_sc = @$post['to_email'];
						$to_email = array();

						if( count($_sc) > 0 ){

							foreach ($_sc as $key => $value) {

								if( $value['input'] == 0 ){
									$value['email'] = $value['display1'];
								}else{
									$value['email'] = $value['display2'];
								}

								unset($value['display1']);
								unset($value['display2']);

								$to_email[] =  $value;
							}
						}

						$set['to_email'] = json_encode($to_email);

						if( !$this->CRLookupmodel->update($call_lookup_id, $set) ){ 
							$jsonobj->status = 0;
							$jsonobj->msg = 'Failed to save Email To, something went wrong';
						}else{
							$jsonobj->msg = 'Email To successfully save';
						}						   

				 
					break; 
				
				case 'lookup_modal_display': 
 
						$set = array();
						  
						$set['updated_dt'] = date('Y-m-d H:i:s');
						$set['agent_name'] = $this->agent_name;

						$lookup_display = implode(',', $post['lookup_display']);
						$set['lookup_display'] = $lookup_display;
						
						$set['lookup_script'] = addslashes($post['lookup_script']); 

						if( !$this->CRLookupmodel->update($call_lookup_id, $set) ){ 
							$jsonobj->status = 0;
							$jsonobj->msg = 'Failed to save Lookup Modal Display, something went wrong';
						}else{
							$jsonobj->msg = 'Lookup Modal Display To successfully save';
						}						   

				 
					break; 

				default:
					$jsonobj->status = 0;
					$jsonobj->msg = 'Error no registered action';
					break;
			}

			echo json_encode($jsonobj);
 		 
		} catch (Exception $e) {
			
			$jsonobj->status = 0;
			$jsonobj->msg = $e->getMessage();
			echo json_encode($jsonobj);
		}

	}


 	public function ajax_screen_display_newtr(){
 		$this->load->model('CRLookupmodel'); 

		$get = $this->input->get();
		$_field_list = $get['pipe_lookupfields']; 
		$_field_list = explode('|', $_field_list);

		foreach ($_field_list as $row) {
			$field_list[$row] = $row;
		}

 		$jsonobj = new stdClass();
 	 	$jsonobj->html = $this->CRLookupmodel->screen_display_new_tr(time().'1', @$field_list, '');

 	 	echo json_encode($jsonobj);
 	}


 	public function ajax_escalation_display_newtr(){
 		$this->load->model('CRLookupmodel'); 

		$get = $this->input->get();
		$spreasheet_input = $get['spreasheet_input']; 
		$_field_list = $get['pipe_lookupfields']; 
		$_field_list = explode('|', $_field_list);

		foreach ($_field_list as $row) {
			$field_list[$row] = $row;
		}

 		$jsonobj = new stdClass();
 	 	$jsonobj->html = $this->CRLookupmodel->escalation_display_new_tr(time().'1', @$field_list, '', $spreasheet_input);

 	 	echo json_encode($jsonobj);
 	}



 	public function ajax_toemail_newtr(){
 		$this->load->model('CRLookupmodel'); 

		$get = $this->input->get();
		$_field_list = $get['pipe_lookupfields']; 
		$_field_list = explode('|', $_field_list);

		foreach ($_field_list as $row) {
			$field_list[$row] = $row;
		}

 		$jsonobj = new stdClass();
 	 	$jsonobj->html = $this->CRLookupmodel->to_email_new_tr(time().'1', @$field_list, '');

 	 	echo json_encode($jsonobj);
 	}


 	public function ajax_callactivity_modal_tpl(){
 		
 		$this->load->model('CRLookupmodel');
 		$this->load->model('Clientcustomtable');

 		$jsonobj = new stdClass();

 		try {
 			$data = '';

 			$get = $this->input->get();
			$call_lookup_id = $get['call_lookup_id'];

 			$params = array();
 			$params['where'] = array('call_lookup_id'=>$call_lookup_id);
 			$record = $this->CRLookupmodel->row($params);

 			$data['tran_id'] = @$get['tran_id']; 
 			$data['lookup_script'] = @$record->lookup_script;
 			$data['call_lookup_id'] = $call_lookup_id;

 			$lookup_fields = explode(',', @$record->lookup_display);
 			$data['lookup_fields'] = $lookup_fields;

 			$lookup_fields_sort = '';
 			foreach ($lookup_fields as $col) {
 				$lookup_fields_sort[] = ' '.$col.' ASC ';
 			}
 			$lookup_fields_sort = implode(', ', $lookup_fields_sort);

 			$data['table_data'] = $this->Clientcustomtable->get_table_data($record->source_tablename, '', '', $lookup_fields_sort);
 			$data['is_view_only'] = @$get['is_view_only'];

 			$jsonobj->status = 1;
			$jsonobj->html = $this->load->view('maintenance/call_response_lookup/modal_tpl', $data, TRUE); 

			echo json_encode($jsonobj);
			exit;

 		} catch (Exception $e) {
 			
 		}

 	}

 	public function ajax_callactivity_lookup_select(){
 		
 		$this->load->model('CRLookupmodel');
 		$this->load->model('Clientcustomtable');
 		$this->load->model('Transactionmodel');

 		$jsonobj = new stdClass();

 		try {
 			$data = '';

 			$get = $this->input->get();
			$call_lookup_id = @$get['call_lookup_id']; 
			$tran_id 		= @$get['tran_id']; 
			$lookup_data_id = @$get['lookup_data_id']; 
			$is_view_only 	= @$get['is_view_only']; 
		 

 			$params = array();
 			$params['where'] = array('call_lookup_id'=>$call_lookup_id);
 			$record = $this->CRLookupmodel->row($params);

 			 
 			$data_row = $this->Clientcustomtable->get_table_row($record->source_tablename, array('where'=>array('id'=>$lookup_data_id)));

 			//print_r($data_row);

 			$jsonobj->status = 1; 
 			 


			//call_res_display
			
			//{"call_direction":"0","caller_name":"","caller_phone":"Mobile"}
			$call_res_display_obj = (object)json_decode($record->call_res_display); 
		  
			$call_res_display = array();
			if( @$call_res_display_obj->call_direction != '' ){
				$call_res_display['call_direction'] = @$call_res_display_obj->call_direction;
			}
			if( @$call_res_display_obj->caller_name != '' ){				 
				$call_res_display['caller_name'] = @$data_row->{$call_res_display_obj->caller_name};
			}
			if( @$call_res_display_obj->caller_phone != '' ){
				$call_res_display['caller_phone'] = @$data_row->{$call_res_display_obj->caller_phone};
			}
 			$jsonobj->call_res_display = (object)$call_res_display;

			//screen_display
 			//[{"name":"Name","input":"0","display":"Injury_Management_Advisor"},{"name":"Phone","input":"0","display":"Mobile"}]
 			$screen_display_obj = (object)json_decode($record->screen_display); 
 			$screen_display_table = '<table class="table table-sm  table-bordered" style="width:50%">';
 			$_tran_sc_obj = array();
 			foreach ($screen_display_obj as $row) {
 				$_sd = array();
 				$_sd['name'] = $row->name;
 				$_sd['display'] = (($row->input=='0')?@$data_row->{$row->display}:$row->display);

 				$screen_display_table .= '<tr>
					<th style="width: 40%" class="border-top-0">'.$_sd['name'].'</th>
					<td class="border-top-0">'.$_sd['display'].'</td>
 				</tr>';

 				$_tran_sc_obj[] = $_sd; //store for transaction save
 			}
 			$screen_display_table .= '</table>';
 			$jsonobj->screen_display = $screen_display_table;


 			//escalation_display
 			//[{"input":"0","esc_name":"Business_Unit","name_field":"Injury_Management_Advisor","phone_field":"Mobile","notes":"freetype EG:  Call 2 times"},
 			//{"input":"1","esc_name":"Unit Manager","name_field":"test1","phone_field":"test2","notes":"test3"}]
 			$escalation_display_obj = (object)json_decode($record->escalation_display); 
 			$escalation_display_table = '<strong>Escalation</strong>';
 			$escalation_display_table .= '<table class="table table-sm table-bordered">';
 			$_tran_esc_obj = array(); 
 			foreach ($escalation_display_obj as $row) {
 				
 				
 				$esc_name = (($row->input=='0')?@$data_row->{$row->esc_name}:$row->esc_name);
 				$name_field = (($row->input=='0')?@$data_row->{$row->name_field}:$row->name_field);
 				$phone_field = (($row->input=='0')?@$data_row->{$row->phone_field}:$row->phone_field);
 				$notes	= $row->notes;

 				//$on_click_escalation_param['name_field'] = '{name_field:\''.$name_field.'\', phone_field: \''.$phone_field.'\', notes:\''.$notes.' \' }';
 				$params = array();
 				$params['esc_name'] = $esc_name;
 				$params['name_field'] = $name_field;
 				$params['phone_field'] = $phone_field;
 				$params['notes'] = $notes;
 				
 				$_tran_esc_obj[] = $params; //store for transaction save

 				$params = str_replace('"', '\"', json_encode($params));

 				$escalation_display_table .= '<tr> 
					<td>'.$esc_name.'</td>
					<td>'.$name_field.'</td>
					<td>'.$phone_field.'</td>
					<td>'.$notes.'</td>
					<td><button onclick="callactivity.lookup.on_click_escalation(\''.$name_field.'\', \''.$phone_field.'\', \''.$notes.'\')" class="btn btn-sm btn-success py-0"> <i class="fas fa-copy"></i> Select</button></td>
 				</tr>';

 			}
 			$escalation_display_table .= '</table>';
 			$jsonobj->escalation_display = $escalation_display_table; 


 			//email_output
 			
 			$jsonobj->email_output = addslashes($record->email_output);

 			//$jsonobj->to_email = addslashes($record->to_email);

 			$to_email = json_decode($record->to_email);
 			if( count(@$to_email) > 0){

	 			foreach(@$to_email as $row ){ 

	 				$jsonobj->to_email[] = (($row->input=='0')?@$data_row->{$row->email}:$row->email);

	 			}
 			}

 			$jsonobj->to_email = (isset($jsonobj->to_email))?implode(',', @$jsonobj->to_email):'';



 			if( @$is_view_only != 1 ){
 				
	 			//update transaction.lookup_data 
	 			//When we SET THE LOOKUP it needs to stay there for future opening of the record.
	 			$json_tran_data = new stdClass();
	 			$json_tran_data->call_res_display = $jsonobj->call_res_display;
	 			$json_tran_data->screen_display = $_tran_sc_obj;
	 			$json_tran_data->escalation_display = $_tran_esc_obj;
	 			$update_set = array();
	 			$update_set['lookup_data'] = json_encode($json_tran_data);
	 			$this->Transactionmodel->update($tran_id, $update_set);
 			}

 			//to_email

			echo json_encode($jsonobj);
			exit;

 		} catch (Exception $e) {
 			
 		}

 	}


	public function test(){

		$this->load->model('Clientcustomtable');

		print_r($this->Clientcustomtable->create_base_table(1, 'drivers2'));


	}

	
}
