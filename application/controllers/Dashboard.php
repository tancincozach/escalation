<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function __construct(){
		
		parent::__construct();

	}

	public function index(){

		redirect('register');

		$data = '';

		$this->view_data['data'] = $data;
		$this->view_data['menu_active'] = 'dashboard';
		$this->view_data['view_file'] = 'pages/dashboard';
		//$this->view_data['js_file'] = '<script src="assets/js/page_js/dashboard.js"></script>'; 
		
		$this->load->view('template', $this->view_data); 

	}

	public function smsregister(){
		
		$this->load->library("pagination");
		
		$this->load->model('Smsglobalmodel');

		$data = '';

		$params = $this->input->get();
		$page = (isset($params['per_page'])) ? $params['per_page'] : 0;
		unset($params['per_page']);

		$params_query = @http_build_query($params);

		$per_page = 50;
		$query_params['limits']['start'] = $page;
		$query_params['limits']['limit'] = $per_page;


		$appt_results 				= $this->Smsglobalmodel->get_result_pagination($query_params);
        $p_config["base_url"] 		= base_url() . "callcenter/index/?".$params_query;
        $p_config["total_rows"] 	= $appt_results['total_rows'];
        $p_config["per_page"] 		= $per_page;
        $p_config["uri_segment"] 	= 3; 
        $config = $this->Commonmodel->pagination_config($p_config);	 
        $this->pagination->initialize($config);

        $data['results'] 	= $appt_results['results'];
		$data["links"] 		= $this->pagination->create_links();
		$data['showing']	= 'Showing '.(($page==0)?1:$page+1).' to '.($page+count($data['results'])).' of '.$appt_results['total_rows'].' entries';



		$this->view_data['data'] = $data;
		$this->view_data['menu_active'] = 'smsregister';
		$this->view_data['view_file'] = 'pages/sms_register';
		
		
		$this->load->view('template', $this->view_data); 		
	}

	public function alertregister(){

		$data = '';

		$this->view_data['data'] = $data;
		$this->view_data['menu_active'] = 'alertregister';
		$this->view_data['view_file'] = 'pages/alert_register';
		
		
		$this->load->view('template', $this->view_data); 

	}

	public function callregister(){

		$data = '';

		$this->view_data['data'] = $data;
		$this->view_data['menu_active'] = 'callregister';
		$this->view_data['view_file'] = 'pages/call_register';
		//$this->view_data['js_file'] = '<script src="assets/js/page_js/dashboard.js"></script>'; 
		
		$this->load->view('template', $this->view_data); 

	}

	public function escalation(){
		$data = '';

		$this->view_data['data'] = $data;
		$this->view_data['menu_active'] = 'escalation';
		$this->view_data['view_file'] = 'pages/escalation';
		//$this->view_data['js_file'] = '<script src="assets/js/page_js/dashboard.js"></script>'; 
		
		$this->load->view('template', $this->view_data); 
	}
}
