<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends MY_Controller {

	public function __construct(){
		
		parent::__construct();

		$this->load->model('Usersettingsmodel');
		$this->load->model('Transactionmodel');
	}

	public function index(){

		$this->load->model('Clientmodel');
		$this->load->library("pagination");

		$data = '';

		$params = $this->input->get();
		$page = (isset($params['per_page'])) ? $params['per_page'] : 0;
		unset($params['per_page']);

		$data['filters'] = $params;

		if( $this->client_id > 0 ){
		
			$params['client_id'] = $this->client_id;
		
		}else{

			if( isset($params['client_id']) ){
				if( !is_numeric($params['client_id']) ){ 
					$params['cust_name'] = $params['client_id'];
					unset($params['client_id']);
				}
			}

		}

		 
		$query_params = $this->build_where($params);

		$params_query = @http_build_query($params);

		$per_page = 20;
		$query_params['limits']['start'] = $page;
		$query_params['limits']['limit'] = $per_page; 

		$user_option = $this->Usersettingsmodel->options($this->agent_name);
		//check user default display screen
		if( !isset($query_params['display_screen']) ){
			//$user_option = $this->Usersettingsmodel->options($this->agent_name);

			if( !empty($user_option->display_screen) ){
				$query_params['where']['display_screen'] = @$user_option->display_screen;

				//override display_screen filter
				//$data['filters']['display_screen'] = @$user_option->display_screen;

			}
		}

		$data['useroption'] = @$user_option;

		if(@$user_option->display_screen==3){
				$data['disp_opts'] = array( 3=>'Back Office');
		}else{
			
				$data['disp_opts'] = $this->display_screen;
		}


		$appt_results 				= $this->Transactionmodel->listing($query_params);
        $p_config["base_url"] 		= base_url() . "register/index/?".$params_query;
        $p_config["total_rows"] 	= $appt_results['total_rows'];
        $p_config["per_page"] 		= $per_page;
        $p_config["uri_segment"] 	= 3; 
        $config = $this->Commonmodel->pagination_config($p_config);	 
        $this->pagination->initialize($config);

        $data['results'] 	= $appt_results['results'];
		$data["links"] 		= $this->pagination->create_links();
		$data['showing']	= 'Showing '.(($page==0)?1:$page+1).' to '.($page+count($data['results'])).' of '.$appt_results['total_rows'].' entries';


		// $client_params['select']				 = 'client_id, client_name';
		// $client_params['where']['client_status'] = 1;
		// $client_params['sorting']['sort'] 		 = 'client_name';
		// $client_params['sorting']['order']		 = 'ASC';
		// $data['clients'] = $this->Clientmodel->listing($client_params, false);

		$data['clients'] = $this->Clientmodel->combine_clients_array();


		$this->view_data['data'] = $data;
		$this->view_data['menu_active'] = 'register';
		$this->view_data['view_file'] = 'pages/listing_register';
		$this->view_data['js_file'] = '<script src="assets/js/alert_register.js?v='.JS_VER_ALERTREGISTER.'"></script>';
		
		$this->load->view('template', $this->view_data); 
	}


	public function cma_reminder(){

		$this->load->model('Clientmodel');
		$this->load->library("pagination");

		$data = '';

		$params = $this->input->get();
		$page = (isset($params['per_page'])) ? $params['per_page'] : 0;
		unset($params['per_page']);

		$data['filters'] = $params; 

		if( $this->client_id > 0 ){
			$params['client_id'] = $this->client_id;
		}else{

			if( isset($params['client_id']) ){ 
				$params['cust_name'] = $params['client_id'];
				unset($params['client_id']); 
			}

		}

		$query_params = $this->build_where($params);

		$params_query = @http_build_query($params);

		$per_page = 20;
		$query_params['where']['alert_type'] = 'CMA REMINDER';
		$query_params['limits']['start'] = $page;
		$query_params['limits']['limit'] = $per_page; 

		// if( $this->client_id > 0 ){
		// 	$query_params['where']['client.client_id'] = $this->client_id;
		// }

		$user_option = $this->Usersettingsmodel->options($this->agent_name);
		//check user default display screen
		if( !isset($query_params['display_screen']) ){
			//$user_option = $this->Usersettingsmodel->options($this->agent_name);

			if( !empty($user_option->display_screen) ){
				$query_params['where']['display_screen'] = @$user_option->display_screen;

				//override display_screen filter
				//$data['filters']['display_screen'] = @$user_option->display_screen;

			}
		}

		$data['useroption'] = @$user_option;

		if(@$user_option->display_screen==3){
				$data['disp_opts'] = array( 3=>'Back Office');
		}else{
			
				$data['disp_opts'] = $this->display_screen;
		}
		
		$data['useroption'] = @$user_option;


		$appt_results 				= $this->Transactionmodel->listing($query_params);
        $p_config["base_url"] 		= base_url() . "register/cma_reminder/?".$params_query;
        $p_config["total_rows"] 	= $appt_results['total_rows'];
        $p_config["per_page"] 		= $per_page;
        $p_config["uri_segment"] 	= 3; 
        $config = $this->Commonmodel->pagination_config($p_config);	 
        $this->pagination->initialize($config);

        $data['results'] 	= $appt_results['results'];
		$data["links"] 		= $this->pagination->create_links();
		$data['showing']	= 'Showing '.(($page==0)?1:$page+1).' to '.($page+count($data['results'])).' of '.$appt_results['total_rows'].' entries';


		$client_params['select']				 = 'client_id, client_name';
		$client_params['where']['client_status'] = 1;
		$client_params['sorting']['sort'] 		 = 'client_name';
		$client_params['sorting']['order']		 = 'ASC';
		$data['clients'] = $this->Clientmodel->listing($client_params, false);
		
		$data['customer_names'] = $this->Transactionmodel->get_customer_name();


		$this->view_data['data'] = $data;
		$this->view_data['menu_active'] = 'cma_reminder';
		$this->view_data['view_file'] = 'pages/listing_register';
		$this->view_data['js_file'] = '<script src="assets/js/alert_register.js?v='.JS_VER_ALERTREGISTER.'"></script>';
		
		$this->load->view('template', $this->view_data); 
	}

	function build_where($params){

		$params_where = array();

		if( isset($params['search']) AND $params['search'] != ''){
			
			$search = addslashes(strtolower(trim($params['search'])));

			if( preg_match('/(!)\d+/', $search) ){
	 			$tran_id = str_replace('!', '', $search);
	 			$like = '(`transaction`.`tran_id`='.$tran_id.')';
	 		}else{ 
		 		$like = " ( lower(`ref_number`) LIKE '%{$search}%' ";		 		 
		 		$like .= " OR lower(`brief_description`) LIKE '%{$search}%' "; 
		 		$like .= " OR json_data LIKE '%{$search}%' ) "; 
		 		//$like .= " IF(alert_type='EMAIL', IF( (SELECT COUNT(*) FROM email_data WHERE email_data.tran_id = transaction.tran_id AND (email_from LIKE '%{$search}%' OR  email_to LIKE '%{$search}%' ))> 0, 1 , 0) , '') ) "; 
	 		}
	 		$params['where_str'] = $like;	

	 		unset($params['search']);
		}

		if( isset($params['dt']) &&  $params['dt'] != '' ) {

			 
			$dt = explode(' - ',$params['dt']); 
			$dt_f = date('Y-m-d', strtotime(str_replace('/', '-', $dt[0])));
			$dt_t = date('Y-m-d', strtotime(str_replace('/', '-', $dt[1])));			
			 
 			
 			if( isset($params['where_str']) ) $params['where_str'] .= ' AND ';
 			else $params['where_str'] = '';

			$params['where_str'] .= " (DATE_FORMAT(`transaction`.`tran_created`,'%Y-%m-%d') BETWEEN '{$dt_f}' AND '{$dt_t}') ";			


		}		

		if( isset($params['tran_status']) AND $params['tran_status'] != ''){
			//$params['where'] = array('tran_status'=>$params['tran_status']);			
			$params_where['tran_status'] = $params['tran_status'];
		}		

		if( isset($params['client_id']) AND $params['client_id'] != ''){
			//$params['where'] = array('client.client_id'=>$params['client_id']);			
			$params_where['client.client_id'] = $params['client_id'];
		}

		if( isset($params['display_screen']) AND $params['display_screen'] != ''){

 			$params_where['display_screen'] = $params['display_screen']; 
		}

		if( isset($params['alert_type']) AND $params['alert_type'] != ''){

 			$params_where['alert_type'] = $params['alert_type']; 
		}

		if( isset($params['cust_name']) AND $params['cust_name'] != ''){

 			$params_where['cust_name'] = $params['cust_name']; 
		}

		if( isset($params['filter-priority']) AND $params['filter-priority'] != ''){

 			if( isset($params['where_str']) ) $params['where_str'] .= ' AND ';
 			else $params['where_str'] = '';

 			$params['where_str'] .= " `priority` = '".$params['filter-priority']."'";
		}

		if( count($params_where) > 0 )
				$params['where'] = $params_where;

		return $params;
	}

 	
 	public function ajax_take_control(){
 		
 		$json_return = array('status'=>1, 'msg'=>'');

 		try {
 			
 			$method = $this->input->method(TRUE); 
 			
 			$json_return['method'] = $method;
 			
 			if($method != 'POST') throw new Exception("Error: request is allowed", 1);

 			$post = $this->input->post();

 			if( !isset($post['tran_id']) OR trim(@$post['tran_id']) == '' ) throw new Exception("Error: Tran ID is missing", 1);
 			
 			$tran_id 	= @$post['tran_id'];
 			$alert_type = @$post['alert_type'];


 			//check if someone already working on this Alert#
 			$params['where']['tran_id'] = $tran_id;
 			$row = $this->Transactionmodel->row_plain($params);

 			if( trim($row->agent_name) != '' ) throw new Exception($row->agent_name." already working on this Alert#".$row->ref_number, 1);


 			$set = array(); 			
 			$set['agent_name'] = $this->agent_name;

 			if( $row->call_hand_start == '' )
 				$set['call_hand_start'] = date('Y-m-d H:i:s');

 			if( !$this->Transactionmodel->update($tran_id, $set) ) throw new Exception("Sorry, unable to proceed on this action, something went wrong", 1);
 			

		  	//Audit TRAIL
			$audit_trail_params['tran_id'] = $tran_id;
			$audit_trail_params['message'] = 'Take Control';
			$audit_trail_params['more_info'] = json_encode(array('agent_name'=>$this->agent_name));
			$audit_trail_params['audit_type'] = 'take_control';
			$audit_trail_params['created'] = strtotime('now');
			$this->db->insert('audit_trail', $audit_trail_params);


	 		$json_return['msg'] = 'Successfully take control on Alert#'.$row->ref_number;
 			$json_return['url'] = ($alert_type=='CMA REMINDER')?'callactivity/open_cma_reminder/'.$tran_id:'callactivity/open/'.$tran_id.'/?take_control=1';


 			//AGENT STATUS
 			$_as_r = array();
 			$_as_r['tran_id'] 	= $tran_id;
 			$_as_r['agent_id'] 	= $this->agent_id;
 			$_as_r['cma_id'] 	= @$row->cma_id;
 			$_as_r['reason'] 	= ($alert_type=='CMA REMINDER')?'Escalation Ack':'Escalation For Client';
 			$this->Commonmodel->curl_agentstatus('agentStatusUpdate',$_as_r);



 			echo json_encode($json_return);
 		} catch (Exception $e) {
 			
 			$json_return['status'] = 0;
 			$json_return['msg'] = $e->getMessage();

 			echo json_encode($json_return);
 		}

 	}

 	public function ajax_take_control_off(){

 		$json_return = array('status'=>1, 'msg'=>'');

 		try {
 			
 			$method = $this->input->method(TRUE); 
 			
 			$json_return['method'] = $method;
 			
 			if($method != 'POST') throw new Exception("Error: request is allowed", 1);

 			$post = $this->input->post();

 			if( !isset($post['tran_id']) OR trim(@$post['tran_id']) == '' ) throw new Exception("Error: Tran ID is missing", 1);
 			
 			$tran_id 	= @$post['tran_id'];
 			

 			//check if someone already working on this Alert#
 			$params['where']['tran_id'] = $tran_id;
 			$row = $this->Transactionmodel->row_plain($params);

 			if( trim($row->agent_name) != $this->agent_name ) throw new Exception("Action denied, your are not the owner of this log ", 1);


 			$set = array(); 			
 			$set['agent_name'] = NULL; 
 			$set['call_hand_start'] = NULL;
 			$set['call_hand_end'] = NULL;

 			if( !$this->Transactionmodel->update($tran_id, $set) ) throw new Exception("Sorry, unable to proceed on this action, something went wrong", 1);
 			

		  	//Audit TRAIL
			$audit_trail_params['tran_id'] = $tran_id;
			$audit_trail_params['message'] = 'REMOVE Take Control';
			$audit_trail_params['more_info'] = json_encode(array('agent_name'=>$this->agent_name));
			$audit_trail_params['audit_type'] = 'take_control_remove';
			$audit_trail_params['created'] = strtotime('now');
			$this->db->insert('audit_trail', $audit_trail_params);


	 		$json_return['msg'] = 'Successfully removing take control on Alert#'.$row->ref_number;
 			$json_return['url'] = 'register';


 			//AGENT STATUS
 			$_as_r = array();
 			$_as_r['tran_id'] 	= $tran_id;
 			$_as_r['agent_id'] 	= $this->agent_id;
 			$_as_r['cma_id'] 	= @$row->cma_id;
 			$_as_r['reason'] 	= 'Escalation Duty';
 			$this->Commonmodel->curl_agentstatus('agentStatusUpdate',$_as_r);



 			echo json_encode($json_return);
 		} catch (Exception $e) {
 			
 			$json_return['status'] = 0;
 			$json_return['msg'] = $e->getMessage();

 			echo json_encode($json_return);
 		}
 	}


 	/**
 	 * This is use only for admin
 	 * @return [type] [description]
 	 */
 	public function ajax_close_alerts(){

 		$post = $this->input->post(); 		
 		try {
 			
 			$method = $this->input->method(TRUE); 
 			
 			$json_return['method'] = $method;
 			
 			if($method != 'POST') throw new Exception("Error: request is allowed", 1);

 			$post = $this->input->post();

 			if(!isset($post['t_ids']) ||  count($post['t_ids']) <=0)  throw new Exception("ERROR: Empty Trans IDS");


 			$set = array();
 			foreach ($post['t_ids'] as $key => $value) {

 				$set['tran_status'] = 0;
 				$set['agent_name'] = $this->agent_name;

		 		if( !$this->Transactionmodel->update($value['tran_id'], $set) ) throw new Exception("Sorry, unable to proceed on this action, something went wrong");

 				
 			}
 		

	 		$json_return['msg'] = 'Successfully closed ('.count($post['t_ids']).')';
 			$json_return['url'] = 'escalation/register';

 			echo json_encode($json_return);
 		} catch (Exception $e) {
 			
 			$json_return['status'] = 0;
 			$json_return['msg'] = $e->getMessage();

 			echo json_encode($json_return);
 		}	
   	}


   	/**
   	 * This function is access via ajax call from register checked boxes.
   	 * @return [type] [description]
   	 */
   	public function ajax_multi_closed_alerts(){

 		$post = $this->input->post(); 		


		$this->load->model('Proceduremodel');
		$this->load->model('Clientcustomtable');
		$this->load->model('Proceduremodel');
		$this->load->model('Callsmodel');
		$this->load->model('Callresponsemodel');
		$this->load->model('Emaildatamodel');


 		try {
 			
 			$method = $this->input->method(TRUE); 
 			
 			$json_return['method'] = $method;
 			
 			if($method != 'POST') throw new Exception("Error: request is allowed", 1);

 			$post = $this->input->post();

 			if(!isset($post['t_ids']) ||  count($post['t_ids']) <=0)  throw new Exception("ERROR: Empty Trans IDS");


 			$set = array();

 			foreach ($post['t_ids'] as $key => $value) {

 				$tran_id = $value['tran_id'];

 				$set['tran_status'] = 0;

 				$set['agent_name'] = $this->agent_name;

 				$tran_row = $this->Transactionmodel->row(array('where'=>array('tran_id'=>$tran_id)));

 				$proc_row = $this->Proceduremodel->row(array('where'=>array('pro_id'=>$tran_row->procedure_id)));

 				$other_options = json_decode(@$proc_row->other_options);
 				$limit_text_opt = @$other_options->limit_text;

				$cma = json_decode(@$proc_row->cma_settings);

				$cma_set = array();

				if(isset($tran_row)   && isset($proc_row)){

					$cma_set['audit_type'] 	= 'multiple-deletion';
					$cma_set['tran_id'] 	= $tran_id;
					$cma_set['cma_db'] 	    = $cma->cma_db;
					$cma_set['cust_id'] 	= $cma->cust_id;
					$cma_set['contact_id'] 	= $cma->contact_id_callsmade;
					$cma_set['agent_id'] 	= $this->agent_id;
					$cma_set['more_info'] 	= array('agent_name'=>$this->agent_name);


					switch ($tran_row->alert_type) {
						case 'EMAIL':
							//GRAB email data on table email_data 

							$email_data_where['where'] = array('tran_id'=>$tran_id);
							$email_data_where['sorting']['sort'] 	= 'created_at';
							$email_data_where['sorting']['order'] 	= 'asc';

							$email_data = $this->Emaildatamodel->get_result( $email_data_where );
							
			 
							//$alert_detail = $email_data;

							$email_body = '';
							foreach ($email_data as $email) {

								$email_body .=  (!empty($email->body_html)) ? $email->body_html : $email->body_plain;
								
							}

							$alert_detail = $email_body;

							break; 
						default:
								if( @$proc_row->client_data_src == '' ){
									
									$alert_detail = (object)json_decode($tran_row->json_data);

									$alert_detail = @$alert_detail->BODY;
								}else{

									//$client_data_source = $this->db->get_where($client->client_data_src, array('tran_id'=>$record->tran_id))->result_array();			 				 

					 				$client_data_source = (Array)$this->Clientcustomtable->get_table_data( $proc_row->client_data_src, array('where'=>array('tran_id'=>$tran_id)), 'result_array' );
		 
									//$obj = new stdClass();
									$api_BODY = (object)$client_data_source;
									 
									if( is_object($api_BODY) ){
									 	
									 	$alert_detail = $this->Commonmodel->array_key_val_to_table(@$api_BODY);

									}else{
										$alert_detail = stripslashes($api_BODY);
									}	

									//echo $alert_detail;								
 
								} 
 
							break;
					}



			 		//if( !$this->Transactionmodel->update($tran_row->tran_id, $set) ) throw new Exception("Sorry, unable to proceed on this action, something went wrong");
			 		if( $this->Transactionmodel->update($tran_row->tran_id, $set) ){

						$call_set['tran_id'] 		= $tran_id;
						$call_set['call_res_id'] 	= @$proc_row->multi_del_callres;						
						$call_set['caller_name'] 	= '';
						$call_set['caller_phone'] 	= '';
						$call_set['call_notes'] 	= '';
						$call_set['log_status'] 	= 0;
						$call_set['agent_name'] 	= $this->agent_name;
						$call_set['user_id'] 		= $this->user_id;

						$this->Callsmodel->insert($call_set);

			 		 

			 		}
				    
				    //Callresponsemodel
						
					$_alert_detail = '';

			 		if( isset($limit_text_opt->type) AND @$limit_text_opt->type > 0  ){

			 			$_alert_detail = $this->Commonmodel->clean_htmlcontent($alert_detail);


			 			if( $limit_text_opt->type == 1 ){ //Break by word(s) 

			 				if( @$limit_text_opt->type_val != '' ){
			 					$alert_contant = explode(@$limit_text_opt->type_val, $_alert_detail);
			 					$_alert_detail = @$alert_contant[0];
			 				} 

			 			}elseif ($limit_text_opt->type == 2) { //By character limit
			 				
			 				if( @$limit_text_opt->type_val != '' AND @$limit_text_opt->type_val > 0 ){

			 					$_alert_detail = substr($_alert_detail, 0, @$limit_text_opt->type_val);
			 				}

			 			}

			 		}else{


				 		/*$callresponse = $this->Callresponsemodel->row(array('where'=>array('call_res_id'=>$proc_row->multi_del_callres)));			 		

				 		$alert_detail = '<strong>Alert Details</strong><br />'.$alert_detail;
				 		$alert_detail .= '<br /><br />';
				 		$alert_detail .= '<strong>Call Activity</strong><br />';
				 		$alert_detail .= date('d/m/Y H:i').' '.@$callresponse->call_res_text;
			 		
				 		if( !isset($proc_row->multi_del_callres) ){
				 			$alert_detail .= '<strong style="color:red">Warning!!! Multiple close Call Response is not set, Please set it under maintenance->procedure (update) form field "Multi Close Call Response Set"</strong>';
				 		}*/



			 			$_alert_detail = $this->Commonmodel->clean_htmlcontent($alert_detail);
			 		}			 		 

			 		
			 		//echo $alert_detail;
					$cma_set['message'] = $_alert_detail; 

				    $insert_cma = $this->Commonmodel->insert_cma_call($cma_set);
				}

 				
 			}
 		

	 		$json_return['msg'] = 'Successfully closed ('.count($post['t_ids']).')';
 			$json_return['url'] = 'escalation/register';

 			echo json_encode($json_return);
 		} catch (Exception $e) {
 			
 			$json_return['status'] = 0;
 			$json_return['msg'] = $e->getMessage();

 			echo json_encode($json_return);
 		}	
   }

   	public function ajax_save_useroption(){

	   	$json_return = array('status'=>0, 'msg'=>'Failed to save option');

	   	try {

	   		if( !$_POST ) throw new Exception("Error Processing Request", 1);
	   		

	   		$post = $this->input->post();

	   		$agent_name = $this->agent_name;

	   		$user_options = array();
	   		$user_options['display_screen'] = $post['display_screen'];
 

			if( $this->Usersettingsmodel->set_option($agent_name, $user_options) ){
				$json_return['status'] = 1;
				$json_return['msg'] = 'Successfully save';
			}


			echo json_encode($json_return);

	   	} catch (Exception $e) {
	   		echo json_encode($json_return);
	   	}

   }


}
