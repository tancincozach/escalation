<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This class is used to pull emails using the Imap Library (libraries/Imap.php).
 * This is executed via cron using the shell scripting escalation_incomingemail_cron.sh accessing https://escalation.welldone.net.au/worker/incoming_email/cron.
 * Everytime emails are being read it will get FLAGGED so it will be showing on imap_search next run,
 * 
 */

class Incoming_email extends CI_Controller {
 	
 	//https://escalation.welldone.net.au/worker/incoming_email/cron

	public function __construct(){
		
		parent::__construct();

	}

	/**
	 * This is the function run via cron	 
	 */
	public function cron(){
		 
		try {

			$this->_pullemails();

		} catch (Exception $e) {
			echo 'ERROR: '.$e->getMessage();
		}
	}

	
	/**
	 * This is the function the connect to the email server and pull the UNFLAGGED emails and process
	 * @return [type] [description]
	 */
	function _pullemails(){

		$this->load->library('imap'); 
		$this->load->model('Emaildatamodel');
		$this->load->model('Proceduremodel');
		$this->load->model('Transactionmodel');
		
		$this->imap->connect();  
		//$messages = $this->imap->search('UNFLAGGED' ,'date', false);
		$messages = $this->imap->search('UNFLAGGED' ,'date', false);


		/*echo '<br />';
		print_r($this->imap->get_folders());
		echo '<br />';
		$this->imap->select_folder('INBOX');
		echo '<br />';
		print_r($this->imap->get_quota());*/

		//$messages = $this->imap->search('UNFLAGGED' ,'date', false);

		$counter = 0;
		//echo '<pre>';
		foreach ($messages as $msgid) {
			
			$message = $this->imap->get_message($msgid);

			echo 'start -------------------------------------------<br />';
			// print_r($message);
			

			$email['msg_id'] = $message['id'];
			$email['msg_uid'] = $message['uid'];
			$email['email_from'] = json_encode($message['from']);
			$email['email_to'] = json_encode($message['to']);
			$email['email_cc'] = json_encode($message['cc']);
			$email['email_bcc'] = json_encode($message['bcc']);
			$email['email_subject'] = @addslashes($message['subject']);
			$email['email_reply_to'] = json_encode($message['reply_to']);
			$email['body_plain'] = @addslashes($message['body']['plain']);
			$email['body_html'] = @addslashes($message['body']['html']);
			//$email['msg_date'] = $message['date'];
			$email['email_udate'] = $message['udate'];			


			$check_email_type = $this->check_email_type(trim($message['subject']));
			$email['email_type'] = $check_email_type;

			//check for reply,forward, and duplicate parent
			$_check_reply_parent = $this->check_reply_parent($email);	

			$parent_tran_id = @$_check_reply_parent->tran_id;
			$is_duplicate = @$_check_reply_parent->is_duplicate;

			//Just update the transaction
			//Insert email thread
			if( $parent_tran_id > 0 ){

				//get transaction row
				$tran_row = $this->Transactionmodel->row(array('where'=>array('tran_id'=>$parent_tran_id)));

				//check procedure and update the due date
				$procedure = $this->Proceduremodel->row(array('where'=>array('pro_id'=>$tran_row->procedure_id)));
				$procedure->reminder_alert_duration = @$procedure->reminder_alert_duration==''?0:@$procedure->reminder_alert_duration;

				$update_tran = array();
				$update_tran['alert_updated_dt'] = date('Y-m-d H:i:s');
				$update_tran['alert_due_dt'] 	= date('Y-m-d H:i:s', strtotime(' + '.@$procedure->reminder_alert_duration.' MINUTE '));
				$update_tran['tran_status'] 	= 1;
				
				if( trim($is_duplicate) != '' )
					$update_tran['is_duplicate_email'] 	= $is_duplicate;

				$update_tran['thread_count'] 	= ($this->Transactionmodel->get_thread_count($parent_tran_id)) + 1;
				$update_tran['agent_name'] 		= '';
				$this->Transactionmodel->update($parent_tran_id, $update_tran);


				if( count($message['attachments']) > 0 ){
					$_attachements = $this->save_attachements($parent_tran_id, @$message['attachments']);

					if( isset($_attachements['total_size']) AND @$_attachements['total_size'] > 0){

						$email['attachements'] =json_encode(@$_attachements['files']);
						$email['attachments_size'] = @$_attachements['total_size'];
					}
				}

				$email['tran_id'] = $parent_tran_id;


				if( !in_array($_SERVER['SERVER_NAME'], array('localhost', '127.0.0.1'))  ){
					$this->imap->mark_as_flagged($message['uid']);
				}

				$email_insert_status = $this->Emaildatamodel->insert($email);

				if($email_insert_status != 1 ) {

					$this->send_email_on_error($email, $parent_tran_id, $email_insert_status);

				}

				// if( !in_array($_SERVER['SERVER_NAME'], array('localhost', '127.0.0.1'))  ){
				// 	$this->imap->mark_as_flagged($message['uid']);
				// }

				$counter++;


			}else{
 
				$tran_id = $this->process_transaction($email);


				echo 'process_transaction ... <br />';

				if( $tran_id > 0 ){
 						


					if( count($message['attachments']) > 0 ){

						echo 'checking  attachments ... <br />';
						 
						$_attachements = $this->save_attachements($tran_id, @$message['attachments']);

						if( isset($_attachements['total_size']) AND @$_attachements['total_size'] > 0){
							$email['attachements'] =json_encode(@$_attachements['files']);
							$email['attachments_size'] = @$_attachements['total_size'];						
						}

						echo 'printing attachement results <br />';
						print_r($_attachements);
						echo '<br />';
					}

					$email['tran_id'] = $tran_id;

					echo 'inserting to email data ...  <br />';

					if( !in_array($_SERVER['SERVER_NAME'], array('localhost', '127.0.0.1'))  ){
						$this->imap->mark_as_flagged($message['uid']);
					}


					$email_insert_status = $this->Emaildatamodel->insert($email);
					
					echo $email_insert_status;
					print_r($email_insert_status);
					echo '<br />';					

					if($email_insert_status != 1 ) { 

						echo 'ERROR: Emaildatamodel->insert <br />';

						$this->send_email_on_error($email, $tran_id, $email_insert_status);

					}

					// if( !in_array($_SERVER['SERVER_NAME'], array('localhost', '127.0.0.1'))  ){
					// 	$this->imap->mark_as_flagged($message['uid']);
					// }

					$counter++;

				}

			} 

			 
		}	 
		//echo '</pre>';

		if( $counter > 0 ){
			echo 'There are '.$counter.' email(s) being pull';
		}else{
			echo 'No new email!!!';
		}		 

	} 

	/**
	 * This function is to save the attachment to the location EMAIL_ATTACH_LOC
	 * @param  int $tran_id     
	 * @param  array $attachments 
	 * @return array of attached filename
	 */
	function save_attachements($tran_id, $attachments){


		$attach = array('files'=>'', 'total_size'=>'');

		try { 

			$files = array();
			$total_size = 0;
		    foreach ($attachments as $key => $attachment) {
		        $name = $attachment['name'];
		        $contents = $attachment['content'];
		        $total_size += $attachment['size'];

		        $filename = $tran_id.'_'.$name;
		        if( file_put_contents(EMAIL_ATTACH_LOC.$filename, $contents) ){
		        	$files[] = $filename;
		        }
			}

			
			$attach['files'] = $files;
			$attach['total_size'] = $total_size;

			//return json_encode($files);
			return $attach;

		} catch (Exception $e) {
						 
			$this->send_email_on_error($attachments, $tran_id, $e->getMessage());

			return $attach;

		}

	}

	/**
	 * This function process the email data and inserted to the transaction table with the appropriate procedure attached
	 * @param  array $data email data
	 * @return int tran_id or 0(if failed to insert)
	 */
	function process_transaction($data){

		$this->load->model('Transactionmodel');
		$this->load->model('Proceduremodel');
		$this->load->helper('in_array_r');

		try {
			
 


			$email_from = array(json_decode($data['email_from'], true));
			$email_to 	= json_decode($data['email_to'], true);
			$email_cc = json_decode($data['email_cc'], true);

			$procedure = array();


			if( is_object($row_email_priority = $this->check_email_priority_condition($email_from, $email_to, $email_cc)) )  {
				$procedure = $row_email_priority;
				//echo 'email_priority';
			}else if( is_object($row_email_from = $this->check_procedure_email($email_from)) )  {
				$procedure = $row_email_from;
				//echo 'from';
			}else if( is_object($row_email_to = $this->check_procedure_email($email_to) )  ) {
				$procedure = $row_email_to;
				//echo 'to';
			}else if( is_object($row_email_cc = $this->check_procedure_email($email_cc) ) ) {
				$procedure = $row_email_cc;
				//echo 'cc';
			}


			$reminder_alert_duration = empty($procedure->reminder_alert_duration)?0:@$procedure->reminder_alert_duration;

			$client_id = '';
			$alert_type = 'EMAIL';
			$tran_priority = '';

			$params = array();
			$params['client_id'] = '';
			$params['alert_type'] = 'EMAIL';
			$params['tran_priority'] = '';
			$params['brief_description'] = $data['email_subject'];
			$params['alert_created_dt'] = date('Y-m-d H:i:s', $data['email_udate']);

			$params['alert_due_dt'] = date('Y-m-d H:i:s', strtotime(' + '.$reminder_alert_duration.' MINUTE '));

			if( count($procedure) > 0 ) {
				
				if( !$procedure->display_subject_line ){
					$params['brief_description'] = $procedure->pro_name;					
				}

				$params['client_id'] 		= $procedure->client_id;
				$params['procedure_id'] 	= $procedure->pro_id;
				$params['tran_priority'] 	= $procedure->priority_flag;
				$params['display_screen'] 	= $procedure->display_screen; 
				$params['pro_allow_multi_del']	= @$procedure->allow_multi_del;

				$cma_setting = json_decode($procedure->cma_settings);
				$params['cma_id'] 			= @$cma_setting->cma_id; 
			}


			$json_data['FROM']	= @$data['email_from'];
			$json_data['TO']	= @$data['email_to'];
			$json_data['SUBJECT'] = @$data['email_subject'];			
			$params['json_data'] = json_encode($json_data);

			

			$tran_id = $this->Transactionmodel->insert($params);
			

			//initial auto log
			//cma_settings[auto_log_initial]
			if( @$cma_setting->auto_log_initial == 1 ){

				$new_row = $this->Transactionmodel->row_plain(array('where'=>array('tran_id'=>$tran_id)));

				$cma_set = array();
				$cma_set['audit_type'] 	= 'cma-initial-auto-log';
				$cma_set['tran_id'] 	= $tran_id;
				$cma_set['cma_db'] 		= @$cma_setting->cma_db;
				$cma_set['cust_id'] 	= @$cma_setting->cust_id;
				$cma_set['contact_id'] 	= @$cma_setting->aut_log_contactid; 
				
				$cma_set['agent_id'] 	= '';

				$cma_content = 'Alert #'.@$new_row->ref_number.PHP_EOL.PHP_EOL; 
				$cma_content = (trim(@$data['body_html']) != '')?@$data['body_html']:@$data['body_plain'];

				$cma_set['message'] 	= $cma_content;


 				$this->Commonmodel->insert_cma_call($cma_set);	 


			}


			return $tran_id;
			 

		} catch (Exception $e) {
			
		}


	}

	/**
	 * This function is used to clean email address and wrapped them in a single qoute.
	 * @param  array email addresses
	 * @return array of email address
	 */
	function _incoming_email_clean($incoming_email){

		$emails = array();
		foreach ($incoming_email as $data) {
			if( $data['email'] == 'escalationapp@welldone.com.au') continue;
			if( trim($data['email']) == '' ) continue;

			$emails[] = "'".$data['email']."'";
		}

		return $emails;
	}


	function check_procedure_email($incoming_email){

		$incoming_email = implode(',', array_values($this->_incoming_email_clean($incoming_email)));

		if( trim($incoming_email) == '' ) return false;

		$pro_params = array();
		$pro_params['where'] = array('pro_alert_type'=>'EMAIL', 'pro_status'=>'1');
		$pro_params['where_str'] = " `incoming_email` IN(".$incoming_email.") ";
		$procedure = $this->Proceduremodel->row($pro_params);

		return (count($procedure)>0)?$procedure:false;

	}


	function check_email_priority_condition($from, $to, $cc){

		$from = array_values($from);
		$from = $from[0]['email'];

		$to = array_values($this->_incoming_email_clean($to));
		$cc = array_values($this->_incoming_email_clean($cc));

		$incoming_email = array_merge($to, $cc);
		$incoming_email = implode(',', $incoming_email);

		if( trim($incoming_email) == '' ) return false;

		$this->db->where('email_from_ref', $from);	
		//$this->db->where_in('email_priority', $incoming_email);	
		$this->db->where(" `email_priority` IN(".$incoming_email.") ");	
		$this->db->limit(1);
		$query_res = $this->db->get('incoming_email_condition');
		$row = $query_res->row();

		//echo $this->db->last_query();
		
		//print_r($row);

		if( isset($row->email_priority) AND @$row->email_priority != '' ){

			$new_ar = array(array('email'=>$row->email_priority));

			return $this->check_procedure_email($new_ar);
		}else{
			return false;
		}


	}


	/**
	 * check compare subject to reply-to
	 * @param  array $email
	 * @return stdClass of is_duplicate, tran_id or empty object if there's an error
	 */
	function check_reply_parent($email) {

		$return_object = new stdClass();

		try {
		
			if( strpos($email['email_subject'], 'Alert#') === false ) throw new Exception("Error Processing Request", 1);


			preg_match("/Alert#\d{6,7}/", @$email['email_subject'], $output_array);			
			$alert_match = str_replace('Alert#', '', @$output_array[0]);
			$tran_row = $this->Transactionmodel->row_plain(array('where'=>array('ref_number'=>$alert_match)));
 
			$params = array();
			$params['email_from'] 	= $email['email_from'];
			$params['email_to'] 	= $email['email_to'];
			$params['email_subject'] = $email['email_subject'];
			$params['body_plain'] 	= $email['body_plain'];
			$params['email_udate'] 	= $email['email_udate'];

			$duplicate_record = $this->Emaildatamodel->row(array('where'=>$params));

			if( isset($duplicate_record->tran_id) ){
				
				$return_object->is_duplicate 	= 1;
				$return_object->tran_id 		= @$duplicate_record->tran_id;

			}else if( $alert_match != '' AND @$tran_row->tran_id > 0){

				$return_object->tran_id = @$tran_row->tran_id; 
				
			}else{

				$subject = str_replace(array('Fwd: ', 'Re: '), '', $email['email_subject']);

				//[{"email":"escalationapp@welldone.com.au","name":"EscalationApp"}]
				
				$where_str = " email_subject like '%$subject' ";

				$_where_str = '';
				$tos = json_decode($email['email_to']);

				foreach ($tos as $to) {			
					$_where_str[]= " email_reply_to like '%".$to->email."%' ";
				}

				$params = array();
				$params['where_str'] = $where_str.' AND ('.implode(' OR ', $_where_str).' ) ';
				//$params['where'] = array('email_reply_to'=>$to);
				$record = $this->Emaildatamodel->row($params);
		 		//echo $this->db->last_query();
				$return_object->tran_id = @$record->tran_id;

			}
 

			return $return_object;

		} catch (Exception $e) {
			
			return $return_object;

		}


	}

	/**
	 * This function is use to detect the subject if reply or fowarded
	 * @param  string $email_subject Email Subject
	 * @return string INC-REPLY, FORWARDED, INCOMING
	 */
	function check_email_type($email_subject){

		
		if( preg_grep("/^Re: /", explode("\n", $email_subject)) ){
			return 'INC-REPLY';
		}elseif( preg_grep("/^Fwd: /", explode("\n", $email_subject)) ){
			return 'FORWARDED';
		}else{
			return 'INCOMING';
		}

	}


	function send_email_on_error($data, $tran_id, $error_msg=''){

		try {

			$this->load->library('email');

			$config['protocol'] = 'smtp';
			$config['smtp_host'] = 'mail.welldone.com.au';
			$config['smtp_user'] = 'collection@welldone.com.au';
			$config['smtp_pass'] = 'C0113Ct10n';
			$config['smtp_port'] = '25';
			$config['mailtype'] = 'html';
			$config['wordwrap'] = TRUE;
			$config['charset']  = 'iso-8859-1';
			$config['crlf']     = '\r\n';
			$config['newline']  = '\r\n';

			$this->email->initialize($config);

			$this->email->clear(TRUE);

			$subject = 'ESCALATION INCOMING EMAIL ERROR ID '.$tran_id;

			$message = '<strong>EMAIL ERROR REPORT</strong>';
			$message .= '<br /><br /><br />';
			$message .= $error_msg;
			$message .= '<br /><br /><br />';
			$message .= 'EMAIL CONTENT/ATTACHMENT JSON<br /><br />';
			$message .= json_encode($data);

			$this->email->subject($subject);
			$this->email->message(stripslashes($message));


			$this->email->from('escalationapp@welldone.com.au');
			$this->email->to('carmeloroda.wdi@gmail.com, josh.atlee@welldone.com.au, kellie.johnson@welldone.com.au ');

			$this->email->send(FALSE);

		} catch (Exception $e) {

		}

	}

}
