<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reminder extends CI_Controller {
 	
 	//https://escalation.welldone.net.au/worker/reminder/cron

	public function __construct(){
		
		parent::__construct();

	}

	public function cron(){
		 
		try {
 
			$sql = "
				SELECT
					client_procedure.*,
					DATE_FORMAT(
						CASE reminder_type
							WHEN 'ONCE' THEN CONCAT(reminder_year,'-',reminder_month,'-',reminder_day,' ', reminder_time)
							WHEN 'EVERY_DAY' THEN CONCAT(CURRENT_DATE,' ', reminder_time)
							WHEN 'WEEKLY' THEN 
								IF( DATE_SUB(NOW(), INTERVAL 5 MINUTE) > STR_TO_DATE(CONCAT(YEAR(CURRENT_DATE),WEEK(CURRENT_DATE), ' ',reminder_weekday,' ',reminder_time),'%Y%U %w %H:%i:%s'), 
									CONCAT(DATE_ADD(STR_TO_DATE(CONCAT(YEAR(CURRENT_DATE),WEEK(CURRENT_DATE), ' ',reminder_weekday),'%Y%U %w'), INTERVAL 1 WEEK), ' ',reminder_time),
									STR_TO_DATE(CONCAT(YEAR(CURRENT_DATE),WEEK(CURRENT_DATE), ' ',reminder_weekday,' ',reminder_time),'%Y%U %w %H:%i:%s')				
								)		
							WHEN 'MONTHLY' THEN CONCAT(YEAR(CURRENT_DATE),'-',MONTH(CURRENT_DATE),'-',reminder_day,' ', reminder_time)
							WHEN 'YEARLY' THEN CONCAT(YEAR(CURRENT_DATE),'-',reminder_month,'-',reminder_day,' ', reminder_time)		
							WHEN 'FORTHNIGHTLY' THEN 
								IF( DATE_SUB(NOW(), INTERVAL 5 MINUTE) > DATE_FORMAT(CONCAT(YEAR(CURRENT_DATE),'-',MONTH(CURRENT_DATE),'-',reminder_day,' ', reminder_time), '%Y-%m-%d %H:%i:%s'), 
									DATE_ADD(CONCAT(YEAR(CURRENT_DATE),'-',MONTH(CURRENT_DATE),'-',reminder_day,' ', reminder_time), INTERVAL 2 WEEK),
									CONCAT(YEAR(CURRENT_DATE),'-',MONTH(CURRENT_DATE),'-',reminder_day,' ', reminder_time)				
								)
						END , '%Y-%m-%d %H:%i:%s')AS reminder_dt
					FROM client_procedure
					LEFT OUTER JOIN client ON client.client_id = client_procedure.client_id
					WHERE pro_alert_type = 'REMINDER'
					AND pro_status = 1
					AND `client`.`client_status` = 1
					AND (SELECT reminder_date_run FROM reminder_log WHERE DATE_FORMAT(reminder_date_run, '%Y-%m-%d') = CURRENT_DATE AND procedure_id = pro_id) IS NULL
					AND IF( reminder_until_dt IN(NULL, '0000-00-00 00:00:00'), 1, IF(CURRENT_TIMESTAMP > reminder_until_dt, 0, 1))
					HAVING (reminder_dt BETWEEN DATE_SUB(NOW(), INTERVAL 5 MINUTE) AND DATE_ADD(NOW(), INTERVAL 6 MINUTE))

				";
 
			$query = $this->db->query($sql);
			$reminders = $query->result();

			// echo '<pre>';
			// print_r($reminders);
			// echo '</pre>';

			// exit;

			foreach ($reminders as $reminder) {
				$this->process_transaction($reminder);
				$this->insert_reminder_log($reminder);
			}

			$data['reminders'] = $reminders;

			$this->load->view('test_folder/reminder_cron', $data);

		} catch (Exception $e) {
			echo 'ERROR: '.$e->getMessage();
		}
	}

  

/*client_id
alert_type
brief_description
tran_priority
display_screen
json_data
procedure_id*/


	function process_transaction($procedure){

		$this->load->model('Transactionmodel');
		$this->load->model('Proceduremodel');

		try {
		  
			//$client_id = ''; 
			//$tran_priority = '';

			$params = array();
			//$params['client_id'] = '';
			$params['alert_type'] = 'REMINDER';
			//$params['tran_priority'] = '';
			//$params['brief_description'] = '';
		 
			$params['alert_created_dt'] =  $procedure->reminder_dt;
			$params['alert_due_dt'] 	=  date('Y-m-d H:i:s', strtotime($procedure->reminder_dt.' +'.($procedure->reminder_alert_duration==''?5:$procedure->reminder_alert_duration).' minutes'));
			
			$params['brief_description'] = $procedure->pro_name;
			$params['client_id'] 		= $procedure->client_id;
			$params['procedure_id'] 	= $procedure->pro_id;
			$params['tran_priority'] 	= $procedure->priority_flag;
			$params['display_screen'] 	= $procedure->display_screen; 
			$params['pro_allow_multi_del']	= @$procedure->allow_multi_del;

			$cma_setting = json_decode($procedure->cma_settings);
			$params['cma_id'] 			= @$cma_setting->cma_id; 		 
 
			$json_data['BODY'] = $procedure->reminder_alert_details;			
			$params['json_data'] = json_encode($json_data);
			  
			return $this->Transactionmodel->insert($params);
			 

		} catch (Exception $e) {
			
		}
 
	}

	function insert_reminder_log($procedure){

		$set['client_id'] = $procedure->client_id;
		$set['reminder_date_run'] = $procedure->reminder_dt;
		$set['procedure_id'] = $procedure->pro_id;

		$this->db->insert('reminder_log', $set);
	}


}
