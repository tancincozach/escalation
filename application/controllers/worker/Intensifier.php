<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Intensifier extends CI_Controller {
 	
 	//https://escalation.welldone.net.au/worker/intensifier/insert

	public function __construct(){
		
		parent::__construct();

	}
	
	function insert(){
		
		/*$this->load->model('Transactionmodel');

		try {
			
			$post = $this->input->post(); 

			if( !$_POST ) throw new Exception("Error Processing Request", 1);
			

			$params = array(); 
			$params['alert_type'] = 'CMA REMINDER'; 
			$params['alert_created_dt'] =  date('Y-m-d H:i:s');
			
			if(!isset($post['expiry'])){
				$post['expiry'] = date('U') + 1800;
			}else{
				$params['alert_due_dt'] 	=  date('Y-m-d H:i:s', @$post['expiry']);
			} 

			$params['tran_priority'] 	= 2; //urgent
			$params['display_screen'] 	= 1; //(@$post['tbl_type']=='events')?1:2;
			$params['cust_name'] 		= @$post['cust_name'];
 		 	$params['cma_id'] 			= @$post['cma_id'];
 	
			$json_data['BODY'] 		= @$post['message'];			
			$json_data['name'] 		= @$post['name'];			
			$json_data['phone'] 	= @$post['phone'];			
			$json_data['cust_id'] 	= @$post['cust_id'];

			$params['json_data'] = json_encode($json_data);
			  
			return $this->Transactionmodel->insert($params);			


		} catch (Exception $e) {
			


		}*/

		exit;

	}

	public function cron(){

		$this->load->library("Cma_external");
		$this->load->model('Transactionmodel');

		try {
		
			$this->db->where('id', 1);
			$last_query = $this->db->get('intensifier_retrieve_log');
			$last_row = $last_query->row();

			$records = $this->cma_external->retrieve_intensifier($last_row->last_intensifier_id);


			if( empty($records) ) throw new Exception("Error Processing Request", 1);
			
			$last_intensifier_id = '';
			foreach ($records as $row) { 

				$last_intensifier_id = $row->id;

				$params = array(); 
				$params['intensifier_id'] 	= $row->id;
				$params['alert_type'] 		= 'CMA REMINDER'; 
				$params['alert_created_dt'] =  date('Y-m-d H:i:s', $row->added);
				$params['alert_due_dt'] 	=  date('Y-m-d H:i:s', $row->expiry);
				
				//$params['brief_description'] = $procedure->pro_name; 
				
				//$params['client_id'] = 0;

				$params['tran_priority'] 	= 2; //urgent
				$params['display_screen'] 	= 1; //(@$post['tbl_type']=='events')?1:2;
				$params['cust_name'] 		= $row->cust_name;
	 		 	$params['cma_id'] 			= $row->cma_id;
	 	
				$json_data['BODY'] 		= $row->message;			
				$json_data['name'] 		= $row->name;			
				$json_data['phone'] 	= $row->phone;			
				$json_data['cust_id'] 	= $row->cust_id;
				$json_data['site_id'] 	= @$row->site_id;

				$params['json_data'] = json_encode($json_data);
				  
				$this->Transactionmodel->insert($params);
 

			}


			$log['last_intensifier_id'] = $last_intensifier_id;
			$this->db->where('id', 1);
			$this->db->update('intensifier_retrieve_log', $log);

		} catch (Exception $e) {
			
		}

	}

 
}
