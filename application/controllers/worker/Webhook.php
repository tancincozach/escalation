<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Webhook extends CI_Controller {

	//https://escalation.welldone.net.au/worker/webhook/alert/API_KEY

	public function __construct(){
		
		parent::__construct();

	}
	
	public function alert($api_key=''){
		 
		try {

			if( empty($api_key) ) throw new Exception("Invalid API_KEY", 1);			
 			
 			//check procedure
			$procedure = $this->_verify_procedure($api_key); 
 			if( isset($procedure->error) ){
 				throw new Exception($procedure['error'], 1); 			
 			}


			$method = $this->input->method(TRUE); 
			$request_data = '';
			
			
			if($_POST) {
				//$set['method_type'] = 'POST';
				$request_data = $this->input->post();
			}elseif($_GET) {
				//$set['method_type'] = 'GET';
				$request_data = $this->input->get();
			}else{
				//$set['method_type'] = $method;
				$request_data = $this->input->raw_input_stream;
			}
 			//echo $request_data;
 			
 			$_json_data = json_decode_nice($request_data, true);

 			if( is_string($request_data) && is_array($_json_data) ){

 				$_json_data = (array)$_json_data;

 			}else{

 				if( is_array($request_data ) ){
 					$_json_data = $request_data;
 				}else{

 					$_json_data = (trim($request_data)!='')?$request_data:'';
 					
 				}
 			}
 

			if( count((array)($_json_data)) > 1 ){
    
	 			//$client = $this->_verify_client($procedure->client_id); 
 				//if( count($client) == 0 ) throw new Exception("Invalid client", 1);
	 			 
	 			$set = array();
	 			$json_data = array();

	 			$json_data['body'] = $_json_data;

	 			if( trim($procedure->client_data_src) == '' ){
	 				$set['json_data'] 	= json_encode($json_data);
	 			}

	 			$set['alert_type'] 	= 'API';
	 			$set['client_id'] 	= $procedure->client_id; 
	 			$set['alert_created_dt'] 	= date('Y-m-d H:i:s');
	 			$set['brief_description'] 	= @$procedure->pro_name; 
	 			$set['tran_priority'] 		= @$procedure->priority_flag;
	 			$set['display_screen'] 		= @$procedure->display_screen;	 			
	 			$set['procedure_id'] 		= @$procedure->pro_id; 

	 			$cma_settings = json_decode(@$procedure->cma_settings);
	 			$set['cma_id'] 	= @$cma_settings->cma_id; 
	 			
	 			$tran_id = $this->_insert($set);

				if( is_int($tran_id) AND $tran_id > 0  ){
					
					if( trim($procedure->client_data_src) != '' ){
						$this->_insert_client_data_src($procedure->client_data_src, $tran_id, $_json_data);
					}

					echo 'OK'.PHP_EOL;
					//print_r($json_data);
				}else{
					throw new Exception('Unable to process this request', 1);				
				}

			}else{
				echo 'OK'.PHP_EOL;
			}


		} catch (Exception $e) {
			echo 'ERROR: '.$e->getMessage();
		}
	}

	

	function _verify_client($client_id){

		$api_key = trim($client_id);

		try {

			if( empty($client_id) ) throw new Exception("CLIENT not recognised", 1);
			
			$this->db->where("client_id", $client_id);			
			$this->db->where("client_status", 1);			
			$row = $this->db->get('client')->row();

			if( !isset($row->client_id) ) throw new Exception("CLIENT not recognised", 1);
			
			return $row; 

		} catch (Exception $e) {
			return $e->getMessage();
		}

	}

	function _verify_procedure($api_key){

		$this->load->model('Proceduremodel');

		$api_key = trim($api_key);

		try {

			if( empty($api_key) ) throw new Exception("API KEY not recognised", 1);

			$procedure = $this->Proceduremodel->row(array('where'=>array('pro_alert_type'=>'API', 'api_key'=>$api_key)));
		 
			if( !isset($procedure->pro_id) ) throw new Exception("API KEY not recognised", 1);
			
			return $procedure; 

		} catch (Exception $e) {
			$return = new stdClass();
			$return->error = $e->getMessage();
			return $return;
		}

	}

	function _insert( $set=array() ){

		$this->load->model('Transactionmodel');
		
		try {
		
			if( empty($set) ) throw new Exception("Empty data", 1);
			
			$_set = $set;

			$tran_id = $this->Transactionmodel->insert($_set);

			return $tran_id;


		} catch (Exception $e) {
			return $e->getMessage();
		}
		
	}

	function _insert_client_data_src($table, $tran_id, $json_data ){

        $this->load->dbforge();

        
        $list_fields = $this->db->list_fields($table);


        $fields = array();
        foreach ($list_fields as $field) {
        	$fields[$field] =  $field;
        }

        unset($fields['id']);
        unset($fields['tran_id']);
        unset($fields['created_dt']);
  

		$json_data_field = $json_data; 
        $result_array = array_intersect_key($json_data_field, $fields);
		
 
		
		foreach( $result_array as $key=>$field ){
			//echo $field.'..---..';
			unset($json_data_field[$key]);
		}
 

		//adding new fields if not exist
		$new_table_fields = array();
		foreach ($json_data_field as $new_fld => $value) {
 
        	$new_table_fields[$new_fld] =  array('type' => 'TEXT');							        

		}
		//add new field
		if( count($new_table_fields) > 0 ){
        	$this->dbforge->add_column($table, $new_table_fields);
		}

		//insert to cs_2_alert
        $set = $json_data;
        $set['tran_id'] = $tran_id;
        $this->db->insert($table, $set);
         
	}

}
