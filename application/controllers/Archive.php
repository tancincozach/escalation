<?php

ini_set('max_execution_time', 0);
ini_set('max_input_time',360);
ini_set('memory_limit','1000M');
date_default_timezone_set('UTC'); 


defined('BASEPATH') OR exit('No direct script access allowed');

class Archive extends CI_Controller {


    public $quarter = array();


    public function __construct(){

            parent::__construct();


           $this->quarter[1]['start'] = 1;
           $this->quarter[1]['end']   = 4;

           $this->quarter[2]['start'] = 5;
           $this->quarter[2]['end']   = 8;

           $this->quarter[3]['start'] = 9;
           $this->quarter[3]['end']   = 12;

    }
 
	public function index(){

       $this->archive_email_data();

	}


    public function archive_email_data(){
        
           foreach ($this->quarter as $month){

                $this->archive_data($month);

           }
           
    }

    public function archive_data($quarter = array()){

        try {

            if(empty($quarter) || !isset($quarter['start']) || !isset($quarter['end']))                
              throw new Exception("ERROR : @archive_data , empty parameter : start|end ");

                $table_name = "email_data_".date('Y').$quarter['end'];

                if($quarter['start'] <= intval(date('m'))){

                    $this->create_email_data_archive($table_name);

                    $this->insert_email_data_archive($table_name,$quarter);

                    $this->delete_email_data($quarter);

                }

        } catch (Exception $e) {

            echo $e->getMessage();
            exit();

        }

    }


    public function delete_email_data($quarter=array()){


        try {
            if(empty($quarter) || !isset($quarter['start']) || !isset($quarter['end']))
                throw new Exception("ERROR : @delete_email_data , empty parameter : start | end ");
                            
            $qry = $this->db->query("DELETE FROM email_data where  created_at 
                                BETWEEN STR_TO_DATE('".$quarter['start']."-".date("Y")."' ,'%m-%Y') AND  STR_TO_DATE('".$quarter['end']."-".date("Y")."' ,'%m-%Y')");

            if(!$qry)
                throw new Exception("ERROR: @delete_email_data, unable to delete archived data.");
                

        } catch (Exception $e) {

            echo $e->getMessage();
            exit();

        }

    }

    public function create_email_data_archive($archive_tbl_name=''){        

                try {


                    if(empty($archive_tbl_name))
                            throw new Exception("ERROR : @create_email_data_archive , empty parameter : archive_tbl_name");


                       if(!$this->db->table_exists($archive_tbl_name)){

                            $qry_string = "CREATE TABLE `".$archive_tbl_name."` (                                                                                                     
                                              `archive_id` INT(11) NOT NULL AUTO_INCREMENT,  
                                              `id` INT(11) DEFAULT NULL,                                                                                              
                                              `tran_id` INT(11) DEFAULT NULL,                                                                                               
                                              `msg_id` INT(11) DEFAULT NULL,                                                                                                
                                              `msg_uid` INT(11) DEFAULT NULL,                                                                                               
                                              `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,                                                                    
                                              `email_from` VARCHAR(200) COLLATE utf8_unicode_ci DEFAULT NULL,                                                               
                                              `email_to` TEXT COLLATE utf8_unicode_ci,                                                                                      
                                              `email_cc` TEXT COLLATE utf8_unicode_ci,                                                                                      
                                              `email_bcc` TEXT COLLATE utf8_unicode_ci,                                                                                     
                                              `email_subject` VARCHAR(200) COLLATE utf8_unicode_ci DEFAULT NULL,                                                            
                                              `email_reply_to` VARCHAR(200) COLLATE utf8_unicode_ci DEFAULT NULL,                                                           
                                              `body_plain` TEXT COLLATE utf8_unicode_ci,                                                                                    
                                              `body_html` LONGTEXT COLLATE utf8_unicode_ci,                                                                                 
                                              `msg_udate` INT(11) DEFAULT NULL,                                                                                             
                                              `msg_date` VARCHAR(100) COLLATE utf8_unicode_ci DEFAULT NULL,                                                                 
                                              `email_udate` INT(11) DEFAULT NULL,                                                                                           
                                              `email_type` VARCHAR(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'INCOMING' COMMENT 'INCOMING FORWARDED INC-REPLY REPLYTO',  
                                              `attachements` TEXT COLLATE utf8_unicode_ci,                                                                                  
                                              `attachments_size` INT(11) DEFAULT NULL COMMENT 'in bytes',                                                                   
                                          PRIMARY KEY (`archive_id`),                                                                                                   
                                          KEY `tran_id_Index1` (`tran_id`),                                                                                             
                                          KEY `id_Index1` (`id`)                                                                                           
                                            ) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci      
                                ";

                            if(!$this->db->query($qry_string)){

                                throw new Exception("ERROR : @create_email_data_archive, failure to create archive table : {$archive_tbl_name}");
                            }



                        }   
                            
                    
                } catch (Exception $e) {

                    echo $e->getMessage();
                    exit();
                    
                }
    }

    public function insert_email_data_archive( $archive_tbl_name="",$quarter= array()){

            try {

                if(empty($archive_tbl_name))
                    throw new Exception("ERROR : @insert_email_data_archive , empty parameter : archive_tbl_name");

                if(empty($quarter) || !isset($quarter['start']) || !isset($quarter['end']))
                    throw new Exception("ERROR : @insert_email_data_archive , empty parameter : start | end ");

                

                    $qry_string = "INSERT INTO {$archive_tbl_name}
                                                    (id,
                                                     tran_id,
                                                     msg_id,
                                                     msg_uid,
                                                     created_at,
                                                     email_from,
                                                     email_to,
                                                     email_cc,
                                                     email_bcc,
                                                     email_subject,
                                                     email_reply_to,
                                                     body_plain,
                                                     body_html,
                                                     msg_udate,
                                                     email_type,
                                                     attachements,
                                                     attachments_size)
                                        SELECT
                                          id,
                                          tran_id,
                                          msg_id,
                                          msg_uid,
                                          created_at,
                                          email_from,
                                          email_to,
                                          email_cc,
                                          email_bcc,
                                          email_subject,
                                          email_reply_to,
                                          body_plain,
                                          body_html,
                                          msg_udate,
                                          email_type,
                                          attachements,
                                          attachments_size
                                        FROM email_data
                                        WHERE created_at 
                                    BETWEEN STR_TO_DATE('".$quarter['start']."-".date("Y")."' ,'%m-%Y') AND  STR_TO_DATE('".$quarter['end']."-".date("Y")."' ,'%m-%Y')";

                    if(!$this->db->query($qry_string)){

                        throw new Exception("ERROR : @insert_email_data_archive , failure to insert data on archive table : {$archive_tbl_name}");
                        
                    }                    
                                                
            } catch (Exception $e) {
                
                 echo $e->getMessage();
                exit();

            }

    }


}
