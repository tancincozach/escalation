<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Callactivity extends MY_Controller {

	public function __construct(){
		
		parent::__construct();

		$this->load->model('Clientmodel');
		$this->load->model('Transactionmodel');
		$this->load->model('Callsmodel');
		$this->load->model('Callresponsemodel');
		$this->load->model('Commonmodel');		
	}

	public function open($tran_id = ''){
		
		$this->load->model('Proceduremodel');
		$this->load->model('Audittrailmodel');
		$this->load->model('CRLookupmodel');
		$this->load->model('Emaildatamodel');
		$this->load->model('Clientcustomtable');

		$data = '';

		if( $_POST ){


			$this->call_submit($tran_id);

		}
		
		$record_params['where']['tran_id'] = $tran_id;
		$record = $this->Transactionmodel->row($record_params);
		//echo $this->db->last_query();

		$client_params['where']['client_id'] = $record->client_id;
		$data['client'] = $this->Clientmodel->row($client_params);
		//echo $this->db->last_query();

		$calls_params['where']['tran_id'] = $tran_id;
		$data['calls'] = $this->Callsmodel->listing($calls_params,false);

		//print_r($data['calls']); 

		/*$procedure_params['where'] = array(
						'pro_alert_type'=>$record->alert_type, 
						'client_procedure.client_id'=>$record->client_id
					);*/

		$procedure_params['where'] = array(
						'pro_id'=>$record->procedure_id
					);

		$procedure = $this->Proceduremodel->row($procedure_params);
		$data['procedure'] = $procedure;		
		$data['cma_settings'] = json_decode(@$procedure->cma_settings);

		//echo $procedure->call_group;
		if( isset($procedure->pro_id) ){

			if( isset($_GET['call_form_required']) ){

				//$callresponse_params['where']['call_group'] = @$procedure->call_group;
				$callresponse_params['where']['call_res_status'] = 1;
				//$callresponse_params['where_str'] = '(call_responses.client_id='.$record->client_id.' AND call_form_required=1)';
				$callresponse_params['where_str'] = '(call_responses.client_id='.$record->client_id.')';
				$callresponse_params['sorting'] = ' call_res_order ASC ';
				$data['callresponse'] = $this->Callresponsemodel->listing($callresponse_params, false);


			}elseif( !is_numeric($procedure->call_group) ){			 
				$callresponse_params['where']['call_group'] = @$procedure->call_group;
				$callresponse_params['where']['call_res_status'] = 1;
				$callresponse_params['where_str'] = '(call_responses.client_id='.$record->client_id.' AND call_form_required=1)';
				$callresponse_params['sorting'] = ' call_res_order ASC ';
				$data['callresponse'] = $this->Callresponsemodel->listing($callresponse_params, false);

			}else{
				//display only call submit & close button
				$callresponse_params['where']['call_res_id'] = @$procedure->call_group;
				$data['callresponse'] = $this->Callresponsemodel->row($callresponse_params, false);
			}
		}

		$data['record'] = $record;


		//$data['alert_details_content'] =  $this->load->view('pages/callactivity/alert-details', $data, TRUE); 
		$data['alert_details_content'] =  $this->alert_details($tran_id, array('record'=>$record, 'client'=>$data['client'], 'procedure'=>$procedure));
 
		$data['audittrail'] = $this->Audittrailmodel->get_results(array('where'=>array('tran_id'=>$tran_id)));
		$data['call_lookup'] = $this->CRLookupmodel->row(array('where'=>array('call_lookup_id'=>@$procedure->call_lookup_id)));


		$this->view_data['data'] = $data;
		$this->view_data['menu_active'] = 'register';
		$this->view_data['view_file'] = 'pages/callactivity/callactivity';

		$this->view_data['js_file'] = '<script type="text/javascript" src="assets/vendor/tinymce/js/tinymce/tinymce.min.js"></script>'.PHP_EOL;
		$this->view_data['js_file'] .= '<script type="text/javascript" src="assets/vendor/tinymce.filemanager/plugin.min.js"></script>'.PHP_EOL;

		$this->view_data['js_file'] .= '<script src="assets/js/callactiviy.js?v='.JS_VER_CALLACTIVITY.'"></script>'; 
		
		$this->load->view('template', $this->view_data); 

	}

	public function view($tran_id = ''){
		
		$this->load->model('Proceduremodel');
		$this->load->model('Audittrailmodel');
		$this->load->model('CRLookupmodel');
		$this->load->model('Emaildatamodel');
		$this->load->model('Clientcustomtable');

		$data = '';
 
		$record_params['where']['tran_id'] = $tran_id;
		$record = $this->Transactionmodel->row($record_params);
		//echo $this->db->last_query();

		$client_params['where']['client_id'] = $record->client_id;
		$data['client'] = $this->Clientmodel->row($client_params);
		//echo $this->db->last_query();

		$calls_params['where']['tran_id'] = $tran_id;
		$data['calls'] = $this->Callsmodel->listing($calls_params,false);

		//print_r($data['calls']); 

		/*$procedure_params['where'] = array(
						'pro_alert_type'=>$record->alert_type, 
						'client_procedure.client_id'=>$record->client_id
					);*/

		$procedure_params['where'] = array(
						'pro_id'=>$record->procedure_id
					);

		$procedure = $this->Proceduremodel->row($procedure_params);
		$data['procedure'] = $procedure;		
		$data['cma_settings'] = json_decode(@$procedure->cma_settings);

		//echo $procedure->call_group;
		if( isset($procedure->pro_id) ){

			if( isset($_GET['call_form_required']) ){

				//$callresponse_params['where']['call_group'] = @$procedure->call_group;
				$callresponse_params['where']['call_res_status'] = 1;
				//$callresponse_params['where_str'] = '(call_responses.client_id='.$record->client_id.' AND call_form_required=1)';
				$callresponse_params['where_str'] = '(call_responses.client_id='.$record->client_id.')';
				$callresponse_params['sorting'] = ' call_res_order ASC ';
				$data['callresponse'] = $this->Callresponsemodel->listing($callresponse_params, false);


			}elseif( !is_numeric($procedure->call_group) ){			 
				$callresponse_params['where']['call_group'] = @$procedure->call_group;
				$callresponse_params['where']['call_res_status'] = 1;
				$callresponse_params['where_str'] = '(call_responses.client_id='.$record->client_id.' AND call_form_required=1)';
				$callresponse_params['sorting'] = ' call_res_order ASC ';
				$data['callresponse'] = $this->Callresponsemodel->listing($callresponse_params, false);

			}else{
				//display only call submit & close button
				$callresponse_params['where']['call_res_id'] = @$procedure->call_group;
				$data['callresponse'] = $this->Callresponsemodel->row($callresponse_params, false);
			}
		}

		$data['record'] = $record;


		//$data['alert_details_content'] =  $this->load->view('pages/callactivity/alert-details', $data, TRUE); 
		$data['alert_details_content'] =  $this->alert_details($tran_id, array('record'=>$record, 'client'=>$data['client'], 'procedure'=>$procedure));
 
		$data['audittrail'] = $this->Audittrailmodel->get_results(array('where'=>array('tran_id'=>$tran_id)));
		$data['call_lookup'] = $this->CRLookupmodel->row(array('where'=>array('call_lookup_id'=>@$procedure->call_lookup_id)));

		$data['is_view'] = '1';

		$this->view_data['data'] = $data;
		$this->view_data['menu_active'] = 'register';
		$this->view_data['view_file'] = 'pages/callactivity/callactivity';

		$this->view_data['js_file'] = '<script type="text/javascript" src="assets/vendor/tinymce/js/tinymce/tinymce.min.js"></script>'.PHP_EOL;
		$this->view_data['js_file'] .= '<script type="text/javascript" src="assets/vendor/tinymce.filemanager/plugin.min.js"></script>'.PHP_EOL;

		$this->view_data['js_file'] .= '<script src="assets/js/callactiviy.js?v='.JS_VER_CALLACTIVITY.'"></script>'; 
		
		$this->load->view('template', $this->view_data); 

	}

	function open_cma_reminder($tran_id = ''){

		$this->load->model('Audittrailmodel');

		try {

			$data = '';

			if( $_POST ){

				$post 		= $this->input->post();
				$tran_id 	= $post['tran_id'];
				$ref_number = $post['ref_number'];
				$cma_id 	= $post['cma_id'];
				$agent_name = $this->agent_name;

				$tran_set = array();
				$tran_set['tran_status'] = 0;
				$tran_set['tran_updated'] = date('Y-m-d H:i:s'); 
				$tran_set['call_hand_end'] 	= date('Y-m-d H:i:s');

				if( $this->Transactionmodel->update($tran_id, $tran_set) ){
				  	

				  	//Audit TRAIL
					$audit_trail_params['tran_id'] = $tran_id;
					$audit_trail_params['message'] = 'Alert ID: '.$ref_number.' ACK';
					$audit_trail_params['more_info'] = json_encode(array('agent_name'=>$agent_name));
					$audit_trail_params['audit_type'] = 'cma-reminder-ack';
					$audit_trail_params['created'] = strtotime('now');
					$this->db->insert('audit_trail', $audit_trail_params);


					//AGENT STATUS TO CMA

		 			$_as_r = array();
		 			$_as_r['tran_id'] 	= $tran_id;
		 			$_as_r['agent_id'] 	= $this->agent_id;
		 			$_as_r['cma_id'] 	= @$cma_id;
		 			$_as_r['reason'] 	= 'Escalation Duty';
		 			$this->Commonmodel->curl_agentstatus('agentStatusUpdate',$_as_r);


					$this->session->set_flashdata('fmesg', 'Alert ID: <a href="callactivity/open_cma_reminder/'.$tran_id.'">'.$ref_number.'</a> ACK');	
					redirect('register');

				}else{
					$this->session->set_flashdata('fmesg', 'Alert ID: <a href="callactivity/open_cma_reminder/'.$tran_id.'">'.$ref_number.'</a> failed to ACK');	
					redirect('register');
				}


			}

			

			$record_params['where']['tran_id'] = $tran_id;
			$record = $this->Transactionmodel->row_plain($record_params);

			if( $record->alert_type != 'CMA REMINDER' ) throw new Exception("Alert ID ".$record->ref_number." is not a CMA Reminder", 1);


			$data['record'] = $record;
			
			$data['audittrail'] = $this->Audittrailmodel->get_results(array('where'=>array('tran_id'=>$tran_id)));

			$this->view_data['data'] = $data;
			$this->view_data['menu_active'] = 'cma_reminder';
			$this->view_data['view_file'] = 'pages/callactivity/cma-reminder-ack';

			//$this->view_data['js_file'] = '<script type="text/javascript" src="assets/vendor/tinymce/js/tinymce/tinymce.min.js"></script>'.PHP_EOL;
			//$this->view_data['js_file'] .= '<script type="text/javascript" src="assets/vendor/tinymce.filemanager/plugin.min.js"></script>'.PHP_EOL;

			//$this->view_data['js_file'] .= '<script src="assets/js/callactiviy.js?v='.JS_VER_CALLACTIVITY.'"></script>'; 
			
			$this->view_data['js_file'] .= '<script src="assets/js/callactiviy.js?v='.JS_VER_CALLACTIVITY.'"></script>'; 

			$this->load->view('template', $this->view_data); 

		} catch (Exception $e) {
			
			$this->session->set_flashdata('fmesg', $e->getMessage());	
			redirect('register');

		}

	}

	function open_cma_reminder_view($tran_id = ''){

		$this->load->model('Audittrailmodel');

		try {

			$data = '';

			  
			$record_params['where']['tran_id'] = $tran_id;
			$record = $this->Transactionmodel->row_plain($record_params);

			if( $record->alert_type != 'CMA REMINDER' ) throw new Exception("Alert ID ".$record->ref_number." is not a CMA Reminder", 1);


			$data['record'] = $record;
			
			$data['audittrail'] = $this->Audittrailmodel->get_results(array('where'=>array('tran_id'=>$tran_id)));
			
			$data['is_view'] = '1';

			$this->view_data['data'] = $data;
			$this->view_data['menu_active'] = 'cma_reminder';
			$this->view_data['view_file'] = 'pages/callactivity/cma-reminder-ack';

			//$this->view_data['js_file'] = '<script type="text/javascript" src="assets/vendor/tinymce/js/tinymce/tinymce.min.js"></script>'.PHP_EOL;
			//$this->view_data['js_file'] .= '<script type="text/javascript" src="assets/vendor/tinymce.filemanager/plugin.min.js"></script>'.PHP_EOL;

			$this->view_data['js_file'] .= '<script src="assets/js/callactiviy.js?v='.JS_VER_CALLACTIVITY.'"></script>'; 
			
			$this->load->view('template', $this->view_data); 

		} catch (Exception $e) {
			
			$this->session->set_flashdata('fmesg', $e->getMessage());	
			redirect('register');

		}

	}


	function alert_details($tran_id, $params=''){

		$this->load->model('Emaildatamodel');
		$this->load->model('Clientcustomtable');
		$this->load->model('Proceduremodel');

		try {
			
			$data = '';

			unset($params['callresponse']);
			unset($params['cma_settings']);
			//unset($params['procedure']);
			unset($params['calls']);

			$data = $params;

			if( empty($params['record']) ){			
				$record_params['where']['tran_id'] = $tran_id;
				$data['record'] = $this->Transactionmodel->row($record_params);				 
			}

			$record = $data['record'];

			if(empty($params['client']) ){

				$client_params['where']['client_id'] = $record->client_id;
				$data['client'] = $this->Clientmodel->row($client_params);
			} 

			//$this->Commonmodel->array_print_to_html($data);

			$client = $data['client'];
			$procedure = @$data['procedure'];

			if( !isset($data['procedure']) ){

				$procedure_params['where'] = array(
					'pro_id'=>$record->procedure_id
				);				
				$procedure = $this->Proceduremodel->row( $procedure_params );	
			} 


			switch ($record->alert_type) {
				case 'EMAIL':
					//GRAB email data on table email_data 

					$email_data_where['where'] = array('tran_id'=>$record->tran_id);
					$email_data_where['sorting']['sort'] 	= 'created_at';
					$email_data_where['sorting']['order'] 	= 'asc';

					$email_data = $this->Emaildatamodel->get_result( $email_data_where );
					
	 
					$alert_detail = $email_data;


					break; 
				default:
						if( @$procedure->client_data_src == '' ){
							
							$alert_detail = (object)json_decode($record->json_data);
							$alert_detail->RECEIVE_DT = date('d/m/Y H:i', strtotime($record->tran_created));

						}else{

							//$client_data_source = $this->db->get_where($client->client_data_src, array('tran_id'=>$record->tran_id))->result_array();			 				 

			 				$client_data_source = (Array)$this->Clientcustomtable->get_table_data( $procedure->client_data_src, array('where'=>array('tran_id'=>$record->tran_id)), 'result_array' );
 
							$obj = new stdClass();
							$obj->BODY = (object)$client_data_source;
							//$obj->RECEIVE_DT = date('d/m/Y H:i', strtotime($client_data_source['created_dt']));
							$alert_detail = $obj;
							
						}
					break;
			}

			$data['alert_detail'] = $alert_detail;

			return $this->load->view('pages/callactivity/alert-details', $data, TRUE); 

		} catch (Exception $e) {
			
		}

	}

	public function iframe_email_body($email_id=''){
		$this->load->model('Emaildatamodel');

		$email_data_where['where'] = array('id'=>$email_id); 

		$email_data = $this->Emaildatamodel->row( $email_data_where );

		$data['email_body'] = ( empty($email_data->body_html) ) ? $email_data->body_plain: '<pre>'.$email_data->body_html.'</pre>';
		echo $this->load->view('pages/callactivity/alert-details-email-inframe', $data, TRUE);		

	}

	public function download_attachment(){
		$this->load->helper('download');

		try {
			
			$get = $this->input->get();

			if( !isset($get['file']) ) throw new Exception("Error Processing Request", 1);

			force_download(EMAIL_ATTACH_LOC.$get['file'], NULL);	

		} catch (Exception $e) {
			echo $e->getMessage();
		}

	}


	function call_submit($tran_id){

		//$this->load->model('Clientcustomtable');
		//$this->load->model('Emaildatamodel');


		$redirect_link = 'callactivity/open/'.$tran_id;

		try {
			

			$post = $this->input->post();

			if( !isset($post['tran_id']) )  throw new Exception("Error: Log not recognised", 1);
			if( !isset($post['call_res_id']) )  throw new Exception("Error: Call Response not recognised", 1);
			
			$tran_id = $post['tran_id'];
			$call_res_id = $post['call_res_id'];
			$ref_number = $post['ref_number'];
			$procedure_id = $post['procedure_id'];
			$caller_name = addslashes(@$post['caller_name']);
			$caller_phone = addslashes(@$post['caller_phone']);
			$call_notes = addslashes(@$post['call_notes']);
			$call_direction = addslashes(@$post['call_direction']);


			$lookup_emailoutput = json_decode(stripslashes(@$post['email_output']));
			$lookup_toemail = @$post['to_email'];


			/*
			$this->Commonmodel->array_print_to_html($lookup_emailoutput);
			//$this->Commonmodel->array_print_to_html($to_email);
			echo $lookup_toemail;
			echo '<br />';
			echo 'show_ad: '.$lookup_emailoutput->show_ad;
			echo '<br />';
			echo 'show_ca: '.$lookup_emailoutput->show_ca;
			echo '<br />';
			echo 'email_every_ca: '.$lookup_emailoutput->email_every_ca;

			exit;*/

			$callresponse = $this->Callresponsemodel->row( array('where'=>array('call_res_id'=>$call_res_id)) );


			$procedure_params['where'] = array(
							'pro_id'=>$procedure_id
						);
			$procedure = $this->Proceduremodel->row($procedure_params);
			$other_option = (object)json_decode($procedure->other_options);		 
 
			$set = array();

			$set['tran_id'] = $tran_id;
			$set['call_res_id'] 	= $call_res_id;

			if( $call_direction != '' )
				$set['call_direction'] 	= $call_direction;

			$set['caller_name'] 	= $caller_name;
			$set['caller_phone'] 	= $caller_phone;
			$set['call_notes'] 		= $call_notes;
			$set['log_status'] 		= $callresponse->call_res_open;
			$set['agent_name'] 		= $this->agent_name;
			$set['user_id'] 		= $this->user_id;

			$call_id = $this->Callsmodel->insert($set);
			
			if( !$call_id ) throw new Exception("Error Processing cll, something went wrong on the form", 1);
			
			$record_params['where']['tran_id'] = $tran_id;
			$record = $this->Transactionmodel->row($record_params);

			$flash_msg[] = 'Log # <a href="callactivity/open/'.$tran_id.'">'.$ref_number.'</a>';

			//transaction log updates
			//if callresponse: all_res_open 0 or 1 (close, open)
			//	transaction log need to be updated
			if( in_array($callresponse->call_res_open, array(0,1))){

				$tran_set['tran_updated'] = date('Y-m-d H:i:s');
				$tran_set['tran_status'] = $callresponse->call_res_open;

				if( $callresponse->call_res_open == 0 AND $record->call_hand_end == '' )
					$tran_set['call_hand_end'] 	= date('Y-m-d H:i:s');
				
				if( $this->Transactionmodel->update($tran_id, $tran_set) ){
					$flash_msg[] = 'Log status: '. $this->tran_status_desc[$callresponse->call_res_open];
				}
 
				$message = ' LOG #: '.$ref_number.'<br /><br />';
				
				$ca_text = ''; 
		 		$ca_text .= 'Call Direction: '.@$this->call_direction_desc[$call_direction].'<br />';
		 		$ca_text .= 'Caller Name: '.$caller_name.'<br />';
		 		$ca_text .= 'Caller Phone: '.$caller_phone.'<br />'; 
		 		$ca_text .= 'Notes: '.$call_notes.'<br />';

		 		//$message .= $ca_text;

				$cma_set['audit_type'] 	= 'call';
				$cma_set['tran_id'] 	= $post['tran_id'];
				$cma_set['cma_db'] 		= $post['cma_db'];
				$cma_set['cust_id'] 	= $post['cust_id'];
				$cma_set['contact_id'] 	= $post['contact_id_callsmade'];

				if( $call_direction == '0' ){
					$cma_set['ob1_name'] 	= $caller_name;
					$cma_set['ob1_phone'] 	= $caller_phone;
				}
				
				$cma_set['message'] 	= $message.$ca_text;
				$cma_set['agent_id'] 	= $this->agent_id;

				$insert_cma = $this->Commonmodel->insert_cma_call($cma_set);
				$flash_msg[] = $insert_cma;
				
				//retreived all the call activity
				$call_listing_text = '';
				$call_listing = $this->Callsmodel->listing(array('where'=>array('tran_id'=>$tran_id)), FALSE);
				foreach ($call_listing as $call) {
					$call_listing_text .= date('d/m/Y H:i', strtotime($call->call_created))
										.' '.$this->call_direction_desc[$call->call_direction]
										.' '.stripslashes($call->caller_name)
										.' '.stripslashes($call->caller_phone)
										.' '.stripslashes($call->call_notes).'<br />';
				}


				//alert details data
				$_alert_details =  @$this->alert_details($tran_id, array('record'=>$record));

				$email_subject = $procedure->pro_name.' Alert#'.$record->ref_number;

				$email_option = $other_option->email;
				//EACH CALL
				if( @$email_option->each_call->is_send AND trim(@$email_option->each_call->email_addrs) != '' ){
					
					$email_param = array();
					$email_body = $message; 

					//EMAIL OUTPUT from procedure 
					if( @$other_option->email_output->show_ad=='1' ){
						 
						$email_body .= '<strong>Alert Details</strong>';
						$email_body .= '<br /><br />';
						//$email_body .=  $this->alert_details($tran_id, array('record'=>$record));
						$email_body .=  $_alert_details;


					}

					if( @$other_option->email_outputs->show_ca !='0' ){

						$email_body .= '<strong>Call Activity</strong>';
						$email_body .= '<br /><br />';
						//$email_body .= $ca_text;
						$email_body .= $call_listing_text;

					}
 

					$email_param['tran_id'] = $tran_id;
					$email_param['subject'] = $email_subject;
					$email_param['to']		= $email_option->each_call->email_addrs;
					$email_param['message']	= $email_body;
					$this->Commonmodel->send_email($email_param);
				}

				//ON CLOSE
				if( !$callresponse->call_res_open AND @$email_option->on_close->is_send AND trim(@$email_option->on_close->email_addrs) != '' ){
					$email_param = array();
					$email_body = $message; 

					//EMAIL OUTPUT from procedure 
					if( @$other_option->email_output->show_ad=='1' ){
						 
						$email_body .= '<strong>Alert Details</strong>';
						$email_body .= '<br /><br />';
						//$email_body .=  $this->alert_details($tran_id, array('record'=>$record));
						$email_body .=  $_alert_details;


					}

					if( @$other_option->email_output->show_ca !='0' ){

						$email_body .= '<strong>Call Activity</strong>';
						$email_body .= '<br /><br />';
						//$email_body .= $ca_text;
						$email_body .= $call_listing_text;
					}


					$email_param['tran_id'] = $tran_id;
					$email_param['subject'] = $email_subject;
					$email_param['to']		= $email_option->on_close->email_addrs;
					$email_param['message']	= $email_body;
					$this->Commonmodel->send_email($email_param);
				}



				//LOOKUP
				// stdClass Object
				// (
				//     [show_ad] => 1
				//     [show_ca] => 1
				//     [email_every_ca] => 0
				// ) 

				if(  @$lookup_emailoutput->email_every_ca || (@!$lookup_emailoutput->email_every_ca AND  !$callresponse->call_res_open ) ){

					$email_param = array();
					$email_param['tran_id'] = $tran_id;
					$email_param['subject'] = $email_subject;
					$email_param['to']		= trim($lookup_toemail);
					
					$email_body = '';
					$email_body .= ' LOG #: '.$ref_number.'<br /><br />';

					if( @$lookup_emailoutput->show_ad=='1' ){
						 
						$email_body .= '<strong>Alert Details</strong>';
						$email_body .= '<br /><br />';
						//$email_body .=  $this->alert_details($tran_id, array('record'=>$record));
						$email_body .=  $_alert_details;


					}

					if( @$lookup_emailoutput->show_ca=='1' ){

						$email_body .= '<strong>Call Activity</strong>';
						$email_body .= '<br /><br />';
						//$email_body .= $ca_text;
						$email_body .= $call_listing_text;

					}

					$email_param['message']	= $email_body;

					//$this->Commonmodel->array_print_to_html($email_param);

					if( !empty($email_param['to']) )
						$this->Commonmodel->send_email($email_param);

				} 


				if( $post['is_lookup_not_found'] == 'No Result Found' ){


					$email_param = array();
					$email_param['tran_id'] = $tran_id;
					$email_param['subject'] = '[Escalation System] No Result found  [Lookup] Alert #'.$record->ref_number;
					$email_param['to']		= $post['email_not_found'];
					
					$email_body = 'Alert #'.$record->ref_number.'<br />';
					$email_body .= 'Client Name: '.$ref_number.'<br />';
					$email_body .= 'CMA ID: '.$ref_number.'<br /><br />';
					$email_body .= 'No Result found using the Lookup feature ('.$post['set_driver_btn_name'].')';
					
 
					$email_param['message']	= $email_body;
					$this->Commonmodel->send_email($email_param);

				}

                $_as_r = array(); 
                $_as_r['tran_id']   = $tran_id;
                $_as_r['agent_id']  = $this->agent_id;
                $_as_r['cma_id']    = @$post['cma_id'];
                $_as_r['reason']    = ($callresponse->call_res_open == 0)?'Escalation Duty':'Escalation Followup';
                $this->Commonmodel->curl_agentstatus('agentStatusUpdate',$_as_r);                

			}

			$this->session->set_flashdata('fmesg', implode('<br />', $flash_msg));	


			//open stays on callactivity
			if( $callresponse->call_res_open ){
				redirect($redirect_link);				
			}else{
				redirect('register');
			}
			


		} catch (Exception $e) {
			//echo $this->$e->getMessage();
			$this->session->set_flashdata('fmesg', $this->$e->getMessage());	
			
			redirect($redirect_link);
		}

	}


	/**
	 *  Request GET call via callactivity.js email_reply.open_modal(type, tran_id)
	 * @param  string $email_action_type  [reply, forward]
	 * @param  GET [tran_id]
	 * @return html modal form
	 */
	public function ajax_reply_modal($email_action_type='reply'){

		$status_array = array('status' => 1 , 'body'=>'' );

		try {
			
			$this->load->model('Emaildatamodel');

			$get = $this->input->get();
			
			$data = '';

			$data['email_action_type'] = $email_action_type;

			$tran_id = $get['tran_id'];



			$record_params['where']['tran_id'] = $tran_id;
			$record = $this->Transactionmodel->row($record_params);


			$email_data_where['where'] = array('tran_id'=>$tran_id);

			$email_data = $this->Emaildatamodel->row( $email_data_where );

			$record->json_data = (object)json_decode($record->json_data);


			$record->json_data->BODY = @$email_data->body_html;				
			if( empty($email_data->body_html)){
				$record->json_data->BODY = str_replace(PHP_EOL, '<br />', @$email_data->body_plain);
			}


			$record->json_data->FROM = json_decode($record->json_data->FROM);
			$record->json_data->TO = json_decode($record->json_data->TO);
			
			if( $email_action_type == 'reply' ){

				if( !preg_grep("/^Re: /", explode("\n", $record->json_data->SUBJECT)) ){
					$record->json_data->SUBJECT = str_replace('Fwd: ', '', $record->json_data->SUBJECT);
					$record->json_data->SUBJECT = 'Re: '.$record->json_data->SUBJECT;
				}				
			}elseif( $email_action_type == 'forward' ){
				if( !preg_grep("/^Fwd: /", explode("\n", $record->json_data->SUBJECT)) ){
					$record->json_data->SUBJECT = str_replace('Re: ', '', $record->json_data->SUBJECT);
					$record->json_data->SUBJECT = 'Fwd: '.$record->json_data->SUBJECT;
				}
			}


			if( !strpos($record->json_data->SUBJECT, 'Alert#') ){
				$record->json_data->SUBJECT = $record->json_data->SUBJECT.' Alert#'.$record->ref_number;
			}


			$data['record'] = $record;
			$data['alert'] = $record->json_data;

			// echo '<pre>';
			// print_r($record->json_data);
			// echo '</pre>';

			//echo $this->load->view('pages/callactivity/modalform-email-replyforward', $data, TRUE); 

			$status_array['body'] = $this->load->view('pages/callactivity/modalform-email-replyforward', $data, TRUE); 

			echo json_encode($status_array);

		} catch (Exception $e) {
			
			$status_array['status'] = 0;
			$status_array['body'] 	= $e->getMessage();

			echo json_encode($status_array);			
		}

	}


	/**
	 * Send email [reply, forward]	 
	 * @param  POST [tran_id]
	 * @return void redirect to current URL
	 */
	public function ajax_replyfoward_send(){

		$flash_msg = array();

		try {
			 
			$this->load->model('Emaildatamodel');

			$post = $this->input->post();
			$tran_id = $post['tran_id'];

			$email_param = array();
			$email_param['subject'] 	= $post['reply_subject'];
			$email_param['to']			= $post['reply_to'];
			$email_param['reply_to']	= $post['reply_from'];
			$email_param['from']		= $post['reply_from'];
			$email_param['message']		= $post['reply_body'];

			$email_param['tran_id']		= $tran_id;
			$email_param['audit_type']	= 'email-'.$post['email_action_type'];
			$email_param['agent_name']	= $this->agent_name;
			
			$_send_email = $this->Commonmodel->send_email($email_param);
			if( in_array($_send_email, array('message_sent','message_sent - localhost')) ){
				$flash_msg[] = 'Email successfully sent';
			}else{
				$flash_msg[] = 'Failed to sent email';
			}

			$set = array();
			$set['email_type'] = ($post['email_action_type'] == 'reply')?'REPLYTO':'FORWARDED';
			$set['email_from'] 		= json_encode(array('email'=>$post['reply_from'], 'name'=>''));
			$set['email_reply_to'] 	= json_encode(array('email'=>$post['reply_from'], 'name'=>''));
			
			$reply_to = str_replace(';', ',', $post['reply_to']);
			$tos = explode(',', $reply_to);
			$to_arr = array();
			foreach($tos as $to){
				$new_to = array();
				$new_to['email'] = trim($to);
				$new_to['name'] = '';
				$to_arr[] = $new_to;
			}

			$set['email_to'] 		= json_encode($to_arr);
			$set['email_subject'] 	= addslashes($post['reply_subject']);
			$set['body_html'] 		= addslashes($post['reply_body']);
			$set['email_udate'] 	= strtotime('now');
			$set['tran_id']			= $tran_id;
			
			if( $this->Emaildatamodel->insert($set) ){

				$flash_msg[] = 'Successfully insert to Email Data';

				//Updated Thread Count
				$update_tran = array();
				$update_tran['thread_count'] 	= ($this->Transactionmodel->get_thread_count($tran_id)) + 1;
				$this->Transactionmodel->update($tran_id, $update_tran);

			}else{
				$flash_msg[] = 'Failed to save email data';
			}

			$this->session->set_flashdata('fmesg', implode('<br />', $flash_msg));	
			redirect('callactivity/open/'.$tran_id);

		} catch (Exception $e) {
			
		}
	}


	/**
	 * Request GET call via callactivity.js set_procedure.open_modal(alert_type, tran_id)
	 * Opens a modal form to set procedure on allowed Alert Type
	 * @param  string $alert_type
	 * @param  GET [tran_id]
	 * @return json object [status, body]
	 */
	public function ajax_set_procedure_modal($alert_type=''){

		$this->load->model('Proceduremodel');

		$status_array = array('status' => 1 , 'body'=>'' );

		try {
			
			$get = $this->input->get();

			if( !isset($_GET) ) throw new Exception("Error: Invalid allowed method type", 1);			
			if( !isset($get['tran_id']) OR @$get['tran_id'] == '' ) throw new Exception("Error: tran_id is not set", 1);
			if( @$alert_type == '' ) throw new Exception("Error: alert_type is not set", 1);
			if(  !in_array($alert_type, array('EMAIL'))) throw new Exception('Updating an ALERT '.$alert_type.' is not yet available', 1);
			

			$tran_id = $get['tran_id'];

			$record_params = array();
			$record_params['where']['tran_id'] = $tran_id;
			$record = $this->Transactionmodel->row($record_params);
 
			$data['record'] = $record;


			$pro_params['where']['client_procedure.pro_status'] = 1;
			$pro_params['where']['pro_alert_type'] = $alert_type;
			$pro_params['sorting'] = 'client_name ASC, pro_name ASC';
			$data['procedures'] = $this->Proceduremodel->listing($pro_params, false);

 
			$status_array['body'] = $this->load->view('pages/callactivity/modalform-set-procedure', $data, TRUE); 

			echo json_encode($status_array);

		} catch (Exception $e) {
			$status_array['status'] = 0;
			$status_array['body'] = $e->getMessage();

			echo json_encode($status_array);
		}

	}

	public function ajax_set_procedure_save(){

		$this->load->model('Proceduremodel');

		try {
			
			if( !isset($_POST) ) throw new Exception("Error: Invalid allowed method", 1);
			
			$post = $this->input->post();
			
			if( !isset($post['tran_id']) OR @$post['tran_id'] == '' ) throw new Exception("Error: tran_id is not set", 1);

			$tran_id = $post['tran_id'];
			$pro_id = $post['pro_id'];

			$pro_param = array();
			$pro_param['where']['pro_id'] = $pro_id;
			$procedure = $this->Proceduremodel->row($pro_param);

			$cma_settings = json_decode($procedure->cma_settings);
			//$this->Commonmodel->array_print_to_html($procedure);
			//$this->Commonmodel->array_print_to_html($cma_settings);
			//exit;

			if( !isset($procedure->pro_id) ) throw new Exception("Error: Procedure selected is not available", 1);
			

			$set = array();
			$set['procedure_id'] 	= $pro_id; 
			$set['client_id'] 		= $procedure->client_id; 
			$set['cma_id'] 			= $cma_settings->cma_id; 
			$set['tran_priority'] 	= $procedure->priority_flag; 
			$set['display_screen'] 	= $procedure->display_screen; 
			
			if( !$this->Transactionmodel->update($tran_id, $set) ) throw new Exception("Error: Unable to update procedure", 1);
			

			$message = 'Set Procedure: '.$procedure->pro_name ;

            $audit['tran_id']      	= $tran_id;
            $audit['audit_type']    = 'SET PROCEDURE';
            $audit['audit_from']    = '';
            $audit['audit_to']      = '';
            $audit['return_message']= 'Procedure successfully set';        
            $audit['message']       = $message;
            $audit['more_info']     = json_encode(array('agent_name'=>$this->agent_name));
                               
            $this->Commonmodel->insert_audit_trail($audit);			

			$this->session->set_flashdata('fmesg', 'Procedure successfully set');	
			redirect('callactivity/open/'.$tran_id);

		} catch (Exception $e) {
			$this->session->set_flashdata('fmesg', $e->getMessage());	
			redirect('callactivity/open/'.$tran_id);			
		}

	}


	/**
	 * POST request via callactivity.js callactivity.closed_alert(tran_id)
	 * @return [type] [description]
	 */
	public function ajax_close_alert(){

		$status_array = array('status' => 1 , 'url'=>'' );

        try {
			
            if( !isset($_POST) ) throw new Exception("Error: Invalid allowed method", 1);

            $post = $this->input->post();

            if( !isset($post['tran_id']) OR @$post['tran_id'] == '' ) throw new Exception("Error: tran_id is not set", 1);
            if( !isset($post['ref_number']) OR @$post['ref_number'] == '' ) throw new Exception("Error: ref_number not set", 1);

            $tran_id = $post['tran_id'];
            $ref_number = $post['ref_number'];



            $params['where']['tran_id'] = $tran_id;
            $record = $this->Transactionmodel->row($params);


            $set = array();
            $set['tran_status'] 	= 0;

            if( $record->call_hand_end == '' )
                $set['call_hand_end'] 	= date('Y-m-d H:i:s');

            if( !$this->Transactionmodel->update($tran_id, $set) ) throw new Exception("Error: Unable to update procedure", 1);


            $message = 'Set Alert to CLOSED';

            $audit['tran_id']      	= $tran_id;
            $audit['audit_type']    = 'CLOSED';
            $audit['audit_from']    = '';
            $audit['audit_to']      = '';
            $audit['return_message']= 'Successfully closed Alert';        
            $audit['message']       = $message;
            $audit['more_info']     = json_encode(array('agent_name'=>$this->agent_name));
                           
            $this->Commonmodel->insert_audit_trail($audit);			


            //AGENT STATUS
            $_as_r = array();
            $_as_r['tran_id'] 	= $tran_id;
            $_as_r['agent_id'] 	= $this->agent_id;
            $_as_r['cma_id'] 	= @$record->cma_id;
            $_as_r['reason'] 	= 'Escalation Duty';
            $this->Commonmodel->curl_agentstatus('agentStatusUpdate',$_as_r);

            $flash_msg = 'Alert# <a href="callactivity/open/'.$tran_id.'">'.$ref_number.'</a> Successfully set to CLOSED ';

            //$this->session->set_flashdata('fmesg', 'Alert#'.$ref_number.' Successfully set to CLOSED ');
            $this->session->set_flashdata('fmesg', $flash_msg);

            $status_array['url'] 	= 'register';
            echo json_encode($status_array);

        } catch (Exception $e) {
            //$this->session->set_flashdata('fmesg', $e->getMessage());	
            redirect('callactivity/open/'.$tran_id);		

            $status_array['status'] = 1;
            $status_array['body'] 	= $e->getMessage;
            echo json_encode($status_array);
        }
	}


	public function ajax_process_log(){

		$status_array = array('status' => 1 , 'url'=>'' );

		try {
			
			$post = $this->input->post();

 			$_as_r = array();
 			$_as_r['tran_id'] 	= @$post['tran_id'];
 			$_as_r['agent_id'] 	= $this->agent_id;
 			$_as_r['cma_id'] 	= @$post['cma_id'];
 			$_as_r['reason'] 	= 'Escalation Issues';
 			$this->Commonmodel->curl_agentstatus('agentStatusUpdate',$_as_r);


 			echo json_encode($status_array);

		} catch (Exception $e) {
			
		}

	}

}
