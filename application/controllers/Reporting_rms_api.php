<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reporting_rms_api extends MY_Controller {

	public function __construct(){
		
		parent::__construct();

		$this->load->model('Transactionmodel');
	}

	function build_where($params){

		$params_where = array();

		if( isset($params['search']) AND $params['search'] != ''){
			
			$search = addslashes(strtolower(trim($params['search'])));

			if( preg_match('/(!)\d+/', $search) ){
	 			$tran_id = str_replace('!', '', $search);
	 			$like = '(`transaction`.`tran_id`='.$tran_id.')';
	 		}else{ 
		 		$like = " ( lower(`ref_number`) LIKE '%{$search}%' OR ";		 		 
		 		$like .= " lower(`brief_description`) LIKE '%{$search}%' ) "; 
	 		}
	 		$params['where_str'] = $like;	

	 		unset($params['search']);
		}

		if( isset($params['dt']) &&  $params['dt'] != '' ) {

			 
			$dt = explode(' - ',$params['dt']); 
			$dt_f = date('Y-m-d', strtotime(str_replace('/', '-', $dt[0])));
			$dt_t = date('Y-m-d', strtotime(str_replace('/', '-', $dt[1])));			
			 
 			
 			if( isset($params['where_str']) ) $params['where_str'] .= ' AND ';
 			else $params['where_str'] = '';

			$params['where_str'] .= " (DATE_FORMAT(`transaction`.`tran_created`,'%Y-%m-%d') BETWEEN '{$dt_f}' AND '{$dt_t}') ";			


		}

		if( isset($params['dt_due']) &&  $params['dt_due'] != '' ) {

			 
			$dt = explode(' - ',$params['dt_due']); 
			$dt_f = date('Y-m-d', strtotime(str_replace('/', '-', $dt[0])));
			$dt_t = date('Y-m-d', strtotime(str_replace('/', '-', $dt[1])));			
			 
 			
 			if( isset($params['where_str']) ) $params['where_str'] .= ' AND ';
 			else $params['where_str'] = '';

			$params['where_str'] .= " (DATE_FORMAT(`alert_due_custom_dt`,'%Y-%m-%d') BETWEEN '{$dt_f}' AND '{$dt_t}') ";			


		}		

		if( isset($params['tran_status']) AND $params['tran_status'] != ''){
			//$params['where'] = array('tran_status'=>$params['tran_status']);			
			$params_where['tran_status'] = $params['tran_status'];
		}		

		if( isset($params['client_id']) AND $params['client_id'] != ''){
			//$params['where'] = array('client.client_id'=>$params['client_id']);			
			$params_where['client.client_id'] = $params['client_id'];
		}

		if( isset($params['display_screen']) AND $params['display_screen'] != ''){

 			$params_where['display_screen'] = $params['display_screen']; 
		}

		if( isset($params['alert_type']) AND $params['alert_type'] != ''){

 			$params_where['alert_type'] = $params['alert_type']; 
		}

		if( isset($params['cust_name']) AND $params['cust_name'] != ''){

 			$params_where['cust_name'] = $params['cust_name']; 
		}

		if( isset($params['filter-priority']) AND $params['filter-priority'] != ''){

 			if( isset($params['where_str']) ) $params['where_str'] .= ' AND ';
 			else $params['where_str'] = '';

 			$params['where_str'] .= " `priority` = '".$params['filter-priority']."'";
		}

		if( count($params_where) > 0 )
				$params['where'] = $params_where;

		return $params;
	}


	public function index(){

		$this->load->model('Clientmodel');
		$this->load->model('Clientcustomtable');
		$this->load->library("pagination");

		$data = '';

		$params = $this->input->get();
		$page = (isset($params['per_page'])) ? $params['per_page'] : 0;
		unset($params['per_page']);


		if( !isset($params['dp_from']) ){
			$params['dp_from'] = date('01/m/Y');
			$params['dp_to'] = date('d/m/Y');
		}

		$data['filters'] = $params;

		$params['dt'] = $params['dp_from'].' - '.$params['dp_to'];

		/*if( $this->client_id > 0 ){
			$params['client_id'] = $this->client_id;
		}else{

			if( isset($params['client_id']) ){
				if( !is_numeric($params['client_id']) ){ 
					$params['cust_name'] = $params['client_id'];
					unset($params['client_id']);
				}
			}

		}*/
		
		
		
		if( isset($params['client_id']) AND @$params['client_id'] > 0 ){

			$params['client_id'] = $params['client_id'];
		}else{
			$params['client_id'] = 2;
		}

		$data['client_id'] = $params['client_id'];


		$query_params = $this->build_where($params);
		

		//download
		if( isset($params['btn_export']) ){
			$this->export_report_single_row($query_params);
		}

		
		unset($params['dt']);				
		$params_query = @http_build_query($params); 

		$per_page = 50;
		$query_params['limits']['start'] = $page;
		$query_params['limits']['limit'] = $per_page; 


		$appt_results 				= $this->Transactionmodel->listing2($query_params);
		//echo $this->db->last_query();
        $p_config["base_url"] 		= base_url() . "reporting_rms_api/?".$params_query;
        $p_config["total_rows"] 	= $appt_results['total_rows'];
        $p_config["per_page"] 		= $per_page;
        $p_config["uri_segment"] 	= 3; 
        $config = $this->Commonmodel->pagination_config($p_config);	 
        $this->pagination->initialize($config);

        $data['results'] 	= $appt_results['results'];
		$data["links"] 		= $this->pagination->create_links();
		$data['showing']	= 'Showing '.(($page==0)?1:$page+1).' to '.($page+count($data['results'])).' of '.$appt_results['total_rows'].' entries';
 		
		$data['clients'] = $this->Clientmodel->combine_clients_array();


		$this->view_data['data'] = $data;
		$this->view_data['menu_active'] = 'reporting';
		$this->view_data['view_file'] = 'pages/reporting/list-rms';
		$this->view_data['js_file'] = '<script src="assets/js/reporting.js"></script>'; 
		
		$this->load->view('template', $this->view_data); 
	}
 

	public function export_report_single_row($query_params) {
 
		$this->load->model('Commonmodel');
		$this->load->model('Callsmodel');

		$this->load->library('ExportDataExcel'); 
  	

  		$results = $this->Transactionmodel->listing2($query_params, FALSE);	 

		$data = array();
		$call_fields_counter[]= array(); //calls counter fields  
		
 
 		$this->db->where('client_id', $query_params['client_id']);
		$this->db->where(" table_desc IN('api data', 'api storage') ");
		$api_storage = $this->db->get('client_custom_table');
		$api_storage = @$api_storage->row()->table_name;

		$custom_table_fields = $this->Clientcustomtable->get_table_fields($api_storage, 'array');
		unset($custom_table_fields['id']);
		unset($custom_table_fields['tran_id']);
		unset($custom_table_fields['created_dt']);
 


		foreach( $results as $row ){

			$inRow = array();
			$inRow[] = $row->client_name;
			$inRow[] = $row->ref_number;
			$inRow[] = $row->alert_type ; 			
			$inRow[] = ' '.date('d/m/Y H:i',strtotime($row->alert_created_dt)); 		

			$cs_2_alert_param['where']['tran_id'] = $row->tran_id;
			$cs_2_alert_param['sorting'] = ' id ASC '; 
			//$cs_2_alert_row = $this->Clientcustomtable->get_table_row('cs_2_alert', $cs_2_alert_param, 'row_array'); 
			$cs_2_alert_row = $this->Clientcustomtable->get_table_row($api_storage, $cs_2_alert_param, 'row_array'); 

			foreach($custom_table_fields as $field){

				$inRow[] = ' '.@$cs_2_alert_row[$field];
			}
  

			$calls = $this->Callsmodel->listing(array('where'=>array('tran_id'=>$row->tran_id)), FALSE);
			
			$inRow[] = count($calls); // total calls
			$inRow[] = isset($calls[0]->call_created)?$this->Commonmodel->timestampdiff($row->alert_created_dt, $calls[0]->call_created):'';


			$i=1;
			foreach($calls as $call){

				$inRow[] = $call->caller_name; 
				$inRow[] = $call->caller_phone;
				$inRow[] = ' '.date('d/m/Y H:i',strtotime($call->call_created)); 
				$inRow[] = @$this->call_direction_desc[$call->call_direction];
				//$inRow[] = $call->call_direction;
				$inRow[] = stripslashes($call->call_res_text);
				$inRow[] = stripslashes($call->call_notes);				
				$inRow[] = ($call->log_status)?'Open':'Closed';
				$inRow[] = ' ';

				$call_fields_counter[$i] = $i;
				$i++;

			}

			$data[] = $inRow;


			// echo '<pre>';
			// print_r($inRow);
			// echo '</pre>';

		}



		$header = array('Client', 'Alert #', 'Alert Type', 'Date/Time SET');

		$header = array_merge($header,  $custom_table_fields); 
		array_push($header,  'Total Calls'); 
		array_push($header,  'Time START & 1st call'); 
		for($i = 1; $i < count($call_fields_counter); $i++ ){ 
			array_push($header,  'Caller Name',  'Caller Phone',  'Call Date/Time', 'Call Direction', 'Call Reponse', 'Call Notes', 'Status', ' '); 
		}

		// echo '<pre>';
		// print_r($data);
		// echo '</pre>';


		$exporter = new ExportDataExcel('browser'); //browser, string, file
		$exporter->filename = 'EscalationSystem-'.rand(date('mdYHis'), 5).'.xls';			
		$exporter->initialize();

		$exporter->addRow($header);  
	
		foreach($data as $row){
			$exporter->addRow($row); 	
		}		
		$exporter->finalize();
		exit();	
	}


}
