<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Webhook_default extends CI_Controller {

	//https://escalation.welldone.net.au/webhook/alert/API_KEY

	public function __construct(){
		
		parent::__construct();

	}
	
	public function alert($api_key=''){
		 
		try {

			if( empty($api_key) ) throw new Exception("Invalid API_KEY", 1);			
 			
			$method = $this->input->method(TRUE); 

			$request_data = '';
			
			
			if($_POST) {
				//$set['method_type'] = 'POST';
				$request_data = $this->input->post();
			}elseif($_GET) {
				//$set['method_type'] = 'GET';
				$request_data = $this->input->get();
			}else{
				//$set['method_type'] = $method;
				$request_data = $this->input->raw_input_stream;
			}
 			//echo $request_data;
 			
 			$_json_data = json_decode_nice($request_data, true);

 			if( is_string($request_data) && is_array($_json_data) ){

 				$_json_data = (array)$_json_data;

 			}else{

 				if( is_array($request_data ) ){
 					$_json_data = $request_data;
 				}else{

 					$_json_data = (trim($request_data)!='')?$request_data:'';
 					
 				}
 			}
 
 			$client = $this->_verify_client($api_key);

 			if( count($client) == 0 ) throw new Exception("Invalid client", 1);
 			 
 			$set = array();
 			$json_data = array();

 			$json_data['body'] = $_json_data;

 			$set['json_data'] 	= json_encode($json_data);
 			$set['alert_type'] 	= 'API';
 			$set['client_id'] 	= $client->client_id;
 			$set['tran_priority'] 	= 1;
 			$set['display_screen'] 	= 1;
 			$set['alert_created_dt'] 	= date('Y-m-d H:i:s');
 			$set['brief_description'] 	= $client->client_name; 
 			
 			$tran_id = $this->_insert($set);

			if( is_int($tran_id) AND $tran_id > 0  ){
				echo 'OK'.PHP_EOL;
				print_r($json_data);
			}else{
				throw new Exception($tran_id, 1);				
			}


		} catch (Exception $e) {
			echo 'ERROR: '.$e->getMessage();
		}
	}

	

	function _verify_client($api_key){

		$api_key = trim($api_key);

		try {

			if( empty($api_key) ) throw new Exception("API KEY not recognised", 1);
			
			$this->db->where("MD5(CONCAT('".APP_API_KEY."',client_id))", $api_key);
			
			$row = $this->db->get('client')->row();

			if( !isset($row->client_id) ) throw new Exception("API KEY not recognised", 1);
			
			return $row; 

		} catch (Exception $e) {
			return $e->getMessage();
		}

	}

	function _insert( $set=array() ){

		$this->load->model('Transactionmodel');
		
		try {
		
			if( empty($set) ) throw new Exception("Empty data", 1);
			
			$_set = $set;

			$tran_id = $this->Transactionmodel->insert($_set);

			return $tran_id;


		} catch (Exception $e) {
			return $e->getMessage();
		}
		
	}

}
