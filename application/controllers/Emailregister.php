<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emailregister extends MY_Controller {

	public function __construct(){
		
		parent::__construct();

		$this->load->model('Emaildatatestmodel');
	}

	public function index(){

		$this->load->library("pagination");
		
		

		$data = '';

		$params = $this->input->get();
		$page = (isset($params['per_page'])) ? $params['per_page'] : 0;
		unset($params['per_page']);

		$params_query = @http_build_query($params);

		$per_page = 50;
		$query_params['limits']['start'] = $page;
		$query_params['limits']['limit'] = $per_page;
		$query_params['sorting']['sort'] = 'msg_udate';
		$query_params['sorting']['order'] = 'DESC';
		
		$data['pullemails'] = $this->_pullemails();



		$appt_results 				= $this->Emaildatatestmodel->get_result_pagination($query_params);
        $p_config["base_url"] 		= base_url() . "emailregister/index/?".$params_query;
        $p_config["total_rows"] 	= $appt_results['total_rows'];
        $p_config["per_page"] 		= $per_page;
        $p_config["uri_segment"] 	= 3; 
        $config = $this->Commonmodel->pagination_config($p_config);	 
        $this->pagination->initialize($config);

        $data['results'] 	= $appt_results['results'];
		$data["links"] 		= $this->pagination->create_links();
		$data['showing']	= 'Showing '.(($page==0)?1:$page+1).' to '.($page+count($data['results'])).' of '.$appt_results['total_rows'].' entries';



		$this->view_data['data'] = $data;
		$this->view_data['menu_active'] = 'emailregister';
		$this->view_data['view_file'] = 'pages/email_register/inbox';
		
		
		$this->load->view('template', $this->view_data); 
	}


	function _pullemails(){

		$this->load->library('imap'); 

		$this->imap->connect();  
		//$messages = $this->imap->search('UNFLAGGED' ,'date', false);
		$messages = $this->imap->search('UNFLAGGED' ,'date', false);


		/*echo '<br />';
		print_r($this->imap->get_folders());
		echo '<br />';
		$this->imap->select_folder('INBOX');
		echo '<br />';
		print_r($this->imap->get_quota());*/

		//$messages = $this->imap->search('UNFLAGGED' ,'date', false);

		$counter = 0;
		//echo '<pre>';
		foreach ($messages as $msgid) {
			
			$message = $this->imap->get_message($msgid);

			// echo '-------------------------------------------<br />';
			// print_r($message);
			

			$email['msg_id'] = $message['id'];
			$email['msg_uid'] = $message['uid'];
			$email['email_from'] = json_encode($message['from']);
			$email['email_to'] = json_encode($message['to']);
			$email['email_subject'] = @addslashes($message['subject']);
			$email['email_reply_to'] = json_encode($message['reply_to']);
			$email['msg_body_plain'] = @addslashes($message['body']['plain']);
			$email['msg_body_html'] = @addslashes($message['body']['html']);
			$email['msg_date'] = $message['date'];
			$email['msg_udate'] = $message['udate'];
			
			//$this->imap->mark_as_unflagged($message['uid']);

			if($this->Emaildatatestmodel->insert($email)) {
				$this->imap->mark_as_flagged($message['uid']);

				$counter++;
			}

			/*echo '<br />';
			echo 'FLAG: '.($this->imap->mark_as_flagged($message['uid']))?'YES':'NO';
			echo '<br />';*/
		}	 
		//echo '</pre>';



		if( $counter > 0 ){
			return 'There are '.$counter.' email(s) being pull';
		}else{
			return 'No new email!!!';
		}		 

	}


	public function open($id=''){

		try {
			
			if($id == '') throw new Exception("Email Error Processing Request", 1);
			

			$data = '';


			$params['where']['id'] = $id;
			$data['record'] = $this->Emaildatatestmodel->get_row($params);

			$this->view_data['data'] = $data;
			$this->view_data['menu_active'] = 'emailregister';
			$this->view_data['view_file'] = 'pages/email_register/view';
			
			
			$this->load->view('template', $this->view_data); 

		} catch (Exception $e) {

			$this->session->set_flashdata('fmesg',$e->getMessage());

			redirect('emailregister');
		}
 

	}


	public function pullemails(){

		$this->load->library('imap'); 

		$this->imap->connect();  
		//$messages = $this->imap->search('UNFLAGGED' ,'date', false);
		$messages = $this->imap->search('UNFLAGGED' ,'date', false);


		echo '<br />';
		print_r($this->imap->get_folders());
		echo '<br />';
		$this->imap->select_folder('INBOX');
		echo '<br />';
		print_r($this->imap->get_quota());

		//$messages = $this->imap->search('UNFLAGGED' ,'date', false);

		/*$counter = 0;
		//echo '<pre>';
		foreach ($messages as $msgid) {
			
			$message = $this->imap->get_message($msgid);

			// echo '-------------------------------------------<br />';
			// print_r($message);
			

			$email['msg_id'] = $message['id'];
			$email['msg_uid'] = $message['uid'];
			$email['email_from'] = json_encode($message['from']);
			$email['email_to'] = json_encode($message['to']);
			$email['email_subject'] = @addslashes($message['subject']);
			$email['email_reply_to'] = json_encode($message['reply_to']);
			$email['msg_body_plain'] = @addslashes($message['body']['plain']);
			$email['msg_body_html'] = @addslashes($message['body']['html']);
			$email['msg_date'] = $message['date'];
			$email['msg_udate'] = $message['udate'];
			
	 
			// echo '<br />';
			// echo 'FLAG: '.($this->imap->mark_as_flagged($message['uid']))?'YES':'NO';
			// echo '<br />';
		}	 
		//echo '</pre>';



		if( $counter > 0 ){
			return 'There are '.$counter.' email(s) being pull';
		}else{
			return 'No new email!!!';
		}	*/	 

	}



}
