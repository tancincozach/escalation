<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct(){
		
		parent::__construct();

	}

	public function index(){
		redirect('login');
	}

	public function login(){

		$this->load->database(); 

		if( isset($_GET['user']) AND $_GET['user']!='' ){

			//$this->autologin();
		
		}elseif( @$this->session->userdata('logged_in') == 1 AND @$this->session->userdata('user_id') > 0 ){
			
			redirect(base_url().'dashboard');
		
		}else{

			$cookie = get_cookie('info');
			$cookie = base64_decode($cookie);
			$cookie = json_decode($cookie);
			
			if(isset($cookie->logged_in)){
				redirect(base_url().'dashboard');
			}
			
		}


		$data = '';

		$sess = array();
				
		if($_POST){

			$post = $this->input->post();
 
			$fullname = @$this->input->post('inputUsername', TRUE);
			$username = $this->input->post('inputUsername', TRUE);
			$password = md5($this->input->post('inputPassword', TRUE));

			//via escalation.users table
			if( $post['inlineRadioOptions'] == '1' ){

				$this->db->limit(1);
				$this->db->where('username', $username);
				$user = $this->db->get('users')->row();

			//cma users via council_manage
			}else{
				$user_where['username'] = $username;
				$user = $this->get_cma_user_info($user_where);
				$fullname = $user->username;
				$user->user_lvl = $user->userlevel;
				$sess['agent_id'] 	= $user->user_id;
			}


			if( isset($user->username) AND $user->password == $password ){

				$sess['logged_in'] 	= 1; 
				$sess['user_id'] 	= $user->id;
				$sess['user_lvl'] 	= $user->user_lvl;
				$sess['agent_name'] = $fullname;
				$sess['client_id']  = @$user->client_id;

				$this->session->set_userdata($sess);

				$base64_encode_str = base64_encode(json_encode($sess));			
				set_cookie('info',$base64_encode_str,28800);

				redirect(base_url().'dashboard');

			}else{
				$flash_msg = '<p class="text-danger bg-danger">Username or Password is Incorrect</p>';
				$this->session->set_flashdata('login_message', $flash_msg);
				redirect(base_url().'login');
			}
 
		}else{
			$this->autologin();
		}

		$this->load->view('login', $data);

	}


	function autologin(){

		$ip = $_SERVER['REMOTE_ADDR'];

 
		$sess = array();

		if(isset($_GET['user']) AND $_GET['user']!=''){

			$user_where['user_id'] = $_GET['user'];

			$user = $this->get_cma_user_info($user_where);
			
			if( count($user) == 1 ){
				 
 				$sess['logged_in'] 	= 1; 
				$sess['user_id'] 	= $user->id;
				$sess['user_lvl'] 	= $user->userlevel;
				$sess['agent_name'] = $user->username;
				$sess['agent_id'] 	= $user->user_id;
				
				$this->session->set_userdata($sess); 

				$base64_encode_str = base64_encode(json_encode($sess));			
				set_cookie('info',$base64_encode_str,28800);

				redirect(base_url().'dashboard');			
			}else{
				$this->session->set_flashdata('login_message', 'Username or Password is Incorrect');
				redirect(base_url().'login');
			}
		}

	}

	/**
	 * This will connect to council_manage database to get cma users info
	 * @return stdClass Object of user info
	 */
	public function get_cma_user_info($params){

		try{
			//check database if exist
			//$this->load->dbutil(); 

			$access = ( (isset($params['user_id']) OR isset($params['username']) ) AND ( $params['user_id'] !='' or $params['username'] != '' )  ) ?TRUE:FALSE;

			if(!$access) throw new Exception("Invalid CMA INFO", 1);  

			$db_config['hostname'] = 'localhost';
			$db_config['username'] = 'root';
			$db_config['password'] = 'A110uRdat4';
			$db_config['database'] = 'council_manage';
			
			$db_config['dbdriver'] = "mysqli";
			$db_config['dbprefix'] = "";
			$db_config['pconnect'] = FALSE;
			$db_config['db_debug'] = TRUE;
			$db_config['cache_on'] = FALSE;
			$db_config['cachedir'] = "";
			$db_config['char_set'] = "utf8";
			$db_config['dbcollat'] = "utf8_unicode_ci";
			$db_config['autoinit'] = TRUE; 
			$db_config['stricton'] = FALSE; 

			$db1 = $this->load->database($db_config, TRUE); 
			$db1->reconnect();

			if( isset($params['user_id']) ){
				$db1->where('user_id', $params['user_id'] );
			}

			if( isset($params['username']) ){
				$db1->where('username', $params['username'] );
			}
				
			$cma_user = $db1->get('users')->row();
 	
 			$db1->close();

			return $cma_user;

		}catch(Exception $e ){
			return false;
		}
	}

	/**
	 * Remote via common-api
	 * @param  array $params username and user_id
	 * @return json jobject         
	 */
	function get_cma_user_info_v2($params){
		try{
			 
			$access = ( (isset($params['user_id']) OR isset($params['username']) ) AND ( @$params['user_id'] !='' or @$params['username'] != '' )  ) ?TRUE:FALSE;

			if(!$access) throw new Exception("Invalid CMA INFO", 1);  

			//auto call log link
			
			if( $_SERVER['SERVER_NAME'] == 'localhost' ){
				$url = "http://localhost/local/common-api/cma/v1/user/".(isset($params['user_id'])?'user_id':'username')."/".(isset($params['user_id'])?$params['user_id']:$params['username']);
			}else{
				$url = "https://common-api.welldone.net.au/cma/v1/user/".(isset($params['user_id'])?'user_id':'username')."/".(isset($params['user_id'])?$params['user_id']:$params['username']);
			} 
			//echo $url;
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			$head = curl_exec($ch);
			$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);
		 

			$obj = (object)json_decode($head);
 
			 
			return $obj;

		}catch(Exception $e ){
			return false;
		}
	}

	public function logout(){
  
		unset(
		        $_SESSION['logged_in'],
		        $_SESSION['user_id']
		);

		$array_items = array('logged_in', 'user_id');
		$this->session->unset_userdata($array_items);

		$this->session->sess_destroy();

		delete_cookie('info');

	    // null the session (just in case):
	    $this->session->set_userdata(array('logged_in' => '', 'user_id' => ''));

		$flash_msg = '<p class="text-success bg-success">You have succesfully logout</p>';

		$this->session->set_flashdata('login_message', $flash_msg); 

		redirect(base_url().'login');

	}	

}
