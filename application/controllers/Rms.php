<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rms extends MY_Controller {

	public function __construct(){
		
		parent::__construct();

		$this->load->model('Rmsmodel');
	}

	public function index(){

		$this->load->library("pagination");

		$data = '';

		$params = $this->input->get();
		$page = (isset($params['per_page'])) ? $params['per_page'] : 0;
		unset($params['per_page']);

		$params_query = @http_build_query($params);

		$per_page = 50;
		$query_params['limits']['start'] = $page;
		$query_params['limits']['limit'] = $per_page; 


		$appt_results 				= $this->Rmsmodel->get_result_pagination($query_params);
        $p_config["base_url"] 		= base_url() . "rms/index/?".$params_query;
        $p_config["total_rows"] 	= $appt_results['total_rows'];
        $p_config["per_page"] 		= $per_page;
        $p_config["uri_segment"] 	= 3; 
        $config = $this->Commonmodel->pagination_config($p_config);	 
        $this->pagination->initialize($config);

        $data['results'] 	= $appt_results['results'];
		$data["links"] 		= $this->pagination->create_links();
		$data['showing']	= 'Showing '.(($page==0)?1:$page+1).' to '.($page+count($data['results'])).' of '.$appt_results['total_rows'].' entries';



		$this->view_data['data'] = $data;
		$this->view_data['menu_active'] = 'rmsregister';
		$this->view_data['view_file'] = 'pages/rms_register';
		
		
		$this->load->view('template', $this->view_data); 
	}


	public function list_files(){

		$this->load->helper('file');

		$path = './files/pinpoint_data/';
		$files = get_filenames($path);

		//print_r($files);

		asort($files);

		echo '<table border="1" style=" border-collapse: collapse">';
		echo '<tr><td>filename</td>
			<td>content</td>
			<td>size</td>
			<td>date</td>
			</tr>';

		foreach ($files as $file) {

			if( $file == 'index.html' ) continue;

			$info = get_file_info($path.$file);

			echo '<tr>
				<td><a href="./read_file/?file='.$path.$file.'" target="_blank">'.$file.'</a><br /></td>   
				<td>'.read_file($path.$file).'</td>
				<td>'.$info['size'].'</td>
				<td>'.date('d/m/Y H:i:s', $info['date']).'</td>
				</tr>';
		}

		echo '</table>';

	}


	public function read_file(){
		$this->load->helper('file');
		//echo $_GET['file'];
		$string = read_file($_GET['file']);
		echo $string;

	}
 
}
