<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//https://escalation.welldone.net.au/smsglobalwebhook/postback/

class Smsglobalwebhook extends CI_Controller {

	public function __construct(){
		
		parent::__construct();

	}
	
	public function postback(){
		 
		try {
 
			$method = $this->input->method(TRUE); 

			$request_data = '';
			$set = array();
			
			$set['method_type'] = $method;

			if($_POST) {
				$request_data = $this->input->post();
			}elseif($_GET) {				
				$request_data = $this->input->get();
			}else{				
				$request_data = $this->input->raw_input_stream;
			}
 
 			$json_data = '';

 			if( is_string($request_data) && is_array(json_decode($request_data, true)) ){
 				$json_data = $request_data;
 			}else{
 				

 				if( is_array($request_data ) ){
 					$json_data = json_encode($request_data);
 				}else{
 					$json_data = (trim($request_data)!='')?json_encode($request_data):'';
 				}
 			} 

			$set['sms_msg'] = $json_data;

			$this->db->insert('smsglobal_incoming_sms', $set);
 

		} catch (Exception $e) {
			echo $e->getMessage();
		}
 		
 		echo "OK";
	}
}
