<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Misc extends MY_Controller {

	public function __construct(){
		
		parent::__construct();



	}

	public function send_email(){

		$this->load->model('Clientmodel','client');
		$this->load->model('Commonmodel','common');
		$this->load->model('Emaildatamodel','email_m');
		$this->load->model('Transactionmodel','trams');



		if($_POST){

				try {

					$post = $this->input->post();

					if($post['action_type']=='send_email'){

						if( $post['from']==''  || $post['to']=='' || $post['subject']=='' || $post['email_body']=='')	
							throw new Exception("ERROR: Please input required fields", 1);
							
						$email_arr = array();

						$email_arr['message'] = $post['email_body'];

						$email_arr['subject'] = $post['subject'];

						$email_arr['from']    = $post['from'];

						$email_arr['to'] 	   = $post['to'];

						$result = $this->common->send_email($email_arr);



						unset($email_arr);
						
						if($result!='message_sent') throw new Exception($result);

						if(isset($post['to'])){


							$to = str_replace(" ","",$post['to']);
							$to = str_replace(";",",",$to);
							$to = explode(",",$to); 

							if(count($to) > 0){	

								$em_arr = array();

								foreach ($to as $email) {

									$em_arr[] = array('email'=>$email,'name'=>'');
					
								}


								$email_arr['email_to'] = json_encode($em_arr);

							} 

						}

						if(isset($post['from'])){

							$em_arr = array();

							$em_arr['email'] = $post['from'];

							$em_arr['name'] = '';

							$email_arr['email_from'] = json_encode($em_arr);

						}
												

						$email_arr['body_html'] = $post['email_body'];

						$email_arr['email_type'] = 'SENDEMAIL';

						$email_arr['email_subject'] = $post['subject'];



						if(!$this->email_m->insert($email_arr)) throw new Exception('Unable to save email details');


							$client_row = $this->client->row( array('where'=>array('client_id'=>$post['client_id'])));
							$insert_tran = array();

							$insert_tran['client_id'] 	      = $client_row->client_id;
							$insert_tran['alert_type'] 	      = 'SEND_EMAIL';
							$insert_tran['tran_priority'] 	  = '4';
							$insert_tran['brief_description'] = $email_arr['email_subject'];
							$insert_tran['alert_created_dt']  = date('Y-m-d H:i:s');
							$insert_tran['alert_due_dt']  	  = date('Y-m-d H:i:s');
							$insert_tran['tran_status'] 	  = 0;
							$insert_tran['cma_id'] 			  = $client_row->cma_id; 

						 	$this->trams->insert($insert_tran);

						   $this->session->set_flashdata('fmesg',"Email Successfully Sent!.");


					}else{


						if( $post['client_name']==''  || $post['cma_id']=='' )	
							throw new Exception("ERROR: Please input required fields", 1);


						$in_arr['cma_id'] = $post['cma_id'];
						$in_arr['client_name'] = $post['client_name'];

						$client_id = $this->client->insert($in_arr);

						unset($in_arr);


						$this->session->set_flashdata('fmesg',"Client Successfully Created!.");


						 redirect(base_url().'send_email?cid='.$client_id);  
						
			
					}



				} catch (Exception $e) {

					$this->session->set_flashdata('error',$e->getMessage());
					
				}

		}


	try {
			
			$data = '';

			$get = $this->input->get();

			if( $this->client_id > 0 ){
				$clients_params['where']['client_id'] = $this->client_id;
			}

			$clients_params['select']				 = 'client_id, client_name';
			$clients_params['where']['client_status'] = 1;
			$clients_params['sorting']['sort'] 		 = 'client_name';
			$clients_params['sorting']['order']		 = 'ASC';
			$data['clients'] = $this->client->listing($clients_params, false);

			$pro_params = array();
			$client_id = '';


			if( $this->client_id > 0 ){
 				$client_id = $this->client_id; 
			}else{
				if( isset($get['client_id']) ){
					$client_id = $get['client_id'];	 
				}
			} 

			 
			if( $client_id > 0 ){
								
			 
				$data['client'] = $this->client->row( array('where'=>array('client_id'=>$client_id)) );

			}

			if(isset($get['cid'])){

				$data['client_created'] = $this->client->row( array('where'=>array('client_id'=>$get['cid'])) );
			}


			$data['client_id'] = $client_id;


			$this->view_data['data'] = $data;
			
			$this->view_data['menu_active'] = 'send_email';
			$this->view_data['view_file'] = 'pages/misc/send_email/send';
			$this->view_data['js_file'] = '<script src="assets/js/pages/misc/send_email.js?v='.JS_VER_ALERTREGISTER.'"></script>';
			$this->view_data['js_file'] .= '<script type="text/javascript" src="assets/vendor/tinymce/js/tinymce/tinymce.min.js"></script>'.PHP_EOL;
			$this->view_data['js_file'] .= '<script type="text/javascript" src="assets/vendor/tinymce.filemanager/plugin.min.js"></script>'.PHP_EOL;
			
			$this->load->view('template', $this->view_data); 

		} catch (Exception $e) {
			echo $e->getMessage();
		}

	}

	public function format_email($posted_values){

	/*	$email = "SUBJECT : "$posted_values."<br";
		$email = "SUBJECT : "$posted_values;
*/
	}

	
}
