<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reporting extends MY_Controller {

	public function __construct(){
		
		parent::__construct();

		$this->load->model('Transactionmodel');
	}

	function build_where($params){

		$params_where = array();

		if( isset($params['search']) AND $params['search'] != ''){
			
			$search = addslashes(strtolower(trim($params['search'])));

			if( preg_match('/(!)\d+/', $search) ){
	 			$tran_id = str_replace('!', '', $search);
	 			$like = '(`transaction`.`tran_id`='.$tran_id.')';
	 		}else{ 
		 		$like = " ( lower(`ref_number`) LIKE '%{$search}%' OR ";		 		 
		 		$like .= " lower(`brief_description`) LIKE '%{$search}%' ) "; 
	 		}
	 		$params['where_str'] = $like;	

	 		unset($params['search']);
		}

		if( isset($params['dt']) &&  $params['dt'] != '' ) {

			 
			$dt = explode(' - ',$params['dt']); 
			$dt_f = date('Y-m-d', strtotime(str_replace('/', '-', $dt[0])));
			$dt_t = date('Y-m-d', strtotime(str_replace('/', '-', $dt[1])));			
			 
 			
 			if( isset($params['where_str']) ) $params['where_str'] .= ' AND ';
 			else $params['where_str'] = '';

			$params['where_str'] .= " (DATE_FORMAT(`transaction`.`tran_created`,'%Y-%m-%d') BETWEEN '{$dt_f}' AND '{$dt_t}') ";			


		}

		if( isset($params['dt_due']) &&  $params['dt_due'] != '' ) {

			 
			$dt = explode(' - ',$params['dt_due']); 
			$dt_f = date('Y-m-d', strtotime(str_replace('/', '-', $dt[0])));
			$dt_t = date('Y-m-d', strtotime(str_replace('/', '-', $dt[1])));			
			 
 			
 			if( isset($params['where_str']) ) $params['where_str'] .= ' AND ';
 			else $params['where_str'] = '';

			$params['where_str'] .= " (DATE_FORMAT(`alert_due_custom_dt`,'%Y-%m-%d') BETWEEN '{$dt_f}' AND '{$dt_t}') ";			


		}		

		if( isset($params['tran_status']) AND $params['tran_status'] != ''){
			//$params['where'] = array('tran_status'=>$params['tran_status']);			
			$params_where['tran_status'] = $params['tran_status'];
		}		

		if( isset($params['client_id']) AND $params['client_id'] != ''){
			//$params['where'] = array('client.client_id'=>$params['client_id']);			
			$params_where['client.client_id'] = $params['client_id'];
		}

		if( isset($params['display_screen']) AND $params['display_screen'] != ''){

 			$params_where['display_screen'] = $params['display_screen']; 
		}

		if( isset($params['alert_type']) AND $params['alert_type'] != ''){

 			$params_where['alert_type'] = $params['alert_type']; 
		}

		if( isset($params['cust_name']) AND $params['cust_name'] != ''){

 			$params_where['cust_name'] = $params['cust_name']; 
		}

		if( isset($params['filter-priority']) AND $params['filter-priority'] != ''){

 			if( isset($params['where_str']) ) $params['where_str'] .= ' AND ';
 			else $params['where_str'] = '';

 			$params['where_str'] .= " `priority` = '".$params['filter-priority']."'";
		}

		if( count($params_where) > 0 )
				$params['where'] = $params_where;

		return $params;
	}


	public function index(){

		$this->load->model('Clientmodel');
		$this->load->library("pagination");

		$data = '';

		$params = $this->input->get();
		$page = (isset($params['per_page'])) ? $params['per_page'] : 0;
		unset($params['per_page']);


		if( !isset($params['dp_from']) ){
			$params['dp_from'] = date('01/m/Y');
			$params['dp_to'] = date('d/m/Y');
		}

		$data['filters'] = $params;

		$params['dt'] = $params['dp_from'].' - '.$params['dp_to'];

		if( $this->client_id > 0 ){
			$params['client_id'] = $this->client_id;
		}else{

			if( isset($params['client_id']) ){
				if( !is_numeric($params['client_id']) ){ 
					$params['cust_name'] = $params['client_id'];
					unset($params['client_id']);
				}
			}

		}

		$query_params = $this->build_where($params);

		//download
		if( isset($params['btn_export']) ){
			$this->export_report_single_row($query_params);
		}

		
		unset($params['dt']);				
		$params_query = @http_build_query($params); 

		$per_page = 50;
		$query_params['limits']['start'] = $page;
		$query_params['limits']['limit'] = $per_page; 


		$appt_results 				= $this->Transactionmodel->listing($query_params);
		//echo $this->db->last_query();
        $p_config["base_url"] 		= base_url() . "reporting/?".$params_query;
        $p_config["total_rows"] 	= $appt_results['total_rows'];
        $p_config["per_page"] 		= $per_page;
        $p_config["uri_segment"] 	= 3; 
        $config = $this->Commonmodel->pagination_config($p_config);	 
        $this->pagination->initialize($config);

        $data['results'] 	= $appt_results['results'];
		$data["links"] 		= $this->pagination->create_links();
		$data['showing']	= 'Showing '.(($page==0)?1:$page+1).' to '.($page+count($data['results'])).' of '.$appt_results['total_rows'].' entries';
 		
		$data['clients'] = $this->Clientmodel->combine_clients_array();


		$this->view_data['data'] = $data;
		$this->view_data['menu_active'] = 'reporting';
		$this->view_data['view_file'] = 'pages/reporting/list';
		$this->view_data['js_file'] = '<script src="assets/js/reporting.js"></script>'; 
		
		$this->load->view('template', $this->view_data); 
	}
 

	public function export_report_single_row($query_params) {
 
		$this->load->model('Commonmodel');
		$this->load->model('Callsmodel');

		$this->load->library('ExportDataExcel'); 
  
  		$results = $this->Transactionmodel->listing($query_params, FALSE);	 

		$data = array();
		$call_fields_counter[]= array(); //calls counter fields  
		
 
		foreach( $results as $row ){

			$inRow = array();
			$inRow[] = $row->alert_type == 'CMA REMINDER'?$row->cust_name:$row->client_name; 
			$inRow[] = $row->ref_number;
			$inRow[] = $row->cma_id;
			$inRow[] = $row->alert_type;
			$inRow[] = $row->alert_type == 'CMA REMINDER'?'CMA / APP reminder activated - needs processing':stripslashes($row->brief_description);
			$inRow[] = @$this->priority_flag[$row->tran_priority]; 
			$inRow[] = @$this->display_screen[$row->display_screen];
			//$inRow[] = ' '.(!in_array($row->alert_custom_dt, array('', '0000-00-00 00:00:00')))?date('d/m/Y H:i', strtotime($row->alert_custom_dt)):'';
			//$inRow[] = ' '.(!in_array($row->alert_due_custom_dt, array('', '0000-00-00 00:00:00')))?date('d/m/Y H:i', strtotime($row->alert_due_custom_dt)):'';
			
			$inRow[] = ' '.date('d/m/Y', strtotime($row->alert_custom_dt));
			$inRow[] = ' '.date('H:i', strtotime($row->alert_custom_dt));
			$inRow[] = ' '.date('d/m/Y', strtotime($row->alert_due_custom_dt));
			$inRow[] = ' '.date('H:i', strtotime($row->alert_due_custom_dt));
			
			$inRow[] = ($row->call_hand_start != '')?' '.date('d/m/Y', strtotime($row->call_hand_start)):' ';
			$inRow[] = ($row->call_hand_start != '')?' '.date('H:i:s', strtotime($row->call_hand_start)):' ';	
			
			$inRow[] = ($row->call_hand_end != '')?' '.date('d/m/Y', strtotime($row->call_hand_end)):' ';
			$inRow[] = ($row->call_hand_end != '')?' '.date('H:i:s', strtotime($row->call_hand_end)):' ';
			
			$inRow[] = @$row->calculate1;
			$inRow[] = @$row->calculate2;
			
			$inRow[] = @$this->tran_status_desc[$row->tran_status];		

			$inRow[] = @$row->agent_name;		 
 
			$calls = $this->Callsmodel->listing(array('where'=>array('tran_id'=>$row->tran_id)), FALSE);
			
			$inRow[] = count($calls); // total calls

			$i=1;
			foreach($calls as $call){

				$inRow[] = $call->caller_name; 
				$inRow[] = $call->caller_phone;
				$inRow[] = ' '.date('d/m/Y H:i',strtotime($call->call_created)); 
				$inRow[] = @$this->call_direction_desc[$call->call_direction];
				//$inRow[] = $call->call_direction;
				$inRow[] = stripslashes($call->call_res_text);
				$inRow[] = stripslashes($call->call_notes);				
				$inRow[] = ($call->log_status)?'Open':'Closed';
				$inRow[] = ' ';

				$call_fields_counter[$i] = $i;
				$i++;

			}

			$data[] = $inRow;


			// echo '<pre>';
			// print_r($inRow);
			// echo '</pre>';

		}



		$header = array('Client', 'Alert #', 'CMA ID', 'Alert Type', 'Description', 'Priority FLAG', 'Display', 
			'Date SET', 'Time SET', 'Date DUE', 'Time DUE', 
			'Date Take Control', 'Time Take Control', 
			'Date CLOSE', 'Time CLOSE', 
			'Time between DUE & TAKE CONTROL (in minute)', 
			'Time between TAKE CONTROL & CLOSE (in minute)', 
			'Status','Agent Took Control', 'Total Calls');

		for($i = 1; $i < count($call_fields_counter); $i++ ){ 
			array_push($header,  'Caller Name',  'Caller Phone',  'Call Date/Time', 'Call Direction', 'Call Reponse', 'Call Notes', 'Status', ' '); 
		}

		// echo '<pre>';
		// print_r($data);
		// echo '</pre>';


		$exporter = new ExportDataExcel('browser'); //browser, string, file
		$exporter->filename = 'EscalationSystem-'.rand(date('mdYHis'), 5).'.xls';			
		$exporter->initialize();

		$exporter->addRow($header);  
	
		foreach($data as $row){
			$exporter->addRow($row); 	
		}		
		$exporter->finalize();
		exit();	
	}


}
