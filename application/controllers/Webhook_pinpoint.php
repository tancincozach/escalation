<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Webhook_pinpoint extends CI_Controller {

	//https://escalation.welldone.net.au/webhook_pinpoint/alert/API_KEY

	public function __construct(){
		
		parent::__construct();

		$this->load->model('Transactionmodel');
	}
	
	public function alert($api_key=''){
		 
		try {

			//if( !$_POST ) throw new Exception("Error Processing Request", 1);			
  
			$method = $this->input->method(TRUE); 

			$request_data = '';
			//$set = array();
			//$set['method_type'] = $method;
			//$json_data = '';

			//$request_data = $this->input->post();
			
			 
			$request_data = $this->input->raw_input_stream;
			
			//print_r($request_data);exit;
  
			/*if($_POST) {
				$request_data = $this->input->post();
			}elseif($_GET) { 
				$request_data = $this->input->get();
			}else{ 
				$request_data = $this->input->raw_input_stream;
			}*/ 
 			
 			$_json_data = json_decode_nice($request_data, true);
 			  
			if( is_array($request_data) ){
				$_json_data = $request_data;

			}elseif( is_array($_json_data ) ){
				//continue use $_json_data
			
			}elseif( is_string($request_data) ){

				parse_str($request_data, $_json_data);
				//echo 'parse_str(str)';
				//print_r($_json_data);
				//echo count($_json_data);
				//exit;
				
				if( count($_json_data) <= 1  ){
					throw new Exception("The data posted is sting, requires JSON or ARRAY post data", 1);
				}

			}else{

				throw new Exception("Requires JSON post data", 1);
 
			}


			//exit;


			/*//record for testing purposes only
 			$set_test['method_type'] = $method;
 			$set_test['json_data'] = addslashes($request_data);
 			$this->db->insert('data_pinpoint', $set_test);
 
 			//clear 1 day old log.
 			$this->clear_morethanday_log();*/



			if( count((array)($_json_data)) > 1 ){
 
				
				if( $api_key == '' )
					$api_key = 'b23d6354186e031ebf201336d92b22fb'; //specific for pinpoint
	 			

	 			$procedure = $this->_verify_procedure($api_key);
	 			$client = $this->_verify_client($procedure->client_id);
	 			
 				if( count($procedure) == 0 ) throw new Exception("Procedure not SET", 1);
 				if( count($client) == 0 ) throw new Exception("Invalid client", 1);	 			 

 				$procedure->reminder_alert_duration = empty($procedure->reminder_alert_duration)?0:$procedure->reminder_alert_duration;

	 			$set = array();
	 			$json_data = array();

	 			$json_data['body'] = $_json_data;

	 			//if table client_data_src is empty
	 			//add data to body
	 			if( trim($procedure->client_data_src) == '' ){
	 				$set['json_data'] 	= json_encode($json_data);
	 			}

	 			$set['alert_type'] 	= 'API';
	 			$set['client_id'] 	= $client->client_id; 
	 			$set['alert_created_dt'] 	= date('Y-m-d H:i:s');
	 			$set['alert_due_dt'] 		= date('Y-m-d H:i:s', strtotime(' + '.$procedure->reminder_alert_duration.' MINUTE '));
	 			$set['brief_description'] 	= @$procedure->pro_name; 
	 			$set['tran_priority'] 		= @$procedure->priority_flag;
	 			$set['display_screen'] 		= @$procedure->display_screen;	 			
	 			$set['procedure_id'] 		= @$procedure->pro_id; 
	 			$set['pro_allow_multi_del']	= @$procedure->allow_multi_del;
	 			
	 			$cma_settings = json_decode(@$procedure->cma_settings);
	 			$set['cma_id'] 	= @$cma_settings->cma_id; 
	 			
	 			//$this->Commonmodel->array_print_to_html($set);

	 			//check previous AlarmOnOff is ON
	 			//Search TrackerId, VehicleName	 			
	 			if( trim($procedure->client_data_src) != '' AND @$_json_data['AlarmOnOff'] == 'OFF'){
	 				$client_data_src_tran_id = @$this->check_prev_alarm_is_on($procedure->client_data_src, $_json_data);
	 			}
	 			 
	 			if( isset($client_data_src_tran_id) AND @$client_data_src_tran_id > 0){
	 				$tran_id = $client_data_src_tran_id;

	 				//update transaction and set to OPEN
					$update_tran = array();
					$update_tran['alert_updated_dt'] 	= date('Y-m-d H:i:s');
					$update_tran['alert_due_dt'] 		= date('Y-m-d H:i:s', strtotime(' + '.$procedure->reminder_alert_duration.' MINUTE '));
					//$update_tran['tran_status']			= 1;
					$update_tran['thread_count'] 		= ($this->Transactionmodel->get_thread_count($tran_id)) + 1;
					$this->Transactionmodel->update($tran_id, $update_tran);

	 			}else{
	 				$tran_id = $this->_insert($set);

	 				if( isset($cma_settings->auto_log_initial) AND @$cma_settings->auto_log_initial==1 ){

						$new_row = $this->Transactionmodel->row_plain(array('where'=>array('tran_id'=>$tran_id)));

						$cma_set['audit_type'] 	= 'cma-initial-auto-log';
						$cma_set['tran_id'] 	= $tran_id;
						$cma_set['cma_db'] 		= $cma_settings->cma_db;
						$cma_set['cust_id'] 	= $cma_settings->cust_id;
						$cma_set['contact_id'] 	= @$cma_settings->aut_log_contactid; 
						
						$cma_set['agent_id'] 	= '';

						$cma_content = 'Alert #'.@$new_row->ref_number.PHP_EOL.PHP_EOL;
						foreach ($_json_data as $key => $value) {
							$cma_content .= $key.': '.$value.PHP_EOL;
						}
						$cma_set['message'] 	= $cma_content;


		 				$this->Commonmodel->insert_cma_call($cma_set);	 				
	 				}

	 			}
	 			
				if( @$tran_id > 0  ){
					
					if( trim($procedure->client_data_src) != '' ){
						$this->_insert_client_data_src($procedure->client_data_src, $tran_id, $_json_data);
					}

					echo 'OK'.PHP_EOL;					
					exit;
				}else{
					throw new Exception('Error transaction id generated', 1);				
				}

			}else{
				echo 'OK'.PHP_EOL;
				exit;
			}

		} catch (Exception $e) {
			echo 'ERROR: '.$e->getMessage();
			exit;
		}
	}

	function _verify_client($client_id){

		$api_key = trim($client_id);

		try {

			if( empty($client_id) ) throw new Exception("CLIENT not recognised", 1);
			
			$this->db->where("client_id", $client_id);			
			$row = $this->db->get('client')->row();

			if( !isset($row->client_id) ) throw new Exception("CLIENT not recognised", 1);
			
			return $row; 

		} catch (Exception $e) {
			return $e->getMessage();
		}

	}

	function _verify_procedure($api_key){

		$this->load->model('Proceduremodel');

		$api_key = trim($api_key);

		try {

			if( empty($api_key) ) throw new Exception("API KEY not recognised", 1);

			$procedure = $this->Proceduremodel->row(array('where'=>array('pro_alert_type'=>'API', 'api_key'=>$api_key)));
		 
			if( !isset($procedure->pro_id) ) throw new Exception("API KEY not recognised", 1);
			
			return $procedure; 

		} catch (Exception $e) {
			return $e->getMessage();
		}

	}

	function check_prev_alarm_is_on($client_data_src, $_json_data){		
				

		try {
			
			if( !isset($_json_data['TrackerId']) OR trim(@$_json_data['TrackerId']) == '' ) throw new Exception("Error: TrackerId is missing", 1);
			if( !isset($_json_data['VehicleName']) OR trim(@$_json_data['VehicleName']) == '' ) throw new Exception("Error: VehicleName is missing", 1);

			if ( !$this->db->field_exists('TrackerId', $client_data_src)){
			    return '';
			}

			if ( !$this->db->field_exists('VehicleName', $client_data_src)){
			    return '';
			}


			$where = array();
			$where['TrackerId'] = trim($_json_data['TrackerId']);
			$where['VehicleName'] = trim($_json_data['VehicleName']);
			$params['limits']['start'] = 0;
			$params['limits']['limit'] = 1; 

			$this->db->where($where);
			$this->db->order_by('created_dt', 'DESC');
			$this->db->limit(1);
			$record = @$this->db->get($client_data_src)->row();

			//$this->Commonmodel->array_print_to_html($record);

			if(@$record->AlarmOnOff == 'ON'){
				return $record->tran_id;
			}else{
				return '';
			}

		} catch (Exception $e) {
			return '';
		}


		return '';
	}

	function _insert( $set=array() ){

		//$this->load->model('Transactionmodel');
		
		try {
		
			if( empty($set) ) throw new Exception("Empty data", 1);
			
			$_set = $set;

			$tran_id = $this->Transactionmodel->insert($_set);

			return $tran_id;


		} catch (Exception $e) {
			return $e->getMessage();
		}
		
	}

	function _insert_client_data_src($table, $tran_id, $json_data ){

        $this->load->dbforge();

        
        $list_fields = $this->db->list_fields($table);


        $fields = array();
        foreach ($list_fields as $field) {
        	$fields[$field] =  $field;
        }

        unset($fields['id']);
        unset($fields['tran_id']);
        unset($fields['created_dt']);
  

		$json_data_field = $json_data; 
        $result_array = array_intersect_key($json_data_field, $fields);
		
 
		
		foreach( $result_array as $key=>$field ){
			//echo $field.'..---..';
			unset($json_data_field[$key]);
		}
 

		//adding new fields if not exist
		$new_table_fields = array();
		foreach ($json_data_field as $new_fld => $value) {
 
        	$new_table_fields[$new_fld] =  array('type' => 'TEXT');							        

		}
		//add new field
		if( count($new_table_fields) > 0 ){
        	$this->dbforge->add_column($table, $new_table_fields);
		}

		//insert to cs_2_alert
        $set = $json_data;
        $set['tran_id'] = $tran_id;
        $this->db->insert($table, $set);
        
        //device check if exist else insert
        //discontinue 20180621
        //$this->_insert_device($set);
	}


	function _insert_device($param){

		try {

			$VehicleName = trim(@$param['VehicleName']);

			$query = $this->db->get_where('cs_2_device', array('VehicleName'=>$VehicleName));

			if( $query->num_rows() == 0 ) {
				$set['VehicleName'] = $VehicleName;
				$this->db->insert('cs_2_device', $set);
			}

		} catch (Exception $e) {
			
		}
 
	}


	function write_it($data=''){

		$this->load->helper('file');
		$file_name = './files/pinpoint_data/'.strtotime('now').'.txt';
		$data = 'some data';
	 
/*
		$data = 'Some file data';
		if ( ! write_file($file_name, $data))
		{
		        echo 'Unable to write the file';
		}
		else
		{
		        echo 'File written!';
		}*/

		file_put_contents($file_name, file_get_contents('php://input'));

	}

	function clear_morethanday_log(){

		$this->db->query("DELETE FROM data_pinpoint WHERE create_at < DATE_SUB(NOW(),INTERVAL 1 DAY)");
		//echo 'log clear';

	}
}
